package com.mars.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsLockApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsLockApplication.class, args);
    }

}
