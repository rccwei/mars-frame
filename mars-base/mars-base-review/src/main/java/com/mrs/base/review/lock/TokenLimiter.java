package com.mrs.base.review.lock;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/7 16:48
 */
public class TokenLimiter {

    /**
     * 令牌
     */
    public static final String TOKEN = "lp";

    /**
     * 阻塞队列 用于存放令牌
     */
    private ArrayBlockingQueue<String> blockingQueue;
    /**
     * 令牌桶容量
     */
    private int limit;
    /**
     * 令牌的产生间隔时间 单位: 毫秒
     */
    private int period;
    /**
     * 令牌每次产生的个数
     */
    private int amount;


    public TokenLimiter(int limit, int period, int amount) {
        this.limit = limit;
        this.period = period;
        this.amount = amount;
        blockingQueue = new ArrayBlockingQueue<>(limit);
        init();
    }

    /**
     * 生产令牌
     *
     * @param lock 锁对象 因为有可能多个地方调用 所以锁对象需要传过来
     */
    public void start(Object lock) {
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
            synchronized (lock) {
                addToken();
                lock.notifyAll();
            }
        }, 500, this.period, TimeUnit.MILLISECONDS);
    }

    /**
     * 获取令牌
     *
     * @return 令牌
     */
    public boolean tryAcquire() {
        // 队首元素出队
        return blockingQueue.poll() != null;
    }

    /**
     * 创建时初始化令牌
     */
    private void init() {
        for (int i = 0; i < limit; i++) {
            blockingQueue.add(TOKEN);
        }
    }

    /**
     * 添加令牌
     */
    private void addToken() {
        for (int i = 0; i < this.amount; i++) {
            // 溢出返回false
            blockingQueue.offer(TOKEN);
        }
    }

}
