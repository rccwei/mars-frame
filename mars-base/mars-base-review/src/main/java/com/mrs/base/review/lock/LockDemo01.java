package com.mrs.base.review.lock;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/3 10:47
 *
 * 乐观锁 和 悲观锁
 * 悲观锁： 认为自己在使用数据的时候一定有别的线程来修改数据  因此在获得数据的时候会先加锁确保数据不会被其他线程修改
 * 1.synchronized
 * 2.Lock都是悲观锁
 * 使用场景: 适合写操作多的场景
 *
 * 乐观锁：为自己在使用数据的时候不会有别的线程来修改数据
 * 1.版本号机制
 * 2.CAS
 * 使用场景: 适合读操作多的场景
 *
 *
 * ====================================
 * 记住: 线程操作资源类
 *
 * 类锁 和对象锁 区别是什么
 *
 * 普通同步代码块 锁的是对象 通常指的是this 具体的一部手机 所有的普通同步代码都是使用的同一把锁
 *
 * 静态同步代码块 锁的是.class 是类锁
 *
 *
 * synchronized （监视器同管程一样的概念）
 * synchronized 底层采用monitor监视器
 * 可以把 普通同步代码块和静态代码块 通过反编译后可以看到 flag标识 可以看到 只有ACC_SYNCHRONIZED 是普通同步代码块（对象锁）
 * 有 ACC_STATIC 和 ACC_SYNCHRONIZED 是类锁
 *
 */

class Phone{


}
public class LockDemo01 {
}
