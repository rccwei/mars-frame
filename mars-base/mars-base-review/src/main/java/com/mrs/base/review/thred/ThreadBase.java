package com.mrs.base.review.thred;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/4 17:07
 * 线程基础信息
 *-------------------------------
 * 1.线程:在系统用中运行的一个应用程序就是一个进程，每一个进程都有 自己的内存空间和系统资源
 *
 * 2.进程: 也被称为轻量级进程，在同一个进程内会有一个或者多个线程，是大多数操作系统进行时序调度的单元
 *
 * 3.协程：一种用户态的轻量级线程，一个线程也可以拥有多个协程。协程的调度完全由用户控制。协程拥有自己的寄存器上下文和栈。协程的上下文切换非常快
 *
 * Object o=new Object()
 * 3.管程:监视器 类似于我们所做的 synchronized(o)
 *
 *
 *
 */
public class ThreadBase {
    public static void main(String[] args) {
        new Thread(()->{
            System.out.println("11111111");
        },"t1").start();
    }
}
