package com.mrs.base.review.mybatis;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/7 15:15
 * mybatis 的延迟加载 比如一个对象里面包含另外一个对象集合
 * 配置 fetchTYpe='lazy' 可以设置成延迟加载
 *
 * =====================
 * 本地缓存：HashMap
 * 一级缓存: 作用域的范围是session
 * 二级缓存: 作用域的范围是namespace和mapper的作用域 不依赖是session
 * 打开二级缓存 默认是关闭的 cacheEnabled 设置成True
 *
 */
public class MybatisDemo {
}
