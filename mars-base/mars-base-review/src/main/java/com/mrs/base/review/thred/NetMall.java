package com.mrs.base.review.thred;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author CGY
 * @date 2022-06-05 16:40
 */
@AllArgsConstructor
@Data
public class NetMall {
    /**
     * 网站名称
     */
    private String mallName;


    /**
     * 计算价格
     *
     * @param mallName mallName
     * @return Double
     */
    public Double calcPrice(String mallName) {
        System.out.println("正在查询中~");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Double.parseDouble("120") + ThreadLocalRandom.current().nextDouble(10);
    }
}
