package com.mrs.base.review.redis;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/3 17:20
 *
 * redis为什么快？
 * 1.redis命令是基于纯内存操作的
 * 2.命令执行是单线程的没有线程切换
 * 3.线程模型是基于io多路复用的
 *
 *------------------
 * 内存淘汰策略  4.0 之前有6种  4.0之后多了两种 共计8种
 * 推荐使用Lru的算法 最近最少使用的
 *
 * redis的事务  提交事务 exec 放弃事务discard
 * redis 不保证事务
 *
 * ==========================
 * redis 实现乐观锁
 * 1. version
 * 2. cas
 * 3. redis 采用watch 命令监视
 *
 */
public class RedisDemo {

    public static void main(String[] args) {



    }
}
