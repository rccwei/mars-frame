package com.mrs.base.review.bean;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/7 9:28
 *
 * 1.spring Bean 的生命周期
 *
 * 如果要对Bean的方法增强可以采用bean的后置处理器 BeanPostProcesser
 *
 *
 * 2.spring的循环依赖问题
 * 2.1 解决方案: 三级缓存来解决循环依赖
 *      一级缓存:singltonObjects
 *      二级缓存:earlySinletonObjects 缓存早期的Bean对象
 *      三级缓存:singletonFactories 缓存工厂
 *
 *
 */
public class BeanDemo {
}
