package com.mrs.base.review.thread2;

/**
 * 1锁
 * synchronized
 * 2并
 * 2.1 并行
 * 2.2 并发
 *
 * 3 程
 * 3.1 进程
 * 3.2 线程
 * 3.3 管程  监视器monitor 也就是我们平时说的锁
 *
 */
public  class ThreadDemo {

    public  static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().isDaemon()?"守护线程":"用户线程");
            while (true){

            }

        }, "t1");
        // 设置t1 为守护线程  主线程执行完成后子线程才停止
        t1.setDaemon(true);
        t1.start();

        Thread.sleep(1000);
        System.out.println(Thread.currentThread()+"主线程执行结束");

    }
}
