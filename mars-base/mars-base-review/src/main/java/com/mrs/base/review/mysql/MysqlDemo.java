package com.mrs.base.review.mysql;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/5 9:55
 * 二叉树
 * 红黑树 高度太高了
 * B Tree 根节点和叶节点放索引和数据
 * B+树  根节点 可以放多个索引 页节点放数据  非页节点只存储索引 data 是索引所在磁盘的地址
 * mysql默认的节点的大小是 16kb 每一个页节点是16kb
 * ========================
 * 1.myisam 存储引擎 （非聚集 索引和数据是分开存储的）
 * .frm 是存储表结构信息
 * .MYD 是存储data数据的
 * .MYI 是存储索引文件的
 *
 *
 * 2.innodb 存储引擎（聚集索引 包含所有的完整记录）
 * .frm 是存储表结构信息
 * .idb 是索引的和数据的文件
 *
 *
 *
 */
public class MysqlDemo {
}
