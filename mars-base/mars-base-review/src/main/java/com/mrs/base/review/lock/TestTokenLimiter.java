package com.mrs.base.review.lock;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/7 16:49
 */
public class TestTokenLimiter {
    final static Object LOCK = new Object();

    /**
     * 先生产2个令牌，减少4个令牌；再每500ms生产2个令牌，减少4个令牌
     */
    public static void main(String[] args) throws InterruptedException {
        int period = 500;
        TokenLimiter limiter = new TokenLimiter(2, period, 2);

        limiter.start(LOCK);

        // 让线程先产生2个令牌
        synchronized (LOCK) {
            LOCK.wait();
        }

        for (int i = 0; i < 4; i++) {
            new Thread(() -> {
                while (true) {
                    String name = Thread.currentThread().getName();
                    if (limiter.tryAcquire()) {
                        System.out.println(name + ":拿到令牌");
                    } else {
                        System.out.println(name + ":没有令牌");
                    }
                    try {
                        Thread.sleep(period);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

    }

}
