package com.mrs.base.review.lock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/3 14:44
 * 公平锁和非公平锁的实现
 * ReentrantLock 和sync 都是可重入锁
 */

class Ticket {
    private Integer number = 50;

    // 默认是非公平锁 可以设置成公平锁 每个线程都可以获取到 像排队打饭一样
    ReentrantLock lock = new ReentrantLock(true);

    public void sale() {
        lock.lock();
        try {
            if (number > 0) {
                System.out.println(Thread.currentThread() + "卖出第" + (number--) + "还剩下" + number);
            }else {
                System.out.println("票不足");
            }
        }finally {
            lock.unlock();
        }


    }

}

public class ReentrantLockDemo {

    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        new Thread(()->{
            for (int i = 0; i < 55; i++) {
                ticket.sale();
            }

        },"t1").start();

        new Thread(()->{
            for (int i = 0; i < 55; i++) {
                ticket.sale();
            }

        },"t2").start();

        new Thread(()->{
            for (int i = 0; i < 55; i++) {
                ticket.sale();
            }

        },"t3").start();


    }
}
