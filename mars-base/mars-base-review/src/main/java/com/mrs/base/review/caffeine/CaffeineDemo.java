package com.mrs.base.review.caffeine;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

public class CaffeineDemo {
    public static void main(String[] args) {

        Cache<String, String> cache = Caffeine.newBuilder().build();
        cache.put("name","lisi");
        String s = cache.get("name", k -> "zs");
        String age = cache.get("age", k -> {
            return "111";
        });
        System.out.println(s);
        System.out.println(age);
    }
}
