package com.mrs.base.review.thread2;

import java.util.concurrent.*;

/**
 * FutureTask 开启异步线程获取 线程返回值
 */
public class FeatureTaskDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        FutureTask<String> futureTask = new FutureTask<>(new MyThread());

        new Thread(futureTask, "t1").start();

//        if (futureTask.isDone()) {
//            System.out.println(futureTask.get());
//        }
        while (true){
            if (futureTask.isDone()){
                System.out.println(futureTask.get());
                break;
            }else {
                Thread.sleep(500);
                System.out.println("后台正在处理中~");
            }
        }






    }


}

class MyThread implements Callable<String> {


    @Override
    public String call() throws Exception {
        System.out.println(Thread.currentThread() + "come in call()");
        Thread.sleep(2000);
        return "hello call()";
    }
}
