package com.mrs.base.review.thred;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/5 9:56
 */
public class FutureThreadPoolDemo {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        long startTime = System.currentTimeMillis();
        // 3个任务，开启多线程来处理
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        FutureTask<String> futureTask = new FutureTask<>(() -> {
            Thread.sleep(300);
            return "task1 over";
        });
        threadPool.submit(futureTask);

        FutureTask<String> futureTask2 = new FutureTask<>(() -> {
            Thread.sleep(500);
            return "task2 over";
        });
        threadPool.submit(futureTask2);

        System.out.println(futureTask.get());
        System.out.println(futureTask2.get());


        Thread.sleep(300);
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:" + (endTime - startTime));

        threadPool.shutdown();
    }
}
