package com.mrs.base.review.thred;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author Mars
 * @date 2022-06-05 16:28
 */
public class CompletableFutureMallDemo {
    static List<NetMall> list = Arrays.asList(new NetMall("jd"),
            new NetMall("dangdang"), new NetMall("taobao"));

    public static void main(String[] args) {

//        futureReview();

        long startTime = System.currentTimeMillis();
//        List<String> list1 = getStepByStepPrice(CompletableFutureMallDemo.list, "mysql");
        List<String> list2 = getPriceByCompletableFuture(CompletableFutureMallDemo.list, "mysql");
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:" + (endTime - startTime));
//        System.out.println(list1);
        System.out.println(list2);


    }

    /**
     * 通过CompletableFuture 返回信息()
     *
     * @param list        list
     * @param productName productName
     * @return List<String>
     */
    public static List<String> getPriceByCompletableFuture(List<NetMall> list, String productName) {
        return list.stream().map(netMall -> CompletableFuture.supplyAsync(() -> String.format(productName + "in %s price is %.2f", netMall.getMallName(),
                netMall.calcPrice(netMall.getMallName()))))
                .collect(Collectors.toList()).stream().map(CompletableFuture::join)
                .collect(Collectors.toList());
    }


    /**
     * 一步一步的执行
     *
     * @param list        list
     * @param productName productName
     * @return List<String>
     */
    public static List<String> getStepByStepPrice(List<NetMall> list, String productName) {
        return list.stream().map(x -> String.format(productName + "in %s price is %.2f", x.getMallName(),
                x.calcPrice(x.getMallName()))).
                collect(Collectors.toList());
    }

    /**
     * get 和join 的区别
     */
    private static void futureReview() {
        CompletableFuture<String> supplyAsync = CompletableFuture.supplyAsync(() -> "hello  supply");
        //  join 和 get 的区别 我们在编译时候get抛出异常
        System.out.println(supplyAsync.join());
//        System.out.println(supplyAsync.get());
    }
}
