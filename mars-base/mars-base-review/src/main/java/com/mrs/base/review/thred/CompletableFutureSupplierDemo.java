package com.mrs.base.review.thred;

import java.util.concurrent.*;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/5 10:30
 * 底层采用异步回调的方式 采用设计模式观察者模式  异步回调完成后通知监听的一方
 */
public class CompletableFutureSupplierDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        m1();
//        m2();
        //futureOldMethod();
//        supplierWhenCompleteThen();

        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        try {
            CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
                System.out.println(Thread.currentThread().getName() + "coming in");
                int num = ThreadLocalRandom.current().nextInt(10);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("1s 后结果是" + num);
                return num;
                // v 上一步完成后的值 e 是抛出的异常
            },threadPool).whenComplete((v, e) -> {
                if (e == null) {
                    System.out.println("计算完成:" + v);
                }
                // 抛出异常后的处理方式
            }).exceptionally(e -> {
                e.printStackTrace();
                System.out.println("异常出现啦" + e.getMessage());
                return null;
            });
            // 这里没有采用get()的方式获取返回结果  上边的异步线程类似于守护线程  主线程执行完成后 守护线程就自动结束
            System.out.println(Thread.currentThread().getName() + "忙活其他事情");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }

    /**
     * 采用异步线程回调的方式  whenCompleteThen
     *
     * @throws InterruptedException
     */
    private static void supplierWhenCompleteThen() throws InterruptedException {
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "coming in");
            int num = ThreadLocalRandom.current().nextInt(10);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("1s 后结果是" + num);
            return num;
            // v 上一步完成后的值 e 是抛出的异常
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("计算完成:" + v);
            }
            // 抛出异常后的处理方式
        }).exceptionally(e -> {
            e.printStackTrace();
            System.out.println("异常出现啦" + e.getMessage());
            return null;
        });
        // 这里没有采用get()的方式获取返回结果  上边的异步线程类似于守护线程  主线程执行完成后 守护线程就自动结束
        System.out.println(Thread.currentThread().getName() + "忙活其他事情");
        Thread.sleep(2000);
    }

    /**
     * 采用CompletableFuture 取代Future的方式
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private static void futureOldMethod() throws InterruptedException, ExecutionException {
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "coming in");
            int num = ThreadLocalRandom.current().nextInt(10);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("1s 后结果是" + num);
            return num;

        });

        System.out.println(Thread.currentThread().getName() + "忙活其他事情");
        System.out.println(completableFuture.get());
    }

    /**
     * 无返回值 自定义线程池的
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private static void m2() throws InterruptedException, ExecutionException {
        ExecutorService threadPool = Executors.newFixedThreadPool(3);
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return "task2 over";
        }, threadPool);
        System.out.println(completableFuture.get());
        threadPool.shutdown();
    }

    /**
     * 无返回值的使用
     *
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private static void m1() throws InterruptedException, ExecutionException {
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread().getName());
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return "task1 over";
        });
        System.out.println(completableFuture.get());
    }


}
