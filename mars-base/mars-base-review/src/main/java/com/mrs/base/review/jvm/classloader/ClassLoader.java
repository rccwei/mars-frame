package com.mrs.base.review.jvm.classloader;

/**
 * @author wq
 * @date 2022/5/12 10:25
 */
public class ClassLoader {

    public static void main(String[] args) {
//        Object o = new Object();
//        System.out.println(o.getClass().getClassLoader());  // null  如果是 jdk 自带的 是bootstrapClassLoader
//
//        ClassLoader loader = new ClassLoader();
//        System.out.println(loader.getClass().getClassLoader());  //AppClassLoader
//
//        System.out.println(String.class.getClassLoader());

        Integer i = 0;
        System.out.println(i instanceof Integer);//编译不通过 i必须是引用类型，不能是基本类型
        System.out.println(i instanceof Object);//编译不通过

        Integer integer = new Integer(1);
        System.out.println(integer instanceof Integer);//true

//false ,在 JavaSE规范 中对 instanceof 运算符的规定就是：如果 obj 为 null，那么将返回false。
        System.out.println(null instanceof Object);

        StringBuilder stringBuilder = new StringBuilder();
        StringBuffer stringBuffer = new StringBuffer();
    }

}
