package com.mrs.base.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot 自动配置原理?
 *
 * @SpringBootConfiguration
 * @EnableAutoConfiguration
 * @ComponentScan
 *
 */
@SpringBootApplication
public class MarsBaseReviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsBaseReviewApplication.class, args);
    }

}
