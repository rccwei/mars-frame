package com.mrs.base.review.thred;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/4 18:31
 */
public class FutureTaskDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 多线程 有返回  异步
        FutureTask futureTask = new FutureTask<>(new MyThread());

        Thread t1 = new Thread(futureTask, "t1");
        t1.start();
        // 获取异步返回值
        System.out.println(futureTask.get());


    }
}

class MyThread implements Callable<String> {

    @Override
    public String call() throws Exception {

        System.out.println("come in call()");
        return "hello";
    }
}
