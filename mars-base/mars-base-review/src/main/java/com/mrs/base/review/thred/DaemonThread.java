package com.mrs.base.review.thred;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/4 17:55
 * 用户线程结束后守护线程也结束
 */
public class DaemonThread {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "开始运行" +
                    (Thread.currentThread().isDaemon() ? "守护线程" : "用户线程"));
            while (true) {

            }
        }, "t1");
        // 设置t1为守护线程
        t1.setDaemon(true);
        t1.start();


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        System.out.println(Thread.currentThread().getName() + "主线程");

    }
}
