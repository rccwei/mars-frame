package com.mrs.base.review.thred;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Mars
 * @date 2022-06-05 22:30
 */
public class CompletableFutureAPIDemo {

    public static void main(String[] args) {

//        m1();
        // 消费类型
        futureConsumer();
    }

    /**
     * 消费类型
     */
    private static void futureConsumer() {
        CompletableFuture.supplyAsync(() -> {
            System.out.println("1111");
            return 1;
        }).thenApply(f -> {
            return f + 2;
        }).thenApply(x -> {
            return x + 3;
        }).thenAccept(f -> {
            System.out.println(f);
        });
    }

    private static void m1() {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("1111");
            return 1;
        }, threadPool).thenApply(f -> {
            System.out.println("2222");
            return f + 2;
        }).thenApply(x -> {
            System.out.println("3333");
            return x + 2;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("计算结果是:" + v);
            }
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        });
        threadPool.shutdown();
    }
}
