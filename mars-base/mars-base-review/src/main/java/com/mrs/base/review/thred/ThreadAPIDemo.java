package com.mrs.base.review.thred;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/6/5 10:11
 */
public class ThreadAPIDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        FutureTask<String> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + "\tcoming in ");
            Thread.sleep(5000);
            return "task over";
        });
        Thread thread = new Thread(futureTask, "t1");
        thread.start();
        // 如果这句代码在执行主线程前面 调用get()获取futureTask的返回值 就会导致线程堵塞
//        System.out.println(futureTask.get());
        System.out.println(Thread.currentThread().getName() + "忙活其他事情");

        while (true) {
            if (futureTask.isDone()) {
                System.out.println(futureTask.get());
                break;
            } else {
                // 轮询
                Thread.sleep(500);
                System.out.println(" 正在处理中");
            }
        }

    }
}
