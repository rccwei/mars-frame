package com.mrs.base.review.thred;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/3 10:02
 */
public class CompletableFeatureUseDemo {


    public static void main(String[] args) {

        ExecutorService executorService = Executors.newFixedThreadPool(3);

        CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "hello world";
        },executorService).whenComplete((v, e) -> {
            if (e == null) {
                // 没有异常 处理其他事情
                System.out.println("调用其他微服务更新值:"+v);
            }
        }).exceptionally(e->{
            e.printStackTrace();
            System.out.println("异常情况:"+e.getCause()+e.getMessage());
            return null;
        });

        System.out.println("main 线程处理事情完成");

        executorService.shutdown();




    }
}
