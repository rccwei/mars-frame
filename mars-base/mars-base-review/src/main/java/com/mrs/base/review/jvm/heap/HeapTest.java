package com.mrs.base.review.jvm.heap;

import java.util.ArrayList;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/2 11:05
 *
 * jvisualvm
 *
 * Arthas 阿里解决线上排查问题
 *
 * 内存屏障:
 * LoadLoad
 * StoreStore
 * LoadStore
 * StoreLoad
 *
 * volatile 底层实现会在前面加一个lock前缀指令
 * 提供内存屏障功能 使lock前后指令不重排
 *
 *
 *
 *
 */
public class HeapTest {

    byte[] a = new byte[1024 * 100];

    public static void main(String[] args) throws InterruptedException {

        ArrayList arraysList = new ArrayList<HeapTest>();

        while (true){
            arraysList.add(new HeapTest());
            Thread.sleep(5);
        }
    }
}
