package com.mars.mars.base.aop.base;


public enum MethodType {
    INSERT, DELETE, UPDATE, OTHER
}
