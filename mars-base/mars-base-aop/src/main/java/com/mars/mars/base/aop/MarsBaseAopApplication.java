package com.mars.mars.base.aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsBaseAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsBaseAopApplication.class, args);
    }

}
