package com.mars.mars.base.aop.controller;

import com.mars.mars.base.aop.annotation.Log;
import com.mars.mars.base.aop.base.MethodType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 11:30
 */
@RestController
public class IndexController {

    /**
     * @return string
     */
    @CrossOrigin
    @Log(description = "index", methodType = MethodType.OTHER)
    @RequestMapping("/index")
    public String index() {
        return "index";
    }
}
