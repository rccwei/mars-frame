package com.mars.marsbasecore.review01.lock;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @date 2021-08-08 22:11
 */
@Data
public class User implements Serializable {

    private Integer id;

    private String name;
}
