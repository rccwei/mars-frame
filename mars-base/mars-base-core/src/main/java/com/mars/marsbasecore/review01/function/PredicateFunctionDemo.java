package com.mars.marsbasecore.review01.function;

import java.util.function.Predicate;

/**
 * @author wq
 * @date 2021/12/31 14:55
 */
public class PredicateFunctionDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            System.out.println(show(i, (num) -> num > 3));
        }

    }

    private static boolean show(Integer number, Predicate<Integer> predicate) {
        return predicate.test(number);
    }
}
