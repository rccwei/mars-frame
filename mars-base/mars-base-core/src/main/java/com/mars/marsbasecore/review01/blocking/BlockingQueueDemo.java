package com.mars.marsbasecore.review01.blocking;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 17:45
 */
public class BlockingQueueDemo {
    public static void main(String[] args) {
        ArrayBlockingQueue<Integer> blockingQueue = new ArrayBlockingQueue<>(16);
        blockingQueue.add(2);
        for (Integer integer : blockingQueue) {
            System.out.println(integer);
        }
    }
}
