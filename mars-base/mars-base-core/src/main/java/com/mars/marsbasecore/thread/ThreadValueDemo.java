package com.mars.marsbasecore.thread;

/**
 * @author wq
 * @date 2022/11/11 14:30
 */
public class ThreadValueDemo {


    static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void main(String[] args) {
        threadLocal.set("dhytest");
        new Thread(()->{
            System.out.println("子线程获取到的值：" + threadLocal.get());
        }).start();

        System.out.println("父线程获取到的值：" + threadLocal.get());
    }
}
