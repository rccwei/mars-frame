package com.mars.marsbasecore.review01.volatiles;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author wq
 * @date 2021-07-31 12:34
 * 1.可见性
 * 2.原子性  完整性 不可分割
 * 3.禁止指令重排  编译器他底层优化 导致数据最终不一致 所有 采用volatile来禁止指令重排
 */

class Mydata {
    /**
     * volatile 保证多个线程的变量可见
     */
    volatile int number = 0;

    Lock lock = new ReentrantLock();

    public void add() {
        this.number = 60;
    }

    public void addPlus() {

        number++;


    }
}

public class VolatileDemo {

    public static void main(String[] args) {

        Mydata mydata = new Mydata();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int i1 = 0; i1 < 1000; i1++) {
                    mydata.addPlus();
                }
            }, String.valueOf(i)).start();
        }
        //一个主线程和一个GC线程
        while (Thread.activeCount() > 2) {
            //线程礼让 让上面的线程执行完成后再执行
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName() + "\t finally value:" + mydata.number);
    }

    private static void seeOk() {
        Mydata mydata = new Mydata();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\tcome in");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mydata.add();
            System.out.println(Thread.currentThread().getName() + "\tget Value：" + mydata.number);
        }, "AA").start();

        while (mydata.number == 0) {

        }
        System.out.println(Thread.currentThread().getName() + "\t任务完成");
    }

}
