package com.mars.marsbasecore.dirver;

/**
 * @author wq
 * @date 2022/1/19 19:07
 */

public class CarDriver extends DeliveryDriver {

    /**
     * 速率
     */
    private final Double rate;


    /**
     * 构造函数
     *
     * @param rate 速率
     */
    public CarDriver(Double rate) {
        this.rate = rate;
    }


    /**
     * 计算汽车司机日薪
     */
    @Override
    public Double dailyPay() {
        return (super.getMileage() * rate);
    }
}
