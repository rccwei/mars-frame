package com.mars.marsbasecore.atomic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wq
 * @date 2022/1/26 15:07
 */
public class AtomicIntegerDemo {

    private static final AtomicInteger count = new AtomicInteger();

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                count.getAndIncrement();
                System.out.println("--------");
            }).start();
        }
        System.out.println(count);

    }
}
