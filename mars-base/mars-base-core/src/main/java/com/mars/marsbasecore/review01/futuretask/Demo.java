package com.mars.marsbasecore.review01.futuretask;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/02 10:46
 */
public class Demo {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        Callable<List> list = Demo::getList01;
        Callable<List> list2 = Demo::getList02;
        Callable<List> list3 = Demo::getList03;

        FutureTask<List> listTask = new FutureTask<>(list);
        FutureTask<List> listTask2 = new FutureTask<>(list2);
        FutureTask<List> listTask3 = new FutureTask<>(list3);
        executorService.submit(listTask);
        executorService.submit(listTask2);
        executorService.submit(listTask3);

        try {
            System.out.println("listTask:" + listTask.get());
            System.out.println("listTask2:" + listTask2.get());
            System.out.println("listTask3:" + listTask3.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        System.out.println("main  执行");

    }

    private static List<String> getList01() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Collections.singletonList("1");
    }

    private static List<String> getList02() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Collections.singletonList("1");
    }


    private static List<String> getList03() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Collections.singletonList("1");
    }
}
