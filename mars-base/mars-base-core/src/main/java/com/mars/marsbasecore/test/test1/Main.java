package com.mars.marsbasecore.test.test1;

/**
 * @author wq
 * @date 2022/8/16 14:57
 */
public class Main {
    private static final int INF = 65535;

    public static void main(String[] args) {
        char[] vertex = {'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        int[][] matrix = new int[vertex.length][vertex.length];
        matrix[0] = new int[]{INF, 5, 7, INF, INF, INF, 2};
        matrix[1] = new int[]{5, INF, INF, 9, INF, INF, 3};
        matrix[2] = new int[]{7, INF, INF, INF, 8, INF, INF};
        matrix[3] = new int[]{INF, 9, INF, INF, INF, 4, INF};
        matrix[4] = new int[]{INF, INF, 8, INF, INF, 5, 4};
        matrix[5] = new int[]{INF, INF, INF, 4, 5, INF, 6};
        matrix[6] = new int[]{2, 3, INF, INF, 4, 6, INF};

        //创建一个图
        Graph graph = new Graph(vertex, matrix);
//        graph.show();
        graph.dijkstra(2);
        graph.showResult();
    }
}
