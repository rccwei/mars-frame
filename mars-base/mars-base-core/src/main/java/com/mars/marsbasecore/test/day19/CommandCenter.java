package com.mars.marsbasecore.test.day19;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 指挥中心
 *
 * @author Administrator
 */
public class CommandCenter extends Construction {


    /**
     * 创建农民
     *
     * @return List<Farmer>
     */
    public Farmer createFarmer() {
        System.out.println(Thread.currentThread().getName() + "正在创建农民~");
        try {
            Random random = new Random();
            int i1 = random.nextInt(1000) + 500;
            Thread.sleep(i1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Farmer farmer = new Farmer();
        farmer.setLifeValue(100);
        farmer.setNeedResource(2);
        farmer.setAttackPower(0);
        return farmer;
    }
}
