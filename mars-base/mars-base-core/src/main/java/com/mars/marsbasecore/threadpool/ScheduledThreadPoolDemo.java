package com.mars.marsbasecore.threadpool;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author wq
 * @date 2022/1/20 14:47
 * 延迟 定期执行
 */
public class ScheduledThreadPoolDemo {
    public static void main(String[] args) {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        pool.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println("任务执行" + LocalDateTime.now());
            }
        }, 1, 3, TimeUnit.SECONDS);
    }
}
