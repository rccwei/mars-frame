package com.mars.marsbasecore.review01.event.controller;

import com.mars.marsbasecore.review01.event.OrderEvent;
import com.mars.marsbasecore.review01.event.SpringUtils;
import com.mars.marsbasecore.review01.test.Person;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 10:29
 */
@RestController
public class IndexController {

    @RequestMapping("publish")
    public void publish() {
        Person person = new Person();
        person.setName("lisi");
        SpringUtils.publishEvent(new OrderEvent(this, person));
    }
}
