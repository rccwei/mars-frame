package com.mars.marsbasecore.review01.test;

import lombok.Data;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 10:03
 */
@Data
public class Person {

    private String name;
}
