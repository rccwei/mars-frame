package com.mars.marsbasecore.feature;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2023/1/4 13:24
 */
public class CompletableFutureDemo {

    public static void main(String[] args) {
        demo1();
    }

    /**
     * 底层默认使用ForkJoinPool
     * 事实上，如果每个操作都很简单的话（比如：上面的例子中按照id去查）没有必要用这种多线程异步的方式，
     * 因为创建线程还需要时间，还不如直接同步执行来得快。
     *
     * 事实证明，只有当每个操作很复杂需要花费相对很长的时间（比如，调用多个其它的系统的接口；
     * 比如，商品详情页面这种需要从多个系统中查数据显示的）的时候用CompletableFuture才合适，不然区别真的不大，还不如顺序同步执行。
     * <p>
     * 1.异步多线程入库，并获取所有入库结果
     */
    public static void demo1() {
        List<String> users = Arrays.asList("张三", "李四", "王五", "马六");
        //批量保存用户
        List<CompletableFuture<HashMap<String, String>>> resultList = users.stream().map(user -> CompletableFuture.supplyAsync(
                () -> {
                    //这里需要一个供给型参数，使用内部线程池异步执行
                    //保存到数据库
                    HashMap<String, String> result = new HashMap<>();
                    result.put("name", user);
                    result.put("id", UUID.randomUUID().toString());
                    return result;
                }
        )).collect(Collectors.toList());
        //这个时候，得到的resultList还没有结果，因为CompletableFuture是内部使用线程池进行执行的
        //等待所有完成
        //CompletableFuture.allOf(resultList.toArray(new CompletableFuture[0])).join();
        //等待所有完成并获取结果
        List<HashMap<String, String>> realResultList = resultList.stream().map(CompletableFuture::join).collect(Collectors.toList());
        System.out.println(realResultList);
    }
}
