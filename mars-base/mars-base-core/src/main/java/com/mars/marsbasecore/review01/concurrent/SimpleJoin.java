package com.mars.marsbasecore.review01.concurrent;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 11:51
 */
public class SimpleJoin {

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            System.out.println("this is T1");
        });
        t1.start();
        //这里阻塞主线程,直到t1线程执行完,继续执行主线程(一个线程执行结束后会执行该线程
        // 自身对象的notifyAll方法)
        //阻塞线程
        t1.join();
        Thread t2 = new Thread(() -> {
            System.out.println("this is T2");
        });
        t2.start();
    }
}
