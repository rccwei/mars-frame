package com.mars.marsbasecore.review01.volatiles;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wq
 * @date 2021-07-31 12:34
 * 1.可见性
 * 2.原子性  完整性 不可分割
 * 采用原子包装整形
 */

public class Mydata1 {

    AtomicInteger auto = new AtomicInteger();


    public void addPlus() {
        auto.getAndIncrement();

    }

    public static void main(String[] args) {
        Mydata1 mydata = new Mydata1();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int i1 = 0; i1 < 1000; i1++) {
                    mydata.addPlus();
                }
            }, String.valueOf(i)).start();
        }
        //一个主线程和一个GC线程
        while (Thread.activeCount() > 2) {
            //线程礼让 让上面的线程执行完成后再执行
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName() + "\t finally value:" + mydata.auto);
    }
}

