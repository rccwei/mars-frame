package com.mars.marsbasecore.queue;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author wq
 * @date 2022/1/11 12:11
 */
public class QueueDemo {
    public static void main(String[] args) {
        LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<>(3);
        queue.offer("a");
        queue.offer("b");
        queue.offer("c");
        queue.offer("d");
        queue.offer("e");
        for (String s : queue) {
            System.out.println(s);
        }
    }
}
