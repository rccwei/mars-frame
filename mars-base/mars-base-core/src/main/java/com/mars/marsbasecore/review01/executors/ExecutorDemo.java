package com.mars.marsbasecore.review01.executors;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wq
 * @date 2021-08-01 11:10
 * <p>
 * int corePoolSize, 核心线程数
 * int maximumPoolSize,  最大线程数
 * long keepAliveTime, 多余线程数存过时间
 * TimeUnit unit,
 * BlockingQueue<Runnable> workQueue,
 * ThreadFactory threadFactory,
 * RejectedExecutionHandler handler
 *
 * 核心线程数满了 和阻塞队列满了就需要 扩容
 */
public class ExecutorDemo {
    public static void main(String[] args) {
        //线程池的使用
        //一池五个线程 工作线程
        //ExecutorService executorService = Executors.newFixedThreadPool(5);

        //一池一个线程

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        //模拟10个用户来办理业务

        try {
            for (int i = 0; i < 100; i++) {
                executorService.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "\t办理业务");
                });

            }
        } catch (Exception e) {
        } finally {
            executorService.shutdown();
        }
    }
}
