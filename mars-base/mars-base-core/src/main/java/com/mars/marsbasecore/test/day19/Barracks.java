package com.mars.marsbasecore.test.day19;

/**
 * 兵营 创建机枪兵
 *
 * @author Administrator
 */
public class Barracks extends Construction {

    /**
     * 创建工兵
     *
     * @return
     */
    public GunMan createGunman() {
        GunMan gunMan = new GunMan();
        // 生命值
        gunMan.setLifeValue(1000);
        // 攻击力10
        gunMan.setAttackPower(10);
        // 消耗资源数
        gunMan.setNeedResource(4);
        return gunMan;
    }
}
