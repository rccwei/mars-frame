package com.mars.marsbasecore.pair;

import javafx.util.Pair;

/**
 * @author wq
 * @date 2022/1/12 9:47
 */
public class PairDemo {
    public static void main(String[] args) {


//        HashMap<Integer, String> map = new HashMap<>();
//        map.put(1, "One");
//        System.out.println(map.keySet());
//        System.out.println(map.values());

        Pair<Integer, String> pair = getUserInfo();
        System.out.println(pair.getKey());
        System.out.println(pair.getValue());
    }

    private static Pair<Integer, String> getUserInfo() {
        Pair<Integer, String> pair = new Pair<>(1, "One");
        return pair;
    }
}
