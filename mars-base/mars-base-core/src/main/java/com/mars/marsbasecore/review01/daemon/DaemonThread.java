package com.mars.marsbasecore.review01.daemon;

/**
 * @author 程序猿Mars
 * @date 2023-10-01 11:14
 */
public class DaemonThread {
    public static boolean flag = true;

    public static void main(String args[]) throws Exception {

        Thread userThread = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "正在运行、i = " + i);
            }
        }, "用户线程");        // 核心功能


        Thread daemonThread = new Thread(() -> {
            for (int i = 0; i < Integer.MAX_VALUE; i++) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "正在运行、i = " + i);
            }
        }, "守护线程");        // 核心功能

        userThread.start();
        daemonThread.setDaemon(true);
        daemonThread.start();
    }
}
