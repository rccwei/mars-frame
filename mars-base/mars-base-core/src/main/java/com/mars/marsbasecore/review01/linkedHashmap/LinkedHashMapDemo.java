package com.mars.marsbasecore.review01.linkedHashmap;

import java.util.LinkedHashMap;

/**
 * @author wq
 * @version 1.0
 * @date 2021/08/02 16:59
 */
public class LinkedHashMapDemo<K, V> extends LinkedHashMap<K, V> {
    private int capacity;

    public LinkedHashMapDemo(int capacity) {
        super(capacity, 0.75F, true);
        this.capacity = capacity;
    }

    @Override
    public boolean remove(Object key, Object value) {
        return super.size() > capacity;
    }

    public static void main(String[] args) {
        LinkedHashMapDemo<Integer, String> mapDemo = new LinkedHashMapDemo<>(3);
        mapDemo.put(1, "a");
        mapDemo.put(2, "b");
        mapDemo.put(3, "c");

        System.out.println(mapDemo.keySet());
        mapDemo.put(4, "d");
        System.out.println(mapDemo.keySet());
    }

}
