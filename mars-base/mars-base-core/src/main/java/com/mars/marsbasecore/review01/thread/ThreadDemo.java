package com.mars.marsbasecore.review01.thread;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 11:24
 */
public class ThreadDemo {
    public static void main(String[] args) {
        Thread a = new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("a");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "A");

        Thread b = new Thread(() -> {
            try {
                Thread.sleep(1000);
                System.out.println("b");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "B");
        b.setPriority(Thread.MAX_PRIORITY);//将线程max设置为最大值10
        a.setPriority(Thread.MIN_PRIORITY);//将线程min设置为最小大值1
        a.start();
        b.start();

        System.out.println("main");
    }
}
