package com.mars.marsbasecore.review01.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 11:51
 * CGLIB底层使用了ASM（一个短小精悍的字节码操作框架）来操作字节码生成新的类。除了CGLIB库外，
 * 脚本语言（如Groovy何BeanShell）也使用ASM生成字节码。ASM使用类似SAX的解析器来实现高性能。
 * 我们不鼓励直接使用ASM，因为它需要对Java字节码的格式足够的了解
 */
public class SimpleCglib {

    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(SimpleCglib.class);
        enhancer.setCallback((MethodInterceptor) (obj, method, args1, proxy) -> {
            System.out.println("before method run...");
            Object result = proxy.invokeSuper(obj, args1);
            System.out.println("after method run...");
            return result;
        });
        SimpleCglib sample = (SimpleCglib) enhancer.create();
        sample.create();
    }

    public void create() {
        System.out.println("hello world");
    }
}
