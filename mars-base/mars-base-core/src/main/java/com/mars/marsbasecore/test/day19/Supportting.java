package com.mars.marsbasecore.test.day19;

/**
 * 供给站
 */
public class Supportting extends Construction {

    public static final int MAX_SUPPORTTING = 10000;


    /**
     * 开始初始化生命值
     *
     * @return Integer
     */
    public Integer initLifeValue() {
        System.out.println(Thread.currentThread().getName() + "开始初始化生命值: " + MAX_SUPPORTTING);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return MAX_SUPPORTTING;
    }

}
