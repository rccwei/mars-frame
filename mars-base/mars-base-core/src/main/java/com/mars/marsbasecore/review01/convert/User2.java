package com.mars.marsbasecore.review01.convert;

/**
 * @description:
 * @author: WQ
 * @create: 2021-08-09 17:55
 */
public class User2 {
    private String name2;
    private Integer age;
    private String agent;

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    @Override
    public String toString() {
        return "User2{" +
                "name2='" + name2 + '\'' +
                ", age=" + age +
                ", agent='" + agent + '\'' +
                '}';
    }
}
