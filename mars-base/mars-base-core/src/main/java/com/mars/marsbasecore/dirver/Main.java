package com.mars.marsbasecore.dirver;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2022/1/19 19:22
 */
public class Main {
    public static void main(String[] args) {
        //设置 汽车司机的速率
        CarDriver carDriver = new CarDriver(1.0);
        carDriver.setMileage(100);

        CarDriver carDriver1 = new CarDriver(1.0);
        carDriver1.setMileage(100);

        //设置卡车司机对象并设置构造函数
        LorryDriver lorryDriver = new LorryDriver(2, 2.0, 100.0);
        lorryDriver.setMileage(100);

        LorryDriver lorryDriver1 = new LorryDriver(2, 2.0, 100.0);
        lorryDriver1.setMileage(100);

        LorryDriver lorryDriver2 = new LorryDriver(2, 2.0, 100.0);
        lorryDriver2.setMileage(100);

        ArrayList<DeliveryDriver> list = new ArrayList<>();
        list.add(lorryDriver);
        list.add(lorryDriver1);
        list.add(lorryDriver2);
        list.add(carDriver1);
        list.add(carDriver);
        System.out.println(carWagePercentage(list));
    }


    /**
     * 返回支付给汽车司机的日工资总额（所有司机的总和）的百分比
     *
     * @param list list
     * @return 返回汽车司机占比
     */
    private static BigDecimal carWagePercentage(ArrayList<DeliveryDriver> list) {
        List<CarDriver> carDriverList = list.stream().filter(x -> x instanceof CarDriver).
                map(x -> (CarDriver) x).collect(Collectors.toList());
        if (carDriverList.size() > 0) {
            //工资求和
            BigDecimal carDriverTotalSalary = carDriverList.stream().map(x -> BigDecimal.valueOf(x.dailyPay())).
                    reduce(BigDecimal::add).get();
            return carDriverTotalSalary.divide(new BigDecimal(list.size()), 2, RoundingMode.HALF_UP);
        }
        return null;
    }
}
