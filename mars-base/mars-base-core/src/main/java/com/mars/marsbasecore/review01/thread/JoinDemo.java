package com.mars.marsbasecore.review01.thread;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 11:35
 */
public class JoinDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("hello");
        });
        System.out.println("start");
        thread.start();
        //等待子线程执行完成后再执行
        thread.join();
        System.out.println("end");


    }
}
