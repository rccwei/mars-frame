package com.mars.marsbasecore.review01.test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.Random;

public class Creater {
	public static int num_problem = 0;
	public static Problem[] p = null;

	public static void Create(int num, int num_oper) {
		p = new Problem[num];
		char[] oper = { '+', '-' };
		String result = null;
		Random r = new Random();
		for (int i = 1; i <= num; i++) {
			result = Level.easyLevel(num_oper);
			// 生成题目
			p[num_problem] = new Problem();
			try {
				p[num_problem].result = (double) Math.round(Double.parseDouble(cal(result)) * 100) / 100;
				p[num_problem].expression = result + " = ";
				num_problem++;
			} catch (ScriptException e) {
				// 计算出错的结果舍弃
			}
		}
	}

	// 传入运算表达式，返回double结果
	public static String cal(String expression) throws ScriptException {
		ScriptEngineManager manager = new ScriptEngineManager();
		ScriptEngine se = manager.getEngineByName("js");
		return se.eval(expression).toString();
	}

	public static void main(String[] args) {
		GUI g = new GUI("出题器");
	}

}

class Problem {
	String expression = null;
	double result;
	double yourAnswer;
	int if_answered = 0;
	int if_correct = 0;
}
