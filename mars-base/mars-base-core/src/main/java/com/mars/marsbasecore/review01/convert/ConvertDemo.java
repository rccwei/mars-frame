//package com.mars.marsbasecore.convert;
//
//import ma.glasnost.orika.MapperFacade;
//import ma.glasnost.orika.MapperFactory;
//import ma.glasnost.orika.impl.DefaultMapperFactory;
//
///**
// * @description:
// * @author: WQ
// * @create: 2021-08-09 17:56
// */
//public class ConvertDemo {
//    public static void main(String[] args) {
//
//        MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
//        mapperFactory.classMap(User1.class, User2.class)
//                .field("name", "name2")
//                .byDefault()
//                .register();
//
//        User1 user1 = new User1();
//        user1.setName("lisi");
//        user1.setAge(1);
//
//        MapperFacade mapperFacade = mapperFactory.getMapperFacade();
//        //对象映射
//        User2 user2 = mapperFacade.map(user1, User2.class);
//        System.out.println("user2: " + user2.toString());
//    }
//
//}
