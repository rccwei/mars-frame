package com.mars.marsbasecore.review01.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 12:04
 */
public class HelloRegistryFacadeImpl extends UnicastRemoteObject implements HelloRegistryFacade {
    public HelloRegistryFacadeImpl() throws RemoteException {
        super();
    }

    @Override
    public String helloWorld(String name) throws RemoteException {
        return "[Registry] 你好! " + name;
    }
}
