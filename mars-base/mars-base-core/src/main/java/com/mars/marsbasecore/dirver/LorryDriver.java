package com.mars.marsbasecore.dirver;

/**
 * @author wq
 * @date 2022/1/19 19:07
 */
public class LorryDriver extends DeliveryDriver {


    /**
     * 等级
     */
    private final Integer grade;

    /**
     * 基本费率
     */
    private final Double basicRate;

    /**
     * 每日奖金
     */
    private final Double dailyBonus;


    /**
     * 创建构造函数
     *
     * @param grade
     * @param basicRate
     * @param dailyBonus
     */
    public LorryDriver(Integer grade, Double basicRate, Double dailyBonus) {
        this.grade = grade;
        this.basicRate = basicRate;
        this.dailyBonus = dailyBonus;
    }


    /**
     * 计算卡车司机日薪
     */
    @Override
    public Double dailyPay() {
        if (grade == 0) {
            return (super.getMileage() * basicRate);
        } else if (grade == 1) {
            return (super.getMileage() * basicRate + dailyBonus);
        } else {
            return (super.getMileage() * basicRate + 3 * dailyBonus);
        }
    }
}
