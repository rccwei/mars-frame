package com.mars.marsbasecore.review01.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 10:24
 */
@Component
public class OrderEventListener implements ApplicationListener<OrderEvent> {


    @Override
    public void onApplicationEvent(OrderEvent event) {
        //真正做业务的地方
        try {
            System.out.println("开始做事" + Thread.currentThread().getName());
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String s = event.getObject().toString();
        System.out.println("结束做事:" + s);
    }
}
