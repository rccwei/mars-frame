package com.mars.marsbasecore.review01.classloader;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author wq
 * @version 1.0
 * @date 2021/08/02 17:27
 * 加载一个类文件 首先去rt.jar 下面去找这个类文件 找不到再去扩展包下面找
 * 保证Java 源码不受影响
 * <p>
 * <p>
 * <p>
 * 静态代理实现较简单，只要代理对象对目标对象进行包装，即可实现增强功能，但静态代理只能为一个目标对象服务，如果目标对象过多，则会产生很多代理类。
 * JDK动态代理需要目标对象实现业务接口，代理类只需实现InvocationHandler接口。
 * 动态代理生成的类为 lass com.sun.proxy.\$Proxy4，cglib代理生成的类为class com.cglib.UserDao\$\$EnhancerByCGLIB\$\$552188b6。
 * 静态代理在编译时产生class字节码文件，可以直接使用，效率高。
 * 动态代理必须实现InvocationHandler接口，通过反射代理方法，比较消耗系统性能，但可以减少代理类的数量，使用更灵活。
 * cglib代理无需实现接口，通过生成类字节码实现代理，比反射稍快，不存在性能问题，但cglib会继承目标对象，需要重写方法，所以目标对象不能为final类。
 */

public class Demo {
    public static void main(String[] args) {
        //可以采用 aqs 底层采用 CAS  多个线程同时抢共享变量  共享变量设置volatile 保证多线程之间变量共享
        // 采用ConcurrentLinkedQueue 将没有抢到共享变量的放入队列中
        ConcurrentLinkedQueue queue = new ConcurrentLinkedQueue();
        //阻塞当前线程
        //LockSupport.park(Thread.currentThread());


        //唤醒当前线程
        //LockSupport.unpark(Thread.currentThread());
        System.out.println(Demo.class.getClassLoader());
        System.out.println(Demo.class.getClassLoader().getParent());
        System.out.println(Demo.class.getClassLoader().getParent().getParent());


    }
}
