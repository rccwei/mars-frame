package com.mars.marsbasecore.review01.test;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 09:59
 */
public class Test {
    public static void main(String[] args) {
        String str1= null;
        try {
            str1 = URLDecoder.decode("%E6%88%91%E5%B0%BC%E7%8E%9B", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println(str1);

    }
}
