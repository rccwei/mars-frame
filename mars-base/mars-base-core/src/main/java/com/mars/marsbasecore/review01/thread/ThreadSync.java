package com.mars.marsbasecore.review01.thread;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 17:31
 */
public class ThreadSync {
    public static void main(String[] args) throws InterruptedException {
        AddThread addThread = new AddThread();
        addThread.start();

        DecThread decThread = new DecThread();
        decThread.start();

        addThread.join();
        decThread.join();

        System.out.println(Counter.count);


    }
}

class Counter {
    public static int count = 0;
}

class AddThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            Counter.count += 1;

        }
    }
}

class DecThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10000; i++) {
            Counter.count -= 1;

        }
    }
}
