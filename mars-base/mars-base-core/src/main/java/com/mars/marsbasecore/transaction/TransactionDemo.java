package com.mars.marsbasecore.transaction;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/17 21:09
 * 事务的四大特性  一原持久隔离
 * 1.原子性
 * 2.一致性
 * 3.持久性
 * 4.隔离性
 * -------------------------------
 * 事务的隔离级别
 * 脏读  不可重复读(另外一个线程修改)  幻读(另外一个线程新增)
 *
 *
 */
public class TransactionDemo {
    public static void main(String[] args) {

    }
}
