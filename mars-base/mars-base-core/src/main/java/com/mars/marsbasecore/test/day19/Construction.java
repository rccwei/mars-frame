package com.mars.marsbasecore.test.day19;

public abstract class Construction implements LifeThing {

    /**
     * 生命值
     */
    public int lifeValue;

    /**
     * 消耗资源数
     */
    public int needSupportting;

    public int getLifeValue() {
        return lifeValue;
    }

    public void setLifeValue(int lifeValue) {
        this.lifeValue = lifeValue;
    }

    public int getNeedSupportting() {
        return needSupportting;
    }

    public void setNeedSupportting(int needSupportting) {
        this.needSupportting = needSupportting;
    }
}
