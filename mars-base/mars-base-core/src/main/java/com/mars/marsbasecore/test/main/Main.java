package com.mars.marsbasecore.test.main;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}
