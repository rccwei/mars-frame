package com.mars.marsbasecore.arraylist;

import lombok.Data;

/**
 * @author wq
 * @date 2022/3/23 17:36
 */
@Data
public class User {

    private String name;

    private Integer gender;

    private Integer score;
}
