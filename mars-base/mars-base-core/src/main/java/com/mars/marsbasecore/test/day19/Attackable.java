package com.mars.marsbasecore.test.day19;

/**
 * @author Administrator
 */
public interface Attackable {

    Integer attack();
}
