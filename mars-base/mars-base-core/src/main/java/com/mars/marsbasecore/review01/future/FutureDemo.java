package com.mars.marsbasecore.review01.future;

import java.util.concurrent.*;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 11:15
 */
public class FutureDemo {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> future = executor.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                Thread.sleep(2000);
                return 1;
            }
        });

        Future<Integer> future1 = executor.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                Thread.sleep(3000);
                return 2;
            }
        });
        //JDK5新增了Future接口，用于描述一个异步计算的结果。
        // 虽然 Future 以及相关使用方法提供了异步执行任务的能力，但是对于结果的获取却是很不方便，
        // 只能通过阻塞或者轮询的方式得到任务的结果。阻塞的方式显然和我们的异步编程的初衷相违背，
        // 轮询的方式又会耗费无谓的 CPU 资源，而且也不能及时地得到计算结果。
        System.out.println(future.get());
        System.out.println(future1.get());

        System.out.println("finish");
        executor.shutdown();
    }
}
