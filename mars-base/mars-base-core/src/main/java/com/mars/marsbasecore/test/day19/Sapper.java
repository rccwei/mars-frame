package com.mars.marsbasecore.test.day19;

import java.util.Random;

/**
 * 工兵类
 */
public class Sapper extends Person {

    /**
     * 构建塔
     *
     * @return Construction
     */
    public Tower buildTower() {
        Tower tower = new Tower();
        tower.setLifeValue(new Random().nextInt(500) + 500);
        tower.setNeedSupportting(2);
        System.out.println(Thread.currentThread().getName() + "正在构建宝塔中~");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return tower;
    }

    /**
     * 采集资源
     */
    public void getResource() {
        // 采集资源 每次采集1
        super.needResource++;
        super.lifeValue--;
    }
}
