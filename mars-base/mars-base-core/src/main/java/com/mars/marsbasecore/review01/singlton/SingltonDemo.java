package com.mars.marsbasecore.review01.singlton;

/**
 * @author wq
 * @date 2021-07-31 14:35
 */
public class SingltonDemo {

    /**
     * 禁止指令重排
     */
    private static SingltonDemo instance = null;

    private SingltonDemo() {
        System.out.println(Thread.currentThread().getName() + "我是构造方法执行");
    }

    /**
     * 双端检索机制 DCL
     *
     * @return
     */
    public static SingltonDemo getInstance() {
        if (instance == null) {
            synchronized (SingltonDemo.class) {
                if (instance == null) {
                    instance = new SingltonDemo();
                }
            }
        }
        return instance;
    }
}
