package com.mars.marsbasecore.review01.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 12:04
 */
public interface HelloRegistryFacade extends Remote {

    String helloWorld(String name) throws RemoteException;
}
