package com.mars.marsbasecore.review01.function;

import java.util.function.Supplier;

/**
 * @author wq
 * @date 2021/12/31 14:35
 */
public class SupplierFunctionDemo {
    public static void main(String[] args) {
        int[] arr = {2, 8, 4, 10, 9, 0};
        int maxNum = getMax(arr, () -> {
            int max = arr[0];
            for (int i : arr) {
                if (max < i) {
                    max = i;
                }
            }
            return max;
        });
        System.out.println(maxNum);

    }

    private static int getMax(int[] arr, Supplier<Integer> supplier) {
        return supplier.get();
    }
}
