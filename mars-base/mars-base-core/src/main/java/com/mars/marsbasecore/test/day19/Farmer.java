package com.mars.marsbasecore.test.day19;

/**
 * @author Mars
 * @date 2022-06-19 13:23
 */
public class Farmer extends Person {


    /**
     * 采集资源
     */
    public void getResource() {
        // 采集资源 每次采集1
        super.needResource++;
        super.lifeValue--;
    }
}
