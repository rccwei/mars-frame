package com.mars.marsbasecore.feature;


import org.springframework.util.StopWatch;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * 举个常见的案例 ，在APP查询首页信息的时候，一般会涉及到不同的RPC远程调用来获取很多用户相关信息数据，比如：商品banner轮播图信息、
 * 用户message消息信息、用户权益信息、用户优惠券信息 等，假设每个rpc invoke()耗时是250ms，那么基于同步的方式获取到话，
 * 算下来接口的RT至少大于1s，这响应时长对于首页来说是万万不能接受的，因此，我们这种场景就可以通过多线程异步的方式去优化。
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 15:47:04
 */
public class CompletableFutureTask {


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        thenCombine();
    }

    public static void thenCombine() throws ExecutionException, InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        CompletableFuture<Integer> f1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询粉丝以及关注数量");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return 1;
        });
        CompletableFuture<Integer> f2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询个人信息");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return 2;
        });
        CompletableFuture<Integer> f3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询个人中心作品列表");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            return 3;
        });
         CompletableFuture.allOf(f2, f1, f3).join();
        stopWatch.stop();
        System.out.printf("共计耗时毫秒:%s%n", stopWatch.getTotalTimeMillis());
    }
}
