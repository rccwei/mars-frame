package com.mars.marsbasecore.review01.event;

import com.mars.marsbasecore.review01.test.Person;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 10:23
 */
public class OrderEvent extends BaseEvent<Person> {

    private Object object;


    public OrderEvent(Object source, Person object) {
        super(source, object);
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
