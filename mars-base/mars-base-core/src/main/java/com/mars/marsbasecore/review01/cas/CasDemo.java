package com.mars.marsbasecore.review01.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wq
 * @date 2021-07-31 14:54
 * cas  unSafe
 * 缺点: 循环开销大 只能保证一个共享原子操作
 * ABA 问题
 * 解决方式: 原子引用 AtomicReference  版本号
 */
public class CasDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);
        atomicInteger.compareAndSet(5, 10);
        System.out.println(atomicInteger.get());
    }
}
