package com.mars.marsbasecore.review01.function;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

/**
 * @author wq
 * @date 2021/12/31 14:41
 */
public class ConsumerFunctionDemo {

    static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
//        show(s -> System.out.println(s + " No1"));
//        show(s -> System.out.println(s + " No2"));
//        for (int i = 0; i < 100; i++) {
//            int finalI = i;
//            show(s -> System.out.println("s" + "No" + finalI));
//        }

//        for (int i = 0; i < 1000; i++) {
//            int finalI = i;
//            new Thread(() -> {
//                show((s) -> System.out.println(s + "No" + finalI));
//            }, i + "").start();
//        }

        showAndThen(s -> {
            System.out.println(s + "No1");
        }, s -> {
            System.out.println(s + "No2");
        });


    }

    private static void showAndThen(Consumer<String> consumer1, Consumer<String> consumer2) {
        consumer1.andThen(consumer2).accept("我是消费者");
    }


    private static void show(Consumer<String> consumer) {
        lock.lock();
        try {
            consumer.accept("我是消费方法");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }


    }
}
