package com.mars.marsbasecore.review01.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

/**
 * @author 程序猿Mars
 * @date 2023-10-03 10:32
 */
@Component
public abstract class BaseEvent<T> extends ApplicationEvent {
    private static final long serialVersionUID = 895628808370649881L;
    protected T eventData;

    public BaseEvent(Object source, T eventData) {
        super(source);
        this.eventData = eventData;
    }

    public BaseEvent(T eventData) {
        super(eventData);
    }

    public T getEventData() {
        return this.eventData;
    }

    public void setEventData(T eventData) {
        this.eventData = eventData;
    }
}
