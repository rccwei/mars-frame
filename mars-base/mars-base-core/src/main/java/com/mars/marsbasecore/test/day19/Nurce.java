package com.mars.marsbasecore.test.day19;

import java.util.Random;

/**
 * 护士
 */
public class Nurce extends Person {
    /**
     * 自愈
     *
     * @param gunman person
     */
    public Integer cure(GunMan gunman, Integer lifeValue) {
        // 机枪兵开始回血

        Random random = new Random();
        int i = random.nextInt(lifeValue);
        gunman.setLifeValue(gunman.getLifeValue() + i);
        System.out.println(Thread.currentThread().getName() + "护士开始治疗~增加生命值:" + i);
        System.out.println(Thread.currentThread().getName() + "护士治疗后机枪兵生命值:" + gunman.getLifeValue());
        return gunman.getLifeValue();
    }
}
