package com.mars.marsbasecore.review01.test;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame implements ActionListener {
	// 出题面板组件
	JPanel p_create = new JPanel();
	JLabel l_numInput = new JLabel("所出题数量：");
	JTextField t_numInput = new JTextField(10);
	JLabel l_num_oper = new JLabel("运算符数量：");
	JTextField t_num_oper = new JTextField(10);
	JButton btn_start = new JButton("开始出题");
	// 答题面板组件
	JPanel p_answer = new JPanel();
	JLabel l_problemAlert = new JLabel("第1题：");
	JLabel l_problem = new JLabel("X ? X = ");
	JTextField t_answer = new JTextField(10);
	JButton btn_pre = new JButton("上一题");
	JButton btn_next = new JButton("下一题");
	JButton btn_submit = new JButton("提交分数");
	// 输出面板组件
	JPanel p_output = new JPanel();
	JTextArea t_output = new JTextArea();
	// 定义题目位置
	int pos_problem = 0;

	GUI(String title) {
		super(title);// 设置标题
		this.setLayout(null);// 设置空布局
		/* 以下设置出题面板 */
		p_create.setLayout(null);// 设置空布局
		p_create.setBorder(BorderFactory.createTitledBorder("出题区域"));
		p_create.setBounds(0, 0, 485, 80);// 出题面板布局
		// 设置输入出题数提示标签
		l_numInput.setBounds(50, 10, 130, 30);
		l_numInput.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		p_create.add(l_numInput);
		// 设置题目数量输入框
		t_numInput.setBounds(130, 15, 40, 25);
		p_create.add(t_numInput);
		// 设置输入操作符数量提示标签
		l_num_oper.setBounds(50, 40, 130, 30);
		l_num_oper.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		p_create.add(l_num_oper);
		// 设置运算符数量输入框
		t_num_oper.setBounds(130, 45, 40, 25);
		p_create.add(t_num_oper);
		// 设置按钮
		btn_start.setBounds(300, 30, 120, 30);
		btn_start.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btn_start.addActionListener(this);
		p_create.add(btn_start);// 出题按钮加入出题面板
		this.add(p_create);// 出题面板加入框架

		/* 以下设置答题面板 */
		p_answer.setLayout(null);// 设置空布局
		p_answer.setBorder(BorderFactory.createTitledBorder("答题区域"));
		p_answer.setBounds(0, 80, 485, 160);// 出题面板布局
		this.add(p_answer);// 答题面板加入框架
		// 设置答题提示标签
		l_problemAlert.setBounds(50, 20, 100, 30);
		l_problemAlert.setForeground(Color.red);
		l_problemAlert.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		p_answer.add(l_problemAlert);
		// 设置问题
		l_problem.setBounds(130, 20, 300, 30);
		l_problem.setFont(new Font("微软雅黑", Font.BOLD, 13));
		p_answer.add(l_problem);
		// 设置答案输入框
		t_answer.setBounds(330, 20, 100, 30);
		t_answer.setEnabled(false);// 答案框不可输入
		p_answer.add(t_answer);
		// 设置上一题按钮
		btn_pre.setBounds(50, 70, 150, 30);
		btn_pre.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btn_pre.addActionListener(this);
		p_answer.add(btn_pre);
		// 设置下一题按钮
		btn_next.setBounds(300, 70, 150, 30);
		btn_next.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btn_next.addActionListener(this);
		p_answer.add(btn_next);
		// 设置提交按钮
		btn_submit.setBounds(150, 115, 200, 30);
		btn_submit.setFont(new Font("微软雅黑", Font.PLAIN, 18));
		btn_submit.addActionListener(this);
		p_answer.add(btn_submit);
		/* 以下设置输出面板 */
		p_output.setLayout(null);
		p_output.setBorder(BorderFactory.createTitledBorder("控制台"));
		p_output.setBounds(0, 240, 485, 320);
		this.add(p_output);
		// 设置输出栏
		t_output.setLineWrap(true);// 设置自动换行
		t_output.setEditable(false);// 不可编辑
		JScrollPane jsp = new JScrollPane(t_output);// 设置滚动条
		jsp.setBounds(10, 20, 465, 290);
		p_output.add(jsp);

		this.setSize(500, 600);// 设置界面大小
		this.setResizable(false);// 窗口大小不可调
		this.setLocationRelativeTo(null);// 界面居中
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);// 关闭程序
		this.setVisible(true);// 界面可视
	}

	// 开始出题
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_start) {
			int n = 0;
			try {
				n = Integer.parseInt(t_numInput.getText().trim());
				if (n <= 0)
					throw new NumberFormatException();// 出题数量应为正整数
				Creater.num_problem = 0;// 题目计数清零
				t_output.setText("");// 清空结果
				Creater.Create(n, Integer.parseInt(t_num_oper.getText().trim()));
				for (int i = 0; i < n; i++) {
					t_output.append("第" + (i + 1) + "题：" + Creater.p[i].expression + "?\n");
				}
				t_output.append("出题结束，共计" + Integer.toString(n) + "道，开始准备答题");
				l_problemAlert.setText("第1题：");
				l_problem.setText(Creater.p[0].expression);// 初始化题目面板
				pos_problem = 0;// 题目位置初始化
				t_answer.setText("");// 清空答案框
				t_answer.setEnabled(true);// 答案框可输入
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(this, "输入错误", "错误", JOptionPane.ERROR_MESSAGE);
			}
		} else if (e.getSource() == btn_pre) {
			if (pos_problem - 1 >= 0) {
				try {
					double yourAnswer = Double.parseDouble(t_answer.getText().trim());
					Creater.p[pos_problem].yourAnswer = yourAnswer;// 保留当前题目答案
					Creater.p[pos_problem].if_answered = 1;
				} catch (NumberFormatException exception) {
					t_output.setText("第" + (pos_problem + 1) + "题，答案为空或答案格式不对，答案无效");
					Creater.p[pos_problem].if_answered = 0;
				}
				if (Creater.p[pos_problem - 1].if_answered == 1)
					if (if_isInt(Creater.p[pos_problem - 1].yourAnswer))
						t_answer.setText(Integer.toString((int) Creater.p[pos_problem - 1].yourAnswer));// 整数不显示为小数
					else
						t_answer.setText(Double.toString(Creater.p[pos_problem - 1].yourAnswer));
				else
					t_answer.setText("");
				pos_problem--;
				l_problemAlert.setText("第" + (pos_problem + 1) + "题：");
				l_problem.setText(Creater.p[pos_problem].expression);
			}
		} else if (e.getSource() == btn_next) {
			if (Creater.p != null && pos_problem + 1 < Creater.p.length) {
				try {
					double yourAnswer = Double.parseDouble(t_answer.getText().trim());
					Creater.p[pos_problem].yourAnswer = yourAnswer;// 保留当前题目答案
					Creater.p[pos_problem].if_answered = 1;
				} catch (NumberFormatException exception) {
					t_output.setText("第" + (pos_problem + 1) + "题，答案为空或答案格式不对，答案无效");
					Creater.p[pos_problem].if_answered = 0;
				}
				if (Creater.p[pos_problem + 1].if_answered == 1)
					if (if_isInt(Creater.p[pos_problem + 1].yourAnswer))
						t_answer.setText(Integer.toString((int) Creater.p[pos_problem + 1].yourAnswer));// 整数不显示为小数
					else
						t_answer.setText(Double.toString(Creater.p[pos_problem + 1].yourAnswer));
				else
					t_answer.setText("");
				pos_problem++;
				l_problemAlert.setText("第" + (pos_problem + 1) + "题：");
				l_problem.setText(Creater.p[pos_problem].expression);
			}
		} else if (e.getSource() == btn_submit) {
			if (Creater.p != null) {
				int num_problemCorrect = 0;
				int num_unAnswered = 0;
				try {// 确保最后一道题能保存
					double yourAnswer = Double.parseDouble(t_answer.getText().trim());
					Creater.p[pos_problem].yourAnswer = yourAnswer;// 保留当前题目答案
					Creater.p[pos_problem].if_answered = 1;
				} catch (NumberFormatException exception) {
					t_output.setText("第" + (pos_problem + 1) + "题，答案为空或答案格式不对，答案无效");
					Creater.p[pos_problem].if_answered = 0;
				}
				t_output.setText("");// 清空输出
				for (int i = 0; i < Creater.p.length; i++) {
					if (Creater.p[i].if_answered == 1 && Creater.p[i].yourAnswer == Creater.p[i].result) {
						Creater.p[i].if_correct = 1;
						num_problemCorrect++;
					}
					if (Creater.p[i].if_correct == 0) {
						t_output.append("第" + (i + 1) + "题回答错误：\n");
						t_output.append(Creater.p[i].expression + "?\n");
						if (Creater.p[i].if_answered == 1)
							if (if_isInt(Creater.p[i].yourAnswer))
								t_output.append("你的答案：" + (int) Creater.p[i].yourAnswer);// 整数不显示为小数
							else
								t_output.append("你的答案：" + Creater.p[i].yourAnswer);
						else {
							t_output.append("你的答案：无");
							num_unAnswered++;// 未回答数加一
						}
						if (if_isInt(Creater.p[i].result))
							t_output.append(" 正确答案：" + (int) Creater.p[i].result + "\n");// 整数不显示为小数
						else
							t_output.append(" 正确答案：" + Creater.p[i].result + "\n");
					}
				}
				// 统计结果输出
				t_output.append("总计题目：" + Creater.p.length + "道\n");
				t_output.append("答对题目：" + num_problemCorrect + "道\n");
				t_output.append("答错题目：" + (Creater.p.length - num_problemCorrect - num_unAnswered) + "道\n");
				t_output.append("未答题目：" + num_unAnswered + "道\n");
				t_output.append("正确率："
						+ (double) (Math.round((num_problemCorrect / 1.0 / Creater.p.length * 100.0) * 100) / 100.0)
						+ "%");
			}
		}

	}

	// 判断小数是否为整数
	public boolean if_isInt(double n) {
		if ((int) n == n)
			return true;
		else
			return false;
	}
}
