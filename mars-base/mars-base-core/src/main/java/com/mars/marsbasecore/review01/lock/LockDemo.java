//package com.mars.marsbasecore.lock;
//
//import org.openjdk.jol.info.ClassLayout;
//
///**
// * @author wq
// * @version 1.0
// * @date 2021/08/02 11:56
// * synchronized  jvm层面的锁  可能造成线程堆积  1.6之后对大量的优化
// * Lock 接口  比较灵活 底层采用cas自旋的方式  try Lock  可以设置尝试获取锁的时间
// * 区别
// * <p>
// * synchronized：偏向锁 向当前对象中保存一个线程ID
// * <p>
// * 一个对象的内部组成
// */
//public class LockDemo {
//    public static void main(String[] args) {
//        User user = new User();
//        //打印对象
//        System.out.println(ClassLayout.parseInstance(user).toPrintable());
//    }
//}
