package com.mars.marsbasecore.dirver;

/**
 * @author wq
 * @date 2022/1/19 19:03
 */
public abstract class DeliveryDriver {

    /**
     * 司机名称
     */
    private String name;


    /**
     * 里程数
     */
    private Integer mileage;

    /**
     * 定义抽象方法
     *
     * @return String
     */
    public abstract Double dailyPay();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }
}
