package com.mars.marsbasecore.test.day19;

import java.util.Random;

public class Person implements Mobile, Attackable, LifeThing {
    /**
     * 生命值
     */
    public int lifeValue;
    /**
     * 攻击力
     */
    public int attackPower;
    /**
     * 占用的供给数
     */
    public int needSupportting;
    /**
     * 消耗资源数
     */
    public int needResource;

    @Override
    public Integer attack() {
        System.out.println(Thread.currentThread().getName() + "机枪兵开始攻击");
        // 生命值
        int total = 0;
        for (int i1 = 0; i1 < 5; i1++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int i = new Random().nextInt(100);
            System.out.println(Thread.currentThread().getName() + "正在攻击中~ 机枪兵攻击受伤值:" + i);
            total += i;
        }
        this.lifeValue = this.lifeValue - total;
        return total;
    }

    @Override
    public void move() {
        Random random = new Random();
        int i = random.nextInt(1) + 1;
        try {
            // 移动有快又慢 这里做为游戏的比较点
            Thread.sleep(i * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 移动消耗资源数量
        this.needResource--;
    }

    public Person() {
    }

    public Person(int lifeValue, int attackPower, int needSupportting, int needResource) {
        this.lifeValue = lifeValue;
        this.attackPower = attackPower;
        this.needSupportting = needSupportting;
        this.needResource = needResource;
    }

    public int getLifeValue() {
        return lifeValue;
    }

    public void setLifeValue(int lifeValue) {
        this.lifeValue = lifeValue;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public int getNeedSupportting() {
        return needSupportting;
    }

    public void setNeedSupportting(int needSupportting) {
        this.needSupportting = needSupportting;
    }

    public int getNeedResource() {
        return needResource;
    }

    public void setNeedResource(int needResource) {
        this.needResource = needResource;
    }
}
