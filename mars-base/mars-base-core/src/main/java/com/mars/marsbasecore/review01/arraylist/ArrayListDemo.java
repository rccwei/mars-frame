package com.mars.marsbasecore.review01.arraylist;

import com.mars.marsbasecore.arraylist.User;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2021-07-31 17:04
 */
public class ArrayListDemo {
    public static void main(String[] args) {
        ArrayList<User> list = new ArrayList<>();
        User user = new User();
        user.setGender(1);
        user.setName("lisi");
        user.setScore(22);
        list.add(user);

        User user1 = new User();
        user1.setGender(1);
        user1.setName("lisi");
        user1.setScore(99);
        list.add(user1);

        User user3 = new User();
        user3.setGender(1);
        user3.setName("lisi212");
        user3.setScore(66);
        list.add(user3);


        User user2 = new User();
        user2.setGender(2);
        user2.setName("mars");
        user2.setScore(22);
        list.add(user2);


        User user4 = new User();
        user4.setGender(2);
        user4.setName("mars1");
        user4.setScore(88);
        list.add(user4);

//        Map<Integer, List<User>> map = list.stream().collect(Collectors.groupingBy(User::getGender,
//                Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(User::getScore))), ArrayList::new)));
//        System.out.println(map);
        // 分组排序
        Map<Integer, User> collect = list.stream().collect(Collectors.groupingBy(User::getGender,
                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparingInt(User::getScore)), Optional::get)));
        Map<Integer, User> collect1 = list.stream().collect(Collectors.toMap(User::getGender, Function.identity(),
                BinaryOperator.maxBy(Comparator.comparingInt(User::getScore))));
        System.out.println(collect);
        System.out.println(collect1);
        Collections.sort(list, Comparator.comparing(User::getScore));
        Map<Integer, List<User>> map = list.stream().collect(Collectors.groupingBy(User::getGender));
        System.out.println(map);


        // 分组求和

    }
}
