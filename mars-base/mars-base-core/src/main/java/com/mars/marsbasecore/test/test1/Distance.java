package com.mars.marsbasecore.test.test1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author wq
 * @date 2022/8/16 15:07
 */
public class Distance {
    HashMap<String, String> parent;
    HashSet<String> bests;
    int[] distance;

    public Distance(int len) {
        parent = new HashMap<>(); // key: 当前节点，value：从原点到达key的最短路径的前驱节点
        distance = new int[len]; // distance[i]: 原节点到节点i的最短距离
        bests = new HashSet<>(); // 选择过的最优的节点的集合
    }

    public static void main(String[] args) {
        int[][] weight = {{0, -1, 10, -1, 30, 100}, {-1, 0, 5, -1, -1, -1}, {-1, -1, 0, 50, -1, -1}, {-1, -1, -1, 0, -1, 10},
                {-1, -1, -1, 20, 0, 60}, {-1, -1, -1, -1, -1, 0}};
        String[] str = {"V1", "V2", "V3", "V4", "V5", "V6"};
        int len = str.length;
        Distance p1 = new Distance(len);
        p1.dijkstra(weight, str, 1);
    }

    public void dijkstra(int[][] weights, String[] nodes, int v) {
        // 1. 初始化：源点到自身距离为0，到其他节点距离为无穷大
        for (int i = 0; i < nodes.length; i++) {
            if (i == v - 1)
                this.distance[i] = 0;
            else this.distance[i] = Integer.MAX_VALUE;
        }
        // 不断迭代
        while (true) {
            int min = Integer.MAX_VALUE;
            int index = -1;
            // 1. 在未标记的节点中找出从源点到该点最短的路径节点
            for (int i = 0; i < this.distance.length; i++) {
                if (this.distance[i] < min && !bests.contains(nodes[i])) {
                    min = this.distance[i];
                    index = i;
                }
            }
            // 找不到可达的路径了
            if (index == -1) {
                break;
            }
            // 将该节点放入bests集合中
            this.bests.add(nodes[index]);
            // 2. 扩展该节点的子节点，计算源点到子节点的距离，若小于原来距离，则更新
            // distance表和parent表
            for (int i = 0; i < nodes.length; i++) {
                // 找到一个为扩展的子节点
                if (weights[index][i] > 0 && !bests.contains(nodes[i])) {
                    int new_dis = distance[index] + weights[index][i];
                    // 新距离小于原始距离，更新
                    if (new_dis < distance[i]) {
                        distance[i] = new_dis;
                        parent.put(nodes[i], nodes[index]);
                    }
                }
            }
        }
        // 倒叙找到路径
        for (int i = 0; i < nodes.length; i++) {
            if (i != v - 1) {
                if (this.distance[i] == Integer.MAX_VALUE) {
                    System.out.print(nodes[v - 1] + "到" + nodes[i] + "之间没有最短路径");
                } else {
                    ArrayList<String> path = new ArrayList<>();
                    String current = nodes[i];
                    path.add(current);
                    while (current != nodes[v - 1]) {
                        current = parent.get(current);
                        path.add(current);
                    }
                    for (int j = path.size() - 1; j >= 0; j--) {
                        if (j != 0)
                            System.out.print(path.get(j) + "->");
                        else System.out.print(path.get(j));
                    }
                }
                System.out.println();
            }

        }
    }

}
