package com.mars.marsbasecore.test.test02;

import java.io.InputStream;
import java.util.Scanner;

/**
 * https://blog.csdn.net/LanTingShuXu/article/details/81277306?utm_medium=distribute.pc_relevant.none-task-blog-2~default~baidujs_title~default-0-81277306-blog-75654438.pc_relevant_multi_platform_whitelistv4&spm=1001.2101.3001.4242.1&utm_relevant_index=3
 * @author Mars
 * @date 2022-08-14 17:44
 */
public class Main {
    // 起始点
    static final Point START = new Point(0, 0);
    // 记录最短路径（默认是“无限大”表示不可达）
    static int minPath = Integer.MAX_VALUE;

    public static void main(String[] args) {
        // 创建输入解析器
        InputParser parser = new InputParser(System.in);
        parser.parse();// 获取输入，开始转换
        System.out.println("最短路径:" + rangeAll(parser.getPoints(), 0));
    }

    /**
     * 用全排列解决此问题
     *
     * @param points
     * @param n
     */
    public static int rangeAll(Point[] points, int n) {
        if (n == points.length) {
            // 计算这次排列的路径长度
            int sum = points[0].getLength(START);
            for (int i = 1; i < points.length; i++) {
                sum += points[i - 1].getLength(points[i]);
            }
            // 【千万不要忘了加上回到原点的路径】
            sum += points[points.length - 1].getLength(START);
            // 记录最短长度【其实这里也可以记录下路径详情(也就是这次全排列的顺序)】
            minPath = Math.min(minPath, sum);
            return minPath;
        }
        // 全排列算法部分
        for (int i = n; i < points.length; i++) {
            swap(points, n, i);
            rangeAll(points, n + 1);
            swap(points, n, i);
        }
        return minPath;
    }

    /**
     * 交换数组的i,j两个值
     *
     * @param points
     * @param i
     * @param j
     */
    public static void swap(Point[] points, int i, int j) {
        if (i == j) {
            return;
        }
        Point temp = points[i];
        points[i] = points[j];
        points[j] = temp;
    }

}

/**
 * 输入解析类
 *
 * @author Mars
 */
class InputParser {

    private Scanner scanner;// 输入扫描器
    private Point[] points;// 点集
    private int num;// 总共有几个点

    public InputParser(InputStream in) {
        scanner = new Scanner(in);
    }

    public Point[] getPoints() {
        return points;
    }

    public int getNum() {
        return num;
    }

    /**
     * 解析输入，格式例如：<br/>
     * 3<br/>
     * 2,2<br/>
     * 2,8<br/>
     * 6,6<br/>
     */
    public void parse() {
        // 获取有几个点
        num = Integer.parseInt(scanner.nextLine().trim());
        points = new Point[num];// 构建点数组
        // 分别获取各个点的坐标
        for (int i = 0; i < num; i++) {
            String[] locations = scanner.nextLine().trim().split(",");
            points[i] = new Point(Integer.parseInt(locations[0]), Integer.parseInt(locations[1]));
        }
    }
}

/**
 * 送货点类
 *
 * @author Mars
 */
class Point {
    int px;
    int py;

    public Point(int px, int py) {
        this.px = px;
        this.py = py;
    }

    /**
     * 获取与指定点之间的距离
     *
     * @param p
     * @return
     */
    public int getLength(Point p) {
        return Math.abs(px - p.px) + Math.abs(py - p.py);
    }
}
