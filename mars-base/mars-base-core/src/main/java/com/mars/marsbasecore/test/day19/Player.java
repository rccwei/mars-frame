package com.mars.marsbasecore.test.day19;

import java.util.List;

/**
 * @author Administrator
 */
public class Player {
    /**
     * 名称
     */
    public String name;
    /**
     * 玩家的最大人口数
     */
    public int maxCount;
    /**
     * 玩家的当前人数
     */
    public int count;
    /**
     * 玩家的资源数
     */
    public int supporttingCount;
    /**
     * 玩家所拥有的人口对象
     */
    public List<Person> persons;
    /**
     * 玩家所拥有的建筑对象
     */
    public List<Construction> constructions;

    public Player() {
    }

    public Player(String name, int maxCount, int count, int supporttingCount, List<Person> persons, List<Construction> constructions) {
        this.name = name;
        this.maxCount = maxCount;
        this.count = count;
        this.supporttingCount = supporttingCount;
        this.persons = persons;
        this.constructions = constructions;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", maxCount=" + maxCount +
                ", count=" + count +
                ", supporttingCount=" + supporttingCount +
                ", persons=" + (persons) +
                ", constructions=" + (constructions) +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(int maxCount) {
        this.maxCount = maxCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSupporttingCount() {
        return supporttingCount;
    }

    public void setSupporttingCount(int supporttingCount) {
        this.supporttingCount = supporttingCount;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

    public List<Construction> getConstructions() {
        return constructions;
    }

    public void setConstructions(List<Construction> constructions) {
        this.constructions = constructions;
    }
}
