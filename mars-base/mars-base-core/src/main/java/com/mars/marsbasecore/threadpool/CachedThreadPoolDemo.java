package com.mars.marsbasecore.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author wq
 * @date 2022/1/20 14:31
 * 创建一个可缓存线程池，如果线程池长度超过处理需要，可灵活回收空闲线程
 */
public class CachedThreadPoolDemo {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName());
                print();
            });
        }
        executorService.shutdown();
    }

    public static void print() {
        System.out.println("CachedThreadPool execute");
    }
}
