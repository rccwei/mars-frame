package com.mars.marsbasecore.review01.test;

import org.apache.commons.lang.StringUtils;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

/**
 * @description:
 * @author: WQ
 * @create: 2021-08-11 15:47
 */
public class Demo {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("1", "2", "3");
        String data = StringUtils.join(list, ",");
        System.out.println(data);
        System.out.println();
    }
}
