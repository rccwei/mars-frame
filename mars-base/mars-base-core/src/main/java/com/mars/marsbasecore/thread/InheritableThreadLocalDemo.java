package com.mars.marsbasecore.thread;

/**
 * @author wq
 * @date 2022/11/11 14:36
 */
public class InheritableThreadLocalDemo {


    static InheritableThreadLocal<String> threadLocal = new InheritableThreadLocal<>();

    public static void main(String[] args) {
        threadLocal.set("dhytest");
        new Thread(()->{
            System.out.println("子线程获取到的值：" + threadLocal.get());
        }).start();

        System.out.println("父线程获取到的值：" + threadLocal.get());
    }

}
