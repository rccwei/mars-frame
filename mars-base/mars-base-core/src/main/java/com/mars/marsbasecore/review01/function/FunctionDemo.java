package com.mars.marsbasecore.review01.function;

import java.util.function.Function;

/**
 * @author wq
 * @date 2021/12/31 15:01
 */
public class FunctionDemo {
    public static void main(String[] args) {
        Integer show = show("123", Integer::parseInt);
        System.out.println(show instanceof Integer);
    }

    private static Integer show(String str, Function<String, Integer> function) {
        return function.apply(str);
    }
}
