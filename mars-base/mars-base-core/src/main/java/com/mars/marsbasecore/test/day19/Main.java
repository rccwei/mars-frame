package com.mars.marsbasecore.test.day19;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author Mars
 * @date 2022-06-19 11:40
 */
public class Main {
    public static void main(String[] args) {
        try {
            toHome();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void toHome() throws ExecutionException, InterruptedException {
        menu();
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入玩家A名称:");
        String playerA = sc.next();

        System.out.print("请输入玩家B名称:");
        String playerB = sc.next();
        int winACount = 0;
        int winBCount = 0;
        int playCount = 0;
        String win = "";
        while (true) {
            if (playCount < 3) {
                playCount++;
                System.out.println("第" + playCount + "回合正在比拼中~");
                MyThread myThread = new MyThread();
                FutureTask<Integer> futureTask = new FutureTask<>(myThread);
                new Thread(futureTask, playerA + "玩家").start();
                Integer totalA = futureTask.get();

                MyThread myThreadB = new MyThread();
                FutureTask<Integer> futureTaskB = new FutureTask<>(myThreadB);
                new Thread(futureTaskB, playerB + "玩家").start();
                Integer totalB = futureTaskB.get();
                if (totalA > totalB) {
                    System.out.println("第" + playCount + "回合" + playerA + "胜利");
                    winACount++;
                    if (winACount >= 2) {
                        win = playerA;
                        break;
                    }
                } else {
                    winBCount++;
                    System.out.println("第" + playCount + "回合" + playerB + "胜利");
                    if (winBCount >= 2) {
                        win = playerB;
                        break;
                    }
                }
            } else {
                break;
            }
        }
        System.out.println("最终赢家是:" + win);


    }

    private static void menu() {
        System.out.println("******欢迎来到对战策略游戏********");
        System.out.println("游戏规则如下:\n" +
                "重要(每局以生命值多少来决定胜负,机枪兵攻击消耗生命值，护士对机枪兵治疗)\n" +
                "1.供给站开始创建(初始化生命值，双方生命值一样)\n" +
                "1.工兵(生命值150，攻击力8，消耗数2)开始建筑宝塔和采集矿资源。\n" +
                "2.指挥中心开始创建农民(生命值100，消耗数2)并农民开始创建资源。\n" +
                "3.兵营创建机枪兵(生命值200，攻击力10，消耗数4)以及机枪兵开始攻击。\n" +
                "4.护士对机枪兵治疗增加血量\n" +
                "5.比较机总生命值\n" +
                "本游戏采用三局两胜回合制的方式来决定胜负。");
        System.out.println("********************************");
    }


    static class MyThread implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            return execute();
        }
    }


    private static Integer execute() {
        Player player = new Player();
        player.setName(Thread.currentThread().getName());
        List<Person> personList = new ArrayList<>();
        List<Construction> constructionList = new ArrayList<>();
        // 1.供给站开始初始化生命值
        Supportting supportting = new Supportting();
        // 生命值
        Integer initLifeValue = supportting.initLifeValue();
        constructionList.add(supportting);

        // 2.工兵开始建筑宝塔和采集矿资源。
        Sapper sapper = new Sapper();
        Tower tower = sapper.buildTower();
        // 采集资源
        sapper.getResource();
        // 宝塔生命值
        int towerLifeValue = tower.getLifeValue();
        personList.add(sapper);
        constructionList.add(tower);
        CommandCenter commandCenter = new CommandCenter();
        Farmer farmer = commandCenter.createFarmer();
        // 农民采集资源 减少生命值
        farmer.getResource();

        personList.add(farmer);
        constructionList.add(commandCenter);

        // 兵营创建机枪兵
        Barracks barracks = new Barracks();
        GunMan gunman = barracks.createGunman();
        // 机枪兵开始攻击
        Integer attackNum = gunman.attack();
        System.out.println("机枪兵血量:" + gunman.getLifeValue());
        personList.add(gunman);
        constructionList.add(barracks);

        // 护士对机枪兵治疗增加血量
        Nurce nurce = new Nurce();
        Integer gunmanLiveValue = nurce.cure(gunman, attackNum);
        personList.add(nurce);
        int total = initLifeValue + towerLifeValue + gunmanLiveValue;
        System.out.println("******" + "【" + player.getName() + "】" + "最终生命值:" + total + "*******");
        return total;

    }
}
