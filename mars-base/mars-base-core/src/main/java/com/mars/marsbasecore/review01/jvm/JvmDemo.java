package com.mars.marsbasecore.review01.jvm;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author wq
 * @date 2021-08-03 21:27
 * <p>
 * 线程公有:方法区和堆
 * 线程私有:java栈 本地方法栈 程序计数器
 * <p>
 * 垃圾回收算法:标记清除  标记整理 复制 以及 引用计数
 * <p>
 * 哪些对象可以用作GC ROOT 的对象
 * 1.虚拟机栈(局部变量表中引用的对象)
 * 2.方法区中的类静态属性引用的对象
 * 3.方法区中常量引用的对象
 * 4.本地方法栈中JNI 引用的对象
 */
public class JvmDemo {
    public static void main(String[] args) {
//        Integer a = null;
//        Optional<Integer> optional = Optional.ofNullable(a);
//        //如果对象为空会报错
//        System.out.println(optional.get());

        Integer a = null;
        Integer b = 1;
        //如果为空的话就执行orElse中的
        Integer result = Optional.ofNullable(a).orElse(b);
        System.out.println(result);
//        synchronized (new Object()) {
//            JvmDemo jvmDemo = new JvmDemo();
//            try {
//                jvmDemo.wait();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

    }
}
