package com.mars.marsbasecore.review01.test;

import java.util.Random;

public class Level {
    public static String easyLevel(int num_oper) {// 简单难度，不带括号
        char oper[] = {'+', '-', '*', '/'};
        String[] s = new String[2 * num_oper + 1];
        StringBuffer sb = new StringBuffer();
        Random r = new Random();
        for (int i = 0; i < 2 * num_oper + 1; i++)
            if (i % 2 == 0)
                s[i] = Integer.toString(r.nextInt(101));// 操作数
            else
                s[i] = Character.toString(oper[r.nextInt(4)]);// 操作符
        for (int i = 0; i < s.length; i++)
            sb.append(s[i]);
        return sb.toString();
    }
}
