package com.mars.marsbasecore.review01.cas;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author wq
 * @date 2021-07-31 16:09
 */
public class AbaDemo {

    static AtomicReference<Integer> atomicReference = new AtomicReference<>(100);

    public static void main(String[] args) {

        new Thread(() -> {
            atomicReference.compareAndSet(100, 101);
            atomicReference.compareAndSet(101, 100);
        }, "t1").start();


        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            atomicReference.compareAndSet(100, 2021);
            System.out.println(Thread.currentThread().getName() + "线程:" + atomicReference.get());

        }, "t2").start();
    }
}
