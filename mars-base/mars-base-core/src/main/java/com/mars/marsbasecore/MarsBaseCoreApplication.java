package com.mars.marsbasecore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsBaseCoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsBaseCoreApplication.class, args);
    }

}
