package com.mars.marsbasecore.jframe;

import javax.swing.*;
import java.awt.*;

/**
 * @author Mars
 * @date 2022-06-18 9:43
 */
public class JFrameWindow extends JFrame {
    public static void main(String[] args) {

        new JFrameWindow("窗口");
    }

    public JFrameWindow(String title) {
        JFrame jf = new JFrame(title);
        //得到窗口的容器
        Container conn = jf.getContentPane();
        //创建一个标签 并设置初始内容
        JLabel L1 = new JLabel("Hello,world!");
        conn.add(L1);
        //设置窗口的属性 窗口位置以及窗口的大小
        jf.setBounds(200, 200, 300, 200);

        //设置窗口可见
        jf.setVisible(true);
        //设置关闭方式 如果不设置的话 似乎关闭窗口之后不会退出程序
        jf.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }
}
