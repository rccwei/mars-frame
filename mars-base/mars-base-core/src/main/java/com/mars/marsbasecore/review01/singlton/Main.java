package com.mars.marsbasecore.review01.singlton;

/**
 * @author wq
 * @date 2021-07-31 14:36
 */
public class Main {
    public static void main(String[] args) {

        //System.out.println(SingltonDemo.getInstance() == SingltonDemo.getInstance());


        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                SingltonDemo.getInstance();
            }, String.valueOf(i)).start();
        }
    }
}
