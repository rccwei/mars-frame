package com.mars.base.upload.controller;


import com.mars.base.upload.utils.QiniuUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;

@RestController
@Slf4j
public class FileUploadController {


    @PostMapping(value = "/upload")
    private String upload(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        if (multipartFile.isEmpty()) {
            throw new RemoteException("请选择文件");
        }
        FileInputStream inputStream = (FileInputStream) multipartFile.getInputStream();
        String key = System.currentTimeMillis() + ".jpg";
        String path = QiniuUtils.uploadQNImg(inputStream, key);
        log.info("path===>" + path);
        return path;
    }

}
