package com.mars.base.upload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsBaseUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsBaseUploadApplication.class, args);
    }

}
