package com.mars.jms.rocketmq.producer;

import com.mars.jms.rocketmq.common.RocketmqConstant;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/4 10:40
 * NameServer 是Broker的管理者
 * Broker主动上报自己的信息
 *
 *
 */
@RestController
public class IndexController {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/send")
    public void send(){
        rocketMQTemplate.convertAndSend("test1","test-message");
    }
}
