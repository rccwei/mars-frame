package com.mars.jms.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsJmsRocketmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsJmsRocketmqApplication.class, args);
    }

}
