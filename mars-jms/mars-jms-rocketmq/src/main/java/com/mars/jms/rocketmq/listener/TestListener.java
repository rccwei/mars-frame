package com.mars.jms.rocketmq.listener;

import com.mars.jms.rocketmq.common.RocketmqConstant;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
@RocketMQMessageListener(topic = RocketmqConstant.TOPIC_TEST, consumerGroup = RocketmqConstant.CONSUMER_GROUP_TEST,
        messageModel = MessageModel.CLUSTERING, consumeMode = ConsumeMode.ORDERLY)
public class TestListener implements RocketMQListener<String> {

    private Logger logger = LoggerFactory.getLogger(TestListener.class);


    @Override
    public void onMessage(String message) {
        logger.info("普通消息接收:" + message);
    }
}
