package com.mars.jms.rocketmq.common;


public class RocketmqConstant {
    public static final String TOPIC_TEST = "topic_test";
    public static final String TOPIC_ORDER = "topic_order";
    public static final String TOPIC_TRANSACTION = "topic_transaction";
    public static final String CONSUMER_GROUP_TEST = "group_test";
    public static final String CONSUMER_ORDER_TEST = "group_order";
    public static final String CONSUMER_TRANSACTION_TEST = "group_transaction";
}
