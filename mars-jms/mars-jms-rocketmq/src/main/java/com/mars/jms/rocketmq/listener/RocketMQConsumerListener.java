package com.mars.jms.rocketmq.listener;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2023/6/4 10:43
 *
 *
 *
 */
@Service
@RocketMQMessageListener(consumerGroup = "test1", topic = "test1")
public class RocketMQConsumerListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {
        System.out.println("消费消息：" + s);
    }
}

