package com.mars.jms.kafka.entity;

import lombok.Data;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/15 19:24
 */
@Data
public class MessageResponse {

    /**
     * 主题
     */
    private String topic;

    /**
     * 消息内容
     */
    private String message;
}
