package com.mars.jms.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsJmsKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsJmsKafkaApplication.class, args);
    }

}
