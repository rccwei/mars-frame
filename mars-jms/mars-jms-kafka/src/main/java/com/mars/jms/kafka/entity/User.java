package com.mars.jms.kafka.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/04/16 15:44
 */
@Data
public class User implements Serializable {

    private String name;

    private Integer age;
}
