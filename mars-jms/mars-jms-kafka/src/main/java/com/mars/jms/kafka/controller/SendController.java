package com.mars.jms.kafka.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 11:39
 */
@RestController
@Slf4j
public class SendController {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;


    /**
     * 同时向两个队列中发送消息
     *
     * @return String
     */
    @GetMapping("/send")
    public String sendMessage() {
        for (int i = 0; i < 1000; i++) {
            kafkaTemplate.send("pre-command-ticket", "测试消息" + i);
        }

        return "success";
    }


}
