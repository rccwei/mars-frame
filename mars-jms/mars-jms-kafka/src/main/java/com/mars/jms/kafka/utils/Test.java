package com.mars.jms.kafka.utils;

import com.alibaba.fastjson.JSONObject;
import com.mars.jms.kafka.entity.User;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/17 14:18
 */
public class Test {
    public static void main(String[] args) {
        // HashMap<String, String> map = new HashMap<>();
        // map.put("xxx","name");
        // map.put("xxx2","name");
        // map.put("xxx3","name");
        //
        // System.out.println(JSONObject.toJSONString(map));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "lisi");
        jsonObject.put("age", 12);

        User user = JSONObject.parseObject(jsonObject.toJSONString(), User.class);
        System.out.println(user);


    }
}
