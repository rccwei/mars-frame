package com.mars.jms.kafka.consumer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 更新用户消费者
 *
 * @author mjy
 * @date 2020-12-17
 */
@Slf4j
@Component
@AllArgsConstructor
public class UpdateUserConsumer {


    @KafkaListener(topics = "pre-command-ticket")
    public void receive(ConsumerRecord<?, ?> consumer) {
        log.info("主题:" + consumer.topic() + " 消费者接收:" + consumer.value());
        try {
            String json = (String) consumer.value();
            System.out.println(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
