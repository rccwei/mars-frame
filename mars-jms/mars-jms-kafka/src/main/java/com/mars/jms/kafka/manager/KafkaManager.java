package com.mars.jms.kafka.manager;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.ListTopicsResult;
import org.apache.kafka.common.KafkaFuture;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ExecutionException;

/**
 * @author wq
 * @version 1.0
 * @date 2021/04/30 14:02
 */
@Component
public class KafkaManager {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootStrapServer;

    @PostConstruct
    private void acquireTopicList() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", bootStrapServer);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        AdminClient adminClient = AdminClient.create(properties);

        ListTopicsResult result = adminClient.listTopics();
        KafkaFuture<Set<String>> names = result.names();
        try {
            System.out.println("Kafka already create Topic list following:");
            for (String topic : names.get()) {
                System.out.println("\t\t" + topic);
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        adminClient.close();
    }


}
