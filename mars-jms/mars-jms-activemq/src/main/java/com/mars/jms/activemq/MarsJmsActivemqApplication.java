package com.mars.jms.activemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsJmsActivemqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsJmsActivemqApplication.class, args);
    }

}
