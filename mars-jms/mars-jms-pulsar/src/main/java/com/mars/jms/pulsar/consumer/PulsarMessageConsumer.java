package com.mars.jms.pulsar.consumer;

import com.mars.jms.pulsar.dto.MessageDto;
import io.github.majusko.pulsar.annotation.PulsarConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 程序猿Mars
 * @date 2023-10-16 15:33
 */
@Slf4j
@Component
public class PulsarMessageConsumer {

    @PulsarConsumer(topic = "bootTopic", clazz = MessageDto.class)
    public void consume(MessageDto message) {
        log.info("消费者接收消息 id:{},content:{}", message.getId(), message.getContent());
    }
}
