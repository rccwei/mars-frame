package com.mars.jms.pulsar.controller;

import com.mars.jms.pulsar.dto.MessageDto;
import io.github.majusko.pulsar.producer.PulsarTemplate;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 程序猿Mars
 * @date 2023-10-16 15:47
 */
@RestController
public class PulsarController {


    @Resource
    private PulsarTemplate<MessageDto> producer;


    @RequestMapping(value = "/sendMessage")
    public void sendMessage() {
        try {
            for (int i = 0; i < 10; i++) {
                MessageDto message = new MessageDto();
                message.setId(i);
                message.setContent("测试消息" + i);
                producer.send("bootTopic", message);
            }

        } catch (PulsarClientException e) {
            e.printStackTrace();
        }

    }

}
