package com.mars.jms.pulsar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsJmsPulsarApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsJmsPulsarApplication.class, args);
    }

}
