package com.mars.jms.pulsar.dto;

import lombok.Data;

/**
 * @author 程序猿Mars
 * @date 2023-10-16 15:31
 */
@Data
public class MessageDto {

    private Integer id;

    private String content;
}
