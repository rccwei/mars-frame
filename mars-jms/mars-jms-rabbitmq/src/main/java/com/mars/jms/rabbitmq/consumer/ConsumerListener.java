package com.mars.jms.rabbitmq.consumer;

import com.mars.jms.rabbitmq.constant.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 10:00
 */
@Component
@Slf4j
public class ConsumerListener {

    @RabbitHandler
    @RabbitListener(queues = Constant.ITEM_QUEUE) //监听队列名为hello的队列
    public void ProcessClient(String msg) {
        log.info("消费端接收到消息：" + msg);
    }
}
