package com.mars.jms.rabbitmq.controller;

import com.mars.jms.rabbitmq.constant.Constant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/01 17:46
 */
@RestController
public class ProducerController {

    @Resource
    private RabbitTemplate rabbitTemplate;

    /**
     * 测试
     */
    @GetMapping("/sendMsg")
    public String sendMsg(String message) {
        /**
         * 发送消息
         * 参数一：交换机名称
         * 参数二：路由key: item.springboot-rabbitmq,符合路由item.#规则即可
         * 参数三：发送的消息
         */
        String key = "mars.helloworld";
        rabbitTemplate.convertAndSend(Constant.ITEM_TOPIC_EXCHANGE, key, message);
        return "success";
    }
}
