package com.mars.jms.rabbitmq.config;

import com.mars.jms.rabbitmq.constant.Constant;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/01 17:43
 */
@Configuration
public class RabbitMQConfig {



    //声明交换机
    @Bean("marsTopicExchange")
    public Exchange topicExchange() {
        return ExchangeBuilder.topicExchange(Constant.ITEM_TOPIC_EXCHANGE).durable(true).build();
    }

    //声明队列
    @Bean("marsQueue")
    public Queue itemQueue() {
        return QueueBuilder.durable(Constant.ITEM_QUEUE).build();
    }


    @Bean
    public Binding itemQueueExchange(@Qualifier("marsQueue") Queue queue,
                                     @Qualifier("marsTopicExchange") Exchange exchange) {
        //绑定队列和交换机
        return BindingBuilder.bind(queue).to(exchange).with("mars.#").noargs();
    }

}
