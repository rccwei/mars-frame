package com.mars.jms.rabbitmq.constant;

public interface Constant {
    //交换机名称
    public static final String ITEM_TOPIC_EXCHANGE = "mars_topic_exchange";
    //队列名称
    public static final String ITEM_QUEUE = "mars_queue";
}
