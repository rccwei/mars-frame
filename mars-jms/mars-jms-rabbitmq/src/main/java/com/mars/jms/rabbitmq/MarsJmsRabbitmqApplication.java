package com.mars.jms.rabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsJmsRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsJmsRabbitmqApplication.class, args);
    }

}
