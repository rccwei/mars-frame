package com.mars.spring.aspect;

import com.alibaba.fastjson.JSONObject;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.data.Stat;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class UserAspect {


    @Autowired
    CuratorFramework curatorFramework;

    @Pointcut("execution(* com.mars.spring.service.impl..*.*(..))")
    public void userAdd() {

    }

    @Around("userAdd()")
    public Object doAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Object result = joinPoint.proceed();
        System.out.println(result);
        // 添加zk节点
        String data = JSONObject.toJSONString(result);
        Stat stat = curatorFramework.setData().forPath("/", data.getBytes());
        System.out.println(stat);
        return result;
    }


}
