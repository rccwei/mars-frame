package com.mars.spring.watch;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mars.spring.entity.User;
import com.mars.spring.utils.SpringUtil;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.*;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ZookeeperWatches {

    private CuratorFramework curatorFramework;


    public ZookeeperWatches(CuratorFramework curatorFramework) {
        this.curatorFramework = curatorFramework;
    }

    public void znodeWatcher() throws Exception {
        NodeCache nodeCache = new NodeCache(curatorFramework, "/");
        nodeCache.start();
        nodeCache.getListenable().addListener(() -> {
            System.out.println("=======监听到节点改变===========");
            String currentData = new String(nodeCache.getCurrentData().getData());

            // 获取数据 删除缓存 保证数据一致性
            if (StringUtils.isNotBlank(currentData)) {
                User user = JSONObject.parseObject(currentData, User.class);
                System.out.println("监听到节点改变数据:" + JSONObject.toJSONString(user));
                try {
                    StringRedisTemplate redisTemplate = SpringUtil.getApplicationContext().getBean(StringRedisTemplate.class);
                    Boolean hasKey = redisTemplate.hasKey("user:id:" + user.getId());
                    if (Objects.nonNull(hasKey) && hasKey) {
                        redisTemplate.delete("user:id" + user.getId());
                        redisTemplate.opsForValue().set("user:id" + user.getId(), currentData);
                    } else {
                        redisTemplate.opsForValue().set("user:id" + user.getId(), currentData);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        System.out.println("节点监听注册完成");
    }


    public void znodeChildrenWatcher() throws Exception {
        PathChildrenCache pathChildrenCache = new PathChildrenCache(curatorFramework, "/app", true);
        pathChildrenCache.start();
        pathChildrenCache.getListenable().addListener(new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                System.out.println("=======节点子节点改变===========");
                String childrenData = new String(event.getData().getData());
                System.out.println("子节点数据：" + childrenData);

            }
        });
        System.out.println("子节点监听注册完成");
    }

}
