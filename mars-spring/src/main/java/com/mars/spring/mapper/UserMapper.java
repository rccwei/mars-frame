package com.mars.spring.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.spring.entity.User;

public interface UserMapper extends BaseMapper<User> {
}
