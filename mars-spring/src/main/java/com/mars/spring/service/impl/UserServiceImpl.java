package com.mars.spring.service.impl;

import com.mars.spring.entity.User;
import com.mars.spring.mapper.UserMapper;
import com.mars.spring.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.UUID;

@Service
public class UserServiceImpl implements IUserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public User add(String name) {
        String id = UUID.randomUUID().toString();
        User user = User.builder().id(id).name(name).build();
        userMapper.insert(user);
        return user;
    }

    @Override
    public User update(String name) {
        User user = userMapper.selectById("2");
        user.setName(name);
        userMapper.updateById(user);
        return user;
    }
}
