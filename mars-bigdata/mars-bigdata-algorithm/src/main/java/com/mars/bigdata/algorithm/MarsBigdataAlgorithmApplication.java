package com.mars.bigdata.algorithm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsBigdataAlgorithmApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsBigdataAlgorithmApplication.class, args);
    }

}
