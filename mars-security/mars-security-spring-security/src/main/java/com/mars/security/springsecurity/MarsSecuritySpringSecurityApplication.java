package com.mars.security.springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSecuritySpringSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSecuritySpringSecurityApplication.class, args);
    }

}
