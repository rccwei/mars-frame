package com.mars.security.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSecurityShiroApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSecurityShiroApplication.class, args);
    }

}
