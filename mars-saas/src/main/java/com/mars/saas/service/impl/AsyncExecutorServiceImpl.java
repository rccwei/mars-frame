//package com.mars.saas.service.impl;
//
//import com.mars.saas.entity.Student;
//import com.mars.saas.mapper.StudentMapper;
//import com.mars.saas.service.AsyncExecutorService;
//import com.mars.saas.service.IStudentService;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.scheduling.annotation.Async;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.Resource;
//import java.util.List;
//import java.util.concurrent.CountDownLatch;
//
///**
// * @author wq
// * @date 2022/8/17 16:21
// */
//@Service
//@Slf4j
//public class AsyncExecutorServiceImpl implements AsyncExecutorService {
//
//
////    @Resource
////    private IStudentService studentService;
////
////
////    @Override
////    public void executeAsync(List<Student> list, CountDownLatch countDownLatch) {
////        // 多线程批量处理入库
////        try {
////            studentService.saveBatch(list, list.size());
////        } finally {
////            // 很关键, 无论上面程序是否异常必须执行countDown,否则await无法释放
////            countDownLatch.countDown();
////        }
////    }
//}
