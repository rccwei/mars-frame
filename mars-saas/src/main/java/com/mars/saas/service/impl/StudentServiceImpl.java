package com.mars.saas.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.mars.saas.entity.Student;
import com.mars.saas.exception.BusinessException;
import com.mars.saas.mapper.StudentMapper;
//import com.mars.saas.service.AsyncExecutorService;
import com.mars.saas.service.IStudentService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 源码字节
 * @version 1.0
 * @date 2021/02/26 17:33
 * Transactional  除了RuntimeException及其子类，其他的类默认不回滚
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


//    @Resource
//    private AsyncExecutorService asyncExecutorService;


    @Autowired
    @Qualifier("asyncServiceExecutor")
    private Executor executor;

    /**
     * 是否回滚  不回滚
     *
     * @param name name
     * @param age  age
     * @throws Exception Exception
     */
    @Override
    @Transactional
    public void add(String name, Integer age) throws Exception {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new Exception("异常抛出");
    }

    /**
     * 是否回滚  回滚
     *
     * @param name
     * @param age
     */
    @Override
    @Transactional
    public void add1(String name, Integer age) {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new BusinessException("异常抛出");
    }

    /**
     * 是否回滚  回滚
     *
     * @param name
     * @param age
     * @throws Exception
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add2(String name, Integer age) throws Exception {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new Exception("异常抛出");
    }

    /**
     * 是否回滚 回滚
     *
     * @param name
     * @param age
     * @throws Exception
     */
    @Override
    @Transactional
    public void add3(String name, Integer age) {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new RuntimeException("异常抛出");
    }

    /**
     * 是否回滚 否
     *
     * @param name
     * @param age
     */
    @Override
    public void add4(String name, Integer age) {
        add5(name, age);
    }

    @Transactional
    public void add5(String name, Integer age) {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new RuntimeException("异常抛出");
    }

    /**
     * 回滚
     *
     * @param name
     * @param age
     */
    @Transactional
    public void add6(String name, Integer age) {
        add7(name, age);
    }


    public void add7(String name, Integer age) {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
        throw new RuntimeException("异常抛出");
    }

    @Override
    public void batchSave() {
        long startTime = System.currentTimeMillis();
        List<Student> list = getStudents();
        super.saveBatch(list); //32816
//        studentMapper.insertBatch(list);//35335
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:" + (endTime - startTime));
    }

    @Override
    public List<Student> getList() {
        return list();
    }

    @Override
    public void tenantMultiThread() {

        // 租户 主线程 传值给子线程 保存
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        executorService.submit(()->{
            List<Student> students = getStudents();
            super.saveBatch(students);
        });
        executorService.shutdown();

//        executor.execute(()-> {
//            super.saveBatch(getStudents());
//        });
//        super.saveBatch(getStudents());



//        new Thread(()-> super.saveBatch(getStudents()),"sub-thread").start();
    }

    @Override
    public void multiThread() {

    }

    @NotNull
    private List<Student> getStudents() {
        List<Student> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            Student student = new Student();
            student.setAge(i);
            student.setName("lisi" + i);
            student.setAddress("成都市双流区");
            student.setEmail("850994281@qq.com");
            student.setMobile("18483678377");
            student.setSign("这是签名这是签名这是签名");
            list.add(student);
        }
        return list;
    }


//    @Override
//    public void multiThread() {
//        List<Student> list = this.getStudents();
//        long startTime = System.currentTimeMillis();
//        List<List<Student>> listList = Lists.partition(list, 1000);
//        CountDownLatch countDownLatch = new CountDownLatch(listList.size());
//        listList.parallelStream().forEach(students -> asyncExecutorService.executeAsync(students, countDownLatch));
//        try {
//            //保证之前的所有的线程都执行完成，才会走下面的；这样就可以在下面拿到所有线程执行完的集合结果
//            countDownLatch.await();
//            long endTime = System.currentTimeMillis();
//            System.out.println("耗时:" + (endTime - startTime));
//        } catch (Exception e) {
//            log.error("阻塞异常:" + e.getMessage());
//        }
//    }
}
