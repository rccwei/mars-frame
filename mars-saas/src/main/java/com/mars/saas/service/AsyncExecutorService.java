//package com.mars.saas.service;
//
//import com.mars.saas.entity.Student;
//
//import java.util.List;
//import java.util.concurrent.CountDownLatch;
//
///**
// * @author wq
// * @date 2022/8/17 16:23
// */
//public interface AsyncExecutorService {
//
//    void executeAsync(List<Student> list, CountDownLatch countDownLatch);
//
//}
