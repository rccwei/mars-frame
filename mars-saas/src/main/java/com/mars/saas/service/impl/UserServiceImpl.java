package com.mars.saas.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.saas.base.PageInfo;
import com.mars.saas.entity.TechType;
import com.mars.saas.entity.User;
import com.mars.saas.mapper.TechTypeMapper;
import com.mars.saas.mapper.UserMapper;
import com.mars.saas.request.UserRequest;
import com.mars.saas.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Resource
    private TechTypeMapper techTypeMapper;


    @Override
    public PageInfo<User> page(UserRequest request) {
        IPage<User> page = new Page<>(request.getPageNumber(), request.getPageSize());
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        List<User> records = baseMapper.selectPage(page, wrapper).getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            return new PageInfo<>(page.getTotal(), records);
        }
        return null;
    }

    @Override
    public void add() {
        try {
            User user = new User().setNickname("lisi").setAvatar("111111").setGender(1).setStatus(1).setOpenId("111");
            baseMapper.insert(user);
        } catch (Exception e) {
            techTypeMapper.insert(new TechType().setId(51).setName("node"));
            throw new RuntimeException(e.getMessage());
        }
    }
}
