package com.mars.saas.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.saas.entity.Student;

import java.util.List;

public interface IStudentService extends IService<Student> {

    void add(String name, Integer age) throws Exception;

    void add1(String name, Integer age);

    void add2(String name, Integer age) throws Exception;

    void add3(String name, Integer age);

    void add4(String name, Integer age);

    void add6(String name, Integer age);

    void batchSave();

    List<Student> getList();

    void tenantMultiThread();

    void multiThread();
}
