package com.mars.saas.service;


import com.mars.saas.base.PageInfo;
import com.mars.saas.entity.User;
import com.mars.saas.request.UserRequest;

public interface IUserService {

    /**
     * 分页
     *
     * @param request 请求参数
     * @return PageInfo<User>
     */
    PageInfo<User> page(UserRequest request);

    /**
     * 获取用户
     *
     * @return 用户列表
     */
    void add();
}
