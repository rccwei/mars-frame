package com.mars.saas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wq
 * @date 2022/1/17 10:59
 */
@Controller
public class IndexController {

    @RequestMapping("/{page}")
    public String index(@PathVariable String page) {
        return page;
    }
}
