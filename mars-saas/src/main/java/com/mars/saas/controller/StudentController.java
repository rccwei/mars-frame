package com.mars.saas.controller;

import com.mars.saas.entity.Student;
import com.mars.saas.service.IStudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author wq
 * @date 2022/8/17 10:31
 */
@RestController
public class StudentController {

    @Resource
    private IStudentService studentService;

    @GetMapping("/saveBatch")
    public String saveBatch() {
        studentService.batchSave();
        return "success";
    }

    @GetMapping("/list")
    public List<Student> list() {
        return studentService.getList();
    }

    @GetMapping("/tenantMultiThread")
    public String tenantMultiThread() {
         studentService.tenantMultiThread();
         return "success";
    }



    @GetMapping("/saveBatchMultiThread")
    public String saveBatchMultiThread() {
        studentService.multiThread();
        return "success";
    }
}
