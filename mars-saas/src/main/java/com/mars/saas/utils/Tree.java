package com.mars.saas.utils;

import com.mars.saas.entity.Node;
import com.mars.saas.entity.resp.TreeResponse;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2022/1/15 17:33
 */
public class Tree {

    /**
     * 构建树形结构
     *
     * @param nodes nodes
     * @return List<Node>
     */
    public static List<TreeResponse> buildTree(List<Node> nodes) {
        return nodes.stream().peek(node -> node.setChildren(nodes.stream().filter(s -> s.getPid() != 0).
                collect(Collectors.groupingBy(Node::getPid)).get(node.getId()))).
                collect(Collectors.toList()).stream().filter(node -> node.getPid() == 0).collect(Collectors.toList()).stream().map(x -> {
            TreeResponse treeResponse = new TreeResponse();
            BeanUtils.copyProperties(x, treeResponse);
            return treeResponse;
        }).collect(Collectors.toList());
    }
}
