package com.mars.saas.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @date 2022/1/15 16:34
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Node {

    private int id;

    private int pid;

    private String label;

    private List<Node> children = new ArrayList<>();


    public Node(int id, int pid, String label) {
        this.id = id;
        this.pid = pid;
        this.label = label;
    }
}
