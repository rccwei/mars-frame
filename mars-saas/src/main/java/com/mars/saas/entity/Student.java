package com.mars.saas.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author wq
 * @date 2022/1/18 11:12
 */
@Data
@TableName("student")
public class Student {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer age;

    private String address;

    private String sign;

    private String mobile;

    private String email;

    private Date createTime;

    private Date updateTime;

    private Integer tenantId;


}
