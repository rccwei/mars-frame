package com.mars.saas.interceptor;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mars.saas.context.TenantRequestContext;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wq
 * @date 2022/11/11 16:11
 */
public class AuthInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tenantId = request.getHeader("tenant_id");
        if (StringUtils.isNotBlank(tenantId)) {
            TenantRequestContext.setTenantLocal(tenantId);
            System.out.println("当前租户ID:" + tenantId);
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

}
