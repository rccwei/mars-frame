package com.mars.saas;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan("com.mars.saas.mapper")
@EnableAsync
public class MarsSaasApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSaasApplication.class, args);
    }

}
