package com.mars.saas.base;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 分页
 *
 * @author : wq
 * @date : 2020-06-18
 **/
public class PageInfo<T> {

    /**
     * 总的记录数
     */
    private Integer total;
    /**
     * 数据集合
     */
    private List<T> list;

    public PageInfo() {

    }

    public PageInfo(Long total, List<T> list) {
        this.total = total.intValue();
        this.list = list;
    }

    /**
     * 转换参数
     *
     * @param page 分页
     * @param <T>  T
     * @return PageInfo
     */
    public static <T> PageInfo<T> buildPage(IPage<T> page) {
        PageInfo<T> pageInfo = new PageInfo<>();
        pageInfo.setList(page.getRecords());
        pageInfo.setTotal(page.getTotal());
        return pageInfo;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total.intValue();
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
