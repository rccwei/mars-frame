package com.mars.saas.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author wq
 * @Date 2020/4/26
 **/
@Data
public class BasePageRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer pageNumber = 1;

    private Integer pageSize = 10;

}
