package com.mars.saas.exception;

/**
 * @author wq
 * @date 2022/8/17 10:54
 */
public class BusinessException extends RuntimeException {
    private static final long serialVersionUID = 9213584003139969215L;

    public BusinessException(String message) {
        super(message);
    }
}
