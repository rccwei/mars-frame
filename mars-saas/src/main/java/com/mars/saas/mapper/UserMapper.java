package com.mars.saas.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.saas.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {


}
