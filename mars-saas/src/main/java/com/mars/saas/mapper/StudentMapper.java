package com.mars.saas.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.saas.entity.Student;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface StudentMapper extends BaseMapper<Student> {


    @Insert("<script>" +
            "INSERT INTO student(address,name,sign,mobile,email,age)VALUES" +
            "<foreach collection='list' item='student'   separator=','> " +
            "(#{student.address},#{student.name},#{student.sign},#{student.mobile},#{student.email},#{student.age})" +
            "</foreach> " +
            "</script>")
    void insertBatch(@Param("list") List<Student> list);

}
