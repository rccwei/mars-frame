package com.mars.saas.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.saas.entity.TechType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TechTypeMapper extends BaseMapper<TechType> {


}
