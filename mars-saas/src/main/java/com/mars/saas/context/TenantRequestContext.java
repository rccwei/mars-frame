package com.mars.saas.context;

/**
 * @author wq
 * @date 2022/11/11 16:10
 */
public class TenantRequestContext {

    private static ThreadLocal<String> tenantLocal = new ThreadLocal<>();

    public static void setTenantLocal(String tenantId) {
        tenantLocal.set(tenantId);
    }

    public static String getTenantLocal() {
        return tenantLocal.get();
    }

    public static void remove() {
        tenantLocal.remove();
    }
}
