package com.mars.cloud.dubbo.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCloudDubboConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCloudDubboConsumerApplication.class, args);
    }

}
