package com.mars.cloud.dubbo.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCloudDubboProducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCloudDubboProducerApplication.class, args);
    }

}
