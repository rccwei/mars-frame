package com.mars.springcloud.consumer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 17:01
 */
@RestController
public class UserController {

    @GetMapping("/getUsername")
    public String getUsername() {
        return "wangqiang";
    }
}
