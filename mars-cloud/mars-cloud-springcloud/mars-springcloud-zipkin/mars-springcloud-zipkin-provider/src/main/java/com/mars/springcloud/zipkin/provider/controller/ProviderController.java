package com.mars.springcloud.zipkin.provider.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Auther: wq
 * @Date: 2021/2/26 15:08
 * @Description:
 */
@RestController
@AllArgsConstructor
public class ProviderController {

    private static final Logger LOG = Logger.getLogger(ProviderController.class.getName());

    private final RestTemplate restTemplate;

    @RequestMapping("/provider")
    public String callHome() {
        LOG.log(Level.INFO, "请求 service-provider");
        LOG.log(Level.INFO, "远程调用：http://localhost:9015/consumerInfo");
        return restTemplate.getForObject("http://localhost:9015/consumerInfo", String.class);
    }

    @RequestMapping("/providerInfo")
    public String info() {
        LOG.log(Level.INFO, "请求 service-provider ");
        return "i'm service-provider";
    }

}
