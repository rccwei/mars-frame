package com.mars.springcloud.zipkin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zipkin.server.EnableZipkinServer;

/**
 * Zipkin是 Twitter 的一个 开源项目 ，它基于 Google Dapper实现。我们可以使用它来收集各个 服务器 上请求链路的跟踪数据，
 * 并通过它提供的 REST API 接口来辅助我们查询跟踪数据以实现对分布式系统的监控程序，
 * 从而及时地发现系统中出现的延迟升高问题并找出系统性能瓶颈的根源。除了面向 开发 的API接口之外，它也提供了方便的 UI 组件来帮助
 * 我们直观的搜索跟踪信息和分析请求链路明细，比如：可以查询某段时间内各用户请求的处理时间等。
 */
@SpringBootApplication
@EnableZipkinServer
public class MarsSpringcloudZipkinServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringcloudZipkinServerApplication.class, args);
    }

}
