package com.mars.springcloud.zipkin.consumer.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @Auther: wq
 * @Date: 2021/2/26 15:16
 * @Description:
 */
@RestController
@AllArgsConstructor
public class ConsumerController {

    private static final Logger LOG = Logger.getLogger(ConsumerController.class.getName());

    private final RestTemplate restTemplate;

    @RequestMapping("/consumer")
    public String callHome() {
        LOG.log(Level.INFO, "请求 service-consumer");
        LOG.log(Level.INFO, "远程调用：http://localhost:9200/provider");
        return restTemplate.getForObject("http://localhost:9200/provider", String.class);
    }

    @RequestMapping("/consumerInfo")
    public String info() {
        LOG.log(Level.INFO, "请求 service-consumer");
        return "i'm service-consumer";
    }

}
