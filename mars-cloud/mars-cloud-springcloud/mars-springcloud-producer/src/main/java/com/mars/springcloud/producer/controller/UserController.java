package com.mars.springcloud.producer.controller;

import com.mars.springcloud.producer.feign.RemoteUserFeign;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 17:02
 */
@RestController
public class UserController {

    @Resource
    private RemoteUserFeign remoteUserFeign;

    @GetMapping("/getUsername")
    public String getUsername() {
        return remoteUserFeign.getUsername();
    }
}
