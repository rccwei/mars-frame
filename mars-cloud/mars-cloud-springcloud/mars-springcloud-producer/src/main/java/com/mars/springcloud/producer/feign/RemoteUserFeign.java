package com.mars.springcloud.producer.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "springcloud-consumer")
public interface RemoteUserFeign {

    @GetMapping("/getUsername")
    String getUsername();
}
