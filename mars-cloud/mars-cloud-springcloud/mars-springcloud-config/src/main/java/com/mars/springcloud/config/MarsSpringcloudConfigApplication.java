package com.mars.springcloud.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
//@EnableConfigServer
@EnableDiscoveryClient
@EnableEurekaClient
public class MarsSpringcloudConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringcloudConfigApplication.class, args);
    }

}
