package com.mars.springcloud.rest.template.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 11:40
 */
@RestController
@AllArgsConstructor
public class IndexController {

    /**
     * springcloud 中有两种调用方式：restTemplate,fegin(springcloud)
     * restTemplate 是spring 提供，默认已经整合了 ribbon 负载均衡器
     * restTemplate 底层就是用的httpclient
     */
    private final RestTemplate restTemplate;

    @RequestMapping("/index")
    public String index() {
        //两种调用，一种是服务别名，另一种直接调用
        //String url = "http://127.0.0.1:9009/getUsername";
        String url = "http://springcloud-consumer/getUsername";
        return restTemplate.getForObject(url, String.class);
    }
}
