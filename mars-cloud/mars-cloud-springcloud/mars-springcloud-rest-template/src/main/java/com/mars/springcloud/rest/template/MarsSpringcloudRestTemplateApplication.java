package com.mars.springcloud.rest.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class MarsSpringcloudRestTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringcloudRestTemplateApplication.class, args);
    }


    /**
     * @return RestTemplate
     * LoadBalanced //该注解可以让该resttemplate在请求时拥有客户端负载均衡的能力
     */
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
