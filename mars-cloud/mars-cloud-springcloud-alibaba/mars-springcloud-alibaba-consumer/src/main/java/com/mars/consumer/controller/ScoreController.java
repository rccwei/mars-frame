package com.mars.consumer.controller;

import com.mars.consumer.service.IScoreService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 11:02
 */
@AllArgsConstructor
@RestController
@RequestMapping("/score")
public class ScoreController {

    final IScoreService scoreService;

    @GetMapping("/add")
    public void add(Integer userId, Integer score) {
        scoreService.add(userId, score);
    }
}
