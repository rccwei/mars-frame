package com.mars.consumer.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.consumer.entity.Score;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScoreMapper extends BaseMapper<Score> {


}
