package com.mars.consumer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.consumer.entity.Score;
import com.mars.consumer.mapper.ScoreMapper;
import com.mars.consumer.service.IScoreService;
import org.springframework.stereotype.Service;


/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
public class ScoreServiceImpl extends ServiceImpl<ScoreMapper, Score> implements IScoreService {


    @Override
    public void add(Integer userId, Integer score) {
        Score score1 = new Score();
        score1.setUserId(userId);
        score1.setScore(score);
        save(score1);
    }
}
