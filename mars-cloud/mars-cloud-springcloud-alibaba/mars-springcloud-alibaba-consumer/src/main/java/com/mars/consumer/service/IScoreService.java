package com.mars.consumer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.consumer.entity.Score;

public interface IScoreService extends IService<Score> {


    /**
     * 添加得分
     *
     * @param userId 用户ID
     * @param score  得分
     */
    void add(Integer userId, Integer score);

}
