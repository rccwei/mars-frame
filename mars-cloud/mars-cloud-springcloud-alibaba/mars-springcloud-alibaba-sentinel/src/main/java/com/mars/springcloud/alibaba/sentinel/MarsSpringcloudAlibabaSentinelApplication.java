package com.mars.springcloud.alibaba.sentinel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSpringcloudAlibabaSentinelApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringcloudAlibabaSentinelApplication.class, args);
    }

}
