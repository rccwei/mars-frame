package com.mars.producer.controller;

import com.mars.producer.request.UserRequest;
import com.mars.producer.result.R;
import com.mars.producer.utils.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/07 11:18
 */
@RestController
@RequestMapping("/producer")
@Slf4j
public class LoginController {

    HashMap<String, Integer> userMap = new HashMap<String, Integer>() {
        {
            put("18483678377", 10001);
            put("18483678378", 10002);
            put("18483678379", 10003);

        }
    };

    /**
     * @param request
     * @return
     */
    @PostMapping("/login")
    public R<String> login(@RequestBody UserRequest request) {
        log.info("用户名称:{},用户验证码:{}", request.getMobile(), request.getCode());
        //类似查询数据库
        Integer userId = userMap.get(request.getMobile());
        String sign = JwtUtil.sign(String.valueOf(userId));
        System.out.println("签名信息:" + sign);
        return R.success(sign);
    }
}
