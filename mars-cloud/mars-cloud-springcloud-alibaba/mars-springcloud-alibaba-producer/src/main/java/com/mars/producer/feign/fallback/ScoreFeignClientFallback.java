package com.mars.producer.feign.fallback;

import com.mars.producer.feign.ScoreFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author wq
 * @date 2021-08-11 21:31
 */
@Component
@Slf4j
public class ScoreFeignClientFallback implements ScoreFeignClient {

    @Override
    public void add(Integer userId, Integer score) {
        log.error("获取用户信息远程调用失败");
    }
}
