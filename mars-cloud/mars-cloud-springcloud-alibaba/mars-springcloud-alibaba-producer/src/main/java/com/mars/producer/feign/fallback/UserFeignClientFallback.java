package com.mars.producer.feign.fallback;

import com.mars.producer.feign.UserFeignClient;
import org.springframework.stereotype.Component;

/**
 * @author wq
 * @date 2021-08-11 21:31
 */
@Component
public class UserFeignClientFallback implements UserFeignClient {
    @Override
    public String getUsername() {
        System.out.println("获取用户信息远程调用失败");
        return "false";
    }
}
