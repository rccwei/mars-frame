package com.mars.producer.feign;

import com.mars.producer.feign.fallback.UserFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(contextId = "userFeignClient", value = "mars-consumer",
        fallback = UserFeignClientFallback.class)
public interface UserFeignClient {

    @GetMapping("/getUsername")
    String getUsername();


}
