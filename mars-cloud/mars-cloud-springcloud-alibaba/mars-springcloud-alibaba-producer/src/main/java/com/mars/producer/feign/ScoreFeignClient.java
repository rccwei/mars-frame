package com.mars.producer.feign;

import com.mars.producer.feign.fallback.ScoreFeignClientFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(contextId = "scoreFeignClient", value = "mars-consumer",
        fallback = ScoreFeignClientFallback.class)
public interface ScoreFeignClient {

    @GetMapping("/score/add")
    void add(@RequestParam Integer userId, @RequestParam Integer score);
}
