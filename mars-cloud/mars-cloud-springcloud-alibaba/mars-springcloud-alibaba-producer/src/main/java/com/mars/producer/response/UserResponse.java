package com.mars.producer.response;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/07 11:06
 */
@Data
public class UserResponse implements Serializable {

    private Integer userId;

    private String username;
}
