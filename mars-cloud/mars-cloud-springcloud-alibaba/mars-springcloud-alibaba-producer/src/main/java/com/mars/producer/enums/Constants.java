package com.mars.producer.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum Constants {
    AES_DATA("aesData", "指定参数"),

    AES("AES_KEY", "36CAA1C88F7F8D1D"),

    IV("AES_IV", "31129048100F0494");
    private String code;
    private String desc;

}
