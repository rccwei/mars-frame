package com.mars.producer.controller;

import com.alibaba.fastjson.JSONObject;
import com.mars.producer.result.R;
import com.mars.producer.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 11:01
 */
@RestController
@RequestMapping("/producer")
@Slf4j
public class UserController extends BasicController {


    @Resource
    private IUserService userService;


    /**
     * 添加
     *
     * @return R
     */
    @GetMapping("/add")
    private R<Void> add() {
        userService.add();
        return R.success();
    }

    @GetMapping("/info")
    public R<String> info() {
        Integer userId = getUserId();
        System.out.println("用户ID:" + userId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId", userId);
        jsonObject.put("username", "wq");
        return R.success(jsonObject.toJSONString());
    }

    @PostMapping("/info1")
    public R<String> info1() {
        Integer userId = getUserId();
        System.out.println("用户ID:" + userId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId", userId);
        jsonObject.put("username", "wq");
        return R.success(jsonObject.toJSONString());
    }
}
