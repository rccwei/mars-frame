package com.mars.producer.request;

import com.mars.producer.result.BaseResult;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/07 10:50
 */
@Data
public class UserRequest extends BaseResult implements Serializable {

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 验证码
     */
    private String code;
}
