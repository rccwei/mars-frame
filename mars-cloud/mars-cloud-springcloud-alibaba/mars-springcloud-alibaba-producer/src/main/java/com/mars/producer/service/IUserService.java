package com.mars.producer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.producer.entity.User;

import java.util.List;

public interface IUserService extends IService<User> {


    /**
     * 添加用户
     */
    void add();


    /**
     * 查询用户
     *
     * @return List<User>
     */
    List<User> getList();
}
