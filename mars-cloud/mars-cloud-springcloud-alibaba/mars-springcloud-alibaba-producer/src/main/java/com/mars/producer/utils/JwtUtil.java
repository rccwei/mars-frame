package com.mars.producer.utils;


import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.JWTVerifier;

import java.util.Date;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/04 16:16
 */
public class JwtUtil {

    /**
     * 过期时间一个月
     */
    private static final long EXPIRE_TIME = 30 * 24 * 60 * 60;
    /**
     * jwt 密钥
     */
    private static final String SECRET = "d6f7c121-d2e9-436d-8d9c-952de82b38aa";

    /**
     * 生成签名，五分钟后过期
     *
     * @param userId
     * @return
     */
    public static String sign(String userId) {
        try {
            Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME * 1000);
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            return JWT.create()
                    // 将 userid 保存到 token 里面
                    .withAudience(userId)
                    // 五分钟后token过期
                    .withExpiresAt(date)
                    // token 的密钥
                    .sign(algorithm);
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
//        String sign = sign("12345");
//        System.out.println("签名数据:" + sign);
//        System.out.println("用户ID:" + getUserId(sign));
        System.out.println(30 * 24 * 60 * 60);
    }

    /**
     * 根据token获取userId
     *
     * @param token
     * @return
     */
    public static String getUserId(String token) {
        try {
            return JWT.decode(token).getAudience().get(0);
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 校验token
     *
     * @param token
     * @return
     */
    public static boolean checkSign(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTVerifier verifier = JWT.require(algorithm)
                    .build();
            verifier.verify(token);
            return true;
        } catch (JWTVerificationException exception) {
            throw new RuntimeException("token 无效，请重新获取");
        }
    }
}
