package com.mars.producer.controller;

import com.alibaba.fastjson.JSONObject;
import com.mars.producer.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 11:01
 */
@RestController
@RequestMapping("/producer")
@Slf4j
public class IndexController extends BasicController {


    @GetMapping("/index")
    public R index() {
        Integer userId = getUserId();
        System.out.println("用户ID:" + userId);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userId", userId);
        jsonObject.put("username", "wq");
        return R.success(jsonObject);
    }
}
