package com.mars.producer.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.exceptions.JWTDecodeException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

/**
 * @author mjy
 * @date 2020-08-07
 */
@Slf4j
public class BasicController extends AbstractBasicController {

    @Override
    public Integer getUserId() {
        try {
            return Integer.parseInt(JWT.decode(getToken()).getAudience().get(0));
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 获取token
     *
     * @return token
     */
    public String getToken() {
        String authorization = request.getHeader("token");
        if (StringUtils.isEmpty(authorization)) {
            return request.getParameter("token");
        }
        return authorization;
    }
}
