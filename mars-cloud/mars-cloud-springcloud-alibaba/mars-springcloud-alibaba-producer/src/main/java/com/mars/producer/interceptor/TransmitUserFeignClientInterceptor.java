//package com.mars.producer.interceptor;
//
//import com.alibaba.fastjson.JSON;
//import com.mars.producer.context.UserContext;
//import com.mars.producer.entity.User;
//import feign.RequestInterceptor;
//import feign.RequestTemplate;
//import lombok.extern.slf4j.Slf4j;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//
///**
// * @author wq
// * @date 2022/1/27 15:50
// */
//@Slf4j
//public class TransmitUserFeignClientInterceptor implements RequestInterceptor {
//    @Override
//    public void apply(RequestTemplate requestTemplate) {
//        //从应用上下文中取出user信息，放入Feign的请求头中
//        User user = UserContext.getUser();
//        if (user != null) {
//            try {
//                String userJson = JSON.toJSONString(user);
//                requestTemplate.header("KEY_USER_IN_HTTP_HEADER", URLDecoder.decode(userJson, "UTF-8"));
//            } catch (UnsupportedEncodingException e) {
//                log.error("用户信息设置错误", e);
//            }
//        }
//    }
//}
