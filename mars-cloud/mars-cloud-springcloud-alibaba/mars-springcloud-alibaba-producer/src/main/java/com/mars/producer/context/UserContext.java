package com.mars.producer.context;

import com.mars.producer.entity.User;

/**
 * @author wq
 * @date 2022/1/27 15:51
 */
public class UserContext {

    //此处线程变量传递的，可以是实体，也可以是某一个变量
    private static final ThreadLocal<User> USER_INFO = new ThreadLocal<>();

    public UserContext() {
    }

    public static User getUser() {
        return USER_INFO.get();
    }

    public static void setUser(User user) {
        USER_INFO.set(user);
    }
}
