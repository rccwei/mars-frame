package com.mars.producer.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.producer.entity.User;
import com.mars.producer.feign.ScoreFeignClient;
import com.mars.producer.mapper.UserMapper;
import com.mars.producer.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Resource
    private ScoreFeignClient scoreFeignClient;

    @Override
    public void add() {
        User user = new User();
        user.setName("mars");
        user.setAge(22);
        save(user);
        scoreFeignClient.add(user.getId(), 100);
    }

    @Override
    public List<User> getList() {
        return list();
    }
}
