package com.mars.producer.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/07 16:23
 */
public class SecrectUtils {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String time = System.currentTimeMillis() + "";
        String str = "app_id=8c87417e77a894bf10c010fe2e5e89bd&game_code=INDEX&third_uid=11882&third_username=az&timestamp=1623225164401188174fb452d1b676f7ced6597850e67";
        System.out.println(str);
        String sign = stringToMD5(str);
        System.out.println(sign);
        String resultOri = "app_id=8c87417e77a894bf10c010fe2e5e89bd&game_code=INDEX&third_uid=11882&third_username=az&timestamp=" + "1623225164401";
        String result ="http://account-api.yilewan.com/account/thirdParty/gameLogin/thirdPlay?"+ resultOri + "&sign=" + sign;
        System.out.println(result);

//        String sign = (stringToMD5("app_id=8c87417e77a894bf10c010fe2e5e89bd&game_code=INDEX&third_uid=10135&third_username=VPN侯OK哦莫lo&timestamp=1623065465549188174fb452d1b676f7ced6597850e67"));
//
//        String resultOri = "app_id=8c87417e77a894bf10c010fe2e5e89bd&game_code=INDEX&third_uid=10135&third_username=VPN侯OK哦莫lo&timestamp=" + time;
//        String result = resultOri + "&sign=" + sign;
//        System.out.println(result);
    }

    public static String stringToMD5(String plainText) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
}
