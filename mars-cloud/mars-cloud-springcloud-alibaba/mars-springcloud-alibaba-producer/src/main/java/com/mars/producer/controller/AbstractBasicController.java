package com.mars.producer.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 基础controller
 *
 * @author wq
 * @date 2019/12/6
 **/
@Slf4j
public abstract class AbstractBasicController {


    public HttpServletRequest request;

    public HttpServletResponse response;

    /**
     * 封装参数 (每次执行方法前都会先调用该方法封装)
     *
     * @param request  response
     * @param response response
     */
    @ModelAttribute
    public void getReqAndRes(HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
    }


    /**
     * 获取当前用户ID
     *
     * @return getUserId
     */
    public abstract Integer getUserId();


}
