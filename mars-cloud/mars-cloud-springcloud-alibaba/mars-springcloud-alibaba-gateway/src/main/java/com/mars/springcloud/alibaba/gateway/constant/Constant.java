package com.mars.springcloud.alibaba.gateway.constant;

public interface Constant {
    /**
     * 请求token
     */
    String ACCESS_TOKEN = "token";


    String POST = "POST";

    String GET = "GET";
}
