package com.mars.springcloud.alibaba.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class MarsSpringcloudAlibabaGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringcloudAlibabaGatewayApplication.class, args);
    }

}
