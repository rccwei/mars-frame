package com.mars.springcloud.alibaba.gateway.utils;

import com.alibaba.fastjson.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * @program: AESUtils
 * @description: AES工具类
 * @author: wq
 * @create: 2021-06-05 13:53
 **/
public class AESUtils {

    private static final String SECRECT_KEY = "338027690f214377bb97412b5d976344";

    /**
     * 加密参数
     *
     * @param src 字符串
     * @return String
     * @throws Exception e
     */
    public static String encrypt(String src) throws Exception {

        byte[] raw = SECRECT_KEY.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(src.getBytes());
        return byte2hex(encrypted);
    }

    public static void main(String[] args) throws Exception {
//        JSONObject jsonpObject = new JSONObject();
//        jsonpObject.put("username", "wq");
//        jsonpObject.put("password", "12345");
//        String encrypt = encrypt(jsonpObject.toJSONString());
//        System.out.println(encrypt);
        System.out.println(decrypt("38034A1DE49A772486CDBFADB116800C7011101612EB8FC8082DFB6E4126376B"));
        //System.out.println(UUID.randomUUID().toString());
    }

    /**
     * 解密参数
     *
     * @param data 参数
     * @return String
     * @throws Exception e
     */
    public static String decrypt(String data) throws Exception {

        byte[] raw = SECRECT_KEY.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] encrypted1 = hex2byte(data);
        byte[] original = cipher.doFinal(encrypted1);
        return new String(original);
    }


    private static byte[] hex2byte(String strhex) {
        if (strhex == null) {
            return null;
        }
        int l = strhex.length();
        if (l % 2 == 1) {
            return null;
        }
        byte[] b = new byte[l / 2];
        for (int i = 0; i != l / 2; i++) {
            b[i] = (byte) Integer.parseInt(strhex.substring(i * 2, i * 2 + 2),
                    16);
        }
        return b;
    }

    private static String byte2hex(byte[] b) {
        StringBuilder hs = new StringBuilder();
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1) {
                hs.append("0").append(stmp);
            } else {
                hs.append(stmp);
            }
        }
        return hs.toString().toUpperCase();
    }

}
