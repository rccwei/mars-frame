package com.mars.marscloudregisterclient.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class CloudAutoConfiguration {

    @Bean
    public CloudConfig cloudConfig() {
        return new CloudConfig();
    }

}
