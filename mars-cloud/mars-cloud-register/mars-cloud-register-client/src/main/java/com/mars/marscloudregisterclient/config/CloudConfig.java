package com.mars.marscloudregisterclient.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@EnableConfigurationProperties(value = {CloudConfig.class})
@ConfigurationProperties(prefix = "mars.cloud")
public class CloudConfig {
    /**
     * 服务的名称
     */
    private String serviceName;
    /**
     * 服务注册中心地址
     */
    private String serverRegisterUrl;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServerRegisterUrl() {
        return serverRegisterUrl;
    }

    public void setServerRegisterUrl(String serverRegisterUrl) {
        this.serverRegisterUrl = serverRegisterUrl;
    }
}
