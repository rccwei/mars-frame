package com.mars.marscloudregisterclient.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * service Renewal
 */
@Service
public class RenewalService {

    private static final Log log = LogFactory.getLog(RenewalService.class);

    private final ScheduledExecutorService scheduledService = Executors.newSingleThreadScheduledExecutor();

    @Resource
    private RegisterService registerService;

    /**
     * 客户端服务续约线程什么时候启动？
     * 服务注册完成之后
     */
    public void start() {
        log.info("start heart beat thread...");
        scheduledService.scheduleAtFixedRate(new ServiceRenewalThread(), 3, 3, TimeUnit.SECONDS);
    }


    /**
     * 服务续约线程
     */
    class ServiceRenewalThread implements Runnable {
        @Override
        public void run() {
            registerService.renewal();
        }
    }
}