package com.mars.marscloudregisterclient.callback;

import com.mars.marscloudregisterclient.service.RegisterService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class CloseCallbackImpl implements ApplicationListener<ContextClosedEvent> {
    private static final Log log = LogFactory.getLog(CloseCallbackImpl.class);

    @Resource
    private RegisterService registerService;

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        log.info("service client stop");
        registerService.remove();
    }

}