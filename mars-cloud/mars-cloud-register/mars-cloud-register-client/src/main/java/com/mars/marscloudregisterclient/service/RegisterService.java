package com.mars.marscloudregisterclient.service;

import com.alibaba.fastjson.JSONObject;
import com.mars.marscloudregisterclient.config.CloudConfig;
import com.mars.marscloudregisterclient.config.ServerConfig;
import com.mars.marscloudregisterclient.utils.HttpClientUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RegisterService {

    private static final Log log = LogFactory.getLog(RegisterService.class);

    ExecutorService executorService = Executors.newSingleThreadScheduledExecutor();


    @Resource
    private ServerConfig serverConfig;

    @Resource
    private CloudConfig cloudConfig;

    @Resource
    @Lazy
    private RenewalService serviceRenewal;

    public void register() {
        JSONObject data = requestParameter();
        executorService.submit(() -> {
            JSONObject result = HttpClientUtils.httpPost(cloudConfig.getServerRegisterUrl() + "/register", data);
            log.info("service register result：" + result);
            if (Objects.nonNull(result) && result.getInteger("code").equals(200)) {
                serviceRenewal.start();
            } else {
                //retry todo
                log.error(data.get("serviceName") + " register fail");
            }
        });
    }

    public void remove() {
        JSONObject data = requestParameter();
        JSONObject result = HttpClientUtils.httpPost(cloudConfig.getServerRegisterUrl() + "/remove", data);
        log.info("服务下线的结果：" + result);
    }

    public JSONObject requestParameter() {
        return requestParameter(3000L);
    }

    public JSONObject requestParameter(Long renewalDuration) {
        String appHostAndPort = serverConfig.getAppHostAndPort();
        String serviceName = cloudConfig.getServiceName();
        JSONObject data = new JSONObject();
        data.put("serviceName", serviceName);
        data.put("serviceAddress", appHostAndPort);
        if (renewalDuration != null) {
            data.put("renewalDuration", renewalDuration);
        }
        return data;
    }


    /**
     * 发送rest请求达到注册中心  服务续约
     */
    public void renewal() {
        JSONObject data = requestParameter(5000L);
        JSONObject result = HttpClientUtils.httpPost(cloudConfig.getServerRegisterUrl() + "/mayikt/renewal", data);
        log.info("服务续约结果：" + result);
    }
}
