package com.mars.marscloudregisterclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCloudRegisterClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCloudRegisterClientApplication.class, args);
    }

}
