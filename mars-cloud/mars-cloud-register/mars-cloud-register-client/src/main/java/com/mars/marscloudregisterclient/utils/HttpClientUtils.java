package com.mars.marscloudregisterclient.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class HttpClientUtils {

    private static final Log log = LogFactory.getLog(HttpClientUtils.class);
    private static final int SOCKET_TIMEOUT = 5000;
    private static final int CONNECT_TIMEOUT = 5000;
    private static final RequestConfig requestConfig;

    static {
        requestConfig = RequestConfig.custom().
                setSocketTimeout(SOCKET_TIMEOUT).
                setConnectTimeout(CONNECT_TIMEOUT).build();
    }

    /**
     * send post request
     *
     * @param url       url地址
     * @param jsonParam 参数
     * @return
     */
    public static JSONObject httpPost(String url, JSONObject jsonParam) {
        JSONObject jsonResult = null;
        HttpPost httpPost = new HttpPost(url);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        httpPost.setConfig(requestConfig);
        try {
            if (null != jsonParam) {
                StringEntity entity = new StringEntity(jsonParam.toString(), "utf-8");
                entity.setContentEncoding("UTF-8");
                entity.setContentType("application/json");
                httpPost.setEntity(entity);
            }
            CloseableHttpResponse result = httpClient.execute(httpPost);
            if (result.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String str = "";
                try {
                    str = EntityUtils.toString(result.getEntity(), "utf-8");
                    jsonResult = JSONObject.parseObject(str);
                } catch (Exception e) {
                    log.error("request server error：", e);
                }
            }
        } catch (IOException e) {
            log.error("request server error：", e);
        } finally {
            httpPost.releaseConnection();
        }
        return jsonResult;
    }

    public static void main(String[] args) {
        JSONObject data = new JSONObject();
        data.put("serviceName", "mars");
        data.put("serviceAddress", "127.0.0.1:8080");
        data.put("renewalDuration", 200L);
        JSONObject result = HttpClientUtils.httpPost("http://127.0.0.1:8848/register", data);
        System.out.println(result);
    }

}
