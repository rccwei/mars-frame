package com.mars.marscloudregisterclient.callback;


import com.mars.marscloudregisterclient.service.RegisterService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class StartCallbackImpl implements ApplicationRunner {

    @Resource
    private RegisterService registerService;

    /**
     * springboot项目启动时 完成 走该回调  实现对该服务做服务注册
     * 将ip和端口  注册存放在我们 注册中心上
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) {
        registerService.register();
    }
}