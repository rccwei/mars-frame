package com.mars.cloud.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.mars")
public class MarsCloudSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCloudSpringbootApplication.class, args);
    }

}
