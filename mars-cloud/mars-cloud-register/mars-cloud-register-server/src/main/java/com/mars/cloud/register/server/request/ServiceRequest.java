package com.mars.cloud.register.server.request;

/**
 * @author wq
 * @date 2022/1/21 10:45
 */
public class ServiceRequest {


    /**
     * 服务地址
     */
    private String serviceAddress;
    /**
     * 服务的名称
     */
    private String serviceName;
    /**
     * 续约时长
     */
    private Long renewalDuration;

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Long getRenewalDuration() {
        return renewalDuration;
    }

    public void setRenewalDuration(Long renewalDuration) {
        this.renewalDuration = renewalDuration;
    }
}
