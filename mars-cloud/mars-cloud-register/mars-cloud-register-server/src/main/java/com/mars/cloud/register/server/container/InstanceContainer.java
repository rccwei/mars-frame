package com.mars.cloud.register.server.container;

import com.mars.cloud.register.server.entity.ServiceInstance;
import com.mars.cloud.register.server.request.ServiceRequest;
import com.mars.cloud.register.server.resp.ServiceInstanceListResp;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2022/1/21 9:34
 */
@Component
public class InstanceContainer {

    private static final Log log = LogFactory.getLog(InstanceContainer.class);

    private static final Map<String, HashSet<ServiceInstance>> mapServiceInstances = new ConcurrentHashMap<>();

    private static final Lock lock = new ReentrantLock();

    /**
     * register service
     *
     * @param request request
     * @return Boolean
     */
    public Boolean register(ServiceRequest request) {
        HashSet<ServiceInstance> serviceInstances = mapServiceInstances.get(request.getServiceName());
        if (Objects.isNull(serviceInstances)) {
            lock.lock();
            try {
                serviceInstances = new HashSet<>();
                mapServiceInstances.put(request.getServiceName(), serviceInstances);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        ServiceInstance instance = new ServiceInstance(request.getServiceName(), request.getServiceAddress(), request.getRenewalDuration());
        log.info("service register success ===>" + instance.getServiceName());
        return serviceInstances.add(instance);
    }


    /**
     * remove service
     *
     * @param request
     * @return
     */
    public void remove(ServiceRequest request) {
        HashSet<ServiceInstance> serviceInstances = mapServiceInstances.get(request.getServiceName());
        if (Objects.isNull(serviceInstances)) {
            throw new RuntimeException(request.getServiceName() + "not found");
        }
        String serviceAddress = request.getServiceAddress();
        if (StringUtils.isEmpty(serviceAddress)) {
            throw new RuntimeException(request.getServiceName() + request.getServiceAddress() + "not found");
        }
        serviceInstances.removeIf(instance -> instance.getServiceAddress().equals(serviceAddress) && instance.getServiceName().equals(instance.getServiceName()));
    }

    /**
     * acquire service list
     *
     * @return List<ServiceInstanceListResp>
     */
    public List<ServiceInstanceListResp> getServiceInstances() {
        List<ServiceInstanceListResp> list = new ArrayList<>();
        mapServiceInstances.forEach((serviceName, instances) -> {
            ServiceInstanceListResp serviceListResp = new ServiceInstanceListResp();
            serviceListResp.setServiceName(serviceName);
            List<String> serviceAddressList = instances.stream().map(ServiceInstance::getServiceAddress).collect(Collectors.toList());
            serviceListResp.setServiceAddress(acquireAddressList(serviceAddressList));
            serviceListResp.setHealthyInstanceCount(instances.size());
            serviceListResp.setIpCount(instances.size());
            list.add(serviceListResp);
        });
        return list;
    }

    /**
     * char  concat
     *
     * @param serviceAddressList serviceName list
     * @return String
     */
    private String acquireAddressList(List<String> serviceAddressList) {
        return StringUtils.join(serviceAddressList, ";");
    }


}
