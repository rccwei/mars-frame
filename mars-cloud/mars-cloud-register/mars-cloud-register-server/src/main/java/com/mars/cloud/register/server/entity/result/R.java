package com.mars.cloud.register.server.entity.result;

import lombok.Data;

/**
 * 公共响应的实体
 *
 * @author : wq
 * @date : 2020-06-18
 **/
@Data
public class R<T> {

    /**
     * 状态码
     */
    private Integer status;
    /**
     * 说明
     */
    private String msg;
    /**
     * 数据
     */
    private T data;

    public R(Integer status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public R() {
    }

    public static <T> R<T> success() {
        return new R<>(ResponseCode.SUCCESS.getCode(), "成功", null);
    }


    public static <T> R<T> success(T data) {
        return new R<>(ResponseCode.SUCCESS.getCode(), "成功", data);
    }


    public static <T> R<T> error() {
        return new R<>(ResponseCode.FAIL.getCode(), "失败", null);
    }

    public static <T> R<T> error(String msg) {
        return new R<>(ResponseCode.FAIL.getCode(), msg, null);
    }

    public static <T> R<T> error(Integer status, String msg) {
        return new R<>(status, msg, null);
    }

}
