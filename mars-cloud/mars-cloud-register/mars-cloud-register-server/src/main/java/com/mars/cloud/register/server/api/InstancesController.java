package com.mars.cloud.register.server.api;

import com.mars.cloud.register.server.container.InstanceContainer;
import com.mars.cloud.register.server.entity.result.R;
import com.mars.cloud.register.server.request.ServiceRequest;
import com.mars.cloud.register.server.resp.ServiceInstanceListResp;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wq
 * @date 2022/1/21 12:17
 */
@RestController
public class InstancesController {


    @Resource
    private InstanceContainer instanceContainer;

    /**
     * service register
     *
     * @param reqInfo reqInfo
     * @return ResponseEntity
     */
    @PostMapping("/register")
    public R<Boolean> register(@RequestBody ServiceRequest reqInfo) {
        return R.success(instanceContainer.register(reqInfo));
    }


    /**
     * service remove
     *
     * @param reqInfo reqInfo
     * @return ResponseEntity
     */
    @PostMapping("/remove")
    public R<Void> remove(@RequestBody ServiceRequest reqInfo) {
        instanceContainer.remove(reqInfo);
        return R.success();
    }


    /**
     * service list
     *
     * @return R<List < ServiceInstanceListResp>>
     */
    @GetMapping("/list")
    public R<List<ServiceInstanceListResp>> list() {
        return R.success(instanceContainer.getServiceInstances());
    }

}
