package com.mars.cloud.register.server.resp;

/**
 * @author wq
 * @date 2022/1/24 13:44
 */
public class ServiceInstanceListResp {

    /**
     * 服务名称
     */
    private String serviceName;

    /**
     * 服务地址
     */
    private String serviceAddress;

    /**
     * 健康实例数
     */
    private Integer healthyInstanceCount;


    /**
     * 实例数
     */
    private Integer ipCount;

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public Integer getHealthyInstanceCount() {
        return healthyInstanceCount;
    }

    public void setHealthyInstanceCount(Integer healthyInstanceCount) {
        this.healthyInstanceCount = healthyInstanceCount;
    }

    public Integer getIpCount() {
        return ipCount;
    }

    public void setIpCount(Integer ipCount) {
        this.ipCount = ipCount;
    }
}
