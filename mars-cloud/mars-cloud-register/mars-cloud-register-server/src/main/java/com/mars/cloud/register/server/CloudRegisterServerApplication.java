package com.mars.cloud.register.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudRegisterServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudRegisterServerApplication.class, args);
    }

}
