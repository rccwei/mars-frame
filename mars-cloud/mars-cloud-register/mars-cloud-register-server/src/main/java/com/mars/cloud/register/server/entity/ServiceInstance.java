package com.mars.cloud.register.server.entity;

/**
 * @author wq
 * @date 2022/1/21 9:32
 */
public class ServiceInstance {

    /**
     * 服务名称
     */
    private String serviceName;
    /**
     * 服务地址
     */
    private String serviceAddress;
    /**
     * 服务最后续约时间
     */
    private long lastRenewalTime;


    public ServiceInstance(String serviceName, String serviceAddress, long lastRenewalTime) {
        this.serviceName = serviceName;
        this.serviceAddress = serviceAddress;
        this.lastRenewalTime = lastRenewalTime;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceAddress() {
        return serviceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        this.serviceAddress = serviceAddress;
    }

    public long getLastRenewalTime() {
        return lastRenewalTime;
    }

    public void setLastRenewalTime(long lastRenewalTime) {
        this.lastRenewalTime = lastRenewalTime;
    }

    @Override
    public String toString() {
        return "ServiceInstance{" +
                "serviceName='" + serviceName + '\'' +
                ", serviceAddress='" + serviceAddress + '\'' +
                ", lastRenewalTime=" + lastRenewalTime +
                '}';
    }
}
