package com.mars.design.pattern.behavioral.chainofresponsibility;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:38:37
 */
public class ConcreteHandlerC extends Handler {
    @Override
    public void handleRequest(Request request) {
        if (request.getType().equals(RequestType.TYPE_C)) {
            System.out.println("ConcreteHandlerC处理请求" + request.getName());
        } else if (successor != null) {
            successor.handleRequest(request);
        }
    }
}
