package com.mars.design.pattern.behavioral.command;

/**
 * 接收者类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:43:07
 */
public class Receiver {

    public void action() {
        System.out.println("Receiver is performing an action.");
    }
}
