package com.mars.design.pattern.behavioral.command;

/**
 * 请求者类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:44:31
 */
public class Invoker {

    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    /**
     * 执行命令
     */
    public void executeCommand() {
        command.execute();
    }
}
