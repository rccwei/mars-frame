package com.mars.design.pattern.behavioral.command;

/**
 * 命令接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:42:50
 */
public interface Command {

    void execute();
}
