package com.mars.design.pattern.structural.adapter;

/**
 * 需要适配的类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:11:06
 */
public class Adaptee {

    public void specificRequest() {
        System.out.println("Adaptee's specific request");
    }
}
