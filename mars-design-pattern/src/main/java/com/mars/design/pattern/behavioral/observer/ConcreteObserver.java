package com.mars.design.pattern.behavioral.observer;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:13:24
 */
public class ConcreteObserver implements Observer{

    private int observerState;
    
    @Override
    public void update(int state) {
        observerState = state;
        System.out.println("Observer state updated: " + observerState);
    }
}
