package com.mars.design.pattern.creational.factory;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:08:23
 */
public class Main {
    public static void main(String[] args) {
        Product productA = Factory.createProduct("A");
        productA.doSomething();

        Product productB = Factory.createProduct("B");
        productB.doSomething();
    }
}
