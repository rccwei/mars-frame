package com.mars.design.pattern.structural.facade;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:23:32
 */
public interface Shape {

    void draw();
}
