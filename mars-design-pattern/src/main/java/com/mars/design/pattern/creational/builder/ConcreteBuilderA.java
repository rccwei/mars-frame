package com.mars.design.pattern.creational.builder;

/**
 * 具体建造者A
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:31:12
 */
public class ConcreteBuilderA extends Builder{
    @Override
    public void buildPartA() {
        product.setPartA("PartA from ConcreteBuilderA");
    }

    @Override
    public void buildPartB() {
        product.setPartB("PartB from ConcreteBuilderA");
    }

    @Override
    public void buildPartC() {
        product.setPartC("PartC from ConcreteBuilderA");
    }
}
