package com.mars.design.pattern.structural.proxy;

/**
 * 主题接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:42:55
 */
public interface Subject {

    void request();
}
