package com.mars.design.pattern.creational.prototype;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:45:26
 */
public class Cat extends Animal {
    public Cat(String name, Integer age, String color) {
        super(name, age, color);
    }

    @Override
    public Cat clone() {
        try {
            Cat cat = (Cat) super.clone(); // 克隆父类的对象
            cat.setName("Copy of " + this.getName()); // 设置新对象的名称为原名称的副本
            cat.setAge(0); // 年龄设置为0
            cat.setColor(this.getColor()); // 颜色与原对象相同
            return cat;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }


}
