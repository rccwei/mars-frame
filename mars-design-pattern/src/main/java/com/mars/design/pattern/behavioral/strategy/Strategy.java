package com.mars.design.pattern.behavioral.strategy;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:49:20
 */
public interface Strategy {

    void execute();
}
