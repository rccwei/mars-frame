package com.mars.design.pattern.behavioral.iterator;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:23:03
 */
public class Main {

    public static void main(String[] args) {

        NameRepository namesRepository = new NameRepository();
        for (Iterator iter = namesRepository.getIterator(); iter.hasNext(); ) {
            String name = (String) iter.next();
            System.out.println("Name : " + name);
        }
    }

}
