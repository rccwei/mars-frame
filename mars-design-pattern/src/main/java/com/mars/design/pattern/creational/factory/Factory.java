package com.mars.design.pattern.creational.factory;

/**
 * 工厂类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:07:28
 */
public class Factory {

    /**
     * 创建产品
     *
     * @param type 类型
     * @return Project
     */
    public static Product createProduct(String type) {
        if ("A".equals(type)) {
            return new ConcreteProductA();
        } else if ("B".equals(type)) {
            return new ConcreteProductB();
        }
        throw new IllegalArgumentException("Invalid product type: " + type);
    }
}
