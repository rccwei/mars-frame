package com.mars.design.pattern.creational.builder;

/**
 * 抽象构建类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:30:12
 */
public abstract class Builder {

    protected Product product;

    /**
     * 创建产品
     */
    public void createProduct() {
        product = new Product();
    }

    public abstract void buildPartA();

    public abstract void buildPartB();

    public abstract void buildPartC();

    /**
     * 获取产品
     *
     * @return Product
     */
    public Product getProduct() {
        return product;
    }
}
