package com.mars.design.pattern.creational.builder;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:32:32
 */
public class Director {

    private Builder builder;

    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    /**
     * 建造者 指挥构建者构建产品
     *
     * @return project
     */
    public Product construct() {
        builder.createProduct();
        builder.buildPartA();
        builder.buildPartB();
        builder.buildPartC();
        return builder.getProduct();
    }
}
