package com.mars.design.pattern.creational.prototype;

/**
 * 主类
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:54:53
 */
public class Main {
    public static void main(String[] args) {
        Cat originalCat = new Cat("Tom", 2, "White");
        // 克隆新的 Cat 对象
        Cat newCat = originalCat.clone();
        // 输出原对象和新对象的属性
        System.out.println("Original cat: " + originalCat.getName() + ", " + originalCat.getAge() + ", " + originalCat.getColor());
        System.out.println("New cat: " + newCat.getName() + ", " + newCat.getAge() + ", " + newCat.getColor());
    }
}
