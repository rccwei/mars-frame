package com.mars.design.pattern.behavioral.templatemethod;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:53:26
 */
public abstract class Game {


    /**
     * 模板方法
     */
    public void play() {
        initialize();
        startPlay();
        endPlay();
    }


    /**
     * 初始化游戏
     */
    protected abstract void initialize();

    /**
     * 开始游戏
     */
    protected abstract void startPlay();

    /**
     * 结束游戏
     */
    protected abstract void endPlay();
}
