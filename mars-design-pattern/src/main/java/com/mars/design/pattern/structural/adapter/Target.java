package com.mars.design.pattern.structural.adapter;

/**
 * 目标接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:10:35
 */
public interface Target {

    /**
     * 目标方法
     */
    void request();
}
