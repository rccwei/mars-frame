package com.mars.design.pattern.structural.bridge;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:27:15
 */
public class ConcreteAbstractionB extends Abstraction {
    public ConcreteAbstractionB(Implementor implementor) {
        super(implementor);
    }

    @Override
    public void operation() {
        System.out.println("ConcreteAbstractionB ");
        implementor.operationImpl();
    }
}
