package com.mars.design.pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 创建型模式（Creational Patterns）：
 *      1.单例模式（Singleton）、
 *      2.工厂模式（Factory）、
 *      3.抽象工厂模式（Abstract Factory）、
 *      4.建造者模式（Builder）、
 *      5.原型模式（Prototype）等。
 * 结构型模式（Structural Patterns）：
 *      1.适配器模式（Adapter）、
 *      2.桥接模式（Bridge）、
 *      3.装饰者模式（Decorator）、
 *      4.组合模式（Composite）、
 *      5.外观模式（Facade）、
 *      6.享元模式（Flyweight）等。
 *      7.代理模式（Proxy）
 * 行为型模式（Behavioral Patterns）：
 *      1.策略模式（Strategy）、
 *      2.模板方法模式（Template Method）、
 *      3.观察者模式（Observer）、
 *      4.迭代器模式（Iterator）、
 *      5.责任链模式（Chain of Responsibility）、
 *      6.命令模式（Command）、
 *      7.备忘录模式（Memento）、
 *      8.状态模式（State）、
 *      9.访问者模式（Visitor）等。
 *      10.中介者模式（Mediator）、
 *      11.解释器模式（Interpreter）
 */

/**
 * OOP 七大原则
 * 面向对象程序设计（Object Oriented Programming，OOP）。
 *
 * 1、开闭原则
 *
 * 对扩展开放， 对修改关闭。
 *
 * 2、单一职责原则
 * 每个类应该实现单一的职责，不要存在多于一个导致类变更的原因，否则就应该把类拆分。该原则是实现高内聚、低耦合的指导方针。
 *
 * 3、里氏替换原则（Liskov Substitution Principle）
 * 任何基类可以出现的地方，子类一定可以出现。里氏替换原则是继承复用的基石，只有当衍生类可以替换基类，软件单位的功能不受到影响时，基类才能真正被复用，而衍生类也能够在基类的基础上增加新的行为。
 *
 * 里氏替换原则是对开闭原则的补充。实现开闭原则的关键就是抽象化。而基类与子类的继承关系就是抽象化的具体实现，所以里氏替换原则是对实现抽象化的具体步骤的规范。里氏替换原则中，子类对父类的方法尽量不要重写和重载。因为父类代表了定义好的结构，通过这个规范的接口与外界交互，子类不应该随便破坏它。
 *
 * 4、依赖倒转原则（Dependence Inversion Principle）
 * 面向接口编程，依赖于抽象而不依赖于具体。用到具体类时，不与具体类交互，而与具体类的上层接口交互。
 *
 * 5、接口隔离原则（Interface Segregation Principle）
 * 每个接口中不存在子类用不到却必须实现的方法，否则就要将接口拆分。使用多个隔离的接口，比使用单个接口（多个接口中的方法聚合到一个的接口）要好。
 *
 * 6、迪米特法则（最少知道原则）（Demeter Principle）
 * 一个类对自己依赖的类知道的越少越好。无论被依赖的类多么复杂，都应该将逻辑封装在方法的内部，通过 public 方法提供给外部。这样当被依赖的类变化时，才能最小的影响该类。
 *
 * 7、合成复用原则（Composite Reuse Principle）
 * 软件复用时，要先尽量使用组合或者聚合等关联关系实现，其次才考虑使用继承。即在一个新对象里通过关联的方式使用已有对象的一些方法和功能。
 */


@SpringBootApplication
public class MarsDesignPatternApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsDesignPatternApplication.class, args);
    }

}
