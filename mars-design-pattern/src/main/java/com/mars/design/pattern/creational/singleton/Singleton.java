package com.mars.design.pattern.creational.singleton;

/**
 * Singleton类的构造函数被声明为私有的，这样其他类无法直接实例化Singleton对象。
 * getInstance()方法是获取单例实例的入口，它使用双重检查锁定（double-checked locking）的方式确保只有在实例不存在时才会创建实例
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-19 14:55:01
 */
public class Singleton {

    private static Singleton instance;


    /**
     * 私有构造函数，防止外部直接实例化
     */
    private Singleton() {
    }


    /**
     * 获取单例实例的方法
     *
     * @return Singleton
     */
    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                // 双重检查，确保实例只被创建一次
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }


    /**
     * 其他实例方法
     */
    public void doSomething() {
        // 单例的操作
        System.out.println("执行单例方法");
    }

}
