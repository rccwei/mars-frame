package com.mars.design.pattern.structural.bridge;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:26:32
 */
public class ConcreteAbstractionA extends Abstraction {

    public ConcreteAbstractionA(Implementor implementor) {
        super(implementor);
    }

    @Override
    public void operation() {
        System.out.println("ConcreteAbstractionA ");
        implementor.operationImpl();
    }
}
