package com.mars.design.pattern.creational.factory;

/**
 * 产品接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:05:59
 */
public interface Product {

    /**
     * 定义接口做什么事情
     */
    void doSomething();
}
