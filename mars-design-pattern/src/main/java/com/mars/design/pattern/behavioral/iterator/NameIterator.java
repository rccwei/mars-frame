package com.mars.design.pattern.behavioral.iterator;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:21:51
 */
public class NameIterator implements Iterator {

    public String[] names = {"Robert", "John", "Julie", "Lora"};

    private int index;


    @Override
    public boolean hasNext() {
        if (index < names.length) {
            return true;
        }
        return false;
    }

    @Override
    public Object next() {
        if (this.hasNext()) {
            return names[index++];
        }
        return null;
    }
}
