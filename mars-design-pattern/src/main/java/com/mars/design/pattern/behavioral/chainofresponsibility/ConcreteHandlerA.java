package com.mars.design.pattern.behavioral.chainofresponsibility;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:37:08
 */
public class ConcreteHandlerA extends Handler {
    @Override
    public void handleRequest(Request request) {
        if (request.getType().equals(RequestType.TYPE_A)) {
            System.out.println("ConcreteHandlerA处理请求" + request.getName());
        } else if (successor != null) {
            successor.handleRequest(request);
        }
    }
}
