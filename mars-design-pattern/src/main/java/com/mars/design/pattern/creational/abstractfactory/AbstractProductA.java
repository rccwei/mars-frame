package com.mars.design.pattern.creational.abstractfactory;

/**
 * 抽象产品A
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:15:49
 */
public interface AbstractProductA {

    /**
     * 产品A做事情
     */
    void doSomething();
}
