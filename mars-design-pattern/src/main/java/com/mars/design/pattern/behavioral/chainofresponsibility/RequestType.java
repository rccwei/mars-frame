package com.mars.design.pattern.behavioral.chainofresponsibility;

/**
 * 定义请求枚举
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:35:32
 */
public enum RequestType {

    TYPE_A, TYPE_B, TYPE_C

}
