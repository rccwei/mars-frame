package com.mars.design.pattern.structural.bridge;

/**
 * 我们首先创建了具体实现部分的对象（ConcreteImplementorA和ConcreteImplementorB），
 * 然后创建了具体抽象部分的对象（ConcreteAbstractionA和ConcreteAbstractionB），
 * 并将相应的实现部分对象传递给抽象部分的构造函数。
 * 最后，我们调用抽象部分的operation方法来实现对实现部分的操作。
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:28:20
 */
public class Main {
    public static void main(String[] args) {
        Implementor implementorA = new ConcreteImplementorA();
        Abstraction abstractionA = new ConcreteAbstractionA(implementorA);
        abstractionA.operation();

        Implementor implementorB = new ConcreteImplementorB();
        Abstraction abstractionB = new ConcreteAbstractionB(implementorB);
        abstractionB.operation();
    }
}
