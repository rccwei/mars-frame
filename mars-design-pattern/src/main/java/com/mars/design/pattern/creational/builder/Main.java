package com.mars.design.pattern.creational.builder;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:33:31
 */
public class Main {
    public static void main(String[] args) {
        // 指挥者
        Director director = new Director();

        // 构建者
        Builder builderA = new ConcreteBuilderA();
        director.setBuilder(builderA);
        // 指挥者开始构建 返回产品
        Product productA = director.construct();
        productA.show();

        Builder builderB = new ConcreteBuilderB();
        director.setBuilder(builderB);
        Product productB = director.construct();
        productB.show();
    }
}
