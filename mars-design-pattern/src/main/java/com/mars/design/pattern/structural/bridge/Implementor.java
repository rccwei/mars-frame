package com.mars.design.pattern.structural.bridge;

/**
 * 实现部分的接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:23:33
 */
public interface Implementor {

    void operationImpl();

}
