package com.mars.design.pattern.creational.abstractfactory;

/**
 * 具体产品B2
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:18:58
 */
public class ConcreteProductB2 implements AbstractProductB{
    @Override
    public void doSomething() {
        System.out.println("ConcreteProductB2 do something");
    }
}
