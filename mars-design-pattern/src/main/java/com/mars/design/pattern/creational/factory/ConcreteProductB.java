package com.mars.design.pattern.creational.factory;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:06:27
 */
public class ConcreteProductB implements Product{
    @Override
    public void doSomething() {
        System.out.println("ConcreteProductB do something");
    }
}
