package com.mars.design.pattern.creational.abstractfactory;

/**
 * 具体产品A2
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:16:59
 */
public class ConcreteProductA2 implements AbstractProductA {
    @Override
    public void doSomething() {
        System.out.println("ConcreteProductA2 do something");
    }
}
