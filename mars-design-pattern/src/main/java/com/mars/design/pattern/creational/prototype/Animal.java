package com.mars.design.pattern.creational.prototype;

/**
 * 抽象原型类
 * <p>
 * 原型模式，它通过克隆现有对象来创建新对象，而无需依赖于昂贵的实例化过程
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:44:27
 */
public abstract class Animal implements Cloneable {

    private String name;

    private Integer age;

    private String color;

    // 构造函数
    public Animal(String name, Integer age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

}
