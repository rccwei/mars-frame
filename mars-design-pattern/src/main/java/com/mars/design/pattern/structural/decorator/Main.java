package com.mars.design.pattern.structural.decorator;

/**
 * 装饰器模式
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:02:11
 */
public class Main {
    public static void main(String[] args) {

        // 使用具体装饰者装饰该组件对象
        Pizza decoratedPizza = new Cheese(new PlainPizza());
        // 输出描述和费用
        System.out.println("Description: " + decoratedPizza.getDescription());
        System.out.println("Cost: $" + decoratedPizza.getCost());

    }
}
