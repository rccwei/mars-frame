package com.mars.design.pattern.creational.abstractfactory;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:19:42
 */
public interface AbstractFactory {

    /**
     * 创建产品A
     *
     * @return AbstractProductA
     */
    AbstractProductA createProductA();

    /**
     * 创建产品B
     *
     * @return AbstractProductB
     */
    AbstractProductB createProductB();
}
