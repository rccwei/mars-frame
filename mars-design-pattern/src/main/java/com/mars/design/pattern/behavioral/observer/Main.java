package com.mars.design.pattern.behavioral.observer;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:14:30
 */
public class Main {
    public static void main(String[] args) {
        ConcreteSubject subject = new ConcreteSubject();

        ConcreteObserver observer1 = new ConcreteObserver();
        subject.registerObserver(observer1);

        ConcreteObserver observer2 = new ConcreteObserver();
        subject.registerObserver(observer2);

        subject.setState(1);
        subject.setState(2);

        subject.removeObserver(observer1);

        subject.setState(3);
    }
}
