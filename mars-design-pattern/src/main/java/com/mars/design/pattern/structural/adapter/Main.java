package com.mars.design.pattern.structural.adapter;

/**
 * 当谈到适配器模式时，它用于将一个类的接口转换成客户端所期望的另一个接口
 * 
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-24 10:12:32
 */
public class Main {
    public static void main(String[] args) {
        Adaptee adaptee = new Adaptee();
        Target adapter = new Adapter(adaptee);
        adapter.request();
    }
}
