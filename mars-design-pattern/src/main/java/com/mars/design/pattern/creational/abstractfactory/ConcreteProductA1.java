package com.mars.design.pattern.creational.abstractfactory;

/**
 * 具体产品A1
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-23 22:16:30
 */
public class ConcreteProductA1 implements AbstractProductA {
    @Override
    public void doSomething() {
        System.out.println("ConcreteProductA1 do something");
    }
}
