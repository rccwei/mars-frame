package com.mars.design.pattern.structural.decorator;

/**
 * 具体装饰者
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:00:56
 */
public class Cheese extends PizzaDecorator {
    protected Cheese(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String getDescription() {
        return super.getDescription() + ",cheese";
    }

    @Override
    public double getCost() {
        return super.getCost() + 2.0;
    }
}
