package com.mars.design.pattern.structural.proxy;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:43:48
 */
public class Proxy implements Subject {

    private RealSubject realSubject;

    public Proxy() {
        this.realSubject = new RealSubject();
    }

    @Override
    public void request() {
        // 在调用真实主题之前可以执行一些额外的操作
        System.out.println("Proxy: Pre-processing request.");
        // 调用真实主题的方法
        realSubject.request();
        // 在调用真实主题之后可以执行一些额外的操作
        System.out.println("Proxy: Post-processing request.");
    }
}
