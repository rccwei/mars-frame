package com.mars.design.pattern.structural.decorator;

/**
 * 抽象组件
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 15:58:05
 */
public interface Pizza {

    /**
     * 获取描述
     *
     * @return 描述
     */
    String getDescription();

    /**
     * 花费
     *
     * @return double
     */
    double getCost();
}
