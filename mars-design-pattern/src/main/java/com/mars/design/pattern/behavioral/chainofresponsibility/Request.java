package com.mars.design.pattern.behavioral.chainofresponsibility;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:36:01
 */
public class Request {

    private String name;
    private RequestType type;

    public Request(String name, RequestType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public RequestType getType() {
        return type;
    }
}
