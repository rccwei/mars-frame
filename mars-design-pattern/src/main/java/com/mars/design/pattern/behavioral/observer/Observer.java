package com.mars.design.pattern.behavioral.observer;

/**
 * 观察者接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:10:33
 */
public interface Observer {

    void update(int state);
}
