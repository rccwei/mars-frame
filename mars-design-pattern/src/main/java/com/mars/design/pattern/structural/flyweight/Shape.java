package com.mars.design.pattern.structural.flyweight;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:31:59
 */
public interface Shape {

    void draw();

}
