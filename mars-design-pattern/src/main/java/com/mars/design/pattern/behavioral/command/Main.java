package com.mars.design.pattern.behavioral.command;

/**
 * 请求发起命令并执行命令 然后接受者开始执行
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:45:08
 */
public class Main {

    public static void main(String[] args) {
        // 创建接受者
        Receiver receiver = new Receiver();
        // 创建命令
        ConcreteCommand command = new ConcreteCommand(receiver);

        // 创建请求类
        Invoker invoker = new Invoker();
        // 设置命令
        invoker.setCommand(command);
        // 执行命令
        invoker.executeCommand();

    }
}
