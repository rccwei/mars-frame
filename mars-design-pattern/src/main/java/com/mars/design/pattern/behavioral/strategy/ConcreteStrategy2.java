package com.mars.design.pattern.behavioral.strategy;

/**
 * 具体策略类2
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 16:49:39
 */
public class ConcreteStrategy2 implements Strategy{
    @Override
    public void execute() {
        System.out.println("Executing ConcreteStrategy2");
    }
}
