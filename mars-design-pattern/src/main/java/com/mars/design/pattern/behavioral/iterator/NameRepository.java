package com.mars.design.pattern.behavioral.iterator;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-26 17:20:03
 */
public class NameRepository implements Container {
    @Override
    public Iterator getIterator() {
        return new NameIterator();
    }
}
