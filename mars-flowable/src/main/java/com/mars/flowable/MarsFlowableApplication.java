package com.mars.flowable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MarsFlowableApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsFlowableApplication.class, args);
	}

}
