package com.mars.cache.redis.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author wq
 * @date 2022/1/20 10:06
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiIdempotent {

    /**
     * 超过请求幂等性提示信息
     *
     * @return String
     */
    String message() default "请勿重复提交";

    /**
     * 该请求的唯一key 不填则为接口方法名称+接口api+接口参数值
     *
     * @return String
     */
    String key() default "";

    /**
     * 有效期 默认：1 有效期要大于程序执行时间，否则请求还是可能会进来
     *
     * @return expireTime
     */
    long expireTime() default 5L;

    /**
     * 时间单位 默认：s
     *
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 是否在业务完成后删除key true:删除 false:不删除
     *
     * @return boolean
     */
    boolean delKey() default false;
}
