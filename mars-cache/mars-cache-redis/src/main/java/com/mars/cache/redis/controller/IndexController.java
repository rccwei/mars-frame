package com.mars.cache.redis.controller;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 15:11
 */
@RestController
public class IndexController {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedissonClient redissonClient;

    @GetMapping("/set")
    public String index() {
        stringRedisTemplate.opsForValue().set("store", "1000");
        return "success";
    }

    @GetMapping("/getAll")
    public void getAll() {
        for (int i = 0; i < 100; i++) {
            this.get();
        }
    }

    @GetMapping("/get")
    public void get() {
        RLock lock = redissonClient.getLock("store_lock");
        lock.lock();
        try {
            String store = stringRedisTemplate.opsForValue().get("store");
            assert store != null;
            int i = Integer.parseInt(store);
            if (i > 0) {
                int result = i - 1;
                System.out.println("当前数量是:" + result);
                stringRedisTemplate.opsForValue().set("store", String.valueOf(result));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } finally {
            //如果当前线程是持有锁 并且是当前线程持有的
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }

        }
    }
}
