package com.mars.cache.redis.exception;

/**
 * @author wq
 * @date 2020/12/30 10:17
 */

public class IdempotentException extends RuntimeException {

    private String msg;

    private Integer code;

    public IdempotentException(String msg, Integer code) {
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
