package com.mars.cache.redis.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:32
 */
@Data
public class User implements Serializable {

    private Integer id;

    private String name;

    private String password;


}
