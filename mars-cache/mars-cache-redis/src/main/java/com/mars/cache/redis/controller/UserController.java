package com.mars.cache.redis.controller;

import com.mars.cache.redis.annotation.ApiIdempotent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:34
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @GetMapping("/create")
    @ApiIdempotent
    public String create() {
        return "success";
    }

    @GetMapping("/getSession")
    public void getSession(HttpServletRequest request) {

        HttpSession session = request.getSession();
        Object age = session.getAttribute("isLogin");
        System.out.println(age);
    }

//    public CommomReturnType login(@RequestParam(name="name")String name, @RequestParam(name="password")String password,
//                                  HttpServletRequest request) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
//        //入参校验
//        if(org.apache.commons.lang3.StringUtils.isEmpty(name)|| StringUtils.isEmpty(password)){
//            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
//        }
//        //用户登录服务
//        UserModel userModel=userService.validateLogin(name,this.EncodeByMd5(password));
//        //将登陆凭证加入到用户登录成功的session内
//        ssion.setAttribute("IS","DJIWSDAADAD");
//        HttpSession session=request.getSession();
//        session.setAttribute("IS",true);
//        //this.httpServletRequest.getSession().setAttribute("IS",true);
//        this.httpServletRequest.getSession().setAttribute("LOGIN_USER",userModel);
//        System.out.println(session.getAttribute("IS"));
//        return CommomReturnType.create(null);
//    }
//
//
//    public CommomReturnType createOrder(@RequestParam(name="itemId")Integer itemId,
//                                        @RequestParam(name="amount")Integer amount,
//                                        @RequestParam(name="promoId",required = false)Integer promoId,
//                                        HttpServletRequest request) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
//        HttpSession session=request.getSession();
//        Boolean isLogin=(Boolean)session.getAttribute("IS");
//        //String isLogin=(String)session.getAttribute("IS");
//        System.out.println("islogin =="+isLogin);
//        isLogin=true;
//        if(isLogin == null || !isLogin.booleanValue()){
//            throw new BusinessException(EmBusinessError.USER_NOT_LOGIN,"用户未登陆");
//        }
//        //UserModel userModel = (UserModel) httpServletRequest.getSession().getAttribute("LOGIN_USER");
//        UserModel userModel=userService.validateLogin("人才",this.EncodeByMd5("260664707"));
//        OrderModel orderModel=orderService.createOrder(userModel.getId(),itemId,promoId,amount);
//        return CommomReturnType.create(null);
//    }


}
