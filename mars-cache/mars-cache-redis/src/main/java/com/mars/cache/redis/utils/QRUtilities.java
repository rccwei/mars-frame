//
//package com.mars.cache.redis.utils;
//
//import java.io.File;
//import java.nio.charset.StandardCharsets;
//import java.security.KeyFactory;
//import java.security.NoSuchAlgorithmException;
//import java.security.PublicKey;
//import java.security.Signature;
//import java.security.spec.InvalidKeySpecException;
//import java.security.spec.X509EncodedKeySpec;
//import java.util.Base64;
//import javax.crypto.Cipher;
//
//public class QRUtilities {
//    private static final String TAG = "QRUtilities";
//
//    private PublicKey mPublicKey = getFromString();
//
//    public QRUtilities() {
//    }
//
//    public QRUtilities(int paramInt) {
//    }
//
//    private PublicKey getFromString() {
//        String str;
//        str = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDy7y9DIdAMdKsfIInrhhyXER4h\n/FnsUDOiLcuJK1YUDfDZNLfY54p8TFeIdH66gUPwF+7vjX5RgDMwYfIxTMsFFhMB\nsnN+KTha6v5Q56BZ9fuIpH7vESdZn/lfpMkH/uoDZfenNnFbg6YDjRk2u7YgymSt\nyTfXu6SoGSk2lMEvcQIDAQAB";
//        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(Base64.decode(str.getBytes()));
//        try {
//            return KeyFactory.getInstance("RSA").generatePublic(x509EncodedKeySpec);
//        } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
//
//        } catch (InvalidKeySpecException invalidKeySpecException) {
//        }
//        return null;
//    }
//
//
//    public String decryptData(String paramString) {
//        try {
//            Cipher cipher = Cipher.getInstance("RSA");
//            cipher.init(2, this.mPublicKey);
//            byte[] arrayOfByte = cipher.doFinal(Base64.getDecoder().decode(paramString.getBytes()));
//            if (arrayOfByte != null)
//                return new String(arrayOfByte, StandardCharsets.UTF_8);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }
//        return null;
//    }
//
//    public static void main(String[] args) {
//        QRUtilities utilities = new QRUtilities();
//        String s = utilities.decryptData("yw/Z2CGl61WEESDZPRrVoEhKC6uDdDIkAVcFFVZi4iOBXal9vwVIQtw/NRQ/OWaWhiBxgjs0qJFf6NJv3dHFd4bREcz/ewGZvotQFoUnBiW1aPnhHygzMNLoV9O1s+zIc5AYyyutnvnZtGRUkDfHV66IQvtlY6Sncw6DLux7+z8=");
//        System.out.println(s);
//    }
//
////    public boolean verifyData(String paramString1, String paramString2) {
////        if (TextUtils.isEmpty(paramString2))
////            return false;
////        byte[] arrayOfByte1 = paramString1.getBytes();
////        byte[] arrayOfByte2 = Base64.decode(paramString2, 0);
////        try {
////            Signature signature = Signature.getInstance("SHA1WithRSA");
////            signature.initVerify(this.mPublicKey);
////            signature.update(arrayOfByte1);
////            return signature.verify(arrayOfByte2);
////        } catch (Exception exception) {
////            exception.printStackTrace();
////            return false;
////        }
////    }
//}
