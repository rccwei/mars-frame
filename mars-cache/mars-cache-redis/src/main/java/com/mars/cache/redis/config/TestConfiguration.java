package com.mars.cache.redis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author wq
 * @date 2022/1/25 16:29
 */
@Configuration
@Import({MarsTest.class})
public class TestConfiguration {
}
