package com.mars.cache.redis;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.ApplicationListener;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * redis 内存淘汰策略
 * LRU：hash+链表
 */
@SpringBootApplication
@Slf4j
public class MarsCacheRedisApplication implements ApplicationListener<WebServerInitializedEvent>, CommandLineRunner, ApplicationRunner {


    public static void main(String[] args) {
        SpringApplication.run(MarsCacheRedisApplication.class, args);
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(WebServerInitializedEvent event) {
        log.info("IpAddress==={},ServerPort===>{}", InetAddress.getLocalHost().getHostAddress(), event.getWebServer().getPort());
    }

    @Override
    public void run(ApplicationArguments args) {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        pool.scheduleAtFixedRate(() -> log.info("ApplicationRunner任务执行" + LocalDateTime.now()), 1, 3, TimeUnit.SECONDS);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("=======" + Arrays.stream(args));
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        pool.scheduleAtFixedRate(() -> log.info("CommandLineRunner任务执行" + LocalDateTime.now()), 1, 3, TimeUnit.SECONDS);
    }
}
