package com.mars.cache.redis.result;

import com.mars.cache.redis.enums.ResultEnum;
import lombok.Data;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:44
 */
@Data
public class ResponseResult {

    private Integer code;
    private String msg;

    public ResponseResult() {
    }

    public ResponseResult(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMessage();
    }

    public ResponseResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
