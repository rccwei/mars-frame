package com.mars.cache.redis.request;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:35
 */
@Data
public class UserRequest implements Serializable {

    private Integer id;

    @NotNull(message = "名称不能为空")
    private String name;

    @NotNull(message = "密码不能为空")
    private String password;

    @Length(min = 0, max = 1, message = "请选择正确性别")
    private Integer gender;
}
