package com.mars.cache.redis.config;

import com.mars.cache.redis.controller.UserController;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author wq
 * @date 2022/1/20 19:22
 */
@Component
public class TestSmartCycle implements SmartLifecycle, ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void start() {
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);
        for (int i = 0; i < 100; i++) {
            pool.scheduleAtFixedRate(this::print, 2, 2, TimeUnit.SECONDS);
        }
        System.out.println("bean 创建完成");
//        String[] strings = applicationContext.getBeanDefinitionNames();
//        for (String string : strings) {
//            System.out.println(string);
//        }
        UserController bean = applicationContext.getBean(UserController.class);
        System.out.println(bean);
    }

    public void print() {
        System.out.println("定时任务执行");
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
