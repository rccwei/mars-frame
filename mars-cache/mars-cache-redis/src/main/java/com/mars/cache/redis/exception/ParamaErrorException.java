package com.mars.cache.redis.exception;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:45
 */
public class ParamaErrorException extends RuntimeException {
    public ParamaErrorException() {
    }

    public ParamaErrorException(String message) {
        super(message);
    }
}
