package com.mars.cache.redis.aspect;

import com.mars.cache.redis.annotation.ApiIdempotent;
import com.mars.cache.redis.exception.IdempotentException;
import com.mars.cache.redis.utils.IpUtils;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Objects;

/**
 * @author wq
 * @date 2022/1/20 10:06
 */
@Aspect
@Order(1)
@Component
public class IdempotentAspects {

    public static final String REQUEST_KEY = "RequestKey:";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Pointcut("@annotation(com.mars.cache.redis.annotation.ApiIdempotent)")
    public void IdempotentPoint() {

    }

    @Around(value = "IdempotentPoint()")
    public Object apiIdempotentCheck(ProceedingJoinPoint pjp) throws Throwable {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = (HttpServletRequest) requestAttributes.resolveReference(RequestAttributes.REFERENCE_REQUEST);
        //宿主机访问地址
        assert request != null;
        String ipAddress = IpUtils.getIpAddr(request);
        String requestURI = request.getRequestURI();
        Signature signature = pjp.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        ApiIdempotent annotation = method.getAnnotation(ApiIdempotent.class);
        if (Objects.isNull(annotation)) {
            return pjp.proceed();
        }
        Object arg = null;
        if (pjp.getArgs().length > 0) {
            arg = pjp.getArgs()[0];
        }
        String apiName = null;
        String apiMethodName, key;
        //接口方法名称
        apiMethodName = method.getName();
        Parameter[] parameters = method.getParameters();
        //获取接口间隔时间
        long expireTime = annotation.expireTime();
        Annotation[] declaredAnnotations = method.getDeclaredAnnotations();
        apiName = getRequestName(requestURI, apiName, declaredAnnotations);
        String parameterName = "";
        if (parameters.length > 0) {
            Parameter parameter = parameters[0];
            //接口参数名称
            parameterName = parameter.getName();
        }
        //如果用户自定义则默认使用修改后的规则
        if (StringUtils.isNotBlank(annotation.key())) {
            //TODO 后续对用户设置自定义key进行校验
            key = annotation.key();
        } else {
            key = REQUEST_KEY + ipAddress + ":" + apiName + ":" + apiMethodName + ":" + parameterName;
        }
        //如果当前key存在直接抛出异常
        if (Boolean.TRUE.equals(stringRedisTemplate.hasKey(key))) {
            throw new IdempotentException(annotation.message(), 500);
        }
        if (arg != null && !"".equals(arg)) {
            stringRedisTemplate.opsForValue().set(key, arg.toString(), expireTime, annotation.timeUnit());
        } else {
            stringRedisTemplate.opsForValue().set(key, "1", expireTime, annotation.timeUnit());
        }
        Object proceed = pjp.proceed();
        //默认不删除 如果修改则使用修改后的配置
        if (annotation.delKey()) {
            stringRedisTemplate.delete(key);
        }
        return proceed;
    }

    /**
     * 获取请求参数
     *
     * @param requestURI          请求地址
     * @param apiName             名称
     * @param declaredAnnotations 注解
     * @return String
     */
    private String getRequestName(String requestURI, String apiName, Annotation[] declaredAnnotations) {
        // 获取接口请求类型以及接口api
        for (Annotation declaredAnnotation : declaredAnnotations) {
            if (declaredAnnotation instanceof RequestMapping) {
                RequestMapping requestMapping = (RequestMapping) declaredAnnotation;
                String requestType = requestMapping.method()[0].name();
                apiName = requestType + ":" + requestURI;
            }
            if (declaredAnnotation instanceof DeleteMapping) {
                apiName = RequestMethod.DELETE + requestURI;
            }
            if (declaredAnnotation instanceof GetMapping) {
                apiName = RequestMethod.DELETE + requestURI;
            }
            if (declaredAnnotation instanceof PutMapping) {
                apiName = RequestMethod.DELETE + requestURI;
            }
            if (declaredAnnotation instanceof PostMapping) {
                apiName = RequestMethod.DELETE + requestURI;
            }
        }
        return apiName;
    }
}
