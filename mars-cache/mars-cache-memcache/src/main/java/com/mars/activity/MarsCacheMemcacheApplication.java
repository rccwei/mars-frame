package com.mars.activity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCacheMemcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCacheMemcacheApplication.class, args);
    }

}
