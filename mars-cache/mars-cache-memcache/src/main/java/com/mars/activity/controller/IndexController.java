package com.mars.activity.controller;

import com.whalin.MemCached.MemCachedClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 15:56
 */
@RestController
public class IndexController {

    @Resource
    private MemCachedClient memCachedClient;

    @GetMapping("/index")
    public String index() {
        memCachedClient.set("name", "lisi");
        return (String) memCachedClient.get("name");
    }
}
