package com.mars.cache.mongo.controller;

import com.mars.cache.mongo.entity.User;
import com.mars.cache.mongo.entity.UserTest;
import com.mars.cache.mongo.service.UserService;
import com.mars.cache.mongo.service.UserTestService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 10:03
 */
@RestController
public class UserTestController {

    @Resource
    private UserTestService userTestService;

    /**
     * 保存用户
     */
    @GetMapping("/add")
    public void add() {
        userTestService.save();
    }

    /**
     * 查询
     */
    @GetMapping("/findUserTest")
    public UserTest findUser(Integer Id, String name, String body) {
        return userTestService.findUserTest(Id, name, body);
    }


}
