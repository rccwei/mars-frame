package com.mars.cache.mongo.controller;

import com.mars.cache.mongo.entity.User;
import com.mars.cache.mongo.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 10:03
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 保存用户
     */
    @GetMapping("/save")
    public void save() {
        userService.save();
    }

    /**
     * 查询
     *
     */
    @GetMapping("/findUser")
    public User findUser(Integer Id, String name, String body) {
        return userService.findUser(Id, name, body);
    }


}
