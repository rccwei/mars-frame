package com.mars.cache.mongo.service.impl;

import com.mars.cache.mongo.entity.UserTest;
import com.mars.cache.mongo.service.UserTestService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/25 20:48
 */
@Service
public class UserTestServiceImpl implements UserTestService {

    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public void save() {
        UserTest test = new UserTest();
        test.setID(2);
        test.setBody("this  is body");
        test.setName("this is name");
        test.setUserId(1);
        mongoTemplate.save(test);
    }

    @Override
    public UserTest findUserTest(Integer Id, String name, String body) {
//        Query query = new Query();
//        query.addCriteria(Criteria.where("ID").is(Id));
//        Pattern pattern = Pattern.compile("^.*" + name + ".*$", Pattern.CASE_INSENSITIVE);
//        query.addCriteria(Criteria.where("Name").regex(pattern));
//
//        UserTest userTest = mongoTemplate.findOne(query, UserTest.class);
//        System.out.println(userTest);
        // 1、消除@DBRef引用对象中的"$id"的"$"符号
//        RemoveDollarOperation removeDollarOperation = new RemoveDollarOperation("newDepartmentFieldName", "department");
//
//        // 2、使用mongodb $lookup实现左连接部门表
//        LookupOperation lookupOperation = LookupOperation.newLookup().from("department")
//                .localField("newDepartmentFieldName.id").foreignField("_id").as("newDepartment");
//
//        TypedAggregation aggregation = Aggregation.newAggregation(Employee.class, removeDollarOperation, lookupOperation);
//        AggregationResults<Document> results = mongoTemplate.aggregate(aggregation, Document.class);
        return null;
    }
}
