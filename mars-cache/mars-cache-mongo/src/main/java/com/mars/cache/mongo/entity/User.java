package com.mars.cache.mongo.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 9:59
 */
@Document(collection = "user")
@Data
public class User implements Serializable {

    private Integer id;

    private String name;

    private Integer body;
}
