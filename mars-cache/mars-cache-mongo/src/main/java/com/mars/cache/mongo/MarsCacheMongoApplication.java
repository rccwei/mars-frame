package com.mars.cache.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCacheMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCacheMongoApplication.class, args);
    }

}
