package com.mars.cache.mongo.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/25 20:46
 */
@Data
@Document(collection = "user_test")
public class UserTest implements Serializable {

    private Integer ID;

    private String Name;

    private String Body;

    private Integer UserId;
}
