package com.mars.cache.mongo.service.impl;

import com.mars.cache.mongo.entity.User;
import com.mars.cache.mongo.service.UserService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.regex.Pattern;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 10:01
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private MongoTemplate mongoTemplate;

    @Override
    public void save() {
        User user = new User();
        user.setId(1);
        user.setName("this is name");
        user.setBody(23);
        mongoTemplate.save(user);
    }


    @Override
    public User findUser(Integer Id, String name, String body) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(Id));
        Pattern pattern = Pattern.compile("^.*" + name + ".*$", Pattern.CASE_INSENSITIVE);
        query.addCriteria(Criteria.where("name").regex(pattern));
        
        User one = mongoTemplate.findOne(query, User.class);
        System.out.println(one);
        return one;
    }
}
