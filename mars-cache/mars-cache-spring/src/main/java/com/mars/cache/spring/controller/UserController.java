package com.mars.cache.spring.controller;

import com.mars.cache.spring.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 15:26
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    @GetMapping("/getUsername")
    public String getUsername(Integer userId) {
        return userService.getUsername(userId);
    }
}
