package com.mars.cache.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class MarsCacheSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCacheSpringApplication.class, args);
    }

}
