package com.mars.cache.spring.service;

public interface UserService {

    String getUsername(Integer userId);
}
