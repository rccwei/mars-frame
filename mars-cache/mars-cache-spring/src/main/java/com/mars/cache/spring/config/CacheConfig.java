package com.mars.cache.spring.config;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 15:37
 */
@Configuration
public class CacheConfig {

    /**
     * 自定义缓存key生成
     *
     * @return KeyGenerator
     */
    @Bean("keyGenerator")
    public KeyGenerator keyGenerator() {
        return (target, method, params) -> method.getName() + "[" + Arrays.asList(params).toString() + "]";
    }
}
