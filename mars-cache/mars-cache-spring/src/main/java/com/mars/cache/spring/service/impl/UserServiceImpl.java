package com.mars.cache.spring.service.impl;

import com.mars.cache.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 15:22
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {
    /**
     * cacheNames: 用来指定缓存组件的名字，将方法的返回结果放在哪个缓存中，可以是数组的方式，支持指定多个缓存。
     * key: 缓存数据时使用的 key。默认使用的是方法参数的值。可以使用 spEL 表达式去编写。
     * keyGenerator: key 的生成器，可以自己指定 key 的生成器，通过这个生成器来生成 key。
     * condition :符合条件的情况下才缓存。方法返回的数据要不要缓存，可以做一个动态判断。
     * unless: 否定缓存。当 unless 指定的条件为 true ，方法的返回值就不会被缓存。
     * sync: 是否使用异步模式。默认是方法执行完，以同步的方式将方法返回的结果存在缓存中。
     *
     * @param userId 用户ID
     * @return String
     */
    @Override
    @Cacheable(value = "users", key = "#userId", unless = "#userId>0", condition = "#userId>0", sync = true)
    public String getUsername(Integer userId) {
        log.info("select db");
        return "wq";
    }


}
