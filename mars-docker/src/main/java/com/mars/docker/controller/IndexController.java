package com.mars.docker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/02 10:16
 */
@RestController
public class IndexController {

    @GetMapping("/index")
    public String index() {
        return "success";
    }
}
