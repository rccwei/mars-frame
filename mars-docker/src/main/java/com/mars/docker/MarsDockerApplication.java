package com.mars.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsDockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsDockerApplication.class, args);
    }

}
