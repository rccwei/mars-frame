package com.mars.crawler.webmagic.utils;

import org.apache.commons.codec.digest.DigestUtils;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/13 14:09
 */
public class XhsUtils {
//    public static void main(String[] args) {
//        System.out.println(DigestUtils.md5(S1("/api/sns/v5/note/comment/list?note_id=5f1bf342000000000100734c&num=10&show_priority_sub_comments=1&source=search%26keyword%3D%E8%BE%85%E9%A3%9F%26searchId%3D28s5pkk2g47sprzv209jiplatform=iOS&version=6.98&build=6980135&deviceId=3A60E1C1-5B08-440E-81F5-806ACA204002&bundle=com.xingin.discover"));
//)}

    private static String S1(String params) {
        byte[] md5Bytes = DigestUtils.md5(params);
        byte[] newKey = new byte[8];
        byte[] encrypt = encrypt(params.getBytes(), newKey);
        return DigestUtils.sha256Hex(encrypt);
    }


    public static byte[] encrypt(byte[] content, byte[] password) {
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");// 创建AES的Key生产者

            kgen.init(128, new SecureRandom(content));// 利用用户密码作为随机数初始化出
            // 128位的key生产者
            //加密没关系，SecureRandom是生成安全随机数序列，password.getBytes()是种子，只要种子相同，序列就一样，所以解密只要有password就行
            SecretKey secretKey = kgen.generateKey();// 根据用户密码，生成一个密钥
            byte[] enCodeFormat = secretKey.getEncoded();// 返回基本编码格式的密钥，如果此密钥不支持编码，则返回
            // null。
            SecretKeySpec key = new SecretKeySpec(enCodeFormat, "AES");// 转换为AES专用密钥
            Cipher cipher = Cipher.getInstance("AES");// 创建密码器
            cipher.init(Cipher.ENCRYPT_MODE, key);// 初始化为加密模式的密码器
            byte[] result = cipher.doFinal(password);// 加密

            return result;

        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
