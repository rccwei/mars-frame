package com.mars.crawler.webmagic.manager;

import com.mars.crawler.webmagic.constant.Constant;
import com.mars.crawler.webmagic.processor.BlogPageProcessor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.downloader.HttpClientDownloader;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/24 22:45
 */
@Component
public class CrawlerManager {


    @Async
    public void execute() {
        new Thread(() -> Spider.create(new BlogPageProcessor()).
                setDownloader(new HttpClientDownloader()).
                addUrl(Constant.DOUBAN_URL).thread(50).run()).start();
    }
}
