package com.mars.crawler.webmagic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCrawlerWebmagicApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCrawlerWebmagicApplication.class, args);
    }

}
