package com.mars.crawler.webmagic.controller;

import com.mars.crawler.webmagic.manager.CrawlerManager;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/24 22:38
 */
@RestController
@AllArgsConstructor
public class BlogController {

    private final CrawlerManager crawlerManager;

    @RequestMapping("/acquire")
    public String acquireData() {
        crawlerManager.execute();
        return "success";
    }
}
