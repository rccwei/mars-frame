package com.mars.crawler.jsoup.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.crawler.jsoup.entity.Content;


public interface ContentMapper extends BaseMapper<Content> {
}
