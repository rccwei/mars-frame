package com.mars.crawler.jsoup.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.crawler.jsoup.entity.Content;

import java.io.IOException;

public interface IContentService extends IService<Content> {

    /**
     * 采集商品
     *
     * @param keyword 爬取名称
     */
    void getContent(String keyword) throws IOException;
}
