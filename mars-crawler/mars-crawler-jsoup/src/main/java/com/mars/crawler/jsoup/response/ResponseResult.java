package com.mars.crawler.jsoup.response;

import lombok.Data;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/14 14:44
 */
@Data
public class ResponseResult {

    private Integer code;
    private String msg;

    public ResponseResult() {
    }

    public ResponseResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public static ResponseResult error(Integer code, String error) {
        return new ResponseResult(code, error);
    }
}
