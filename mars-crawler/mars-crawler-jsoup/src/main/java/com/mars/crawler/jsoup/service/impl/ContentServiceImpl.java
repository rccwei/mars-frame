package com.mars.crawler.jsoup.service.impl;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.crawler.jsoup.entity.Content;
import com.mars.crawler.jsoup.mapper.ContentMapper;
import com.mars.crawler.jsoup.service.IContentService;
import com.mars.crawler.jsoup.utils.JDHtmlParseUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class ContentServiceImpl extends ServiceImpl<ContentMapper, Content> implements IContentService {

    @Override
    public void getContent(String keyword) throws IOException {
        List<Content> contents = JDHtmlParseUtil.parseJD(keyword);
        if (CollectionUtils.isNotEmpty(contents)) {
            saveBatch(contents);
            log.info("商品爬取完成入库成功~");
        }

    }
}
