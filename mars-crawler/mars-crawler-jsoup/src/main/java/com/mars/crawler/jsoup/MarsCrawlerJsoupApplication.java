package com.mars.crawler.jsoup;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan("com.mars.crawler.jsoup.mapper")
@EnableScheduling
public class MarsCrawlerJsoupApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCrawlerJsoupApplication.class, args);
    }

}
