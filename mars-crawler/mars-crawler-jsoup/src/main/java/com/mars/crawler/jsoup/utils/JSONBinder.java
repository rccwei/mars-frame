package com.mars.crawler.jsoup.utils;

import com.fasterxml.jackson.core.JsonGenerator.Feature;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;

import java.io.IOException;
import java.text.SimpleDateFormat;


public final class JSONBinder<T> {

    static final ObjectMapper DEFAULT_OBJECT_MAPPER;

    static {
        DEFAULT_OBJECT_MAPPER = createMapper();
    }

    public static ObjectMapper getDefaultMapper() {
        return DEFAULT_OBJECT_MAPPER;
    }

    public static <T> JSONBinder<T> binder(Class<T> beanClass, Class<?>... elementClasses) {
        return new JSONBinder<>(beanClass, elementClasses);
    }

    public static <T> JSONBinder<T> binder(ObjectMapper objectMapper, Class<T> beanClass, Class<?>... elementClasses) {
        return new JSONBinder<>(objectMapper, beanClass, elementClasses);
    }

    private static ObjectMapper createMapper() {
        ObjectMapper mapper = new ObjectMapper();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mapper.setDateFormat(dateFormat);
        final AnnotationIntrospector primary = new JacksonAnnotationIntrospector();
        mapper.registerModule(new JavaTimeModule())
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module());
        mapper.setAnnotationIntrospectors(primary, primary);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME, true);
        mapper.enable(Feature.WRITE_BIGDECIMAL_AS_PLAIN);
        mapper.enable(Feature.WRITE_NUMBERS_AS_STRINGS);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS);
        return mapper;
    }

    private final Class<T> beanClass;

    private final Class<?>[] elementClasses;

    ObjectMapper objectMapper;

    private JSONBinder(Class<T> beanClass, Class<?>... elementClasses) {
        this.beanClass = beanClass;
        this.elementClasses = elementClasses;
        this.objectMapper = DEFAULT_OBJECT_MAPPER;
    }

    private JSONBinder(ObjectMapper objectMapper, Class<T> beanClass, Class<?>... elementClasses) {
        this.beanClass = beanClass;
        this.elementClasses = elementClasses;
        this.objectMapper = objectMapper;
    }

    public T fromJSON(String json) {
        try {
            return elementClasses == null || elementClasses.length == 0 ? objectMapper.readValue(json, beanClass) : fromJSONToGeneric(json);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private T fromJSONToGeneric(String json) throws IOException {
        return objectMapper.readValue(json, objectMapper.getTypeFactory().constructParametricType(beanClass, elementClasses));
    }

    public String toJSON(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}