package com.mars.crawler.jsoup.utils;

import org.apache.commons.lang3.StringUtils;

public class JSONConvert {

    public static <T> T fromString(String value, Class<T> targetClass, Class<?>... elementClasses) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return JSONBinder.binder(targetClass, elementClasses).fromJSON(value);
    }

    @SuppressWarnings("unchecked")
    public static <T> String toString(T value) {
        if (value == null) {
            return null;
        }
        return JSONBinder.binder((Class<T>) value.getClass()).toJSON(value);
    }

}