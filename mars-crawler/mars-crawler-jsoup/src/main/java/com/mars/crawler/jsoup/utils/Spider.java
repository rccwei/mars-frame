package com.mars.crawler.jsoup.utils;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public class Spider {

    public static void main(String[] args) throws IOException, InterruptedException {
        // 图片保存地址
        String desktopPath = System.getProperty("user.home") + "\\Desktop\\images";
        for (int i = 1; i <= 2; i++) {
            System.out.println("正在爬取第" + i + "页数据");
            String data = sendGet(String.format("https://more.yesky.com/c/shtml/592939_25152_display_time_%s.json?_=1690986133997", i));
            downloadImage(desktopPath, data);
        }

    }

    /**
     * 下载图片
     *
     * @param desktopPath
     * @param data
     * @throws InterruptedException
     * @throws IOException
     */
    private static void downloadImage(String desktopPath, String data) throws InterruptedException, IOException {
        JSONObject jsonObject = JSONObject.parseObject(data);
        JSONArray list = jsonObject.getJSONArray("list");
        for (int i = 0; i < list.size(); i++) {
            String title = list.getJSONObject(i).getString("title");
            String lead9x16Large = list.getJSONObject(i).getString("lead9x16Large");
            System.out.println("title:" + title);
            System.out.println("lead9x16Large:" + lead9x16Large);
            System.out.println("正在下载图片:" + title);
            Thread.sleep(200);
            download(lead9x16Large, desktopPath, title + ".jpg");
            System.out.println("==========================");
        }
    }


    /**
     * 文件下载到指定路径
     *
     * @param urlString 链接
     * @param savePath  保存路径
     * @param filename  文件名
     * @throws Exception
     */
    public static void download(String urlString, String savePath, String filename) throws IOException {
        // 构造URL
        URL url = new URL(urlString);
        // 打开连接
        URLConnection con = url.openConnection();
        //设置请求超时为20s
        con.setConnectTimeout(20 * 1000);
        //文件路径不存在 则创建
        File sf = new File(savePath);
        if (!sf.exists()) {
            sf.mkdirs();
        }
        //jdk 1.7 新特性自动关闭
        try (InputStream in = con.getInputStream();
             OutputStream out = new FileOutputStream(sf.getPath() + "\\" + filename)) {
            //创建缓冲区
            byte[] buff = new byte[1024];
            int n;
            // 开始读取
            while ((n = in.read(buff)) >= 0) {
                out.write(buff, 0, n);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /**
     * 发送get请求
     *
     * @param url url
     * @return String
     */
    public static String sendGet(String url) {
        String result = "";
        BufferedReader in = null;
        try {
            URL realUrl = new URL(url);
            URLConnection connection = realUrl.openConnection();
            connection.setRequestProperty("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36");
            connection.setRequestProperty("Accept", "application/json, text/javascript, */*; q=0.01");
            connection.connect();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "GBK"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送GET请求出现异常！" + e);
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return result;
    }


}
