package com.mars.crawler.jsoup.controller;

import com.alibaba.fastjson.JSONObject;
import com.mars.crawler.jsoup.request.UserRequest;
import com.mars.crawler.jsoup.service.IContentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;


@RestController
public class IndexController {


    @Resource
    private IContentService contentService;

    @GetMapping("/index")
    public String index(String keyword) throws IOException {
        contentService.getContent(keyword);
        return "爬取成功";
    }


}
