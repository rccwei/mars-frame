package com.mars.crawler.jsoup.utils;

import com.alibaba.fastjson.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/22 12:06
 */
public class CrawlUtil1s {
    public static void main(String[] args) {

        String url = "https://apps.webofknowledge.com/UA_AdvancedSearch_input.do?product=UA&search_mode=AdvancedSearch&replaceSetId=&goToPageLoc=SearchHistoryTableBanner&SID=8FYOR5DPPv7UukGLILc&errorQid=34#SearchHistoryTableBanner";
        try {
            Connection.Response response = Jsoup.connect(url).timeout(10 * 1000)
                    .method(Connection.Method.GET)
                    .execute();
            if (response.statusCode() == 200) {
                String body = response.body();
                System.out.println(body);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
