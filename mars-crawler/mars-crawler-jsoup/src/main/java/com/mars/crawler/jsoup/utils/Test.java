package com.mars.crawler.jsoup.utils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * @author wq
 * @date 2022/11/25 12:11
 */
public class Test {

    public static void main(String[] args) throws IOException {



        Document document = Jsoup.connect("https://www.baidu.com/s?ie=utf-8&medium=2&bsst=1&rsv_dl=news_t_sk&cl=2&wd=%E4%B8%96%E7%95%8C%E6%9D%AF&tn=news&rsv_bp=1&oq=&rsv_btype=t&f=8").get();
        Elements elements1 = document.getElementsByClass("result-op c-container xpath-log new-pmd");
        for(Element element : elements1){
            Elements elements = element.select("div").select("h3");
            System.out.println(elements.get(0).select("a").attr("href"));
            Document document1 = Jsoup.connect(elements.get(0).select("a").attr("href")).get();
            Elements elements2 = document1.select("document1");
            for(Element element1 : elements2){
                System.out.println(element1.text());
            }
            System.out.println(elements.get(0).text());
        }
//        WebClient webClient = new WebClient(BrowserVersion.CHROME);
//        webClient.getOptions().setJavaScriptEnabled(true);
//        webClient.getOptions().setCssEnabled(false);
//        webClient.getOptions().setActiveXNative(false);
//        webClient.getOptions().setCssEnabled(false);
//        webClient.getOptions().setThrowExceptionOnScriptError(false);
//        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
//        webClient.getOptions().setTimeout(10000);
//        Page page = webClient.getPage("https://baijiahao.baidu.com/s?id=1750418191127146242&wfr=spider&for=pc");
//        WebResponse webResponse = page.getWebResponse();
//        System.out.println(webResponse.getContentAsString());
//        System.out.println(webResponse.getStatusMessage());
//        webClient.waitForBackgroundJavaScript(10000);
//        Document document = Jsoup.connect("https://so.toutiao.com/search?dvpf=pc&source=input&keyword=%E4%B8%96%E7%95%8C%E6%9D%AF&pd=information&action_type=search_subtab_switch&page_num=0&from=news&cur_tab_title=news").get();
//        System.out.println(document.toString());
//        String s = HttpUtils.sendGet("https://so.toutiao.com/search?dvpf=pc&source=search_subtab_switch&keyword=%E4%B8%96%E7%95%8C%E6%9D%AF&page_num=0&pd=information&action_type=search_subtab_switch&search_id=&from=news&cur_tab_title=news", "");
//        System.out.println(s);
    }
}
