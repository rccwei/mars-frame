package com.mars.crawler.jsoup.entity;

import lombok.Data;

@Data
public class Content {

    /**
     * 商品标题
     */
    private String title;
    /**
     * 商品图片
     */
    private String img;
    /**
     * 商品价格
     */
    private String price;
    /**
     * 店铺名称
     */
    private String shopName;
}
