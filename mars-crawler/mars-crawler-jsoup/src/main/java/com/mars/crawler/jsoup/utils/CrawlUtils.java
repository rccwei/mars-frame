package com.mars.crawler.jsoup.utils;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;


public class CrawlUtils {
    public static void main(String[] args) {
//        String url = "https://baijiahao.baidu.com/s?id=1750416953075310009&wfr=spider&for=pc";
//        try {
//            Connection.Response response = Jsoup.connect(url).timeout(10 * 1000)
//                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36")
//                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
//                    .method(Connection.Method.GET)
//                    .execute();
//            if (response.statusCode() == 200) {
//                String body = response.body();
//                Document document = Jsoup.parse(body);
//                System.out.println(document);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


        String url = "https://baijiahao.baidu.com/s?id=1750416953075310009&wfr=spider&for=pc";
        try {
            Connection.Response response = Jsoup.connect(url).timeout(10 * 1000)
                    .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36")
                    .header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
                    .method(Connection.Method.GET)
                    .execute();
            if (response.statusCode() == 200) {
                String body = response.body();
                Document document = Jsoup.parse(body);
                System.out.println(document);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
