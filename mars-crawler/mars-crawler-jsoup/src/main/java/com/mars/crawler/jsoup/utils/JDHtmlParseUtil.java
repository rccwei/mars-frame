package com.mars.crawler.jsoup.utils;

import com.mars.crawler.jsoup.entity.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JDHtmlParseUtil {


    /**
     * 解析京东网页
     *
     * @param keywords
     * @return
     * @throws IOException
     */
    public static List<Content> parseJD(String keywords) throws IOException {
        String url = "https://search.jd.com/Search?keyword=" + keywords;
        Document document = Jsoup.parse(new URL(url), 30000);
        Element element = document.getElementById("J_goodsList");

        //  获取 J_goodsList 中的所有 li
        Elements elements = element.getElementsByTag("li");
        List<Content> goodsList = new ArrayList<>();

        //  获取元素中的内容，这里是 el ，就是每一个li标签
        for (Element el : elements) {
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
            String shopName = el.getElementsByClass("curr-shop hd-shopname").eq(0).text();
            System.out.println(shopName);
            Content content = new Content();
            content.setShopName(shopName);
            content.setImg(img);
            content.setPrice(price);
            content.setTitle(title);
            goodsList.add(content);
        }
        return goodsList;
    }

}
