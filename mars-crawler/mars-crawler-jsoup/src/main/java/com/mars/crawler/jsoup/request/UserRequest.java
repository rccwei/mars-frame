package com.mars.crawler.jsoup.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author 程序猿Mars
 * @date 2021-09-06 12:35
 */
@Data
public class UserRequest {

    @NotBlank(message = "名称不能为空")
    private String name;

    @NotNull(message = "请选择状态")
    private Integer status;
}
