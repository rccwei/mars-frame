package com.mars.common.httpclient.result;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 14:23
 */
@Data
@AllArgsConstructor
public class HttpResult {
    /**
     * 响应的状态码
     */
    private int code;
    /**
     * 响应的响应体
     */
    private String body;
}
