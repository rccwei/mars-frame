package com.mars.common.httpclient.controller;

import com.mars.common.httpclient.result.R;
import com.mars.common.httpclient.utils.IpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 14:26
 */
@Slf4j
@RestController
public class IndexController {


    /**
     * 获取归属地地址
     *
     * @return HttpResult
     */
    @GetMapping("/getAddress")
    public R getAddress() throws IOException {
        String netIp = IpUtils.getNetIp();
        log.info("当前公网ip地址:" + netIp);
        return R.success(IpUtils.getAddress(netIp));
    }
}
