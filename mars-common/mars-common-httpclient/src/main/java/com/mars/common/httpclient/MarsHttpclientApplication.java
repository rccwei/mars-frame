package com.mars.common.httpclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsHttpclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsHttpclientApplication.class, args);
    }

}
