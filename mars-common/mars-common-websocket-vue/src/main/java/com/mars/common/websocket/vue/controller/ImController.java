package com.mars.common.websocket.vue.controller;

import com.mars.common.websocket.vue.server.WebSocketServer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author wq
 * @date 2021-06-20 21:24
 */
@RestController
@RequestMapping("/api/im")
public class ImController {

    /**
     * 发送消息
     *
     * @param message  消息内容
     * @param toUserId 消息ID
     * @return
     * @throws IOException IOException
     */
    @GetMapping("/push")
    public ResponseEntity<String> sendMessage(String message, String toUserId, Integer type) throws IOException {
        WebSocketServer.send(message + "", toUserId, type);
        return ResponseEntity.ok("MSG SEND SUCCESS");
    }
}
