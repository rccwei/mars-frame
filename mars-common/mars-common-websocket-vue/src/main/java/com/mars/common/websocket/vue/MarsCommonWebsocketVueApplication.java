package com.mars.common.websocket.vue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonWebsocketVueApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonWebsocketVueApplication.class, args);
    }

}
