package com.mars.common.websocket.vue.controller;

import com.mars.common.websocket.vue.server.WebSocketServer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * @author wq
 * @date 2021-06-20 21:24
 */
@Controller
@RequestMapping("/im")
public class IndexController {

    @GetMapping("index")
    public String index() {
        return "index";
    }


}
