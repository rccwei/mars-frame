package com.mars.common.websocket.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @author wq
 * @date 2022/11/9 16:20
 */
@Data
@Builder
public class User {
    private String id;
    private String name;
    private Integer age;
    private String desc;
}
