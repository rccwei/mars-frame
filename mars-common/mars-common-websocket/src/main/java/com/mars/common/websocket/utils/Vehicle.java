package com.mars.common.websocket.utils;

public abstract class Vehicle {
    private String num;
    private String brand;
    private double rent;


    public String getNum() {
        return num;
    }


    public void setNum(String num) {
        this.num = num;
    }


    public String getBrand() {
        return brand;
    }


    public void setBrand(String brand) {
        this.brand = brand;
    }


    public double getRent() {
        return rent;
    }


    public void setRent(double rent) {
        this.rent = rent;
    }


    public Vehicle() {
    }

    public Vehicle(String num, String brand, double rent) {
        this.num = num;
        this.brand = brand;
        this.rent = rent;
    }

    public abstract double totalmoney(int days, double rent);

    public abstract boolean equals(Vehicle o);
}
