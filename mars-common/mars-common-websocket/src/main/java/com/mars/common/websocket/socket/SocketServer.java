package com.mars.common.websocket.socket;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/08 11:01
 */
public class SocketServer {
    public static void main(String[] args) {
        try {
            // 创建服务端socket
            ServerSocket serverSocket = new ServerSocket(8088);
            // 创建客户端socket
            Socket socket = new Socket();
            //循环监听等待客户端的连接
            while (true) {
                // 监听客户端
                socket = serverSocket.accept();
                ServerThread thread = new ServerThread(socket);
                thread.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
