package com.mars.common.websocket.utils;
public class Bus extends Vehicle {
    private String seat;
    public String getSeat() {
        return seat;
    }
    public void setSeat(String seat) {
        this.seat = seat;
    }
    public Bus(String num, String brand, double rent, String seat) {
        super(num, brand, rent);
        this.seat = seat;
    }
    public double totalmoney(int days, double rent) {
        if (days>=3){
            return days*rent*0.9;
        }else if (days>=7){
            return days*rent*0.8;
        }else if (days>=30){
            return days*rent*0.7;
        }else if (days>=150){
            return days*rent*0.6;
        }
        return days*rent;
    }
    public boolean equals(Vehicle o) {
        return false;
    }
}