package com.mars.common.websocket.socket;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/08 11:02
 */
public class SocketClient {

    public static void main(String[] args) throws InterruptedException {
        try {
            Menu m = new Menu();
            TelNoteRegex reg = new TelNoteRegex();
            while (true) {
                m.mainMenu();
                //调用校验用户输入的数字方法
                int key = reg.menuRegex(1, 5);
                switch (key) {
                    case 1:
                        connect();
                        break;
                    case 2:
                        login();
                        break;
                    case 3:
                        query();
                        break;
                    case 4:
                        queryAvg();
                        break;
                    case 5:
                        logout();
                        break;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 退出
     *
     * @throws IOException
     */
    private static void logout() throws IOException {
        System.exit(0);
    }

    /**
     * 查询平均值
     *
     * @throws IOException
     */
    private static void queryAvg() throws IOException {
        // 和服务器创建连接
        Socket socket = new Socket("localhost", 8088);
        // 要发送给服务器的信息
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        pw.write("QUERYAVG");
        pw.flush();

        socket.shutdownOutput();

        // 从服务器接收的信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println("服务器返回信息：" + info);
        }

        br.close();
        is.close();
        os.close();
        pw.close();
        socket.close();
    }

    /**
     * 查询学科
     *
     * @throws IOException
     */
    private static void query() throws IOException {
        // 和服务器创建连接
        Socket socket = new Socket("localhost", 8088);
        // 要发送给服务器的信息
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        Menu m = new Menu();
        m.subjectMenu();
        TelNoteRegex reg = new TelNoteRegex();
        int key = reg.menuRegex(1, 3);
        pw.write("QUERY:" + key);
        pw.flush();

        socket.shutdownOutput();

        // 从服务器接收的信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println("服务器返回信息：" + info);
        }

        br.close();
        is.close();
        os.close();
        pw.close();
        socket.close();
    }

    /**
     * 登录
     *
     * @throws IOException
     */
    private static void login() throws IOException {

        // 和服务器创建连接
        Socket socket = new Socket("localhost", 8088);
        // 要发送给服务器的信息
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入用户名(lisi):");
        String username = scanner.nextLine();
        pw.write("LOGIN:" + username);
        pw.flush();

        socket.shutdownOutput();

        // 从服务器接收的信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println("服务器返回信息：" + info);
        }

        br.close();
        is.close();
        os.close();
        pw.close();
        socket.close();
    }

    /**
     * 连接
     *
     * @throws IOException
     */
    private static void connect() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入ip端口号(空格隔开):");
        String connect = scanner.nextLine();
        String[] data = connect.split(" ");
        // 和服务器创建连接
        Socket socket = new Socket(data[0], Integer.parseInt(data[1]));

        // 要发送给服务器的信息
        OutputStream os = socket.getOutputStream();
        PrintWriter pw = new PrintWriter(os);
        pw.write("CONNECT");
        pw.flush();

        socket.shutdownOutput();

        // 从服务器接收的信息
        InputStream is = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String info = null;
        while ((info = br.readLine()) != null) {
            System.out.println("服务器返回信息：" + info);
        }

        br.close();
        is.close();
        os.close();
        pw.close();
        socket.close();
    }

}
