package com.mars.common.websocket.controller;

import com.mars.common.websocket.server.WebSocketServer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * @author wq
 * @date 2021-06-20 21:24
 */
@RestController
@RequestMapping("/api/im")
public class ImController {

    @GetMapping("index")
    public ResponseEntity<String> index() {
        return ResponseEntity.ok("请求成功");
    }

    @GetMapping("page")
    public ModelAndView page() {
        return new ModelAndView("websocket");
    }

    /**
     * 发送消息
     *
     * @param message  消息内容
     * @param toUserId 消息ID
     * @return
     * @throws IOException IOException
     */

    @GetMapping("/push")
    public ResponseEntity<String> sendMessage(String message, String toUserId, Integer type) throws IOException {
        WebSocketServer.send(message, toUserId, type);
        return ResponseEntity.ok("MSG SEND SUCCESS");
    }
}
