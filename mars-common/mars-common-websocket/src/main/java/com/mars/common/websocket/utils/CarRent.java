package homework.exam;

import com.mars.common.websocket.utils.Bus;
import com.mars.common.websocket.utils.Cars;

public class CarRent {


    public Cars[] carMake() {
        Cars c1 = new Cars("京NY28588", "凯迪拉克", 350, "ct5");
        Cars c2 = new Cars("京CNY3284", "凯迪拉克", 400, "xt6");
        Cars c3 = new Cars("京NT37465", "上汽大众", 300, "帕萨特");
        Cars c4 = new Cars("京NT58788", "上汽大众", 400, "辉昂");
        Cars c5 = new Cars("京NT91456", "一汽奥迪", 500, "A4L");
        Cars c6 = new Cars("京NT86786", "一汽奥迪", 550, "Q5");
        Cars[] arr1 = {c1, c2, c3, c4, c5, c6};
        return arr1;
    }

    public Bus[] busMake() {
        Bus b1 = new Bus("京6566754", "金杯", 800, "16座");
        Bus b2 = new Bus("京9562446", "金龙", 800, "16座");
        Bus b3 = new Bus("京2035944", "金杯", 1500, "34座");
        Bus b4 = new Bus("京9745516", "金龙", 1400, "34座");
        Bus[] arr2 = {b1, b2, b3, b4};
        return arr2;
    }
}