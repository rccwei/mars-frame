package com.mars.common.websocket.socket;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/08 11:01
 */
public class ServerThread extends Thread {
    private Socket socket = null;
    //用户名称
    private List<String> list = Collections.singletonList("lisi");
    //学科列表
    private static Map<String, Set<Subject>> map = new HashMap<>();

    static {
        Set<Subject> subjects = new HashSet<>();


        Subject subject2 = new Subject();
        subject2.setName("Math");
        subject2.setScore(90);
        subjects.add(subject2);

        Subject subject1 = new Subject();
        subject1.setName("Chinese");
        subject1.setScore(90);
        subjects.add(subject1);

        Subject subject3 = new Subject();
        subject3.setName("English");
        subject3.setScore(90);
        subjects.add(subject3);

        map.put("lisi", subjects);
    }


    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        InputStream is = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        OutputStream os = null;
        PrintWriter pw = null;
        try {
            is = socket.getInputStream();
            isr = new InputStreamReader(is);
            br = new BufferedReader(isr);

            String info = null;

            while ((info = br.readLine()) != null) {
                System.out.println("info:" + info);
                if (info.equalsIgnoreCase("CONNECT")) {
                    os = socket.getOutputStream();
                    assert os != null;
                    pw = new PrintWriter(os);
                    pw.write("连接成功");
                    pw.flush();
                } else if (info.contains("LOGIN")) {

                    String username = info.split(":")[1];
                    if (list.contains(username)) {
                        os = socket.getOutputStream();
                        assert os != null;
                        pw = new PrintWriter(os);
                        pw.write("登录成功,你的密码:" + "123456");
                        pw.flush();
                    } else {
                        os = socket.getOutputStream();
                        assert os != null;
                        pw = new PrintWriter(os);
                        pw.write("用户不存在");
                        pw.flush();
                    }
                } else if (info.contains("QUERY:")) {
                    String subjectIndex = info.split(":")[1];
                    os = socket.getOutputStream();
                    assert os != null;
                    pw = new PrintWriter(os);
                    Set<Subject> lisi = map.get("lisi");
                    ArrayList<Subject> list = new ArrayList<>(lisi);
                    pw.write("你的" + list.get(Integer.parseInt(subjectIndex) - 1).getName() + "得分:" + list.get(Integer.parseInt(subjectIndex) - 1).getScore());
                    pw.flush();
                } else if (info.equals("QUERYAVG")) {
                    os = socket.getOutputStream();
                    assert os != null;
                    pw = new PrintWriter(os);
                    double avgScore = new ArrayList<>(map.get("lisi")).stream().
                            mapToInt(Subject::getScore).average().getAsDouble();
                    System.out.println(avgScore);
                    pw.write("你的平均分:" + avgScore);
                    pw.flush();
                } else {
                    os = socket.getOutputStream();
                    assert os != null;
                    pw = new PrintWriter(os);
                    pw.write("BYE BYE !!!");
                    pw.flush();
                }

            }
            socket.shutdownInput();


        } catch (Exception e) {
            // TODO: handle exception
        } finally {
            //关闭资源
            try {
                if (pw != null)
                    pw.close();
                if (os != null)
                    os.close();
                if (br != null)
                    br.close();
                if (isr != null)
                    isr.close();
                if (is != null)
                    is.close();
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
