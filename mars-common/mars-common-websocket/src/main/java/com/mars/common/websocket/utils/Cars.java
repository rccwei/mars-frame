package com.mars.common.websocket.utils;

public class Cars extends Vehicle {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Cars(String brand, String type) {
        this.type = type;

    }

    public Cars(String num, String brand, double rent, String type) {
        super(num, brand, rent);
        this.type = type;
    }

    public String toString() {
        return "Cars{" +
                "type='" + type + '\'' +
                '}';
    }

    public double totalmoney(int days, double rent) {
        if (days > 7) {
            return days * rent * 0.9;
        } else if (days > 30) {
            return days * rent * 0.8;
        } else if (days > 150) {
            return days * rent * 0.7;
        }
        return days * rent;
    }

    public boolean equals(Vehicle o) {
        if (o instanceof Cars) {
            Cars cars = (Cars) o;
            return this.getType().equals(cars.getType()) && this.getBrand().equals(o.getBrand());
        }
        return false;
    }
}
