package com.mars.common.websocket.socket;

import java.util.Scanner;

public class TelNoteRegex {
    //对菜单项校验
    @SuppressWarnings("resource")
    public int menuRegex(int min, int max) {
        String regex = "[0-9]{1}";
        Scanner sc = new Scanner(System.in);
        while (true) {
            String input = sc.nextLine();
            //根据合法性的校验
            if (input.matches(regex)) {
                int key = Integer.parseInt(input);
                if (key >= min && key <= max) {
                    return key;
                } else {
                    System.out.println("输入的菜单项不符合要求!");
                }
            } else {
                System.out.println("输入内容不正确!");
            }
        }

    }


}