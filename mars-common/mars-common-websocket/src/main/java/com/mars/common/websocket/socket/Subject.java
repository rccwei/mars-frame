package com.mars.common.websocket.socket;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/08 12:01
 */
public class Subject implements Serializable {

    /**
     * 学科名称
     */
    private String name;
    /**
     * 得分
     */
    private Integer score;

    public Subject(String name, Integer score) {
        this.name = name;
        this.score = score;
    }

    public Subject() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
