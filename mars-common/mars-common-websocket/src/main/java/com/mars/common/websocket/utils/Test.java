package com.mars.common.websocket.utils;


import com.mars.common.websocket.entity.User;

import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        //创建一个user对象
        User user = User.builder().id("1").age(20).name("张三").desc("programmer").build();
        //使用ProtostuffUtils序列化
        byte[] data = ProtostuffUtils.serialize(user);
        System.out.println("序列化后：" + Arrays.toString(data));
        User result = ProtostuffUtils.deserialize(data, User.class);
        System.out.println("反序列化后：" + result.toString());
    }
}
