const service = axios.create({
//    baseURL: 'http://www.mars.vip/vue',
    baseURL: 'http://localhost:8080',
    timeout: 5000
});

service.interceptors.request.use(config => {
    let token = JSON.parse(localStorage.getItem("mars-token"));
    if (token) {
        config.headers.token = token;
    }
    return config;
}, function (error) {
    return Promise.reject(error);
});

service.interceptors.response.use(response => {
    if (response.data.code == '9999') {
        parent.location.href ='';
    }
    if(response.request.responseType == 'arraybuffer') {
        return response;
    }
    return response.data;
});

//获取URL参数
function getQueryString(name) {
   let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
   let r = window.location.search.substr(1).match(reg);
   if (r != null) return decodeURI(r[2]);
   return null;
}

//打开页面
function openPage(title, url, vueObj) {
   parent.layer.open({
       type: 2,
       title: title,
       area: ['800px', '550px'],
       content: url,
       end: function() {
           vueObj.handleQuery();
       }
   });
}

//关闭页面
function closePage() {
   let index = parent.layer.getFrameIndex(window.name);
   parent.layer.close(index);
}

//查询
function query(url, queryParam, vueObj) {
    service.post(url, queryParam)
    .then((res) => {
        if (res.code == '0000') {
            if (res.data.count) {
                vueObj.tableData = res.data.list;
                vueObj.total = res.data.count;
            } else {
                vueObj.tableData = res.data;
            }
        }
    });
}

//删除
function deleteById(url, vueObj) {
   parent.layer.confirm('确定删除?', function(index) {
       service.post(url)
       .then((res) => {
           if (res.code == '0000') {
               parent.layer.msg('删除成功');
               parent.layer.close(index);
               vueObj.handleQuery();
           } else {
               parent.layer.msg(res.message);
           }
       });
   });
}

//新增或修改
function addOrUpdate(url, vueObj) {
    vueObj.$refs.formData.validate((valid) => {
        if (valid) {
            let param = vueObj.formData;
            service.post(url, JSON.parse(JSON.stringify(param)))
            .then((res) => {
                if (res.code == '0000') {
                    if (url.indexOf('/add') > 0) {
                        parent.layer.msg('新增成功');
                    } else if (url.indexOf('/update') > 0) {
                        parent.layer.msg('修改成功');
                    }
                    closePage();
                } else {
                    parent.layer.msg(res.message);
                }
            });
       } else {
           return false;
       }
   });
}

//导出
function exports(url, queryParam) {
   service.post(url, queryParam, {responseType:"arraybuffer"})
   .then((res) => {
       let disposition = res.headers['content-disposition'];
       let fileName = decodeURI(disposition.substring(disposition.indexOf('filename=') + 9, disposition.length));
       let blob = new Blob([res.data]);
       let link = document.createElement('a');
       link.href = window.URL.createObjectURL(blob);
       link.download = fileName;
       link.click();
       link.remove();
   });
}
