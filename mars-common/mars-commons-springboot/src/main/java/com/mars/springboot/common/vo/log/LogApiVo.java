package com.mars.springboot.common.vo.log;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;

/**
 * 接口日志列表VO
 *
 * @author jsbb
 */
public class LogApiVo {

    @Schema(title = "ID")
    private Long id;

    @Schema(title = "用户")
    private String fullName;

    @Schema(title = "接口地址")
    private String url;

    @Schema(title = "请求参数")
    private String param;

    @Schema(title = "耗时")
    private Integer time;

    @Schema(title = "IP")
    private String ip;

    @Schema(title = "创建时间")
    private LocalDateTime createTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }
}
