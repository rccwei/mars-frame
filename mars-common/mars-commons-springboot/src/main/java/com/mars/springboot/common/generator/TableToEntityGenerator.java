package com.mars.springboot.common.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 目的: 将数据库DDL语句转换成 JAVA 实体
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-19 19:33:33
 */
public class TableToEntityGenerator {

    /**
     * 数据库IP
     */
    public static final String MYSQL_HOST = "localhost";
    /**
     * 数据库名称
     */
    public static final String MYSQL_DATABASE = "sys";
    /**
     * 用户名
     */
    public static final String MYSQL_USERNAME = "root";
    /**
     * 数据库密码
     */
    public static final String MYSQL_PASSWORD = "root";


    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        // 表名称
        String tableName = "sys_config";
        // 获取Java代码
        String javaCode = convertDDLToEntity(Objects.requireNonNull(acquireDDLFromMysql(tableName)));
        // 生成代码
        genCode(underscoreToClassName(tableName), javaCode);
        long end = System.currentTimeMillis();
        System.out.println(end-start);
    }

    /**
     * 生成代码
     *
     * @param fileName 文件名称
     * @param javaCode Java代码
     */
    private static void genCode(String fileName, String javaCode) {
        File currentFile = new File(TableToEntityGenerator.class.getResource(".").getPath());
        String fileAbsolutePath = currentFile.getAbsolutePath();
        String genPath = fileAbsolutePath.replace("target\\classes", "src\\main\\java");
        String filePath = genPath + "\\" + fileName + ".java";
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write(javaCode);
            writer.close();
            System.out.println("Java文件生成成功，路径为：" + filePath);
        } catch (IOException e) {
            System.out.println("生成Java文件时出现错误：" + e.getMessage());
        }
    }


    /**
     * 数据库连接
     *
     * @param tableName tableName
     * @return String
     */
    private static String acquireDDLFromMysql(String tableName) {
        String url = String.format("jdbc:mysql://%s:3306/%s", MYSQL_HOST, MYSQL_DATABASE);
        try {
            // 加载MySQL JDBC驱动程序
            Class.forName("com.mysql.cj.jdbc.Driver");

            // 建立数据库连接
            Connection connection = DriverManager.getConnection(url, MYSQL_USERNAME, MYSQL_PASSWORD);

            // 创建Statement对象
            Statement statement = connection.createStatement();

            // 执行SHOW CREATE TABLE语句获取表的建表语句
            String query = "SHOW CREATE TABLE " + tableName;
            ResultSet resultSet = statement.executeQuery(query);

            // 读取结果集中的数据
            String createTableStatement = "";
            if (resultSet.next()) {
                createTableStatement = resultSet.getString("Create Table");
            }
            // 关闭连接和资源
            resultSet.close();
            statement.close();
            connection.close();
            // 打印表的建表语句
            return createTableStatement;


        } catch (ClassNotFoundException e) {
            System.out.println("MySQL JDBC驱动程序未找到");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("数据库连接错误");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取实体
     *
     * @param ddlStatement ddl
     * @return 结果
     */
    public static String convertDDLToEntity(String ddlStatement) {

        String[] split = ddlStatement.split("\n");
        String createTableString = split[0];
        String tableName = extractTableName(createTableString);
        if (Objects.isNull(tableName)) {
            throw new RuntimeException("table name is not found");
        }
        String entityName = underscoreToClassName(tableName);
        StringBuilder result = new StringBuilder();
        String packageName = TableToEntityGenerator.class.getPackage().getName();
        result.append("package " + packageName+";\n\n\n\n");
        result.append("@Data\n");
        result.append("@TableName(value = \"").append(tableName).append("\")\n");
        result.append("public class ").append(entityName).append(" {\n\n");
        if (split[split.length - 2].trim().startsWith("PRIMARY")) {
            for (int i = 1; i < split.length - 2; i++) {
                String detailSql = split[i];
                String tableField = getTableField(detailSql);
                if (Objects.isNull(tableField)) {
                    throw new RuntimeException("table field not fund");
                }
                // 字段名称
                String columnName = underscoreToCamel(tableField);
                // 字段类型
                String fieldType = getFieldType(tableField, detailSql);
                // 字段注释
                String fieldComment = getFieldComment(columnName, detailSql);

                result.append("    /**\n");
                result.append("     * ").append(fieldComment).append("\n");
                result.append("     */\n");
                if (columnName.equalsIgnoreCase("id")) {
                    result.append("    @TableId(type = IdType.AUTO)\n");
                }

                if (columnName.equalsIgnoreCase("deleted")) {
                    result.append("    @TableLogic\n");
                }
                result.append("    private ").append(mapDataTypeToJava(fieldType)).append(" ").append(columnName).append(";\n\n");
            }
        }
        result.append("}");
        return (result.toString());
    }

    /**
     * 获取注释
     *
     * @param tableField tableField
     * @param detailSql  detailSql
     * @return String
     */
    private static String getFieldComment(String tableField, String detailSql) {

        if (!detailSql.contains("COMMENT") && !detailSql.contains("comment")) {
            return tableField;
        }
        String pattern = "";
        if (detailSql.contains("COMMENT")) {
            pattern = "COMMENT\\s+'([^']+)'";
        } else if (detailSql.contains("comment")) {
            pattern = "comment\\s+'([^']+)'";
        }
        // 创建 Pattern 对象
        Pattern regex = Pattern.compile(pattern);

        // 创建 Matcher 对象
        Matcher matcher = regex.matcher(detailSql);

        // 查找匹配
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException(tableField + "not found comment");
        }
    }


    /**
     * 获取字段类型
     *
     * @param field     字段
     * @param detailSql 整行sql
     * @return 字段类型
     */
    private static String getFieldType(String field, String detailSql) {
        String pattern = "`" + field + "`" + "\\s+(\\w+)";
        // 创建 Pattern 对象
        Pattern regex = Pattern.compile(pattern);
        // 创建 Matcher 对象
        Matcher matcher = regex.matcher(detailSql);
        // 查找匹配
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException(field + "field type not found");
        }
    }


    /**
     * 下划线转类名称
     *
     * @param input
     * @return
     */
    public static String underscoreToClassName(String input) {
        String[] parts = input.split("_");
        StringBuilder result = new StringBuilder();

        for (String part : parts) {
            if (!part.isEmpty()) {
                result.append(Character.toUpperCase(part.charAt(0))).append(part.substring(1).toLowerCase());
            }
        }

        return result.toString();
    }


    public static String getTableField(String ddlStatement) {
        Pattern pattern = Pattern.compile("`([^`]+)`");
        Matcher matcher = pattern.matcher(ddlStatement);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }


    public static String extractTableName(String ddlStatement) {
        Pattern pattern = Pattern.compile("CREATE TABLE `([^`]+)`");
        Matcher matcher = pattern.matcher(ddlStatement);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }


    /**
     * 匹配类型
     *
     * @param dataType 数据库字段类型
     * @return 实体类型
     */
    public static String mapDataTypeToJava(String dataType) {
        switch (dataType) {
            case "int":
            case "tinyint":
                return "Integer";
            case "bigint":
                return "Long";
            case "varchar":
            case "char":
            case "text":
            case "longtext":
            case "mediumtext":
                return "String";
            case "decimal":
                return "BigDecimal";
            case "datetime":
                return "LocalDateTime";
            default:
                throw new RuntimeException("type not match");
        }
    }


    /**
     * 下划线转驼峰
     *
     * @param input
     * @return
     */
    public static String underscoreToCamel(String input) {
        StringBuilder result = new StringBuilder();
        boolean capitalizeNext = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            if (currentChar == '_') {
                capitalizeNext = true;
            } else {
                if (capitalizeNext) {
                    result.append(Character.toUpperCase(currentChar));
                    capitalizeNext = false;
                } else {
                    result.append(currentChar);
                }
            }
        }

        return result.toString();
    }


}
