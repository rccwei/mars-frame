package com.mars.springboot.common.generator;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 目的: 将数据库DDL语句转换成 JAVA 实体
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-19 19:33:33
 */
public class DDLToEntityGenerator {


    public static void main(String[] args) {
        String ddl = "CREATE TABLE `t_book_order` (\n" +
                "  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '编号',\n" +
                "  `order_id` varchar(100) NOT NULL COMMENT '订单号',\n" +
                "  `book_id` bigint NOT NULL COMMENT '商品id',\n" +
                "  `user_id` bigint DEFAULT NULL COMMENT '用户id',\n" +
                "  `status` int DEFAULT '1' COMMENT '状态',\n" +
                "  `count` int DEFAULT '0' COMMENT '购买数量',\n" +
                "  `bill_time` datetime DEFAULT NULL COMMENT '下单时间',\n" +
                "  PRIMARY KEY (`id`) USING BTREE\n" +
                ") ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='订单表';";
        //  MySQL DDL
        String code = genCode(ddl);
        String tableName = extractTableName(ddl);
        // 生成代码
        genCode(underscoreToClassName(Objects.requireNonNull(tableName)), code);


    }

    public static String genCode(String ddl) {
        if (StringUtils.isEmpty(ddl)) {
            throw new RuntimeException("ddl is not allowed empty");
        }
        return convertDDLToEntity(ddl);

    }

    public static void genCodeDownLoad(String ddl) {

        String javaCode = convertDDLToEntity(ddl);
        String tableName = extractTableName(ddl);
        // 生成代码
        genCode(underscoreToClassName(Objects.requireNonNull(tableName)), javaCode);
    }

    /**
     * 生成代码
     *
     * @param fileName 文件名称
     * @param javaCode Java代码
     */
    private static void genCode(String fileName, String javaCode) {
        File currentFile = new File(DDLToEntityGenerator.class.getResource(".").getPath());
        String fileAbsolutePath = currentFile.getAbsolutePath();
        String genPath = fileAbsolutePath.replace("target\\classes", "src\\main\\java");
        String filePath = genPath + "\\code\\" + fileName + ".java";
        try {
            FileWriter writer = new FileWriter(filePath);
            writer.write(javaCode);
            writer.close();
            System.out.println("Java文件生成成功，路径为：" + filePath);
        } catch (IOException e) {
            System.out.println("生成Java文件时出现错误：" + e.getMessage());
        }
    }


    /**
     * 获取实体
     *
     * @param ddlStatement ddl
     * @return 结果
     */
    public static String convertDDLToEntity(String ddlStatement) {

        String[] split = ddlStatement.split("\n");
        String createTableString = split[0];
        String tableName = extractTableName(createTableString);
        if (Objects.isNull(tableName)) {
            throw new RuntimeException("table name is not found");
        }
        String entityName = underscoreToClassName(tableName);
        StringBuilder result = new StringBuilder();
        String packageName = DDLToEntityGenerator.class.getPackage().getName();
        result.append("package " + packageName + ".code;\n\n\n\n");
        result.append("@Data\n");
        result.append("@TableName(value = \"").append(tableName).append("\")\n");
        result.append("public class ").append(entityName).append(" {\n\n");
        if (split[split.length - 2].trim().startsWith("PRIMARY")) {
            for (int i = 1; i < split.length - 2; i++) {
                String detailSql = split[i];
                String tableField = getTableField(detailSql);
                if (Objects.isNull(tableField)) {
                    throw new RuntimeException("table field not fund");
                }
                // 字段名称
                String columnName = underscoreToCamel(tableField);
                // 字段类型
                String fieldType = getFieldType(tableField, detailSql);
                // 字段注释
                String fieldComment = getFieldComment(columnName, detailSql);

                result.append("    /**\n");
                result.append("     * ").append(fieldComment).append("\n");
                result.append("     */\n");
                if (columnName.equalsIgnoreCase("id")) {
                    result.append("    @TableId(type = IdType.AUTO)\n");
                }

                if (columnName.equalsIgnoreCase("deleted")) {
                    result.append("    @TableLogic\n");
                }
                result.append("    private ").append(mapDataTypeToJava(fieldType)).append(" ").append(columnName).append(";\n\n");
            }
        }
        result.append("}");
        return (result.toString());
    }

    /**
     * 获取注释
     *
     * @param tableField tableField
     * @param detailSql  detailSql
     * @return String
     */
    private static String getFieldComment(String tableField, String detailSql) {

        if (!detailSql.contains("COMMENT") && !detailSql.contains("comment")) {
            return tableField;
        }
        String pattern = "";
        if (detailSql.contains("COMMENT")) {
            pattern = "COMMENT\\s+'([^']+)'";
        } else if (detailSql.contains("comment")) {
            pattern = "comment\\s+'([^']+)'";
        }
        // 创建 Pattern 对象
        Pattern regex = Pattern.compile(pattern);

        // 创建 Matcher 对象
        Matcher matcher = regex.matcher(detailSql);

        // 查找匹配
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException(tableField + "not found comment");
        }
    }


    /**
     * 获取字段类型
     *
     * @param field     字段
     * @param detailSql 整行sql
     * @return 字段类型
     */
    private static String getFieldType(String field, String detailSql) {
        String pattern = "`" + field + "`" + "\\s+(\\w+)";
        // 创建 Pattern 对象
        Pattern regex = Pattern.compile(pattern);
        // 创建 Matcher 对象
        Matcher matcher = regex.matcher(detailSql);
        // 查找匹配
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            throw new RuntimeException(field + "field type not found");
        }
    }


    /**
     * 下划线转类名称
     *
     * @param input
     * @return
     */
    public static String underscoreToClassName(String input) {
        String[] parts = input.split("_");
        StringBuilder result = new StringBuilder();

        for (String part : parts) {
            if (!part.isEmpty()) {
                result.append(Character.toUpperCase(part.charAt(0))).append(part.substring(1).toLowerCase());
            }
        }

        return result.toString();
    }


    public static String getTableField(String ddlStatement) {
        Pattern pattern = Pattern.compile("`([^`]+)`");
        Matcher matcher = pattern.matcher(ddlStatement);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }


    public static String extractTableName(String ddlStatement) {
        Pattern pattern = Pattern.compile("CREATE TABLE `([^`]+)`");
        Matcher matcher = pattern.matcher(ddlStatement);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }


    /**
     * 匹配类型
     *
     * @param dataType 数据库字段类型
     * @return 实体类型
     */
    public static String mapDataTypeToJava(String dataType) {
        switch (dataType) {

            case "int":
            case "tinyint":
                return "Integer";
            case "bigint":
                return "Long";
            case "varchar":
            case "char":
            case "text":
            case "longtext":
            case "mediumtext":
                return "String";
            case "decimal":
                return "BigDecimal";
            case "datetime":
                return "LocalDateTime";
            default:
                throw new RuntimeException("type not match");
        }
    }


    /**
     * 下划线转驼峰
     *
     * @param input
     * @return
     */
    public static String underscoreToCamel(String input) {
        StringBuilder result = new StringBuilder();
        boolean capitalizeNext = false;

        for (int i = 0; i < input.length(); i++) {
            char currentChar = input.charAt(i);

            if (currentChar == '_') {
                capitalizeNext = true;
            } else {
                if (capitalizeNext) {
                    result.append(Character.toUpperCase(currentChar));
                    capitalizeNext = false;
                } else {
                    result.append(currentChar);
                }
            }
        }

        return result.toString();
    }


}
