package com.mars.springboot.common.vo.tool;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 数据库表列VO
 *
 * @author jsbb
 */
public class ColumnListVo {

    @Schema(title = "字段名")
    private String columnName;

    @Schema(title = "备注")
    private String columnComment;

    @Schema(title = "类型")
    private String columnType;

    @Schema(title = "是否可为空")
    private String isNullable;

    @Schema(title = "是否主键")
    private String columnKey;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnComment() {
        return columnComment;
    }

    public void setColumnComment(String columnComment) {
        this.columnComment = columnComment;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getIsNullable() {
        return isNullable;
    }

    public void setIsNullable(String isNullable) {
        this.isNullable = isNullable;
    }

    public String getColumnKey() {
        return columnKey;
    }

    public void setColumnKey(String columnKey) {
        this.columnKey = columnKey;
    }
}
