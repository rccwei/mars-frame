package com.mars.springboot.common.result;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: BaseVO
 * @description: 基类
 * @author: wq
 * @create: 2021-12-05 15:15
 **/
@Data
public class BaseResult implements Serializable {
    /**
     * 加密密文
     */
    private String encryptStr;
}
