package com.mars.springboot.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 菜单修改DTO
 *
 * @author jsbb
 */
public class SysMenuUpdateDto {

    @NotNull
    @Schema(title = "ID")
    private Long id;

    @NotEmpty
    @Schema(title = "菜单名称")
    private String menuName;

    @Schema(title = "父ID")
    private Long parentId = 0L;

    @Schema(title = "排序")
    private Integer sort;

    @Schema(title = "URL")
    private String url;

    @Schema(title = "权限标识")
    private String perms;

    @Schema(title = "图标")
    private String icon;

    @Schema(title = "备注")
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
