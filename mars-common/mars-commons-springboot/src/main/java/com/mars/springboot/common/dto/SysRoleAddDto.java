package com.mars.springboot.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 角色新增DTO
 *
 * @author jsbb
 */
public class SysRoleAddDto {

    @NotEmpty
    @Schema(title = "角色名称")
    private String roleName;

    @Schema(title = "备注")
    private String remark;

    @Schema(title = "菜单")
    private List<Long> menuId;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Long> getMenuId() {
        return menuId;
    }

    public void setMenuId(List<Long> menuId) {
        this.menuId = menuId;
    }
}
