package com.mars.springboot.base;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * 列表查询封装VO
 *
 * @author jsbb
 */
public class PageVo<T> {

    @Schema(title = "总记录数")
    private Integer count;

    @Schema(title = "列表数据")
    private List<T> list;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public PageVo() {
    }

    public PageVo(Integer count, List<T> list) {
        this.count = count;
        this.list = list;
    }
}
