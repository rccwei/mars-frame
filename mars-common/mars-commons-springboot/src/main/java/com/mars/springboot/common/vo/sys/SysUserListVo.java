package com.mars.springboot.common.vo.sys;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 用户列表VO
 *
 * @author jsbb
 */
public class SysUserListVo {

    @Schema(title = "ID")
    private Long id;

    @Schema(title = "用户名")
    private String userName;

    @Schema(title = "姓名")
    private String fullName;

    @Schema(title = "性别（1男  2女）")
    private Integer sex;

    @Schema(title = "出生日期")
    private LocalDate birthDate;

    @Schema(title = "手机号码")
    private String phone;

    @Schema(title = "头像")
    private String avatar;

    @Schema(title = "创建时间")
    private LocalDateTime createTime;

    @Schema(title = "角色")
    private List<SysRoleListVo> roleList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public List<SysRoleListVo> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<SysRoleListVo> roleList) {
        this.roleList = roleList;
    }
}
