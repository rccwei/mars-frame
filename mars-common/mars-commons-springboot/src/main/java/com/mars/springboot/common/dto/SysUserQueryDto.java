package com.mars.springboot.common.dto;

import com.mars.springboot.base.PageDto;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 用户查询DTO
 *
 * @author jsbb
 */
public class SysUserQueryDto extends PageDto {

    @Schema(title = "用户名")
    private String userName;

    @Schema(title = "姓名")
    private String fullName;

    @Schema(title = "手机号码")
    private String phone;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
