package com.mars.springboot.common.vo.sys;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

/**
 * 省市区VO
 *
 * @author jsbb
 */
public class SysRegionTreeVo {

    @Schema(title = "ID")
    private Long id;

    @Schema(title = "名称")
    private String name;

    @Schema(title = "子节点")
    private List<SysRegionTreeVo> children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SysRegionTreeVo> getChildren() {
        return children;
    }

    public void setChildren(List<SysRegionTreeVo> children) {
        this.children = children;
    }
}
