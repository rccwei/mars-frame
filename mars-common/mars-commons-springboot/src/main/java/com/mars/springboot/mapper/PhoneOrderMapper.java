package com.mars.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.entity.PhoneOrder;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:23:53
 */
public interface PhoneOrderMapper extends BaseMapper<PhoneOrder> {
}
