package com.mars.springboot.base.vo;

import lombok.Data;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 16:37:35
 */
@Data
public class HomeVO {

    private List<BannerVO> banners;

    private List<NotificationVO> notifications;

    private List<BenefitVO> benefits;

    private List<CouponVO> coupons;

}
