package com.mars.springboot.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;

/**
 * 菜单新增DTO
 *
 * @author jsbb
 */
public class SysMenuAddDto {

    @NotEmpty
    @Schema(title = "菜单名称")
    private String menuName;

    @Schema(title = "URL")
    private String url;

    @Schema(title = "父ID")
    private Long parentId;

    @Schema(title = "排序")
    private Integer sort;

    @Schema(title = "权限标识")
    private String perms;

    @Schema(title = "图标")
    private String icon;

    @Schema(title = "备注")
    private String remark;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
