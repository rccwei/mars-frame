package com.mars.springboot.common.result;

import lombok.Data;

/**
 * 公共响应的实体
 *
 * @author : wq
 * @date : 2020-06-18
 **/
@Data
public class R<T> {

    /**
     * 状态码
     */
    private Integer status;
    /**
     * 说明
     */
    private String msg;
    /**
     * 数据
     */
    private T data;

    public R(Integer status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public R(Integer status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public static <T> R<T> success(T data) {
        return result(data, "成功", 200);
    }

    public static <T> R<T> success() {
        return result(null, "成功", 200);
    }


    public static <T> R<T> result(T data, String msg, Integer status) {
        return new R<>(status, msg, data);
    }

    public static <T> R<T> errorResult(String msg, Integer status) {
        return new R<>(status, msg);
    }

    public static <T> R<T> error(String error) {
        return errorResult(error, -1);
    }

    public static <T> R<T> error(String error, Integer code) {
        return result(null, error, code);
    }
}
