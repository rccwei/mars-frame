package com.mars.springboot.common.vo.tool;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 数据库表列表VO
 *
 * @author jsbb
 */
public class TableListVo {

    @Schema(title = "表名")
    private String tableName;

    @Schema(title = "备注")
    private String tableComment;

    @Schema(title = "字段数量")
    private Integer columnSize;

    @Schema(title = "创建时间")
    private LocalDateTime createTime;

    @Schema(title = "字段列表")
    private List<ColumnListVo> columnList;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public Integer getColumnSize() {
        return columnSize;
    }

    public void setColumnSize(Integer columnSize) {
        this.columnSize = columnSize;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public List<ColumnListVo> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<ColumnListVo> columnList) {
        this.columnList = columnList;
    }
}
