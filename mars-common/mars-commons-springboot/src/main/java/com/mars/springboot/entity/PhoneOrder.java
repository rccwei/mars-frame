package com.mars.springboot.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@TableName(value = "t_phone_order")
@AllArgsConstructor
@NoArgsConstructor
public class PhoneOrder {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 商品id
     */
    private Long phoneId;

    /**
     * 用户id
     */
    private Long userId;


    /**
     * 购买数量
     */
    private Integer count;

    /**
     * 下单时间
     */
    private LocalDateTime billTime;

}
