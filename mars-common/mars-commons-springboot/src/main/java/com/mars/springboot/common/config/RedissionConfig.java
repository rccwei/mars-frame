package com.mars.springboot.common.config;

import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 13:45
 */
@Configuration
@Slf4j
public class RedissionConfig {

    @Value("${spring.redis.host}")
    private String server;

    @Value("${spring.redis.port}")
    private String port;

    @Value("${spring.redis.password}")
    private String password;

    @Bean
    public RedissonClient getClient() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://" + server + ":" + port).setPassword(password);
        RedissonClient redisson = Redisson.create(config);
        log.info("redission init success");
        return redisson;
    }
}
