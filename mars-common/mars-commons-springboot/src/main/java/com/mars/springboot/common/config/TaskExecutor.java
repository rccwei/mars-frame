package com.mars.springboot.common.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 16:44:07
 */
@Component
public class TaskExecutor implements Executor {


    @Override
    public void execute(@NotNull Runnable command) {
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        executorService.execute(command);
    }
}
