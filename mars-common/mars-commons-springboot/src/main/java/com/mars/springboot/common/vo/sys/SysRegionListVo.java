package com.mars.springboot.common.vo.sys;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 省市区VO
 *
 * @author jsbb
 */
public class SysRegionListVo {

    @Schema(title = "ID")
    private Long id;

    @Schema(title = "上一级ID")
    private Long pid;

    @Schema(title = "名称")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
