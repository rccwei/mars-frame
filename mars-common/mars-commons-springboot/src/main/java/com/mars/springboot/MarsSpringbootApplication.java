package com.mars.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 线上环境 CPU 打到100%的排查步骤: 简单分为四步

 1.采用top命令查看哪个进程ID CPU过高
 命令: top

 2.查看对应进场的线程信息 top -H -p 进程ID
 命令: top -H -p 18680

 3.以线程ID 10952的线程为例进行排查，将10952 转为16进制。后面排查日志时使用

 printf "0x%x\n"  31261


 4.用jstack来输出进程ID 10760的堆栈信息，然后根据线程ID 10952的十六进制值0x2ac8 grep

 jstack 31225 | grep 0x7a1d -A 100

 5.直接把进程的堆栈信息给导出来
 jstack -l 18613 >> jstacklog.txt
 */
@MapperScan("com.mars.springboot.mapper")
@SpringBootApplication
@EnableScheduling
public class MarsSpringbootApplication  {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringbootApplication.class, args);
    }


}
