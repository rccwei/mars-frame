package com.mars.springboot.controller;

import cn.hutool.core.util.DesensitizedUtil;
import com.mars.springboot.common.resp.R;
import com.mars.springboot.entity.UserInfo;
import com.mars.springboot.service.HomeIndexService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 多线程异步的方式优化app首页加载慢的问题
 *
 * 使用场景 ，在APP查询首页信息的时候，一般会涉及到不同的RPC远程调用来获取很多用户相关信息数据，比如：获取首页banner轮播图信息、
 * 用户message消息信息、用户权益信息、用户优惠券信息 等，假设每个rpc 耗时是250ms，
 * 那么基于同步的方式获取到话，算下来接口的RT至少大于1s，
 * 这响应时长对于首页来说是肯定不能接受的，因此，我们这种场景就可以通过多线程异步的方式去优化。
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-20 17:55:02
 */
@Slf4j
@RestController
@AllArgsConstructor
public class IndexController {


    private final HomeIndexService homeIndexService;


    /**
     * V1/同步查询首页接口信息
     *
     * @param userId 用户ID
     * @return R
     */
    @GetMapping(value = "/v1/homeIndex")
    public R homeIndexV1(String userId) {
        return R.success(homeIndexService.homeIndexV1(userId));
    }


    /**
     * V2/异步编排方式查询首页接口信息
     *
     * @param userId 用户ID
     * @return R
     */
    @GetMapping(value = "/v2/homeIndex")
    public R homeIndexV2(String userId) {
        return R.success(homeIndexService.homeIndexV2(userId));
    }


    /**
     * 获取用户信息
     *
     * @return R
     */
    @GetMapping("/getUserInfo")
    public R getUserInfo() {
        String originPhone = "18483888377";
        String originCardNumber = "510922199411283570";
        // 手机号脱敏处理
        String encryptPhone = DesensitizedUtil.mobilePhone(originPhone);
        // 身份证号脱敏处理
        String encryptCardNum = DesensitizedUtil.idCardNum(originCardNumber, 2, 4);
        UserInfo userInfo = UserInfo.builder()
                .cardNumber(encryptCardNum)
                .phone(encryptPhone)
                .username("zs")
                .build();
        return R.success(userInfo);
    }

}
