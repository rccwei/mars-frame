package com.mars.springboot.common.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:25:58
 */
@Data
public class OrderRequest {
    /**
     * 用户id
     */
    private Long userId;

    /**
     * 手机ID
     */
    @NotNull(message = "手机ID不能为空")
    private Long phoneId;

}
