package com.mars.springboot.base;

/**
 * 用户token
 *
 * @author jsbb
 */
public class UserToken {

    private Long id;

    private Long timestamp;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
}
