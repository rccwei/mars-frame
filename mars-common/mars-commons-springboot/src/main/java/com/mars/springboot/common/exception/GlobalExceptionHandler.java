package com.mars.springboot.common.exception;


import com.mars.springboot.common.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 全局异常处理
 *
 * @author jsbb
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public R exceptionHandler(Exception e) {
        log.error(e.getMessage(), e);
        return R.error("系统错误,请稍后重试");
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public R methodNotSupportHandler(HttpRequestMethodNotSupportedException e) {
        log.error(e.getMessage(), e);
        return R.error("不支持的请求方法");
    }

    @ExceptionHandler(value = {ServiceException.class})
    public R serviceExceptionHandler(ServiceException e) {
        log.error(e.getMessage(), e);
        return R.error(e.getMsg(), Integer.parseInt(e.getCode()));
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R methodArgumentNotValidHandler(MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        BindingResult bindingResult = e.getBindingResult();
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        ObjectError objectError = allErrors.get(0);
        String defaultMessage = objectError.getDefaultMessage();
        return R.error(defaultMessage);
    }
}
