package com.mars.springboot.base.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 优惠券信息
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 16:26:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CouponVO {

    private Integer id;

    private String name;

    private String price;

    private String validTime;
}
