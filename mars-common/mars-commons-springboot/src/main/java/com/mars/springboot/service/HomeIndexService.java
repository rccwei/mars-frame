package com.mars.springboot.service;

import com.mars.springboot.base.vo.HomeVO;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 17:04:19
 */
public interface HomeIndexService {

    /**
     * v1 同步获取
     *
     * @return HomeVO
     */
    HomeVO homeIndexV1(String userId);

    /**
     * v2 异步编排获取
     *
     * @return HomeVO
     */
    HomeVO homeIndexV2(String userId);
}
