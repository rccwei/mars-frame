package com.mars.springboot.base.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 通知响应信息
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 16:24:43
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NotificationVO {

    private Integer id;

    private String title;

    private String from;

    private String content;

    private Date createTime;
}
