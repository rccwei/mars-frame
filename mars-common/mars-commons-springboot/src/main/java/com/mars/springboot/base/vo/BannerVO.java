package com.mars.springboot.base.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 轮播广告信息
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 16:24:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BannerVO {

    private Integer id;

    private String name;

    private String image;

    private Integer sort;
}
