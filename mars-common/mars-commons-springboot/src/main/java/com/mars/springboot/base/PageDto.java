package com.mars.springboot.base;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 分页查询DTO
 *
 * @author jsbb
 */
public class PageDto {

    @Schema(title = "当前页")
    private int pageNo = 1;

    @Schema(title = "每页记录数")
    private int pageSize = 10;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
