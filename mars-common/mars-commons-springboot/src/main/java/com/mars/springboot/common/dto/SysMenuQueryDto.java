package com.mars.springboot.common.dto;

import com.mars.springboot.base.PageDto;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 菜单查询DTO
 *
 * @author jsbb
 */
public class SysMenuQueryDto extends PageDto {

    @Schema(title = "菜单名称")
    private String menuName;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
