package com.mars.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.springboot.entity.Phone;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:23:53
 */
public interface PhoneMapper extends BaseMapper<Phone> {

    @Update("update t_phone set stock=#{stock} where id =#{id}")
    void updateStock(@Param("stock") Integer stock,@Param("id") Long id);
}
