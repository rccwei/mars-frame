package com.mars.springboot.common.init;

import com.mars.springboot.base.Const;
import com.mars.springboot.entity.Phone;
import com.mars.springboot.mapper.PhoneMapper;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Objects;

/**
 * @author wq
 * @date 2022/1/20 19:22
 */
@Component
public class InitManger {

    @Resource
    private PhoneMapper phoneMapper;


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostConstruct
    public void init() {
        Phone phone = phoneMapper.selectById(1);
        if (Objects.nonNull(phone)){
            stringRedisTemplate.opsForValue().set(Const.STOCK, String.valueOf(phone.getStock()));
        }
    }


}
