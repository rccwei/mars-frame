package com.mars.springboot.base;

/**
 * 常量
 *
 * @author jsbb
 */
public class Const {

    public static final String TOKEN = "token";

    public static final String COOKIE_NAME = "jsbb";

    public static final String STOCK = "mobile:stock";
}
