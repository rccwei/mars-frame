package com.mars.springboot.common.enums;

/**
 * 系统状态码
 * 0000：成功
 * 1xxx：失败
 * 9999：token过期
 *
 * @author jsbb
 */
public enum HttpCodeEnum {

    /**
     * 成功
     */
    SUCCESS("200", "成功"),
    /**
     * 失败
     */
    ERROR("500", "系统错误"),
    /**
     * token过期
     */
    TOKEN_INVALID("9999", "token过期");

    private String index;
    private String value;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    HttpCodeEnum(String index, String value) {
        this.index = index;
        this.value = value;
    }

    public static String getValue(String index) {
        for (HttpCodeEnum dataSourceEnum : HttpCodeEnum.values()) {
            if (dataSourceEnum.getIndex().equals(index)) {
                return dataSourceEnum.getValue();
            }
        }
        return null;
    }
}
