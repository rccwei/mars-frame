package com.mars.springboot.common.dto;

import com.mars.springboot.base.PageDto;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 角色查询DTO
 *
 * @author jsbb
 */
public class SysRoleQueryDto extends PageDto {

    @Schema(title = "名称")
    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
