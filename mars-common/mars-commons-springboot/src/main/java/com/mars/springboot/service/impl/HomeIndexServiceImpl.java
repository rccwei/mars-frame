package com.mars.springboot.service.impl;

import com.mars.springboot.base.vo.*;
import com.mars.springboot.common.config.TaskExecutor;
import com.mars.springboot.service.HomeIndexService;
import com.mars.springboot.service.IPhoneService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-14 17:04:50
 */
@Slf4j
@Service
@AllArgsConstructor
public class HomeIndexServiceImpl implements HomeIndexService {

    private final IPhoneService phoneService;

    private final TaskExecutor asyncThreadPoolExecutor;

    @Override
    public HomeVO homeIndexV1(String userId) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // 获取Banner轮播图信息
        List<BannerVO> bannerVOS = phoneService.buildBanners(userId);
        // 获取用户message通知信息
        List<NotificationVO> notificationVOS = phoneService.buildNotifications(userId);
        // 获取用户权益信息
        List<BenefitVO> benefitVOS = phoneService.buildBenefits(userId);
        // 获取优惠券信息
        List<CouponVO> couponVOS = phoneService.buildCoupons(userId);
        HomeVO finalHomeVO = new HomeVO();
        finalHomeVO.setBanners(bannerVOS);
        finalHomeVO.setNotifications(notificationVOS);
        finalHomeVO.setBenefits(benefitVOS);
        finalHomeVO.setCoupons(couponVOS);
        stopWatch.stop();
        System.out.println("共计耗时毫秒:" + stopWatch.getTotalTimeMillis());
        return finalHomeVO;
    }

    @Override
    public HomeVO homeIndexV2(String userId) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // 获取Banner轮播图信息
        CompletableFuture<List<BannerVO>> future1 = CompletableFuture.supplyAsync(() -> phoneService.buildBanners(userId), asyncThreadPoolExecutor);
        // 获取用户message通知信息
        CompletableFuture<List<NotificationVO>> future2 = CompletableFuture.supplyAsync(() -> phoneService.buildNotifications(userId), asyncThreadPoolExecutor);
        // 获取用户权益信息
        CompletableFuture<List<BenefitVO>> future3 = CompletableFuture.supplyAsync(() -> phoneService.buildBenefits(userId), asyncThreadPoolExecutor);
        // 获取优惠券信息
        CompletableFuture<List<CouponVO>> future4 = CompletableFuture.supplyAsync(() -> phoneService.buildCoupons(userId), asyncThreadPoolExecutor);
        CompletableFuture<Void> allOfFuture = CompletableFuture.allOf(future1, future2, future3, future4);
        HomeVO finalHomeVO = new HomeVO();
        CompletableFuture<HomeVO> resultFuture = allOfFuture.thenApply(v -> {
            try {
                finalHomeVO.setBanners(future1.get());
                finalHomeVO.setNotifications(future2.get());
                finalHomeVO.setBenefits(future3.get());
                finalHomeVO.setCoupons(future4.get());
                return finalHomeVO;
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
        HomeVO homeVO = resultFuture.join();
        stopWatch.stop();
        System.out.println("共计耗时毫秒:" + stopWatch.getTotalTimeMillis());
        return homeVO;
    }
}
