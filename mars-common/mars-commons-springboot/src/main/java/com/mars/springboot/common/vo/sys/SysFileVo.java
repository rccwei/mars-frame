package com.mars.springboot.common.vo.sys;

import io.swagger.v3.oas.annotations.media.Schema;

/**
 * 文件VO
 *
 * @author jsbb
 */
public class SysFileVo {

    @Schema(title = "文件ID")
    private Long id;

    @Schema(title = "名称")
    private String fileName;

    @Schema(title = "后缀")
    private String suffix;

    @Schema(title = "URL")
    private String url;

    @Schema(title = "大小（B）")
    private Long fileSize;

    @Schema(title = "内容类型")
    private String contentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
