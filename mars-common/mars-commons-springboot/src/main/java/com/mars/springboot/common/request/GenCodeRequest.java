package com.mars.springboot.common.request;

import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-20 18:04:07
 */
@Data
public class GenCodeRequest {

    private String ddl;
}
