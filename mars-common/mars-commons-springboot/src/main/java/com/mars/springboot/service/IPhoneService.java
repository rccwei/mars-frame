package com.mars.springboot.service;

import com.mars.springboot.base.vo.*;
import com.mars.springboot.common.request.OrderRequest;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:25:00
 */
public interface IPhoneService {

    /**
     * 下单操作（无分布式锁）
     */
    void createOrderV1(OrderRequest request);

    /**
     * 下单操作（有分布式锁）
     *
     * @param request 请求参数
     */
    void createOrderV2(OrderRequest request);

    /**
     * 查询轮播
     *
     * @param userId 用户ID
     * @return List<BannerVO>
     */
    List<BannerVO> buildBanners(String userId);

    /**
     * 查询通知信息
     *
     * @param userId 用户ID
     * @return List<NotificationVO>
     */
    List<NotificationVO> buildNotifications(String userId);

    /**
     * 查询权益信息
     *
     * @param userId 用户ID
     * @return List<BenefitVO>
     */
    List<BenefitVO> buildBenefits(String userId);

    /**
     * 查询优惠券信息
     *
     * @param userId 用户ID
     * @return List<CouponVO>
     */
    List<CouponVO> buildCoupons(String userId);


}
