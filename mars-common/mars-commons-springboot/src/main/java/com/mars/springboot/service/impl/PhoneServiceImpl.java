package com.mars.springboot.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mars.springboot.base.Const;
import com.mars.springboot.base.vo.BannerVO;
import com.mars.springboot.base.vo.BenefitVO;
import com.mars.springboot.base.vo.CouponVO;
import com.mars.springboot.base.vo.NotificationVO;
import com.mars.springboot.common.request.OrderRequest;
import com.mars.springboot.entity.PhoneOrder;
import com.mars.springboot.mapper.PhoneOrderMapper;
import com.mars.springboot.service.IPhoneService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:27:08
 */
@Slf4j
@Service
@AllArgsConstructor
public class PhoneServiceImpl implements IPhoneService {


    private final PhoneOrderMapper phoneOrderMapper;

    private final StringRedisTemplate stringRedisTemplate;

    private final RedissonClient redissonClient;




    @Override
    public void createOrderV1(OrderRequest request) {
        String cacheStock = stringRedisTemplate.opsForValue().get(Const.STOCK);
        if (StringUtils.isEmpty(cacheStock)) {
            return;
        }
        int stock = Integer.parseInt(cacheStock);
        if (stock <= 0) {
            log.error("当前商品库存为空");
            return;
        }
        int endStock = stock - 1;
        stringRedisTemplate.opsForValue().set(Const.STOCK, String.valueOf(endStock));
        log.info("当前库存剩余:" + endStock);
        // 生成订单
        PhoneOrder phoneOrder = PhoneOrder.builder()
                .billTime(LocalDateTime.now())
                .phoneId(request.getPhoneId())
                .userId(request.getUserId())
                .count(1)
                .orderId(UUID.randomUUID().toString())
                .build();
        phoneOrderMapper.insert(phoneOrder);
    }

    @Override
    public void createOrderV2(OrderRequest request) {
        RLock lock = redissonClient.getLock("stock_lock");
        lock.lock();
        try {
            String cacheStock = stringRedisTemplate.opsForValue().get(Const.STOCK);
            if (StringUtils.isEmpty(cacheStock)) {
                return;
            }
            int stock = Integer.parseInt(cacheStock);
            if (stock <= 0) {
                log.error("当前商品库存为空");
                return;
            }
            int endStock = stock - 1;
            stringRedisTemplate.opsForValue().set(Const.STOCK, String.valueOf(endStock));
            log.info("当前库存剩余:" + endStock);
            // 生成订单
            PhoneOrder phoneOrder = PhoneOrder.builder()
                    .billTime(LocalDateTime.now())
                    .phoneId(request.getPhoneId())
                    .userId(request.getUserId())
                    .count(1)
                    .orderId(UUID.randomUUID().toString())
                    .build();
            phoneOrderMapper.insert(phoneOrder);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    @Override
    public List<BannerVO> buildBanners(String userId) {
        // 模拟请求耗时0.5秒
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<BannerVO> list = Lists.newArrayList();
        BannerVO vo = BannerVO.builder().id(1).image("www.baidu.com").name("轮播广告").sort(1).build();
        list.add(vo);
        return list;
    }

    @Override
    public List<NotificationVO> buildNotifications(String userId) {
        // 模拟请求耗时0.5秒
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<NotificationVO> list = Lists.newArrayList();
        NotificationVO vo = NotificationVO.builder().id(1).content("www.baidu.com").from("张三").title("通知").createTime(new Date()).build();
        list.add(vo);
        return list;
    }

    @Override
    public List<BenefitVO> buildBenefits(String userId) {
        // 模拟请求耗时0.5秒
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<BenefitVO> list = Lists.newArrayList();
        BenefitVO vo = BenefitVO.builder().id(1).image("www.baidu.com").name("权益").build();
        list.add(vo);
        return list;
    }

    @Override
    public List<CouponVO> buildCoupons(String userId) {
        // 模拟请求耗时0.5秒
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        List<CouponVO> list = Lists.newArrayList();
        CouponVO vo = CouponVO.builder().id(1).name("优惠券名称").price("20").validTime("2023-12-12").build();
        list.add(vo);
        return list;
    }


}
