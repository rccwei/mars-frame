package com.mars.springboot.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

/**
 * 用户修改DTO
 *
 * @author jsbb
 */
public class SysUserUpdateDto {

    @NotNull
    @Schema(title = "ID")
    private Long id;

    @NotEmpty
    @Schema(title = "用户名")
    private String userName;

    @NotEmpty
    @Schema(title = "姓名")
    private String fullName;

    @NotNull
    @Schema(title = "性别（1男  2女）")
    private Integer sex;

    @NotNull
    @Schema(title = "出生日期（yyyy-MM-dd）")
    private LocalDate birthDate;

    @NotEmpty
    @Schema(title = "手机号码")
    private String phone;

    @Schema(title = "头像")
    private String avatar;

    @Schema(title = "省ID")
    private String province;

    @Schema(title = "市ID")
    private String city;

    @Schema(title = "区ID")
    private String area;

    @Schema(title = "地址")
    private String address;

    @Schema(title = "角色")
    private List<Long> roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Long> getRoleId() {
        return roleId;
    }

    public void setRoleId(List<Long> roleId) {
        this.roleId = roleId;
    }
}
