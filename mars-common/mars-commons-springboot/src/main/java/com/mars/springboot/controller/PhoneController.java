package com.mars.springboot.controller;

import com.mars.springboot.common.request.OrderRequest;
import com.mars.springboot.common.result.R;
import com.mars.springboot.service.IPhoneService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 18:37:19
 * 高并发分布式环境下 库存超卖问题 解决方案
 */
@RestController
@AllArgsConstructor
public class PhoneController {

    private final IPhoneService bookService;

    /**
     * v1版本下单(无分布式锁)
     *
     * @param request 请求参数
     * @return R<String>
     */
    @PostMapping("/v1/createOrder")
    public R<String> createOrder(@Validated @RequestBody OrderRequest request) {
        bookService.createOrderV1(request);
        return R.success();
    }

    /**
     * v2版本下单(有分布式锁)
     *
     * @param request 请求参数
     * @return R<String>
     */
    @PostMapping("/v2/createOrder")
    public R<String> createOrderV2(@Validated @RequestBody OrderRequest request) {
        bookService.createOrderV2(request);
        return R.success();
    }
}
