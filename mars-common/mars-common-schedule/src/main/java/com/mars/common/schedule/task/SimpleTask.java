package com.mars.common.schedule.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 13:52
 */
@Component
public class SimpleTask {

    //fixedDelay = 1000 表示在当前任务执行结束1秒后开启另一个任务,
    //@Scheduled(fixedDelay = 1000)
    public void fixedDelay() {
        System.out.println("fixedDelay" + new Date());
    }

    //fixedRate = 2000表示在当前任务开始执行2秒后开启另一个任务
    //@Scheduled(fixedRate = 2000)
    public void fixedRate() {
        System.out.println("fixedRate:" + new Date());
    }

    //initialDelay = 1000表示首次执行延迟1秒执行
    @Scheduled(initialDelay = 1000, fixedRate = 2000)
    public void initialDelay() {
        System.out.println("initialDelay:" + new Date());
    }

    //代表每分钟执行一次
    @Scheduled(cron = "*/5 * * * * ?")
    public void cron() {
        System.out.println("cron:" + new Date());
    }
}
