package com.mars.common.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication
public class MarsCommonScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonScheduleApplication.class, args);
    }

}
