package com.mars.common.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wq
 */
@SpringBootApplication
public class MarsCommonNettyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonNettyApplication.class, args);
    }

}
