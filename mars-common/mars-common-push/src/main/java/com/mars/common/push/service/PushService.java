package com.mars.common.push.service;

import com.mars.common.push.request.PushReq;

public interface PushService {

    /**
     * 消息推送
     *
     * @param req
     * @return
     */
    boolean send(PushReq req) throws Exception;


    /**
     * 获取AccessToken
     *
     * @return
     */
    String getAccessToken() throws Exception;
}
