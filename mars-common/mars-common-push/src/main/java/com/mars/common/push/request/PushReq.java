package com.mars.common.push.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/04 17:02
 */
@Data
public class PushReq implements Serializable {

    /**
     * 设备型号 huawei  xiaomi  oppo vivo
     */
    private String device;

    /**
     * 通知标题
     */
    private String title;

    /**
     * 推送消息
     */
    private String content;

    /**
     * registerId
     */
    private String registerId;

    /**
     * 图片
     */
    private String images;


}
