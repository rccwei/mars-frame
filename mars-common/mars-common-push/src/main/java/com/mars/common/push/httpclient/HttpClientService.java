package com.mars.common.push.httpclient;

import com.alibaba.fastjson.JSONObject;
import com.mars.common.push.response.SendResponse;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 14:22
 */
@Component
public class HttpClientService {

    @Resource
    private CloseableHttpClient closeableHttpClient;

    @Resource
    private RequestConfig config;


    /**
     * 不带参数的get请求，如果状态码为200，则返回body，如果不为200，则返回null
     */
    public String doGet(String url) throws Exception {
        // 声明 http get 请求
        HttpGet httpGet = new HttpGet(url);
        // 装载配置信息
        httpGet.setConfig(config);
        // 发起请求
        CloseableHttpResponse response = this.closeableHttpClient.execute(httpGet);
        // 判断状态码是否为200
        if (response.getStatusLine().getStatusCode() == 200) {
            // 返回响应体的内容
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        }
        return null;
    }

    /**
     * 带参数的get请求，如果状态码为200，则返回body，如果不为200，则返回null
     */
    public String doGet(String url, Map<String, Object> map) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(url);

        if (map != null) {
            // 遍历map,拼接请求参数
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                uriBuilder.setParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        // 调用不带参数的get请求
        return this.doGet(uriBuilder.build().toString());

    }


    /**
     * 带参数的post请求
     */
    public SendResponse doPost(String url, Map<String, Object> map) throws Exception {
        // 声明httpPost请求
        HttpPost httpPost = new HttpPost(url);
        // 加入配置信息
        httpPost.setConfig(config);

        // 判断map是否为空，不为空则进行遍历，封装from表单对象
        if (map != null) {
            List<NameValuePair> list = new ArrayList<NameValuePair>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                list.add(new BasicNameValuePair(entry.getKey(), entry.getValue().toString()));
            }
            // 构造from表单对象
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list, "UTF-8");

            // 把表单放到post里
            httpPost.setEntity(urlEncodedFormEntity);
        }

        // 发起请求
        CloseableHttpResponse response = this.closeableHttpClient.execute(httpPost);
        return new SendResponse(String.valueOf(response.getStatusLine().getStatusCode()), EntityUtils.toString(
                response.getEntity(), "UTF-8"));
    }

    /**
     * 不带参数post请求
     */
    public SendResponse doPost(String url) throws Exception {
        return this.doPost(url, null);
    }

    /**
     * 发送post json请求
     *
     * @param url
     * @param json
     * @return
     */
    public JSONObject doPostJson(String url, String device, String authToken, JSONObject json) {
        HttpPost postMethod = new HttpPost(url);
        JSONObject response = null;
        try {
            if (device.equals("xiaomi")) {
                postMethod.addHeader("Authorization", "key=" + authToken);
            } else {
                postMethod.addHeader("authToken", authToken);
            }
            postMethod.addHeader("Content-type", "application/json; charset=utf-8");
            postMethod.setEntity(new StringEntity(json.toJSONString(), StandardCharsets.UTF_8));
            HttpResponse res = this.closeableHttpClient.execute(postMethod);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(res.getEntity());// 返回json格式：
                response = JSONObject.parseObject(result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }

    public JSONObject postJson(String format, String accessToken, String toJSONString) {

        HttpPost postMethod = new HttpPost(format);
        JSONObject response = null;
        try {
            postMethod.addHeader("Authorization", "Bearer " + accessToken);
            postMethod.addHeader("Content-type", "application/json; charset=utf-8");
            postMethod.setEntity(new StringEntity(toJSONString, StandardCharsets.UTF_8));
            HttpResponse res = this.closeableHttpClient.execute(postMethod);
            if (res.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                String result = EntityUtils.toString(res.getEntity());// 返回json格式：
                response = JSONObject.parseObject(result);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return response;
    }
}
