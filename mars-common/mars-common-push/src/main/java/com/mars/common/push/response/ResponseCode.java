package com.mars.common.push.response;

/**
 * 响应code
 *
 * @author mjy
 * @date 2020/6/18
 **/
public enum ResponseCode {
    /**
     * 失败
     */
    FAIL("-1", "失败"),
    /**
     * 登录
     */
    LOGIN("401", "需要登录"),
    /**
     * 成功
     */
    SUCCESS("200", "成功");
    /**
     * 响应码
     */
    private String code;
    /**
     * 消息
     */
    private String msg;

    ResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
