package com.mars.common.push.controller;

import com.mars.common.push.request.PushReq;
import com.mars.common.push.response.R;
import com.mars.common.push.service.impl.HuaweiPushImpl;
import com.mars.common.push.service.impl.OppoPushServiceImpl;
import com.mars.common.push.service.impl.VivoPushServiceImpl;
import com.mars.common.push.service.impl.XiaomiPushServiceImpl;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/05 17:52
 */
@RestController
public class PushController {

    @Resource
    private HuaweiPushImpl huaweiPush;

    @Resource
    private XiaomiPushServiceImpl xiaomiPush;

    @Resource
    private OppoPushServiceImpl oppoPush;

    @Resource
    private VivoPushServiceImpl vivoPush;


    @RequestMapping("push")
    public R send(@RequestBody PushReq req) throws Exception {
        switch (req.getDevice().toLowerCase()) {
            case "huawei":
                huaweiPush.send(req);
            case "xiaomi":
                xiaomiPush.send(req);
            case "oppo":
                oppoPush.send(req);
            case "vivo":
                vivoPush.send(req);
            default:
        }
        return R.success("推送成功");
    }
}
