package com.mars.common.push.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mars.common.push.constant.Constant;
import com.mars.common.push.httpclient.HttpClientService;
import com.mars.common.push.request.PushReq;
import com.mars.common.push.response.SendResponse;
import com.mars.common.push.service.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/04 15:46
 */
@Service
@Slf4j
public class HuaweiPushImpl implements PushService {


    @Value("${push.huawei.clientId}")
    private String clientId;

    @Value("${push.huawei.clientSecrect}")
    private String clientSecrect;

    @Resource
    private HttpClientService httpClientService;


    @Override
    public String getAccessToken() throws Exception {
        HashMap<String, Object> map = new HashMap<>();
        map.put("grant_type", "client_credentials");
        map.put("client_id", clientId);
        map.put("client_secret", clientSecrect);
        SendResponse sendResponse = httpClientService.doPost(Constant.HUAWEI_SERVER_TOKEN, map);
        return JSONObject.parseObject(sendResponse.getMsg()).getString("access_token");
    }

    @Override
    public boolean send(PushReq req) throws Exception {
        JSONObject data = new JSONObject();
        data.put("validate_only", false);
        JSONObject message = new JSONObject();
        JSONObject notification = new JSONObject();
        notification.put("title", req.getTitle());
        notification.put("body", req.getContent());
        notification.put("image", req.getImages());
        message.put("notification", notification);
        JSONObject android = new JSONObject();
        JSONObject androidNotification = new JSONObject();
        androidNotification.put("visibility", "PUBLIC");
        androidNotification.put("foreground_show", true);
        JSONObject clickAction = new JSONObject();
        clickAction.put("type", 1);
        clickAction.put("intent", "#Intent;compo=com.rvr/.Activity;S.W=U;end");
        androidNotification.put("click_action", clickAction);
        android.put("notification", androidNotification);
        message.put("android", android);
        List<String> tokens = new ArrayList<>();
        tokens.add(req.getRegisterId());
        message.put("token", tokens);
        data.put("message", message);
        String accessToken = getAccessToken();
        JSONObject jsonData = httpClientService.postJson(String.format(Constant.HUAWEI_PUSH_API, clientId), accessToken, data.toJSONString());
        return "Success".equals(jsonData.getString("msg")) && "800000000".equals(jsonData.getString("code"));
    }
}
