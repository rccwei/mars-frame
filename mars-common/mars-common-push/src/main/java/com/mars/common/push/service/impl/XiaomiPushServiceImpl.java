package com.mars.common.push.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mars.common.push.constant.Constant;
import com.mars.common.push.httpclient.HttpClientService;
import com.mars.common.push.request.PushReq;
import com.mars.common.push.service.PushService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class XiaomiPushServiceImpl implements PushService {

    @Value("${push.xiaomi.appSecret}")
    private String appSecret;

    @Resource
    private HttpClientService httpClientService;

    @Override
    public boolean send(PushReq req) throws Exception {
        String accessToken = getAccessToken();
        JSONObject json = new JSONObject();
        json.put("title", req.getTitle());
        //0 表示通知栏消息 1 表示透传消息
        json.put("pass_through", 0);
        json.put("restricted_package_name", "com.imlianka.lkapp");
        json.put("description", req.getContent());
        json.put("notify_type", -1);
        json.put("registration_id", req.getRegisterId());
        json.put("payload", req.getTitle());
        JSONObject jsonObject = httpClientService.doPostJson(Constant.XIAO_MI_PUSH_URL, "xiaomi", accessToken, json);
        log.info("jsonObject===>" + jsonObject);
        return false;
    }

    @Override
    public String getAccessToken() {
        return appSecret;
    }
}
