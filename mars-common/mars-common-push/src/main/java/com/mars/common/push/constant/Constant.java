package com.mars.common.push.constant;

public interface Constant {
    /**
     * 获取华为access_token地址
     */
    public static final String HUAWEI_SERVER_TOKEN = "https://oauth-login.cloud.huawei.com/oauth2/v3/token";

    /**
     * 华为推送api
     */
    public final String HUAWEI_PUSH_API = "https://api.push.hicloud.com/v1/%s/messages:send";
    /**
     * 100
     */
    Integer HUNDRED = 100;

    Integer ZERO = 0;
    /**
     * 华为推送server
     */
    String HUAWEI_PUSH_URL = "https://push-api.cloud.huawei.com";

    /**
     * OPPO推送server
     */
    String OPPO_SERVER_URL = "https://api.push.oppomobile.com/";

    /**
     * oppo获取token uri
     */
    String OPPO_TOKEN_URL = "/server/v1/auth";

    /**
     * 单推-通知栏消息推送
     */
    String OPPO_PUSH_URL = "/server/v1/message/notification/unicast";

    /**
     * VIVO 服务端地址
     */
    String VIVO_SERVER_URL = "https://api-push.vivo.com.cn";

    /**
     * VIVO 获取token uri
     */
    String VIVO_AUTH_URL = "/message/auth";

    /**
     * vivo 消息单推 url
     */
    String VIVO_MESSAGE_SEND = "/message/send";

    /**
     * xiaomi 推送接口
     */
    String XIAO_MI_PUSH_URL = "https://api.xmpush.xiaomi.com/v3/message/regid";


}
