package com.mars.common.push;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonPushApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonPushApplication.class, args);
    }

}
