# springboot整合knife4j接口文档

## 具体步骤
### 1.添加knife4j-spring-boot-starter依赖 
```
<dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger2</artifactId>
            <version>2.9.2</version>
            <exclusions>
                <exclusion>
                    <groupId>io.swagger</groupId>
                    <artifactId>swagger-annotations</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>io.swagger</groupId>
                    <artifactId>swagger-models</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <!--防止进入swagger页面报类型转换错误，排除2.9.2中的引用，手动增加1.5.21版本-->
        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-annotations</artifactId>
            <version>1.5.21</version>
        </dependency>

        <dependency>
            <groupId>io.swagger</groupId>
            <artifactId>swagger-models</artifactId>
            <version>1.5.21</version>
        </dependency>

        <!-- swagger2-UI-->
        <dependency>
            <groupId>io.springfox</groupId>
            <artifactId>springfox-swagger-ui</artifactId>
            <version>2.9.2</version>
        </dependency>

        <dependency>
            <groupId>com.github.xiaoymin</groupId>
            <artifactId>knife4j-spring-boot-starter</artifactId>
            <version>2.0.9</version>
        </dependency>
```
### 2.添加配置类Knife4jConfig
```
      @Configuration
      @EnableSwagger2
      @EnableKnife4j
      public class Knife4jConfig {
      
          @Bean
          public Docket createRestApi() {
              return new Docket(DocumentationType.SWAGGER_2)
                      .apiInfo(apiInfo())
                      .select()
                      // 为当前包路径
                      .apis(RequestHandlerSelectors.basePackage("com.mars.common.knif4j.controller")).
                              paths(PathSelectors.any())
                      .build();
          }
      
          private ApiInfo apiInfo() {
              return new ApiInfoBuilder()
                      .title("源码字节接口文档")
                      .contact(new Contact("mars", "https://gitee.com/marsFactory/mars-frame",
                              "850994281@qq.com"))
                      .version("1.0")
                      .description("源码字节接口文档")
                      .build();
          }
      }
}
```

### 3.配置类上开启接口文档       
```
      @EnableSwagger2
      @EnableKnife4j
```

