package com.mars.common.knif4j.controller;

import com.mars.common.knif4j.req.UserReq;
import com.mars.common.knif4j.resp.UserResp;
import com.mars.common.knif4j.result.R;
import com.mars.common.knif4j.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/03 11:31
 */
@RestController
@Api(tags = "用户管理")
@AllArgsConstructor
public class UserController {


    private final IUserService userService;

    /**
     * 添加用户
     *
     * @param req req
     * @return R
     */
    @ApiOperation(value = "添加用户")
    @PostMapping("/add")
    public R add(@RequestBody UserReq req) {
        userService.add(req);
        return R.success();
    }

    /**
     * 删除用户
     *
     * @param id id
     * @return R
     */
    @ApiOperation(value = "删除用户信息")
    @DeleteMapping("/delete")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, paramType = "query",dataType = "Long")
    })
    public R delete(Long id) {
        userService.delete(id);
        return R.success();
    }

    /**
     * 修改用户信息
     *
     * @param req req
     * @return R
     */
    @ApiOperation(value = "修改用户信息")
    @PutMapping("/update")
    public R update(@RequestBody UserReq req) {
        userService.update(req);
        return R.success();
    }

    /**
     * 获取用户信息
     *
     * @return R
     */
    @ApiOperation(value = "获取用户信息")
    @GetMapping("/getUserInfo")
    public R<UserResp> getUserInfo() {
        return R.success(userService.getUserInfo());
    }


}
