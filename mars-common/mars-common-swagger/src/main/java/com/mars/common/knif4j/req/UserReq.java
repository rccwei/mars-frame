package com.mars.common.knif4j.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: WQ
 * @create: 2021-08-11 14:05
 */
@Data
public class UserReq implements Serializable {

    @ApiModelProperty(value = "用户名称")
    private String name;
}
