package com.mars.common.knif4j.result;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 公共响应的实体
 *
 * @author : wq
 * @date : 2020-06-18
 **/
@Data
@ApiModel(value="接口返回对象", description="接口返回对象")
public class R<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    @ApiModelProperty(value = "状态码")
    private Integer status;
    /**
     * 说明
     */
    @ApiModelProperty(value = "说明")
    private String msg;
    /**
     * 数据
     */
    @ApiModelProperty(value = "返回数据对象")
    private T data;

    public R(Integer status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public R() {
    }

    public static <T> R<T> success() {
        return new R<>(ResponseCode.SUCCESS.getCode(), "成功", null);
    }


    public static <T> R<T> success(T data) {
        return new R<>(ResponseCode.SUCCESS.getCode(), "成功", data);
    }


    public static <T> R<T> error() {
        return new R<>(ResponseCode.FAIL.getCode(), "失败", null);
    }

    public static <T> R<T> error(String msg) {
        return new R<>(ResponseCode.FAIL.getCode(), msg, null);
    }

    public static <T> R<T> error(Integer status, String msg) {
        return new R<>(status, msg, null);
    }

    public static <T> R<T> toLogin() {
        return new R<T>(401, "请登录", null);
    }

    public static <T> R<T> push() {
        return new R<T>(401, "账户在另外的设备中登录", null);
    }

    public static <T> R<T> lock() {
        return new R<T>(401, "您的账户存在违规行为", null);
    }

    public static <T> R<T> notFundFail() {
        return new R<T>(404, "接口不存在", null);
    }

    public static <T> R<T> authFail() {
        return new R(403, "没有访问权限", null);
    }
}
