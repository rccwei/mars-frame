package com.mars.common.knif4j.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mars.common.knif4j.req.UserReq;
import com.mars.common.knif4j.resp.UserResp;
import com.mars.common.knif4j.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-30 21:48:54
 */
@Service
@Slf4j
public class UserServiceImpl implements IUserService {
    @Override
    public void add(UserReq req) {

    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void update(UserReq req) {

    }

    @Override
    public UserResp getUserInfo() {
        UserResp userResp = new UserResp();
        userResp.setId(1L);
        userResp.setName("源码字节");
        log.info("返回参数:{}", JSONObject.toJSONString(userResp));
        return userResp;
    }
}
