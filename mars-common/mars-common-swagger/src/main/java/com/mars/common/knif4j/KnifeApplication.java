package com.mars.common.knif4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * swagger 整合文档
 * https://swagger.io/docs/specification/data-models/data-types/
 * 简单三步整合
 */
@SpringBootApplication
public class KnifeApplication {

    public static void main(String[] args) {
        SpringApplication.run(KnifeApplication.class, args);
    }

}
