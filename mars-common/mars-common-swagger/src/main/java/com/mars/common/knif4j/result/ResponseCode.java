package com.mars.common.knif4j.result;

/**
 * 响应code
 *
 * @author wq
 * @date 2020/6/18
 **/
public enum ResponseCode {
    /**
     * 失败
     */
    FAIL(-1, "失败"),
    /**
     * 登录
     */
    LOGIN(401, "需要登录"),
    /**
     * 成功
     */
    SUCCESS(200, "成功");
    /**
     * 响应码
     */
    private Integer code;
    /**
     * 消息
     */
    private String msg;

    ResponseCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
