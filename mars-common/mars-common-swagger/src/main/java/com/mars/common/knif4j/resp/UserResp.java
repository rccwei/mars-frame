package com.mars.common.knif4j.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @description:
 * @author: WQ
 * @create: 2021-08-10 17:42
 */
@Data
@ApiModel(value = "用户传输返回对象")
public class UserResp {


    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;
}
