package com.mars.common.knif4j.service;

import com.mars.common.knif4j.req.UserReq;
import com.mars.common.knif4j.resp.UserResp;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-30 21:48:34
 */
public interface IUserService {

    /**
     * 添加用户
     *
     * @param req req
     */
    void add(UserReq req);

    /**
     * 删除用户
     *
     * @param id id
     */
    void delete(Long id);


    /**
     * 修改用户
     *
     * @param req req
     */
    void update(UserReq req);


    /**
     * 查询用户
     *
     */
    UserResp getUserInfo();
}
