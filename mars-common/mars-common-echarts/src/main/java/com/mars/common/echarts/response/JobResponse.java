package com.mars.common.echarts.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wq
 * @date 2021-10-22 18:28
 */
@Data
public class JobResponse {

    private String name;

    private BigDecimal salary;
}
