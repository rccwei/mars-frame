package com.mars.common.echarts.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wq
 * @date 2021-10-22 18:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Teacher {

    private String username;
    private Double salary;
}
