package com.mars.common.echarts.mapper;

import com.mars.common.echarts.entity.Job;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wq
 * @since 2021-10-22
 */
public interface JobMapper extends BaseMapper<Job> {

}
