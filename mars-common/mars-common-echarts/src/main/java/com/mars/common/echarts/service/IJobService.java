package com.mars.common.echarts.service;

import com.mars.common.echarts.entity.Job;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.common.echarts.response.JobRateResponse;
import com.mars.common.echarts.response.JobResponse;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wq
 * @since 2021-10-22
 */
public interface IJobService extends IService<Job> {


    /**
     * 获取地区薪资
     *
     * @return List<Job>
     */
    List<JobResponse> averageSalaryList();

    /**
     * 获取地区最大薪资
     *
     * @return List<Job>
     */
    List<JobResponse> maxSalaryList();

    /**
     * 平均就业率
     *
     * @return List<JobResponse>
     */
    List<JobRateResponse> averageEmploymentRateRList();
}
