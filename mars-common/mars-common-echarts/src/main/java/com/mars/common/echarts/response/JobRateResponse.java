package com.mars.common.echarts.response;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author wq
 * @date 2021-10-22 22:36
 */
@Data
public class JobRateResponse {


    private String name;

    private BigDecimal salary;
}
