package com.mars.common.echarts.service;


import com.mars.common.echarts.base.PageInfo;
import com.mars.common.echarts.entity.User;
import com.mars.common.echarts.request.UserRequest;

public interface IUserService {

    /**
     * 分页
     *
     * @param request 请求参数
     * @return PageInfo<User>
     */
    PageInfo<User> page(UserRequest request);

    /**
     * 添加用户
     *
     * @param user 用户
     */
    void add();
}
