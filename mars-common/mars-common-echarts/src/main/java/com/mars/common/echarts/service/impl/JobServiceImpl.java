package com.mars.common.echarts.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mars.common.echarts.entity.Job;
import com.mars.common.echarts.mapper.JobMapper;
import com.mars.common.echarts.response.JobRateResponse;
import com.mars.common.echarts.response.JobResponse;
import com.mars.common.echarts.service.IJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wq
 * @since 2021-10-22
 */
@Service
public class JobServiceImpl extends ServiceImpl<JobMapper, Job> implements IJobService {

    @Override
    public List<JobResponse> averageSalaryList() {
        Map<String, List<Job>> map = baseMapper.selectList(null).stream().
                collect(Collectors.groupingBy(Job::getArea));
        ArrayList<JobResponse> list = new ArrayList<>();
        map.forEach((k, kList) -> {
            DoubleSummaryStatistics qtySummary = kList.stream().
                    collect(Collectors.summarizingDouble(e -> Double.parseDouble(e.getSalary())));
            JobResponse jobResponse = new JobResponse();
            jobResponse.setName(k);
            jobResponse.setSalary(BigDecimal.valueOf(qtySummary.getAverage()).setScale(2, BigDecimal.ROUND_HALF_UP));
            list.add(jobResponse);
        });
        return list;
    }

    @Override
    public List<JobResponse> maxSalaryList() {
        Map<String, List<Job>> map = baseMapper.selectList(null).stream().
                collect(Collectors.groupingBy(Job::getArea));
        ArrayList<JobResponse> list = new ArrayList<>();
        map.forEach((k, kList) -> {
            DoubleSummaryStatistics qtySummary = kList.stream().
                    collect(Collectors.summarizingDouble(e -> Double.parseDouble(e.getSalary())));
            JobResponse jobResponse = new JobResponse();
            jobResponse.setName(k);
            jobResponse.setSalary(BigDecimal.valueOf(qtySummary.getMax()).
                    setScale(2, BigDecimal.ROUND_HALF_UP));
            list.add(jobResponse);
        });
        return list;
    }

    @Override
    public List<JobRateResponse> averageEmploymentRateRList() {
        Map<String, List<Job>> map = baseMapper.selectList(new LambdaQueryWrapper<Job>()
                .isNotNull(Job::getEmploymentRate)).stream().
                collect(Collectors.groupingBy(Job::getName));
        ArrayList<JobRateResponse> list = new ArrayList<>();
        map.forEach((k, kList) -> {
            DoubleSummaryStatistics qtySummary = kList.stream().
                    collect(Collectors.summarizingDouble(e -> Double.parseDouble(e.getEmploymentRate().toString())));
            JobRateResponse response = new JobRateResponse();
            response.setName(k);
            response.setSalary(BigDecimal.valueOf(qtySummary.getAverage()).setScale(2, BigDecimal.ROUND_HALF_UP));
            list.add(response);
        });
        return list;
    }
}
