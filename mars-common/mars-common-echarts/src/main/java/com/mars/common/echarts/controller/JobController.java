package com.mars.common.echarts.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.mars.common.echarts.base.PageInfo;
import com.mars.common.echarts.entity.Job;
import com.mars.common.echarts.entity.User;
import com.mars.common.echarts.request.UserRequest;
import com.mars.common.echarts.response.JobRateResponse;
import com.mars.common.echarts.response.JobResponse;
import com.mars.common.echarts.service.IJobService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wq
 * @since 2021-10-22
 */
@Controller
@AllArgsConstructor
public class JobController {

    final IJobService jobService;


    @GetMapping("/job")
    public String index() {
        return "job";
    }


    @RequestMapping("/getAverageList")
    @ResponseBody
    public List<JobResponse> getAverageList() {
        return jobService.averageSalaryList();
    }


    @RequestMapping("/getMaxSalaryList")
    @ResponseBody
    public List<JobResponse> getMaxSalaryList() {
        return jobService.maxSalaryList();
    }

    @RequestMapping("/averageEmploymentRateRList")
    @ResponseBody
    public List<JobRateResponse> averageEmploymentRateRList() {
        return jobService.averageEmploymentRateRList();
    }

}
