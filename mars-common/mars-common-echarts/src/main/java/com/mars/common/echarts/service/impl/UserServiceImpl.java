package com.mars.common.echarts.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.common.echarts.base.PageInfo;
import com.mars.common.echarts.entity.User;
import com.mars.common.echarts.mapper.UserMapper;
import com.mars.common.echarts.request.UserRequest;
import com.mars.common.echarts.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Override
    public PageInfo<User> page(UserRequest request) {
        IPage<User> page = new Page<>(request.getPageNumber(), request.getPageSize());
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        List<User> records = baseMapper.selectPage(page, wrapper).getRecords();
        if (!CollectionUtils.isEmpty(records)) {
            return new PageInfo<>(page.getTotal(), records);
        }
        return null;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add() {
        User user = new User();
        user.setAvatar("http://baidu.com");
        user.setGender(1);
        user.setStatus(1);
        user.setNickname("wq");
        user.setOpenId("wangqiang");
        save(user);
        try {
            saveOther();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void saveOther() {

        System.out.println(1 / 0);
        User user = new User();
        user.setAvatar("http://baidu.com");
        user.setGender(1);
        user.setStatus(1);
        user.setNickname("wq1");
        user.setOpenId("wangqiang1");
        save(user);
    }
}
