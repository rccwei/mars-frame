package com.mars.common.echarts.controller;

import com.mars.common.echarts.entity.Teacher;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class IndexController {

    @GetMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/echarts_ajax")
    @ResponseBody
    public List<Teacher> echart_ajax() {
        List<Teacher> userList = new ArrayList<>();
        userList.add(new Teacher("Peter", 8000.0));
        userList.add(new Teacher("张三", 6000.0));
        userList.add(new Teacher("李四", 5000.0));
        userList.add(new Teacher("王五", 9000.0));
        return userList;
    }
}
