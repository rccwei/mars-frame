package com.mars.common.echarts.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.mars.common.echarts.base.PageInfo;
import com.mars.common.echarts.entity.User;
import com.mars.common.echarts.request.UserRequest;
import com.mars.common.echarts.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:39
 */
@RestController
@AllArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final IUserService userService;

    /**
     * 查询全部
     *
     * @return List<User>
     */
    @PostMapping("/page")
    public List<User> page(@RequestBody UserRequest request) {
        PageInfo<User> page = userService.page(request);
        System.out.println(JSONObject.toJSONString(page));
        if (CollectionUtils.isNotEmpty(page.getList())) {
            return page.getList();
        }
        return null;
    }

    /**
     * 添加
     */
    @GetMapping("/add")
    public void add() {
        userService.add();
    }
}
