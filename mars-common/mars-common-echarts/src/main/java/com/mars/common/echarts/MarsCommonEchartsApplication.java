package com.mars.common.echarts;

import com.mrxu.monitor.EnableMonitor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 * 各地区平均薪资 再做一个岗位年龄段的柱状图
 */
@EnableMonitor
@SpringBootApplication
@MapperScan("com.mars.common.echarts.mapper")
public class MarsCommonEchartsApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonEchartsApplication.class, args);
    }

}
