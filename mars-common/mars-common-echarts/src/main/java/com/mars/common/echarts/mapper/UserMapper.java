package com.mars.common.echarts.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.common.echarts.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {


}
