package com.mars.common.actuatar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * http://localhost:9004/actuator
 */
@SpringBootApplication
public class MarsCommonActuatarApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonActuatarApplication.class, args);
    }

}
