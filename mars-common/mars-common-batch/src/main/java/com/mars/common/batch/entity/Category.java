package com.mars.common.batch.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @date 2023-10-14 14:04
 */
public class Category implements Comparable {

    private String name;
    private double avg;// 平均值
    private double min;// 最小值
    private double max;// 最大值
    private int greaterAvgCount;// 高于平均价格的电器数量
    private int lessAvgCount;// 低于平均价格的电器数量

    private List<Product> productList = new ArrayList<Product>();// 产品集合 一 对 多

    public Category(String name, double avg, double min, double max) {
        this.name = name;
        this.avg = avg;
        this.min = min;
        this.max = max;
    }

    public Category(String name) {
        this.name = name;
    }

    public void add(Product product) {
        productList.add(product);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAvg() {
        return avg;
    }

    public void setAvg(double avg) {
        this.avg = avg;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public int getGreaterAvgCount() {
        return greaterAvgCount;
    }

    public void setGreaterAvgCount(int greaterAvgCount) {
        this.greaterAvgCount = greaterAvgCount;
    }

    public int getLessAvgCount() {
        return lessAvgCount;
    }

    public void setLessAvgCount(int lessAvgCount) {
        this.lessAvgCount = lessAvgCount;
    }

    @Override
    public int compareTo(Object obj) {
        Category category = (Category) obj;
        if (this.avg < category.getAvg()) {
            return 1;
        } else if (this.avg > category.getAvg()) {
            return -1;
        }
        return 0;
    }
}
