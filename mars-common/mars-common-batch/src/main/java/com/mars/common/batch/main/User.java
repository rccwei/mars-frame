package com.mars.common.batch.main;

/**
 * @author wq
 * @date 2023-10-12 20:30
 */
public class User {
    /**
     * 卡号
     */
    private String cardAccount;

    /**
     * 名称
     */
    private String name;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 联系方式
     */
    private String phone;

    /**
     * 住址
     */
    private String address;

    public User(String cardAccount, String name, String idCard, String phone, String address) {
        this.cardAccount = cardAccount;
        this.name = name;
        this.idCard = idCard;
        this.phone = phone;
        this.address = address;
    }

    public String getCardAccount() {
        return cardAccount;
    }

    public void setCardAccount(String cardAccount) {
        this.cardAccount = cardAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "User{" +
                "cardAccount='" + cardAccount + '\'' +
                ", name='" + name + '\'' +
                ", idCard='" + idCard + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
