package com.mars.common.batch.entity;

/**
 * @author wq
 * @date 2023-10-14 14:03
 */
public class Product {

    private String model;// 电器型号

    private double price;// 价格
    private int quantity;// 库存

    private Category category;//产品分类


    public Product(Category category, String model, double price, int quantity) {
        this.category = category;
        this.model = model;
        this.price = price;
        this.quantity = quantity;
        this.category.add(this);
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
