package com.mars.common.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonBatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonBatchApplication.class, args);
    }

}
