package com.mars.common.batch.entity;

import com.mars.common.batch.entity.Category;
import com.mars.common.batch.entity.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author wq
 * @date 2023-10-14 14:05
 */
public class TestSale {

    private static List<Product> productList = new ArrayList<Product>();// 产品集合
    private static List<Category> categoryList = new ArrayList<Category>();// 分类集合

    public static void main(String[] args) {
        init();

        System.out.println("统计每类家电的平均价格，最高价和最低价；");
        categoryAvgMaxMinPrice();

        System.out.println("---------------------------------------------------------------------");

        System.out.println("将4类家电按平均价格降序排序；");
        avgDesendingSort();

        System.out.println("---------------------------------------------------------------------");

        System.out.println("统计每类电器高于平均价格和低于平均价格的电器数量；");
        caculateCount();

        System.out.println("---------------------------------------------------------------------");

        System.out.println("输入电器型号查询电器的价格和库存；");
        searchByModel("Sony1000");

    }

    public static void categoryAvgMaxMinPrice() {
        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            List<Product> list = category.getProductList();

            double total = 0; // 总价格
            double avg = 0; // 平均值
            double min = list.get(0).getPrice();// 最小值
            double max = 0;// 最大值

            for (int j = 0; j < list.size(); j++) {
                Product product = list.get(j);
                total += product.getPrice();
                if (min > product.getPrice()) {
                    min = product.getPrice();
                }

                if (max < product.getPrice()) {
                    max = product.getPrice();
                }
            }

            avg = total / list.size();

            category.setAvg(avg);
            category.setMax(max);
            category.setMin(min);

            System.out.println(category.getName() + " : 平均价格=" + new BigDecimal(avg).setScale(2, BigDecimal.ROUND_HALF_UP) + " , 最高价=" + max + " , 最低价=" + min);

        }
    }


    // （3）将4类家电按平均价格降序排序；
    public static void avgDesendingSort() {
        Collections.sort(categoryList);
        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            System.out.println(category.getName() + " : " + new BigDecimal(category.getAvg()).setScale(2, BigDecimal.ROUND_HALF_UP));
        }
    }

    // （4）统计每类电器高于平均价格和低于平均价格的电器数量；
    public static void caculateCount() {
        for (int i = 0; i < categoryList.size(); i++) {
            Category category = categoryList.get(i);
            List<Product> productList = category.getProductList();
            int greaterAvgCount = 0;// 高于平均价格的电器数量
            int lessAvgCount = 0;// 低于平均价格的电器数量

            for (int j = 0; j < productList.size(); j++) {
                Product product = productList.get(j);
                if (product.getPrice() > category.getAvg()) {
                    greaterAvgCount++;
                }

                if (product.getPrice() < category.getAvg()) {
                    lessAvgCount++;
                }
            }

            category.setGreaterAvgCount(greaterAvgCount);
            category.setLessAvgCount(lessAvgCount);

            System.out.println(category.getName() + " : 平均价格=" + new BigDecimal(category.getAvg()).setScale(2, BigDecimal.ROUND_HALF_UP) + " , 高于平均价格的电器数量=" + category.getGreaterAvgCount() + " , 低于平均价格的电器数量=" + category.getLessAvgCount());
        }
    }

    // （5）输入电器型号查询电器的价格和库存。
    public static void searchByModel(String model) {
        for (int i = 0; i < productList.size(); i++) {
            Product product = productList.get(i);
            if (product.getModel().equals(model)) {
                System.out.println(product.getCategory().getName() + " : 价格=" + product.getPrice() + " , 库存=" + product.getQuantity());
            }
        }
    }

    public static void init() {
        // （1）给4类家电各输入3个品牌的电器型号、价格和库存；
        Category colorTVCategory = new Category("彩电 ");
        Category refrigeratorCategory = new Category("冰箱 ");
        Category washingCategory = new Category("洗衣机");
        Category fanCategory = new Category("电风扇");
        categoryList.add(colorTVCategory);
        categoryList.add(refrigeratorCategory);
        categoryList.add(washingCategory);
        categoryList.add(fanCategory);

        productList.add(new Product(colorTVCategory, "Sony1000", 5000, 100));
        productList.add(new Product(colorTVCategory, "Sony1001", 5500, 50));
        productList.add(new Product(colorTVCategory, "Sony1002", 3500, 30));
        productList.add(new Product(refrigeratorCategory, "Haier1000", 4500, 30));
        productList.add(new Product(refrigeratorCategory, "Haier1001", 1500, 30));
        productList.add(new Product(refrigeratorCategory, "Haier1002", 2500, 30));
        productList.add(new Product(washingCategory, "Haier2001", 1500, 20));
        productList.add(new Product(washingCategory, "Haier2002", 1300, 10));
        productList.add(new Product(washingCategory, "Haier2002", 1100, 10));
        productList.add(new Product(fanCategory, "Dongfeng1000", 500, 10));
        productList.add(new Product(fanCategory, "Dongfeng1001", 600, 20));
        productList.add(new Product(fanCategory, "Dongfeng1002", 800, 20));

    }

}
