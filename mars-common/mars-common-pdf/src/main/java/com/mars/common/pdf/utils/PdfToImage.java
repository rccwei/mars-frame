package com.mars.common.pdf.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * 说明
 * 将pdf文件转换为图片
 *
 * @author GQ
 * @date 2022/11/28
 */
@Slf4j
public class PdfToImage {


    /**
     * pdf转图片
     *
     * @param file file
     */
    public static void pdfToImage(MultipartFile file) {
        String userHome = System.getProperty("user.home");
        // 假设桌面文件夹名称为 "Desktop"
        String desktopFolder = "Desktop";
        // 创建一个表示桌面路径的字符串
        String desktopPath = userHome + File.separator + desktopFolder;
        String filename = file.getOriginalFilename();
        pdfToImage(desktopPath, filename, desktopPath + "\\image\\");
    }


    /**
     * 转换全部的pdf
     *
     * @param fileAddress 文件地址
     * @param filename    PDF文件名(不包含后缀)
     * @param imagePath   生成图片路径
     */
    public static void pdfToImage(String fileAddress, String filename, String imagePath) {
        // 将pdf装图片 并且自定义图片的格式大小
        // 注意文件路径拼接方式
        File file = new File(fileAddress.concat(File.separator).concat(filename));
        File imageFile = new File(imagePath);
        if (!imageFile.exists()) {
            imageFile.mkdirs();
        }
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            int pageCount = doc.getNumberOfPages();
            for (int i = 0; i < pageCount; i++) {
                // Windows native DPI
                BufferedImage image = renderer.renderImageWithDPI(i, 144);
                ImageIO.write(image, "png",
                        new File(imagePath.concat(File.separator).concat(filename)
                                .concat("_").concat(String.valueOf(i + 1))
                                .concat(".").concat("png")));
            }
            log.info("转换成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
