package com.mars.common.pdf.controller;

import com.mars.common.pdf.service.IPdfService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author 程序员Mars
 */
@RestController
@AllArgsConstructor
public class IndexController {

    private final IPdfService pdfService;

    /**
     * pdf转图片
     *
     * @param file
     */
    @PostMapping("/pdfToImage")
    public void pdfToImage(@RequestParam("file") MultipartFile file) {
        pdfService.pdfToImage(file);
    }

    /**
     * pdf转word
     *
     * @param file file
     */
    @PostMapping("/pdfToWord")
    public void pdfToWord(@RequestParam("file") MultipartFile file) {
        pdfService.pdfToWord(file);
    }


    /**
     * word转pdf
     *
     * @param file file
     */
    @PostMapping("/wordToPdf")
    public void wordToPdf(@RequestParam("file") MultipartFile file) {
        pdfService.wordToPdf(file);
    }

}
