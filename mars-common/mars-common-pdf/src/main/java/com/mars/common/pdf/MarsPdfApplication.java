package com.mars.common.pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 程序员Mars
 */
@SpringBootApplication
public class MarsPdfApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsPdfApplication.class, args);
    }

}
