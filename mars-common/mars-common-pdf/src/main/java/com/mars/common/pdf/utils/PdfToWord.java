package com.mars.common.pdf.utils;

import com.aspose.pdf.Document;
import com.aspose.pdf.SaveFormat;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;

/**
 * 说明
 * 此方法可将pdf不失真的转换为doc文档，但是耗费时间与文件大小成正比
 * <p>
 * 注意，该方法需要本地引入jar包
 *
 * @author 程序员Mars
 * @date 2023/12/28
 */
public class PdfToWord {


    public static void pdfToWord(MultipartFile file) {
        long old = System.currentTimeMillis();
        try {
            String userHome = System.getProperty("user.home");
            // 假设桌面文件夹名称为 "Desktop"
            String desktopFolder = "Desktop";
            // 创建一个表示桌面路径的字符串
            String desktopPath = userHome + File.separator + desktopFolder;
            String filename = file.getOriginalFilename();
            String fullPath = desktopPath + "\\" + filename;
            //新建一个word文档
            String wordPath = fullPath.substring(0, fullPath.lastIndexOf(".")) + ".doc";
            FileOutputStream os = new FileOutputStream(wordPath);
            //doc是将要被转化的word文档
            Document doc = new Document(fullPath);
            //全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF, EPUB, XPS, SWF 相互转换
            doc.save(os, SaveFormat.DocX);
            os.close();
            //转化用时
            long now = System.currentTimeMillis();
            System.out.println("Pdf 转 Word 共耗时：" + ((now - old) / 1000.0) + "秒");
        } catch (Exception e) {
            System.out.println("Pdf 转 Word 失败...");
            e.printStackTrace();
        }
    }
}
