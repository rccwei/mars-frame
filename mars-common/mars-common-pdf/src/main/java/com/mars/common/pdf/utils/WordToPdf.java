package com.mars.common.pdf.utils;

import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * word 转pdf工具类
 *
 * @author 程序员Mars
 * @date 2023/10/28
 */
public class WordToPdf {



    public static void wordToPdf(MultipartFile multipartFile) {
        FileOutputStream os = null;
        try {
            String userHome = System.getProperty("user.home");
            // 假设桌面文件夹名称为 "Desktop"
            String desktopFolder = "Desktop";
            // 创建一个表示桌面路径的字符串
            String desktopPath = userHome + File.separator + desktopFolder;

            String filename = multipartFile.getOriginalFilename();
            String wordPath = desktopPath + "\\" + filename;
            String fileName = filename.split("\\.")[1];
            String pdfPath = userHome + File.separator + desktopFolder + "\\pdf\\" + fileName + ".pdf";

            String s = "<License><Data><Products><Product>Aspose.Total for Java</Product><Product>Aspose.Words for Java</Product></Products><EditionType>Enterprise</EditionType><SubscriptionExpiry>20991231</SubscriptionExpiry><LicenseExpiry>20991231</LicenseExpiry><SerialNumber>8bfe198c-7f0c-4ef8-8ff0-acc3237bf0d7</SerialNumber></Data><Signature>sNLLKGMUdF0r8O1kKilWAGdgfs2BvJb/2Xp8p5iuDVfZXmhppo+d0Ran1P9TKdjV4ABwAgKXxJ3jcQTqE/2IRfqwnPf8itN8aFZlV3TJPYeD3yWE7IT55Gz6EijUpC7aKeoohTb4w2fpox58wWoF3SNp6sK6jDfiAUGEHYJ9pjU=</Signature></License>";
            ByteArrayInputStream is = new ByteArrayInputStream(s.getBytes());
            License license = new License();
            license.setLicense(is);
            // 新建一个空白pdf文档
            File file = new File(pdfPath);
            os = new FileOutputStream(file);
            // Address是将要被转化的word文档
            Document doc = new Document(wordPath);
            doc.save(os, SaveFormat.PDF);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
