package com.mars.common.pdf.entity;

import lombok.Data;

@Data
public class Person {
    //名称
    private String personName;
    //年龄
    private Integer personAge;
    //性格描述
    private String personalityDesc;
    //性别
    private String personGender;
    //职业
    private String personVocation;
    //现居地址
    private String address;
    //创建时间
    private String createTime;

}
