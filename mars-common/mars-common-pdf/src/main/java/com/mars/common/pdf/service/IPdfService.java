package com.mars.common.pdf.service;


import org.springframework.web.multipart.MultipartFile;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-02 15:33:44
 */
public interface IPdfService {


    /**
     * pdf转word
     *
     * @param file file
     */
    void pdfToWord(MultipartFile file);

    /**
     * pdf转图片
     *
     * @param file file
     */
    void pdfToImage(MultipartFile file);

    /**
     * word转pdf
     *
     * @param file file
     */
    void wordToPdf(MultipartFile file);

}
