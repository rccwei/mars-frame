package com.mars.common.pdf.service.impl;


import com.mars.common.pdf.service.IPdfService;
import com.mars.common.pdf.utils.PdfToImage;
import com.mars.common.pdf.utils.PdfToWord;
import com.mars.common.pdf.utils.WordToPdf;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author 程序员Mars
 */
@Service
public class PdfServiceImpl implements IPdfService {


    @Override
    public void pdfToWord(MultipartFile file) {
        PdfToWord.pdfToWord(file);
    }

    @Override
    public void pdfToImage(MultipartFile file) {
        PdfToImage.pdfToImage(file);
    }

    @Override
    public void wordToPdf(MultipartFile file) {
        WordToPdf.wordToPdf(file);
    }
}
