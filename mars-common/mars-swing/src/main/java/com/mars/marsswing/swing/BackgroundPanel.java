package com.mars.marsswing.swing;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @author 程序猿Mars
 * @date 2021-12-15 10:11
 */
public class BackgroundPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 11111111111111L;
    private BufferedImage background;
    private TexturePaint texture;

    public void changeBackground(final String image) throws IOException {
        this.background = ImageIO.read(this.getClass().getClassLoader().getResource(image));
        Rectangle rect = new Rectangle(0, 0, this.background.getWidth(), this.background.getHeight());
        this.texture = new TexturePaint(this.background, rect);
    }

    @Override
    public void paintComponent(final Graphics g) {

        if (this.texture != null) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setPaint(this.texture);
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        }

        super.paintComponent(g);
    }
}
