package com.mars.marsswing.swing02;

import javax.swing.*;

/**
 * @author wq
 * @date 2022/1/10 13:54
 */
public class Demo {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createGUI();
            }
        });
    }

    private static void createGUI() {
        // 确保一个漂亮的外观风格
        JFrame.setDefaultLookAndFeelDecorated(true);
        JFrame frame = new JFrame("MARS");
        frame.setSize(350, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //添加文本
        JLabel label = new JLabel("Hello  world");
        frame.add(label);

        //显示窗口
        frame.pack();
        frame.setVisible(true);

    }
}
