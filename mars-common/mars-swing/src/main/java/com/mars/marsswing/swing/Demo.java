package com.mars.marsswing.swing;

import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 * -Dfile.encoding=gbk
 */
public class Demo {


    public static void main(String[] args) {

        Frame f = new Frame("Mars");
        f.setBounds(100, 100, 500, 300);
        //设置最佳大小
        f.pack();
        f.setVisible(true);
    }
}
