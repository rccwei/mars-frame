package com.mars.marsswing.jframe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/12 22:09
 * 弹簧布局  就是定位好一个组件  其他组件都参照整个组件来定位
 */
public class SpringLayoutTest {


    public static void main(String[] args) {
        JFrame jFrame = new JFrame("这是jFrame的标题");
        jFrame.setSize(600, 400);
        //设置窗体图标
        initLogo(jFrame);
        //获取容器面板
        Container contentPane = jFrame.getContentPane();
        SpringLayout springLayout = new SpringLayout();
        //jpanel 默认就是流布局
        JPanel jPanel = new JPanel(springLayout);

        JLabel title = new JLabel("文章标题:");
        title.setPreferredSize(new Dimension(200, 30));
        //获取布局约束
        SpringLayout.Constraints constraints = springLayout.getConstraints(title);
        constraints.setX(Spring.constant(100));
        constraints.setY(Spring.constant(50));
        TextField textField = new TextField();
        textField.setPreferredSize(new Dimension(200, 30));

        SpringLayout.Constraints constraints1 = springLayout.getConstraints(textField);
        Spring eastConstraints = constraints.getConstraint(SpringLayout.EAST);
        constraints1.setConstraint(SpringLayout.EAST, Spring.sum(eastConstraints, Spring.constant(20)));
        constraints1.setConstraint(SpringLayout.NORTH, constraints.getConstraint(SpringLayout.NORTH));
//
//        JLabel authorLabel = new JLabel("作者名称:");
//        TextField authorTextField = new TextField();
//
//        JLabel contentLabel = new JLabel("请输入内容:");
//        TextArea textArea = new TextArea(4, 10);

        jPanel.add(title);
        jPanel.add(textField);
//        jPanel.add(authorLabel);
//        jPanel.add(authorTextField);
//        jPanel.add(contentLabel);
//        jPanel.add(textArea);


        jFrame.add(jPanel);
        //居中 两种方式
        jFrame.setLocationRelativeTo(null);
        //关闭后推出程序
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置不可以改变大小
        jFrame.setResizable(false);
        jFrame.setVisible(true);

    }

    /**
     * 初始化logo
     *
     * @param jFrame jFrame
     */
    private static void initLogo(JFrame jFrame) {
        URL resource = SpringLayoutTest.class.getClassLoader().getResource("logo.jpg");
        assert resource != null;
        Image image = new ImageIcon(resource).getImage();
        jFrame.setIconImage(image);
    }
}
