package com.mars.marsswing.swing01;/*
 * 展示功能界面
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class Showfuntion extends JFrame implements ActionListener {
	
    	JButton jb1,jb2,jb3,jb4;
	    JLabel jl;
	    JPanel jp1,jp2,jp3,jp4,jp5;
	
	    public Showfuntion()
	   {
    	jb1=new JButton("列车信息管理");
    	jb2=new JButton("  订单管理   ");
    	jb3=new JButton("  退票管理   ");
    	jb4=new JButton("统计票务管理");
    	jl=new JLabel("欢迎您使用此系统");
    	jp1=new JPanel();
    	jp2=new JPanel();
    	jp3=new JPanel();
    	jp4=new JPanel();
    	jp5=new JPanel();
    	
    	jp1.add(jl);
    	jp2.add(jb1);
    	jp3.add(jb2);
    	jp4.add(jb3);
    	jp5.add(jb4);
    	this.setLayout(new GridLayout(5,1));
    	this.add(jp1,BorderLayout.NORTH);
    	this.add(jp2);
    	this.add(jp3);
    	this.add(jp4);
    	this.add(jp5);
    	this.setSize(300,300);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	this.setVisible(true);
    	jb1.addActionListener(this);
    	jb2.addActionListener(this);
    	jb3.addActionListener(this);
    	jb4.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jb1)
		{
			new Test4();    //列车管理
		}
		else if(e.getSource()==jb2)
		{
			new Test5();          //订单管理
		}
		else if(e.getSource()==jb3)
		{
			new Dropticket();     //退票管理
		}
		else if(e.getSource()==jb4)
		{
			new Conntticket();     //统计管理
		}
	      
	}
	
}

