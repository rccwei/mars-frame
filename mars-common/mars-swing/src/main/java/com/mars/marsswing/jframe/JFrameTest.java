package com.mars.marsswing.jframe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/12 22:09
 */
public class JFrameTest {


    public static void main(String[] args) {
        JFrame jFrame = new JFrame("这是jFrame的标题");
        jFrame.setSize(600, 400);
        //设置窗体图标
        URL resource = JFrameTest.class.getClassLoader().getResource("logo.jpg");
        assert resource != null;
        Image image = new ImageIcon(resource).getImage();
        jFrame.setIconImage(image);

        //设置按钮
        JButton jButton = new JButton("这是按钮");
        jFrame.getContentPane().add(jButton);

        //居中 两种方式
        jFrame.setLocationRelativeTo(null);
        //关闭后推出程序
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置不可以改变大小
        jFrame.setResizable(false);
        jFrame.setVisible(true);

    }
}
