package com.mars.marsswing.swing.jui;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;


public class WenDuJi extends JFrame {
    JPanel contentPane;
    private JTextField textField;
    private JTextField textField_1;
    private JLabel label;
    private JLabel label_1;
    private JLabel label_2;
    private JRadioButton[] radioButton;
    private JButton button;//转化按钮
    private JButton button_1;//退出按钮
    private ButtonGroup bg;


    public WenDuJi() {
        contentPane = new JPanel(); //指定容器
        setContentPane(contentPane);//设置 contentPane 属性
        contentPane.setOpaque(false);//设置面板背景为透明(这一步很重要）


        int width = Toolkit.getDefaultToolkit().getScreenSize().width;//获取你的屏幕的宽和高
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        setLocation(width / 2 - 200, height / 2 - 150);
        setTitle("\u6E29\u5EA6\u8F6C\u6362\u5668");
        getContentPane().setLayout(null);

        textField = new JTextField();//摄氏温度
        textField.setText("0.0");
        textField.setBounds(50, 63, 84, 27);
        getContentPane().add(textField);
        textField.setColumns(10);

        label = new JLabel("\u6444\u6C0F\u6E29\u5EA6");
        label.setBounds(137, 69, 54, 15);
        getContentPane().add(label);

        label_1 = new JLabel("=");
        label_1.setBounds(201, 69, 54, 15);
        getContentPane().add(label_1);

        textField_1 = new JTextField();//华氏温度
        textField_1.setText("0.0");
        textField_1.setBounds(223, 66, 90, 24);
        getContentPane().add(textField_1);
        textField_1.setColumns(10);

        label_2 = new JLabel("\u534E\u6C0F\u6E29\u5EA6");
        label_2.setBounds(323, 69, 54, 15);
        getContentPane().add(label_2);

        radioButton = new JRadioButton[2];
        radioButton[0] = new JRadioButton("\u6444\u6C0F\u8F6C\u534E\u6C0F", true);
        radioButton[0].setBounds(100, 142, 121, 23);
        getContentPane().add(radioButton[0]);

        radioButton[1] = new JRadioButton("\u534E\u6C0F\u8F6C\u6444\u6C0F");
        radioButton[1].setBounds(223, 142, 121, 23);
        getContentPane().add(radioButton[1]);

        bg = new ButtonGroup();
        bg.add(radioButton[0]);
        bg.add(radioButton[1]);

        button = new JButton("\u6E29\u5EA6\u8F6C\u5316");
        button.setBounds(100, 196, 93, 23);
        getContentPane().add(button);


        button.addActionListener(e -> {
            double tc = Double.parseDouble(textField.getText().trim());
            double tf = Double.parseDouble(textField_1.getText().trim());

            if (e.getSource() == button) {
                if (radioButton[0].isSelected()) {//输入摄氏温度
                    textField_1.setText(WenDuJi(tc, tf, true));
                    System.out.println("tc:" + tc);
                    if (tc < 0) {
                        init1();
                    }
                    if (tc >= 0 && tc < 10) {
                        init2();
                    }
                    if (tc >= 10 && tc < 20) {
                        init3();
                    }
                    if (tc >= 20) {
                        init4();
                    }

                }
                if (radioButton[1].isSelected()) {//输入华氏温度
                    textField.setText(WenDuJi(tc, tf, false));
                    if (tf < 0)
                        init1();
                    if (tf >= 0 && tf < 10)
                        init2();
                    if (tf >= 10 && tf < 20)
                        init3();
                    if (tf >= 20)
                        init4();
                }
            }
        });

        button_1 = new JButton("\u9000\u51FA");
        button_1.setBounds(237, 196, 93, 23);
        getContentPane().add(button_1);
        button_1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == button_1) {
                    System.exit(0);
                }
            }
        });
        setSize(450, 300);
        setVisible(true);
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public String WenDuJi(final double tc, final double tf, boolean flag)//用于温度转换的方法-----业务处理
    {
        double wdc = 0.0;
        DecimalFormat df = new DecimalFormat("0.00");//小数格式化，引号中的0.000表示保留小数点后二位（第四位四舍五入）
        String str;
        if (flag) {
            wdc = 9 * (tc - 10) / 5 + 50;    //摄氏温度转化为华氏温度
        } else {
            wdc = 5 * (tf - 50) / 9 + 10;    //华氏温度转化为摄氏温度
        }
        str = df.format(wdc);
        return str;
    }

    public void init1() {

        System.out.println("init1");
        ImageIcon background = new ImageIcon("D:\\images\\3.jpg");
        JLabel label = new JLabel(background);
        getContentPane().add(label);
        label.setBounds(0, 0, 700, 520);

    }

    public void init2() {
        System.out.println("init2");
        ImageIcon background = new ImageIcon("D:\\images\\4.jpg");
        JLabel label = new JLabel(background);
        getContentPane().add(label);
        label.setBounds(0, 0, 700, 520);


    }

    public void init3() {
        System.out.println("init3");
        ImageIcon background = new ImageIcon("D:\\images\\2.jpg");
        JLabel label = new JLabel(background);
        getContentPane().add(label);
        label.setBounds(0, 0, 700, 520);
    }

    public void init4() {
        System.out.println("init4");
        ImageIcon background = new ImageIcon("D:\\images\\1.jpg");
        JLabel label = new JLabel(background);
        getContentPane().add(label);
        label.setBounds(0, 0, 700, 520);

    }


    public static void main(String[] args) {
        new WenDuJi();
    }
}