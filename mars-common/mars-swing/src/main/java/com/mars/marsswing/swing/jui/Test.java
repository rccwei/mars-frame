package com.mars.marsswing.swing.jui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 程序猿Mars
 * @date 2021-12-14 10:37
 */
public class Test extends JFrame implements ActionListener {
    Container ct;
    //创建背景面板。
    BackgroundPanel bgp;

    //创建一个按钮，用来证明我们的确是创建了背景图片，而不是一张图片。
    JButton jb;

    public static void main(String[] args) {
        new Test();
    }

    public Test() {
        //不采用任何布局方式。
        ct = this.getContentPane();
        this.setLayout(null);


        //创建按钮
        jb = new JButton("测试按钮");
        jb.setBounds(60, 30, 160, 30);
        jb.addActionListener(this);
        ct.add(jb);

        this.setSize(400, 300);
        this.setLocation(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e);
        //在这里随便找一张400*300的照片既可以看到测试结果。
        bgp = new BackgroundPanel((new ImageIcon("D:\\images\\1.jpg")).getImage());
        bgp.setBounds(0, 0, 400, 300);
        ct.add(bgp);
    }
}

class BackgroundPanel extends JPanel {
    Image im;

    public BackgroundPanel(Image im) {
        this.im = im;
        this.setOpaque(true);
    }

    //Draw the back ground.
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.drawImage(im, 0, 0, this.getWidth(), this.getHeight(), this);

    }


}
