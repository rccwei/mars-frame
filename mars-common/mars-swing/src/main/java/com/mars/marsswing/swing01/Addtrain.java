package com.mars.marsswing.swing01;
/*
 * 添加信息弹出窗口
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Addtrain extends JDialog implements ActionListener {
    //定义swing部件
    JLabel jl1, jl2, jl3, jl4;
    JButton jb1, jb2;
    JTextField jtf1, jtf2, jtf3, jtf4;
    JPanel jp1, jp2, jp3;

    //定义构造函数
    //oener 是父窗口
    //title 是窗口名
    //modal 表示是模式窗口还是非模式窗口
    public Addtrain(Frame owner, String title, boolean modal) {
        super(owner, title, modal);//调用父类方法
        jl1 = new JLabel("列车号   ");
        jl2 = new JLabel("列车类型");
        jl3 = new JLabel("始发地   ");
        jl4 = new JLabel("目的地   ");

        jtf1 = new JTextField();
        jtf2 = new JTextField();
        jtf3 = new JTextField();
        jtf4 = new JTextField();

        jb1 = new JButton("添加");
        jb2 = new JButton("取消");

        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();

        //布局
        jp1.setLayout(new GridLayout(4, 1));
        jp2.setLayout(new GridLayout(4, 1));

        //添加按钮
        jp1.add(jl1);
        jp1.add(jl2);
        jp1.add(jl3);
        jp1.add(jl4);

        jp2.add(jtf1);
        jp2.add(jtf2);
        jp2.add(jtf3);
        jp2.add(jtf4);

        jp3.add(jb1);
        jp3.add(jb2);

        this.add(jp1, BorderLayout.WEST);
        this.add(jp2, BorderLayout.CENTER);
        this.add(jp3, BorderLayout.SOUTH);

        //添加事件监听
        jb1.addActionListener(this);
        jb2.addActionListener(this);
        //画面可见
        this.setSize(500, 500);
        //this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO 自动生成的方法存根
        if (e.getSource() == jb1) {
            Trainmodel temp = new Trainmodel();
            String sql = "insert into train values(?,?,?,?)";
            String[] paras = {jtf1.getText(), jtf2.getText(), jtf3.getText(), jtf4.getText()};
            if (!temp.Update(sql, paras)) {
                //消息框提示
                JOptionPane.showMessageDialog(this, "添加失败");
            } else {
                JOptionPane.showMessageDialog(this, "添加成功");
                this.dispose();//关窗口
            }
        } else {
            JOptionPane.showMessageDialog(this, "您取消了添加");
            this.dispose();//关窗口
        }
    }

}
	
