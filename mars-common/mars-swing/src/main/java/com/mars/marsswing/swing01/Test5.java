package com.mars.marsswing.swing01;
/*
 * 完成简单的火车票管理系统，列车信息查询
 * 添加了类的封装。
 */
import javax.swing.*;

import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
public class Test5 extends JFrame implements ActionListener {
	
	Ticketmodel model;//全局变量，不用重复声明
	//定义控件
	JPanel jp1,jp2;
	
	JButton jb1,jb2,jb3,jb4;
	JTable jt;
	JScrollPane jsp;
	
    //rowDate存放行数据，colnmnName存放列名
    Vector rowData,columnName;
    //定义数据库所需的变量
	public static void main(String[] args)
	{
		Test5 test5=new Test5();
	}
	
	//定义构造函数
	public Test5()
	{
		//第一面板
		//jp1=new JPanel();
		//jtf=new JTextField(10);//文本框大小标注一下
		//jb1=new JButton("查询");
		//jb1.addActionListener(this);
	   //监听和响应在同一个类里才可以使用
		//jl1=new JLabel("请输入用户名");
		//把各个控件加入jp1;
		//jp1.add(jl1);
		//jp1.add(jtf);
		//jp1.add(jb1);
		//第二面板
		jp2=new JPanel();
		//把各个控件加入jp2;
		jb1=new JButton("查询订单");
		jb1.addActionListener(this); 
		jb2=new JButton("删除订单");
		jb2.addActionListener(this);
		jp2.add(jb1);
		jp2.add(jb2);
		
	//创建一个数据模型
	 model=new Ticketmodel();
	 String []paras= {"1"};
	 model.Query("select * from trainticket where 1=?", paras);
		//开始初始化gui界面
		jt=new JTable(model);
		jsp=new JScrollPane(jt);
		//将jsp放入JFrame中
		this.add(jsp);
		this.add(jp2,"South");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//关掉界面
		this.setVisible(true);
	}

	
	//界面构建完成，对其中的图形功能进行响应
		

	public void actionPerformed(ActionEvent e)
	{
		// TODO 自动生成的方法存根
		//判断是哪个按钮被点击
		if(e.getSource()==jb1)//查询
		{
			Qyeryticket qi=new Qyeryticket();	
		}
		if(e.getSource()==jb2)
		{
			//用getSelectedrow 会返回用户选中的行，如果没选，那么返回-1
			int rownum=this.jt.getSelectedRow();
			if(rownum==-1)
			{
				//弹出提示框
				JOptionPane.showMessageDialog(this,"您未选择任何行，无法删除");
				
			}
			else//得到了用户编号
			{
				String trainnum=(String)model.getValueAt(rownum, 1);
				//System.out.println(trainnum);
				String sql="delete from trainticket where trianno=?";
				String []paras= {trainnum};
			    Ticketmodel  temp=new Ticketmodel();
				temp.Update(sql, paras)	;	//删除操作
				//更新界面
				model=new Ticketmodel();
				String []paras1= {"1"};
				model.Query("select * from trainticket where 1=?", paras1);
				//更新JTable
				jt.setModel(model);//非常重要的更新,否则无法查看更新后的结果		
			}
		}
	}
}
	


