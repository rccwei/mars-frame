package com.mars.marsswing.swing;

import javax.swing.*;
import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 */
public class DemoCalc {


    public static void main(String[] args) {

        JFrame f = new JFrame("计算器");

        //创建Panel
        Panel jPanel = new Panel();
        //当前可以容纳30列
        jPanel.add(new TextField(30));
        f.add(jPanel, BorderLayout.NORTH);

        Panel panel = new Panel();
        //设置网格布局管理器 并设置水平间距4  垂直间距4
        panel.setLayout(new GridLayout(3, 5, 4, 4));
        for (int i = 0; i < 10; i++) {
            panel.add(new Button(i + ""));
        }
        f.add(panel);
        panel.add(new Button("+"));
        panel.add(new Button("-"));
        panel.add(new Button("*"));
        panel.add(new Button("/"));
        panel.add(new Button("="));

        f.setLocation(400, 400);
        //f.setSize();
        //设置最佳大小
        f.pack();
        f.setVisible(true);
    }
}
