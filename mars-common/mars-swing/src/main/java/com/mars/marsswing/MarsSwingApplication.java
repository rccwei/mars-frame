package com.mars.marsswing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSwingApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSwingApplication.class, args);
    }

}
