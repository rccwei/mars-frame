package com.mars.marsswing.awt;

import java.awt.*;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/18 22:36
 */
public class AwtDemo2 {
    public static void main(String[] args) {
        Frame frame = new Frame("这是测试");
        frame.setSize(500, 500);

        Panel panel = new Panel();
        panel.add(new TextField("这是测试文本"));
        panel.add(new Button("按钮"));
        frame.add(panel);
        frame.setBounds(200,200,500,500);

        frame.setVisible(true);
    }
}
