package com.mars.marsswing.swing01;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class DriverSql {


    //构造函数
    public DriverSql()//初始化并连接数据库
    {
        driversql(Driver, url, user, password);

    }


    //定义数据库所需的变量
    String name, pass;
    PreparedStatement ps = null;
    Connection con = null;
    ResultSet rs = null;
    JTable jt = null;
    JScrollPane jsp = null;
    String url = "jdbc:mysql://47.99.40.195:3306/sys?useSSL=false";
    String user = "root";
    String password = "root";
    String Driver = "com.mysql.cj.jdbc.Driver";

    public void driversql(String Driver, String url, String user, String password) {
        try {
            //1.加载驱动
            Class.forName(Driver);
            //2.连接数据库
            con = DriverManager.getConnection(url, user, password);
            System.out.println("111");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //关闭数据库释放资源
    public void closesql() {
        try {
            if (rs != null) rs.close();
            if (ps != null) ps.close();
            if (con != null) con.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ResultSet querysql(String sql) {
        ResultSet rs = null;
        try {
            driversql(Driver, url, user, password);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return rs;
        }
    }

    //查询功能
    public ResultSet querysql(String sql, String[] paras) {
        ResultSet rs = null;
        try {
            driversql(Driver, url, user, password);
            ps = con.prepareStatement(sql);
            //插入赋值
            for (int i = 0; i < paras.length; i++) {
                ps.setString(i + 1, paras[i]);
            }
            //执行查询操作
            rs = ps.executeQuery();
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return rs;
        }


    }

    //将增删改合并
    public boolean update(String sql, String[] paras) {
        boolean flag = true;//默认成功
        try {
            driversql(Driver, url, user, password);//连接数据库
            //预编译对象
            ps = con.prepareStatement(sql);
            //插入赋值
            for (int i = 0; i < paras.length; i++) {
                ps.setString(i + 1, paras[i]);
            }
            //执行更新操作
            ps.executeUpdate();

        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        } finally     //关闭资源
        {
            this.closesql();
        }
        return flag;
    }

}


