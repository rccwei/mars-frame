package com.mars.marsswing.swing.test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author wq
 * @date 2021-12-14 10:50
 */
public class Main {
    public static void main(String[] args) {
        //处理空指针问题 通过 orElse
        User user = new User(1, "lisi");
        Optional<User> optionalUser = Optional.ofNullable(user);
        User resultUser = optionalUser.orElse(new User());
        System.out.println(resultUser);

        // FindFirst 使用
        List<String> list = Arrays.asList("lisi", "wq", "zs", "wx");
        String result = list.stream().
                filter(name -> name.equals("lisi")).
                findFirst().get();
        System.out.println(result);


    }
}


 class User {

    private Integer id;

    private String name;

    public User() {
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
