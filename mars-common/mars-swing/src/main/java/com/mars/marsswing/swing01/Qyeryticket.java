package com.mars.marsswing.swing01;
/*
 * 完成简单的火车票管理系统，列车查询功能
 * 添加了类的封装。
 */
import javax.swing.*;

import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
public class Qyeryticket extends JFrame implements ActionListener {
	
	Ticketmodel model;//全局变量，不用重复声明
	//定义控件
	JPanel jp;
	JLabel jl1,jl2,jl3,jl4;
	JButton jb1,jb2,jb3,jb4;
	JTable jt;
	JScrollPane jsp;
	JTextField jtf1,jtf2,jtf3,jtf4;
    //rowDate存放行数据，colnmnName存放列名
    Vector rowData,columnName;
    //定义数据库所需的变量
	public static void main(String[] args)
	{
		Qyeryticket test3=new Qyeryticket();
	}
	
	//定义构造函数
	public Qyeryticket()
	{
		
		
		jtf1=new JTextField(10);
		jtf2=new JTextField(10);
		jtf3=new JTextField(10);
		jtf4=new JTextField(10);//文本框大小标注一下
		jb1=new JButton("按列车号查询");
		jb1.addActionListener(this);
	   //监听和响应在同一个类里才可以使用
		jl1=new JLabel("请输入列车号   ");
		jl2=new JLabel("请输入发车日期");
		jl3=new JLabel("请输入始发地    ");
		jl4=new JLabel("请输入目的地    ");
		
		
		//面板
		jp=new JPanel();
		//把各个控件加入jp2;
		jb2=new JButton("按发车时间查询");
		jb2.addActionListener(this);  
		jb3=new JButton("按始发地查询");
		jb3.addActionListener(this);  
		jb4=new JButton("按目的地查询");
		jb4.addActionListener(this); 
		jp.setLayout(new GridLayout(4,1));
		jp.add(jl1);
		jp.add(jtf1);
		jp.add(jb1);
	    jp.add(jl2);
	    jp.add(jtf2);
	    jp.add(jb2);
	    jp.add(jl3);
		jp.add(jtf3);
		jp.add(jb3);
		jp.add(jl4);
	    jp.add(jtf4);
		jp.add(jb4);
		
		
		
	//创建一个数据模型
	 model=new Ticketmodel();
	 String []paras= {"1"};
	 model.Query("select * from trainticket where 1=?", paras);
		//开始初始化gui界面
		jt=new JTable(model);
		jsp=new JScrollPane(jt);
		//将jsp放入JFrame中
		this.add(jsp);
		this.add(jp,"South");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//关掉界面
		this.setVisible(true);
		}
	
	
	//界面构建完成，对其中的图形功能进行响应
		

	public void actionPerformed(ActionEvent e)
	{
		// TODO 自动生成的方法存根
		//判断是哪个按钮被点击
		if(e.getSource()==jb1)//列车号查询
		{
			
			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String num=this.jtf1.getText().trim();
			//写sql语句
			String sql="select * from trainticket where trainno=?";
			String []paras= {num};
			//构造新的模型类，并更新
			 model=new Ticketmodel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
		}
		else if(e.getSource()==jb2)//发车日期查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String date=this.jtf2.getText().trim();
			//写sql语句
			String sql="select * from trainticket where traindate=?";
			String []paras= {date};
			//构造新的模型类，并更新
			 model=new Ticketmodel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
		}
		else if(e.getSource()==jb3)//始发地查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String start=this.jtf3.getText().trim();
			//写sql语句
			String sql="select * from trainticket where start=?";
			String []paras= {start};
			//构造新的模型类，并更新
			 model=new Ticketmodel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
				
		}
		else if(e.getSource()==jb4)//目的地查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String name=this.jtf4.getText().trim();
			//写sql语句
			String sql="select * from trainticket where arrive=?";
			String []paras= {name};
			//构造新的模型类，并更新
			 model=new Ticketmodel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
			
				
		}
		
					
	}


}
