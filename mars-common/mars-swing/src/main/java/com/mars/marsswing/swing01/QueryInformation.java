package com.mars.marsswing.swing01;
/*
 * 完成简单的火车票管理系统，用户查询功能
 * 添加了类的封装。
 */
import javax.swing.*;

import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
public class QueryInformation extends JFrame implements ActionListener {
	
	MyTableModel model;//全局变量，不用重复声明
	//定义控件
	JPanel jp;
	JLabel jl1,jl2,jl3,jl4;
	JButton jb1,jb2,jb3,jb4;
	JTable jt;
	JScrollPane jsp;
	JTextField jtf1,jtf2,jtf3,jtf4;
    //rowDate存放行数据，colnmnName存放列名
    Vector rowData,columnName;
    //定义数据库所需的变量
	public static void main(String[] args)
	{
		QueryInformation test3=new QueryInformation();
	}
	
	//定义构造函数
	public QueryInformation()
	{
		
		
		jtf1=new JTextField(10);
		jtf2=new JTextField(10);
		jtf3=new JTextField(10);
		jtf4=new JTextField(10);//文本框大小标注一下
		jb1=new JButton("按人名查询");
		jb1.addActionListener(this);
	   //监听和响应在同一个类里才可以使用
		jl1=new JLabel("请输入用户名");
		jl2=new JLabel("请输入用户id");
		jl3=new JLabel("请输入用户密码");
		jl4=new JLabel("请输入用户权限");
		
		
		//面板
		jp=new JPanel();
		//把各个控件加入jp2;
		jb2=new JButton("按id查询");
		jb2.addActionListener(this);  
		jb3=new JButton("按密码查询");
		jb3.addActionListener(this);  
		jb4=new JButton("按权限查询");
		jb4.addActionListener(this); 
		jp.setLayout(new GridLayout(4,1));
		jp.add(jl1);
		jp.add(jtf1);
		jp.add(jb1);
		
	        jp.add(jl2);
	        jp.add(jtf2);
			jp.add(jb2);
	    jp.add(jl3);
		 jp.add(jtf3);
		jp.add(jb3);
		 jp.add(jl4);
	      jp.add(jtf4);
		jp.add(jb4);
		
		
		
	//创建一个数据模型
	 model=new MyTableModel();
	 String []paras= {"1"};
	 model.Query("select * from users where 1=?", paras);
		//开始初始化gui界面
		jt=new JTable(model);
		jsp=new JScrollPane(jt);
		//将jsp放入JFrame中
		this.add(jsp);
		this.add(jp,"South");
		this.setSize(400,300);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//关掉界面
		this.setVisible(true);
		}
	
	
	//界面构建完成，对其中的图形功能进行响应
		

	public void actionPerformed(ActionEvent e)
	{
		// TODO 自动生成的方法存根
		//判断是哪个按钮被点击
		if(e.getSource()==jb1)//人名查询
		{
			
			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String name=this.jtf1.getText().trim();
			//写sql语句
			String sql="select * from users where username=?";
			String []paras= {name};
			//构造新的模型类，并更新
			 model=new MyTableModel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
		}
		else if(e.getSource()==jb2)//id查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String id=this.jtf2.getText().trim();
			//写sql语句
			String sql="select * from users where userno=?";
			String []paras= {id};
			//构造新的模型类，并更新
			 model=new MyTableModel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
		}
		else if(e.getSource()==jb4)//权限查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String popedom=this.jtf4.getText().trim();
			//写sql语句
			String sql="select * from users where userpopedom=?";
			String []paras= {popedom};
			//构造新的模型类，并更新
			 model=new MyTableModel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
				
		}
		else if(e.getSource()==jb3)//密码查询
		{

			System.out.println("用户希望查询");//得到响应
			//把对表的显示封装到MyTableModelzhong，现在进行数据库的操作。
			String name=this.jtf3.getText().trim();
			//写sql语句
			String sql="select * from users where userpassword=?";
			String []paras= {name};
			//构造新的模型类，并更新
			 model=new MyTableModel();
			 model.Query(sql, paras);
			//更新JTable
			jt.setModel(model);
			
				
		}
		
					
	}


}
