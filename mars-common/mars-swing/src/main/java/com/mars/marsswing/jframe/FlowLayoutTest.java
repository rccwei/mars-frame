package com.mars.marsswing.jframe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/12 22:09
 * 流布局
 */
public class FlowLayoutTest {


    public static void main(String[] args) {
        JFrame jFrame = new JFrame("这是jFrame的标题");
        jFrame.setSize(600, 400);
        //设置窗体图标
        initLogo(jFrame);
        //获取容器面板
        Container contentPane = jFrame.getContentPane();
        //jpanel 默认就是流布局
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 80, 30));

        JButton button1 = new JButton("测试");
        JButton button2 = new JButton("测试");
        JButton button3 = new JButton("测试");
        JButton button4 = new JButton("测试");
        JButton button5 = new JButton("测试");
        JButton button6 = new JButton("测试");
        jPanel.add(button1);
        jPanel.add(button2);
        jPanel.add(button3);
        jPanel.add(button4);
        jPanel.add(button5);
        jPanel.add(button6);

        jFrame.add(jPanel);
        //居中 两种方式
        jFrame.setLocationRelativeTo(null);
        //关闭后推出程序
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置不可以改变大小
        jFrame.setResizable(false);
        jFrame.setVisible(true);

    }

    /**
     * 初始化logo
     *
     * @param jFrame jFrame
     */
    private static void initLogo(JFrame jFrame) {
        URL resource = FlowLayoutTest.class.getClassLoader().getResource("logo.jpg");
        assert resource != null;
        Image image = new ImageIcon(resource).getImage();
        jFrame.setIconImage(image);
    }
}
