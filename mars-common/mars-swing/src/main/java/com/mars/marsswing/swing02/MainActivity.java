package com.mars.marsswing.swing02;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

import static javax.swing.JFrame.EXIT_ON_CLOSE;

/**
 * @author wq
 * @date 2022/1/10 14:00
 */
public class MainActivity {
    public static void main(String[] args) {
        JFrame frame = new JFrame("购物");
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JPanel jPanel = new JPanel();
        placeComponents(jPanel);
        frame.setSize(650, 450);
        frame.setLocationRelativeTo(null);
        frame.add(jPanel);
        frame.setVisible(true);
    }

    private static void placeComponents(JPanel jPanel) {
        jPanel.setLayout(null);
        // 创建 JLabel
        JLabel userLabel1 = new JLabel("西瓜20.5元");
        JLabel userLabel2 = new JLabel("香蕉12.6元");
        JLabel userLabel3 = new JLabel("酱油12.8元");
        JLabel userLabel4 = new JLabel("牛奶28.1元");
        userLabel1.setBounds(10, 80, 80, 25);
        userLabel2.setBounds(10, 100, 80, 25);
        userLabel3.setBounds(10, 120, 80, 25);
        userLabel4.setBounds(10, 140, 80, 25);
        jPanel.add(userLabel1);
        jPanel.add(userLabel2);
        jPanel.add(userLabel3);
        jPanel.add(userLabel4);
        showTotal(jPanel);
        // 购物信息
        JButton buyButton = new JButton("购物信息");
        buyButton.setBounds(150, 10, 150, 25);
        buyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e);
            }
        });
        jPanel.add(buyButton);
        JButton statsButton = new JButton("统计结果");
        statsButton.setBounds(350, 10, 150, 25);
        statsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println(e);
                showTotal(jPanel);
            }
        });
        jPanel.add(statsButton);

    }

    private static void showTotal(JPanel jPanel) {
        JLabel userLabel5 = new JLabel("共计花费74.0元");
        userLabel5.setBounds(10, 180, 120, 25);
        jPanel.add(userLabel5);
    }


}
