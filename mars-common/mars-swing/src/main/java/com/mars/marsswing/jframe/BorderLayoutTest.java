package com.mars.marsswing.jframe;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/12 22:09
 * 边界布局
 * 一般作为 面板的布局方式 他默认是占满整个屏幕的
 */
public class BorderLayoutTest {


    public static void main(String[] args) {
        JFrame jFrame = new JFrame("这是jFrame的标题");
        jFrame.setSize(600, 400);
        //设置窗体图标
        //initLogo(jFrame);
        //获取容器面板
        Container contentPane = jFrame.getContentPane();
        jFrame.setLayout(new BorderLayout());
        JButton button = new JButton("底部的按钮");
        //设置南边的按钮
        contentPane.add(button, BorderLayout.SOUTH);
        JButton buttonNorth = new JButton("顶部的按钮");
        //设置北边的按钮
        contentPane.add(buttonNorth, BorderLayout.NORTH);
        //居中 两种方式
        jFrame.setLocationRelativeTo(null);
        //关闭后推出程序
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //设置不可以改变大小
        jFrame.setResizable(false);
        jFrame.setVisible(true);

    }

    /**
     * 初始化logo
     *
     * @param jFrame jFrame
     */
    private static void initLogo(JFrame jFrame) {
        URL resource = BorderLayoutTest.class.getClassLoader().getResource("logo.jpg");
        assert resource != null;
        Image image = new ImageIcon(resource).getImage();
        jFrame.setIconImage(image);
    }
}
