package com.mars.marsswing.swing01;
/*
 * 一个模型，当做表来实现
 * 把对表的各种操作封装到
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
public class MyTableModel extends AbstractTableModel{
	  
	//rowDate存放行数据，colnmnName存放列名
    Vector rowData,columnName;
   
    /*
     * 添加信息,删除信息，修改信息
     */
  
    public boolean Update(String sql,String []paras)
    {
    	//创建Driversql
    	DriverSql DS=new DriverSql();
    	 return DS.update(sql, paras);
    }
   
    //查询本就可以初始化
    public void Query(String sql,String []paras)
    {
    	
    	DriverSql DS=null;
    	columnName=new Vector();
    	//设置列名
    	columnName.add("用户号码");
    	columnName.add("用户名");
    	columnName.add("用户密码");
    	columnName.add("用户权限");
    	//行数据,存放多行
    	rowData=new Vector();
    	try 
    	{
    		DS=new DriverSql();
    		ResultSet rs=DS.querysql(sql, paras);
    		while(rs.next())
    		{
    			Vector hang=new Vector();
    			
    			hang.add(rs.getString(1));
    			hang.add(rs.getString(2));
    			hang.add(rs.getString(3));
    			hang.add(rs.getString(4));
    			rowData.add(hang);
    		//加入数据,设置行数据
    		}
    		
    	}catch(Exception e)
    	{
    	   e.printStackTrace();
    	}finally     //关闭资源
    	{
    		DS.closesql();
    	}
    	
    	
    }
    
    public MyTableModel()
    {
    	
    }
     
    //这些方法都自动执行
	@Override//得到共有多少行
	public int getRowCount() {
		
		// TODO 自动生成的方法存根
		return this.rowData.size();
	}

	@Override//得到共有多少列
	public int getColumnCount() {
		// TODO 自动生成的方法存根
		return this.columnName.size();
	}

	@Override//得到某行某列的数据
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO 自动生成的方法存根
		return ((Vector)this.rowData.get(rowIndex)).get(columnIndex);
	}

	@Override
	public String getColumnName(int column) {
		// TODO 自动生成的方法存根
		return (String)this.columnName.get(column);
	}

}
