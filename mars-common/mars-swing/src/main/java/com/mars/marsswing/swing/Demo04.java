package com.mars.marsswing.swing;

import javax.swing.*;
import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 */
public class Demo04 {


    public static void main(String[] args) {
        JFrame f = new JFrame("Mars");
        //设置流式布局 设置水平间距20 垂直间距20
        f.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 20));
        for (int i = 0; i < 100; i++) {
            f.add(new Button("按钮"));
        }
        f.setBounds(100, 100, 500, 300);
        f.pack();
        f.setVisible(true);
    }
}
