package com.mars.marsswing.swing;

import javax.swing.*;
import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 * <p>
 * 修改编码: -Dfile.encoding=gbk
 */
public class Demo03 {


    public static void main(String[] args) {
        JFrame f = new JFrame("测试窗口");

        ScrollPane scrollPane = new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
        scrollPane.add(new TextField("这是测试"));
        scrollPane.add(new Button("测试按钮"));

        f.add(scrollPane);


        f.setBounds(100, 100, 400, 400);
        f.setVisible(true);
    }
}
