package com.mars.marsswing.awt;

import java.awt.*;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/1/18 22:36
 */
public class AwtDemo {
    public static void main(String[] args) {
        Frame frame = new Frame("这是测试");
        frame.setSize(500, 500);

        frame.setVisible(true);
    }
}
