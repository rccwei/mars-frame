package com.mars.marsswing.swing01;

 
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import javax.swing.*;

public class UpdateInformation extends JDialog implements ActionListener {
	//定义swing部件
	JLabel jl1,jl2,jl3,jl4;
	JButton jb1,jb2;
	JTextField jtf1,jtf2,jtf3,jtf4;
	JPanel jp1,jp2,jp3;
	
	//定义构造函数
	//owner 是父窗口
	//title 是窗口名
	//modal 表示是模式窗口还是非模式窗口
	public UpdateInformation(Frame owner,String title,boolean modal,MyTableModel model,int rowNum)
	{
		super(owner,title,modal);//调用父类方法
		jl1=new JLabel("userno");
		jl2=new JLabel("username");
		jl3=new JLabel("userpassword");
		jl4=new JLabel("userpopedom");
		
		jtf1=new JTextField();
		jtf1.setText((String)model.getValueAt(rowNum, 0));//初始化
		jtf1.setEditable(false);                        //主键不能修改
		jtf2=new JTextField();
		jtf2.setText((String)model.getValueAt(rowNum, 1));
		jtf3=new JTextField();
		jtf3.setText((String)model.getValueAt(rowNum, 2));
		jtf4=new JTextField();
		jtf4.setText((String)model.getValueAt(rowNum, 3));
		
		jb1=new JButton("修改");
		jb2=new JButton("取消");
		
		jp1=new JPanel();
		jp2=new JPanel();
		jp3=new JPanel();
		
		//布局
		jp1.setLayout(new GridLayout(4,1));
		jp2.setLayout(new GridLayout(4,1));
		
		//添加按钮
		jp1.add(jl1);
		jp1.add(jl2);
		jp1.add(jl3);
		jp1.add(jl4);
		
		jp2.add(jtf1);
		jp2.add(jtf2);
		jp2.add(jtf3);
		jp2.add(jtf4);
		
		jp3.add(jb1);
		jp3.add(jb2);
		
		this.add(jp1,BorderLayout.WEST);
		this.add(jp2,BorderLayout.CENTER);
		this.add(jp3,BorderLayout.SOUTH);
		
		//添加事件监听
		jb1.addActionListener(this);
		
		//画面可见 
		this.setSize(300,250);
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		
			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO 自动生成的方法存根
		if(e.getSource()==jb1)
		{
			
			//预编译对象
			String sql="update Users set username=?,userpassword=?,userpopedom=? where userno=?";
			String []paras= {jtf2.getText(),jtf3.getText(),jtf4.getText(),jtf1.getText()};
			MyTableModel temp=new MyTableModel();
			temp.Update(sql, paras);
			if(!temp.Update(sql, paras))
			{
				//消息框提示
				JOptionPane.showMessageDialog(this, "修改失败");
			}
			else
			{
				JOptionPane.showMessageDialog(this, "修改成功");
			    this.dispose();//关窗口
			}
		
	    }
		else if(e.getSource()==jb2)
		{
			JOptionPane.showMessageDialog(this, "您取消了添加");
			  this.dispose();//关窗口	
			
		}
	}
	
	
	
	
}
