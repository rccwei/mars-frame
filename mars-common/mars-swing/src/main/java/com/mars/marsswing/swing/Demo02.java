package com.mars.marsswing.swing;

import javax.swing.*;
import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 */
public class Demo02 {


    public static void main(String[] args) {
        JFrame f = new JFrame("测试窗口");
        Panel panel = new Panel();

        JLabel jLabel = new JLabel();
        jLabel.setText("用户名");

        panel.add(new JButton("测试"));
        panel.add(jLabel);
        panel.add(new TextField(""));


        f.add(panel);
        f.setBounds(100, 100, 400, 400);
        f.setVisible(true);
    }
}
