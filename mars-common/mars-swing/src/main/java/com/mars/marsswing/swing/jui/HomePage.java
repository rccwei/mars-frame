package com.mars.marsswing.swing.jui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author 程序猿Mars
 * @date 2021-12-14 13:39
 */
public class HomePage extends JFrame {

    private JLabel la1, la2;

    private Font laFont = new Font("隶书", Font.BOLD, 60);
    private Font btFont = new Font("宋体", Font.BOLD, 25);


    JButton doctorJButton = new JButton("医生信息管理");
    JButton patientJButton = new JButton("病人信息管理");
    JButton wardJButton = new JButton("科室信息管理");


    public HomePage() {
    }


    public HomePage(String a) {
        super(a);
        this.setLayout(null);

        this.add(doctorJButton);
        this.add(patientJButton);
        this.add(wardJButton);
        doctorJButton.setFont(btFont);
        patientJButton.setFont(btFont);
        wardJButton.setFont(btFont);

        doctorJButton.setBounds(0, 0, 200, 200);
        patientJButton.setBounds(0, 200, 200, 200);
        wardJButton.setBounds(0, 400, 200, 200);


        la1 = new JLabel("欢迎使用");
        la2 = new JLabel("医院信息管理系统");


        this.add(la1);
        this.add(la2);
        la1.setBounds(500, 100, 500, 100);
        la1.setFont(laFont);
        la2.setBounds(400, 200, 600, 100);
        la2.setFont(laFont);


        wardJButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

//                System.out.println(e);
//                ImageIcon background = new ImageIcon("D:\\images\\1.jpg");
//                JLabel bgJLabel = new JLabel(background);
//                getContentPane().add(bgJLabel);
//                bgJLabel.setBounds(0, 0, 1000, 600);
                //BackgroundPanel backgroundPanel = new BackgroundPanel();


            }
        });


        patientJButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                System.out.println(e);
                ImageIcon background = new ImageIcon("D:\\images\\2.jpg");
                JLabel bgJLabel = new JLabel(background);
                getContentPane().add(bgJLabel);
                bgJLabel.setBounds(0, 0, 1000, 600);


            }
        });


        this.setTitle("主页面");
        this.setResizable(true);
        this.setVisible(true);
        this.setSize(1000, 600);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


    }

    public static void main(String[] args) {
        HomePage homePage = new HomePage("主页面1");


    }


}

