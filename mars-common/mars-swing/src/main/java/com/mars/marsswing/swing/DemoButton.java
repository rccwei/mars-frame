package com.mars.marsswing.swing;

import javax.swing.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/15 17:08
 */
public class DemoButton {

    public static void main(String[] args) {
        // 显示应用 GUI
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }


        });
    }

    private static void createAndShowGUI() {
        // 1. 创建一个顶层容器（窗口）
        JFrame jf = new JFrame("测试窗口");
        // 设置窗口大小
        jf.setSize(650, 450);
        jf.setLocationRelativeTo(null);
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        JButton btn = new JButton("测试按钮");
        panel.add(btn);

        jf.setContentPane(panel);
        jf.setVisible(true);
    }


}
