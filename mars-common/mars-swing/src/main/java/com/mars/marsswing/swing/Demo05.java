package com.mars.marsswing.swing;

import javax.swing.*;
import java.awt.*;

/**
 * @author wq
 * @version 1.0
 * @date 2021/05/13 21:10
 * http://www.yiidian.com/java-swing/java-jbutton.html
 */
public class Demo05 {


    public static void main(String[] args) {

        JFrame f = new JFrame("Mars");
        //设置左右 间距 30 10
        f.setLayout(new BorderLayout(30, 10));
        //设置北侧按钮
        f.add(new Button("北侧按钮"), BorderLayout.NORTH);
        f.add(new Button("南侧按钮"), BorderLayout.SOUTH);

        f.add(new Button("中间按钮"), BorderLayout.CENTER);
        f.add(new TextField("测试内容"));
        f.setBounds(200, 200, 800, 600);
        //设置最佳大小
        //f.pack();
        f.setVisible(true);
    }
}
