package com.mars.marsswing.swing01;/*
 * 实现登录界面
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class Login extends JFrame implements ActionListener {

    JButton jb1, jb2;
    JLabel jl1, jl2, jl3;
    JPanel jp1, jp2, jp3, jp4;
    JTextField jtf;//文本框
    JPasswordField jpf;//密码框

    //初始化
    public Login() {
        //设置按钮
        jb1 = new JButton("登录");
        jb2 = new JButton("取消");
        jl1 = new JLabel("姓 名:");
        jl2 = new JLabel("密 码:");
        jl3 = new JLabel("欢迎使用火车票管理系统");
        jtf = new JTextField(10);
        jpf = new JPasswordField(10);
        jp1 = new JPanel();
        jp2 = new JPanel();
        jp3 = new JPanel();
        jp4 = new JPanel();
        //设置布局管理器
        this.setLayout(new GridLayout(4, 1));
        //添加各个组件
        jp1.add(jl3);

        jp2.add(jl1);
        jp2.add(jtf);
        jp4.add(jl2);
        jp4.add(jpf);


        jp3.add(jb1);
        jp3.add(jb2);

        //加入窗口中
        this.add(jp1);
        this.add(jp2);
        this.add(jp4);
        this.add(jp3);

        //设置大小等窗口属性
        this.setSize(400, 400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);//可见
        jb1.addActionListener(this);
        jb2.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO 自动生成的方法存根


        if (e.getSource() == jb1) {


            try {
                if (CheckRole()) {
                    System.out.println("登录成功");
                    new Showfuntion();

                } else {
                    System.out.println("登录");
                    new Showfuntion();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ClassNotFoundException classNotFoundException) {
                classNotFoundException.printStackTrace();
            }
        } else if (e.getSource() == jb2) {
            System.out.println("您取消了登录\n");
        }


    }

    public boolean CheckRole() throws SQLException, ClassNotFoundException {
        boolean ifright = false;
        String username = jtf.getText().trim();
        String password = jpf.getText().trim();
        System.out.println("用户名:" + username);
        System.out.println("密码:" + password);

        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://47.99.40.195:3306/sys?useSSL=false";
        String name = "root";
        String pass = "root";
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet result = null;
        con = DriverManager.getConnection(url, name, pass);
        System.out.println("连接成功");
        String sql = "Select * from users where username=? and userpassword=?";
        stmt = con.prepareStatement(sql);
        stmt.setString(1, username);
        stmt.setString(2, password);
        result = stmt.executeQuery();
        if (result.next())
            ifright = true;
        else
            ifright = false;
        result.close();
        stmt.close();
        con.close();

        return ifright;
    }

    public static void main(String[] args) {
        Login in = new Login();
    }


}








