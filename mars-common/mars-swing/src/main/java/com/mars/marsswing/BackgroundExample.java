package com.mars.marsswing;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * @author 程序猿Mars
 * @date 2021-12-15 10:12
 */

public class BackgroundExample {

    public static void main(final String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final JFrame frame = new JFrame("Rich");
                final String[] images = {"1.jpg",
                        "2.jpg",
                        "3.jpg",
                        "4.jpg"};
                final JComboBox backgrounds;
                final BackgroundPanel backgroundPanel = new BackgroundPanel();
                backgrounds = new JComboBox(images);
                //backgrounds.setSelectedIndex(1);
                backgrounds.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(final ActionEvent e) {
                        JComboBox box = (JComboBox) e.getSource();
                        String background = (String) box.getSelectedItem();
                        try {
                            backgroundPanel.changeBackground(background);
                            backgroundPanel.repaint();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }

                });

                frame.setUndecorated(false);
                backgroundPanel.setOpaque(false);
                frame.getContentPane().add(backgroundPanel, BorderLayout.CENTER);
                frame.getContentPane().add(backgrounds, BorderLayout.NORTH);
                frame.setBounds(100, 100, 750, 560);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.setVisible(true);
            }
        });
    }
}

class BackgroundPanel extends JPanel {

    /**
     *
     */
    private static final long serialVersionUID = 11111111111111L;
    private BufferedImage background;
    private TexturePaint texture;

    public void changeBackground(final String image) throws IOException {
        InputStream resourceAsStream = BackgroundExample.class.getClassLoader().getResourceAsStream(image);
        this.background = ImageIO.read(Objects.requireNonNull(resourceAsStream));
        Rectangle rect = new Rectangle(0, 0, this.background.getWidth(), this.background.getHeight());
        this.texture = new TexturePaint(this.background, rect);
    }

    @Override
    public void paintComponent(final Graphics g) {

        if (this.texture != null) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.setPaint(this.texture);
            g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        }

        super.paintComponent(g);
    }
}
