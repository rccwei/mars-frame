package com.mars.common.idempotent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 什么是幂等性
 * 幂等 用于表示 任意多次请求均与一次请求执行的结果相同，也就是说对于一个接口而言，无论调用了多少次，最终得到的结果都是一样的。
 * 1、前端拦截
 * 		a、前端拦截是指通过 Web 站点的页面进行请求拦截，比如在用户点击完“提交”按钮后，我们可以把按钮设置为不可用或者隐藏状态，避免用户重复点击
 *
 * 2、使用数据库实现幂等性
 * 		a、通过悲观锁来实现幂等性
 *
 * 		b、通过唯一索引来实现幂等性
 *
 * 		c、通过乐观锁来实现幂等性
 *
 * 3、使用 JVM 锁实现幂等性（单机环境）
 * 		a、JVM 锁实现是指通过 JVM 提供的内置锁如 Lock 或者是 synchronized 来实现幂等性
 *
 * 4、使用分布式锁实现幂等性
 */
@SpringBootApplication(scanBasePackages = "com.mars.common.idempotent")
public class MarsIdempotentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsIdempotentApplication.class, args);
	}

}
