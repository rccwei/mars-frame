package com.mars.common.idempotent.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimiter {
    /**
     * 有效期
     *
     * @return 时间(单位毫秒)
     */
    int expireTime() default 1000;

    /**
     * 时间单位
     * 默认：毫秒
     *
     * @return TimeUnit
     */
    TimeUnit timeUnit() default TimeUnit.MILLISECONDS;

    /**
     * 默认的key
     *
     * @return key
     */
    String key() default "";

    /**
     * 提示信息，可自定义
     *
     * @return
     */
    String info() default "请求繁忙，请稍后重试";

}
