package com.mars.common.idempotent.controller;

import com.mars.common.idempotent.anno.RateLimiter;
import com.mars.common.idempotent.entity.User;
import com.mars.common.idempotent.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 11:11:50
 */
@Slf4j
@RestController
public class IndexController {


    /**
     * 获取详情
     *
     * @param id id
     * @return String
     */
    @GetMapping("/detail")
    @RateLimiter(expireTime = 2000, key = "#id")
    public R index(Integer id) {
        log.info(Thread.currentThread().getName() + "查询参数:" + id);
        User user = User.builder().id(id).age(21).username("zs").build();
        return R.success(user);
    }
}
