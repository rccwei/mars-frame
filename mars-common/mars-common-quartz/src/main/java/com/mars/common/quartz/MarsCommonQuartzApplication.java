package com.mars.common.quartz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonQuartzApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonQuartzApplication.class, args);
    }

}
