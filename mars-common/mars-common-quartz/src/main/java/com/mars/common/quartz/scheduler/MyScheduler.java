package com.mars.common.quartz.scheduler;

import com.mars.common.quartz.job.PrintWordsJob;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Random;

/**
 * @author wq
 * @version 1.0
 * @date 2021/06/29 11:25
 */
@Component
public class MyScheduler {


    @Value("${lk.isAutoLike}")
    private boolean isAutoLike;

    @PostConstruct
    public void execute() {
        while (isAutoLike) {
            int i = new Random().nextInt(300) + 60;
            System.out.println("时间:" + i);
            try {
                Thread.sleep(i * 1000L);
                System.out.println("执行任务点赞");
                PrintWordsJob job = new PrintWordsJob();
                job.execute();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
