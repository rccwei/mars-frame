package com.mars.easyexcel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonEasyexcelApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonEasyexcelApplication.class, args);
    }

}
