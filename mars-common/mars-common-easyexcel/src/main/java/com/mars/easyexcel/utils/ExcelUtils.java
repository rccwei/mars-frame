package com.mars.easyexcel.utils;

import com.alibaba.excel.EasyExcel;
import com.mars.easyexcel.entity.User;
import com.mars.easyexcel.listener.ExcelListener;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Objects;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-27 22:34:35
 */
public class ExcelUtils {

    /**
     * 读取excel文件
     *
     * @param file 文件
     * @return List
     */
    public static void readExcel(File file) {
        InputStream ins = null;
        try {
            ins = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // sheet 设置处理的工作簿 headRowNumber 设置从excel第几行开始读取
        EasyExcel.read(ins, User.class, new ExcelListener<User>()).sheet().doRead();//第0行一般是表头，从第1行开始读取
    }


    public static File multipartFileToFile(MultipartFile file) throws Exception {
        File toFile;
        InputStream ins;
        ins = file.getInputStream();
        toFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        inputStreamToFile(ins, toFile);
        ins.close();
        return toFile;

    }

    private static void inputStreamToFile(InputStream ins, File file) {
        try {
            OutputStream os = new FileOutputStream(file);
            int bytesRead;
            byte[] buffer = new byte[8192];
            while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.close();
            ins.close();
        } catch (Exception e) {
            throw new RuntimeException("读取文件错误", e);
        }
    }
}
