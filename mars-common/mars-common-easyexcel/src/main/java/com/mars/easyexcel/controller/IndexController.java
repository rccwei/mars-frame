package com.mars.easyexcel.controller;

import com.alibaba.excel.EasyExcel;
import com.mars.easyexcel.entity.User;
import com.mars.easyexcel.service.UserService;
import com.mars.easyexcel.utils.ExcelUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2023/10/27 12:31
 */
@RestController
@AllArgsConstructor
public class IndexController {

    private final UserService userService;

    private final HttpServletResponse response;


    /**
     * 下载excel
     *
     * @throws IOException
     */
    @RequestMapping(value = "/downExcel")
    public void downExcel() throws IOException {
        List<User> userList = userService.getUserList();
        // 这里注意 有同学反应使用swagger 会导致各种问题，请直接用浏览器或者用postman
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileNameStr = URLEncoder.encode("用户列表", "UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + fileNameStr + ".xlsx");
        //   User.class 是按导出类  data()应为数据库查询数据，这里只是模拟
        EasyExcel.write(response.getOutputStream(), User.class).sheet(fileNameStr).doWrite(userList);
    }

    /**
     * 读取excel并解析
     *
     * @param file file
     */
    @PostMapping("readExcel")
    public void readExcel(@RequestParam("file") MultipartFile file) throws Exception {
        if (file.isEmpty()) {
            throw new RuntimeException("请选择文件");
        }
        ExcelUtils.readExcel(ExcelUtils.multipartFileToFile(file));
    }

}
