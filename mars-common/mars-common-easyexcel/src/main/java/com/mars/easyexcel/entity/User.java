package com.mars.easyexcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.mars.easyexcel.convert.SexConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @ContentRowHeight(15) //设置行高
 * @ColumnWidth(20) //设置列宽
 * @HeadRowHeight(20) //设置表头高度
 * @date 2023/10/27 12:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    /**
     * 通过 @ExcelProperty 注解与 index 变量可以标注成员变量所映射的列
     */
    @ExcelProperty(value = "序号", index = 0)
    private Integer id;

    @ColumnWidth(20)
    @ExcelProperty(value = "姓名", index = 1)
    private String name;

    @ExcelProperty(value = "性别", index = 2, converter = SexConverter.class)
    private Integer sex;

    @ExcelProperty(value = "年龄", index = 3)
    private Integer age;

    @ExcelProperty(value = "地址", index = 4)
    private String address;

    @ColumnWidth(20)
    @ExcelProperty(value = "联系方式", index = 5)
    private String phone;
}
