package com.mars.easyexcel.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-10-27 22:35:17
 */
@Slf4j
public class ExcelListener<T> extends AnalysisEventListener<T> {

    private final List<T> list = new ArrayList<>();

    /**
     * 读取每一行数据执行一次
     *
     * @param t               t
     * @param analysisContext analysisContext
     */
    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        list.add(t);
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        save(list);
        System.out.println("所有数据解析完毕.................");
    }

    /**
     * 保存数据
     *
     * @param list list
     */
    private void save(List<T> list) {
        log.info("保存数据:{}", JSONObject.toJSONString(list));
    }
}
