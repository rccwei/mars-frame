package com.mars.easyexcel.service;

import com.mars.easyexcel.entity.User;

import java.util.List;

public interface UserService {

    /**
     * 获取所有用户
     *
     * @return List<User>
     */
    List<User> getUserList();
}
