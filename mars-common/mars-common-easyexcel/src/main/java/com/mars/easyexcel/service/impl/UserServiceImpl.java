package com.mars.easyexcel.service.impl;

import com.mars.easyexcel.entity.User;
import com.mars.easyexcel.service.UserService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2023/10/27 12:33
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<User> getUserList() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setId(i + 1);
            user.setAddress("成都市");
            user.setAge(18);
            user.setSex(1);
            user.setName("程序员Mars");
            user.setPhone("18888888888");
            list.add(user);
        }
        return list;
    }
}
