package com.mars.common.javafx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonJavafxApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonJavafxApplication.class, args);
    }

}
