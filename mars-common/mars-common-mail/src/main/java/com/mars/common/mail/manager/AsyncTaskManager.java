package com.mars.common.mail.manager;

import org.springframework.stereotype.Component;

import java.util.concurrent.CompletableFuture;

/**
 * @author: wangqiang
 * @create: 2021-08-21 09:47
 */
@Component
public class AsyncTaskManager {

    /**
     * 执行异步任务
     *
     * @param runnable runnable
     */
    public void execute(Runnable runnable) {
        CompletableFuture.runAsync(runnable);
    }
}
