package com.mars.common.mail.service;

/**
 * @author wq
 * @version 1.0
 * @date 2021/1/8 11:30
 */
public interface IMailService {

    /**
     * 发送文本邮件
     *
     * @param subject 主题
     * @param content 内容
     */
    void sendSimpleMail(String subject, String content);

    /**
     * 发送HTML邮件
     *
     * @param subject 主题
     * @param content 内容
     */
    void sendHtmlMail(String subject, String content);


    /**
     * 发送带附件的邮件
     *
     * @param subject  主题
     * @param content  内容
     * @param filePath 附件
     */
    void sendAttachmentsMail(String subject, String content, String filePath);


}
