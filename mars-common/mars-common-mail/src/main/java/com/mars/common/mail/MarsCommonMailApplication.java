package com.mars.common.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonMailApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonMailApplication.class, args);
    }

}
