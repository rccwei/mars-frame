package com.mars.common.mail.controller;

import com.mars.common.mail.service.IMailService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/25 13:39
 */
@RestController
@AllArgsConstructor
public class MailController {

    private final IMailService mailService;

    /**
     * 发送简单邮件
     *
     * @param subject 主题
     * @param content 内容
     */
    @GetMapping("/sendSimpleMail")
    public String sendSimpleMail(String subject, String content) {
        mailService.sendSimpleMail(subject, content);
        return "发送成功";
    }
}
