package com.mars.common.mail.service.impl;


import com.mars.common.mail.config.MailConfig;
import com.mars.common.mail.manager.AsyncTaskManager;
import com.mars.common.mail.service.IMailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;


/**
 * @author wq
 * @version 1.0
 * @date 2021/1/8 11:30
 */
@Service
@Slf4j
public class IMailServiceImpl implements IMailService {


    @Resource
    private JavaMailSender mailSender;

    @Resource
    private AsyncTaskManager asyncTaskManager;


    /**
     * 简单文本邮件
     *
     * @param subject 主题
     * @param content 内容
     */
    @Override
    public void sendSimpleMail(String subject, String content) {
        System.out.println("任务执行开始");
        asyncTaskManager.execute(() -> sendMail(subject, content));
        System.out.println("任务执行完成");
    }

    @Async
    public void sendMail(String subject, String content) {


        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MailConfig.developMail().forEach(mail -> {
            //创建SimpleMailMessage对象
            SimpleMailMessage message = new SimpleMailMessage();
            //邮件发送人
            message.setFrom(MailConfig.FROM);
            //邮件接收人
            message.setTo(mail);
            //邮件主题
            message.setSubject(subject);
            //邮件内容
            message.setText("尊敬的" + mail + "用户" + content);
            //发送邮件
            mailSender.send(message);
        });
    }


    /**
     * html邮件
     *
     * @param subject 主题
     * @param content 内容
     */
    @Override
    public void sendHtmlMail(String subject, String content) {
        MailConfig.developMail().forEach(mail -> {
            //获取MimeMessage对象
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper;
            try {
                messageHelper = new MimeMessageHelper(message, true);
                //邮件发送人
                messageHelper.setFrom(MailConfig.FROM);
                //邮件接收人
                messageHelper.setTo(mail);
                //邮件主题
                message.setSubject(subject);
                //邮件内容，html格式
                messageHelper.setText(content, true);
                //发送
                mailSender.send(message);
                //日志信息
                log.info("邮件已经发送。");
            } catch (MessagingException e) {
                log.error("发送邮件时发生异常！", e);
            }
        });

    }

    /**
     * 带附件的邮件
     *
     * @param subject  主题
     * @param content  内容
     * @param filePath 附件
     */
    @Override
    public void sendAttachmentsMail(String subject, String content, String filePath) {
        MailConfig.developMail().forEach(mail -> {
            MimeMessage message = mailSender.createMimeMessage();
            try {
                MimeMessageHelper helper = new MimeMessageHelper(message, true);
                helper.setFrom(MailConfig.FROM);
                helper.setTo(mail);
                helper.setSubject(subject);
                helper.setText(content, true);
                FileSystemResource file = new FileSystemResource(new File(filePath));
                String fileName = filePath.substring(filePath.lastIndexOf(File.separator));
                helper.addAttachment(fileName, file);
                mailSender.send(message);
                //日志信息
                log.info("邮件已经发送。");
            } catch (MessagingException e) {
                log.error("发送邮件时发生异常！", e);
            }

        });


    }

}
