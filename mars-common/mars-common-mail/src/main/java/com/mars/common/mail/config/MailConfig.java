package com.mars.common.mail.config;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/1/8 11:45
 */
public class MailConfig {

    public static final String FROM = "850994281@qq.com";

    /**
     * 开发人员有效
     *
     * @return list
     */
    public static List<String> developMail() {
        ArrayList<String> list = new ArrayList<>();
        //wq
        list.add("850994281@qq.com");
        return list;
    }

}
