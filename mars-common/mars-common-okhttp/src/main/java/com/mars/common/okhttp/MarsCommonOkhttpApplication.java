package com.mars.common.okhttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonOkhttpApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonOkhttpApplication.class, args);
    }

}
