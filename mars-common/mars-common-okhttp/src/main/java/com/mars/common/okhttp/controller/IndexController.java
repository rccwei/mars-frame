package com.mars.common.okhttp.controller;

import com.mars.common.okhttp.utils.OkHttpUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/09 16:29
 */
@RestController
public class IndexController {

    @Resource
    private OkHttpUtils okHttpUtils;


    @GetMapping("/show")
    public String show() {
        String url = "https://www.baidu.com/";
        return okHttpUtils.doGet(url);
    }
}
