package com.mars.common.captcha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonCaptchaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonCaptchaApplication.class, args);
    }

}
