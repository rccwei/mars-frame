package com.mars.common.captcha.service.impl;

import com.mars.common.captcha.service.CaptchaService;
import com.mars.common.captcha.utils.ValidateCodeUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/6 22:27
 */
@Service
public class CaptchaServiceImpl implements CaptchaService {

    @Resource
    private HttpServletRequest request;

    @Resource
    private HttpServletResponse response;

    public static final String CAPTCHA_NAME = "captcha";

    public static ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>(1);


    @Override
    public void acquire() {
        try {
            response.setContentType("image/png");
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Expire", "0");
            response.setHeader("Pragma", "no-cache");
            map.clear();
            ValidateCodeUtils validateCode = new ValidateCodeUtils();
            //直接返回图片
            String randomCode = validateCode.getRandomCodeImage(request, response);
            map.put(CAPTCHA_NAME, randomCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean check(String code) {
        String captchaCode = map.get(CAPTCHA_NAME);
        if (!StringUtils.isEmpty(captchaCode)) {
            return captchaCode.equals(code);
        }
        return false;
    }
}
