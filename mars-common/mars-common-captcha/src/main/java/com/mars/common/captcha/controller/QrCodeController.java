package com.mars.common.captcha.controller;

import com.mars.common.captcha.service.QRService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author wq
 * @date 2022/11/16 10:53
 */
@RestController
public class QrCodeController {

    @Resource
    private QRService qrService;

    @RequestMapping("getCode")
    public void generateCode(String content, HttpServletResponse servletResponse) throws IOException {
        qrService.generateStream("测试内容", servletResponse);
    }
}
