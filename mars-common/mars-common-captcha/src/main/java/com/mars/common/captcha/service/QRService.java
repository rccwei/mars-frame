package com.mars.common.captcha.service;

import cn.hutool.extra.qrcode.QrCodeUtil;
import cn.hutool.extra.qrcode.QrConfig;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * @author wq
 * @date 2022/11/16 10:57
 */
@Service
public class QRService {


    @Resource
    private QrConfig qrconig;

    public void generateFile(String content, File file) {
        QrCodeUtil.generate(content, qrconig, file);
    }


    /**
     * 输出到流
     * @param content
     * @param response
     * @throws IOException
     */
    public void generateStream(String content, HttpServletResponse response) throws IOException {
        QrCodeUtil.generate(content, qrconig, "png", response.getOutputStream());
    }
}
