package com.mars.common.captcha.controller;

import com.mars.common.captcha.service.CaptchaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 程序猿Mars
 * @version 1.0
 * @date 2021/3/6 22:15
 */
@RestController
@RequestMapping("/captcha")
public class CaptchaController {

    @Resource
    private CaptchaService captchaService;


    /**
     * 1.手写生成验证码
     */
    @RequestMapping("/acquire")
    public void acquire() {
        captchaService.acquire();
    }

    /**
     * 校验验证码
     *
     * @return boolean
     */
    @GetMapping("/check")
    public boolean check(String code) {
        return captchaService.check(code);
    }
}
