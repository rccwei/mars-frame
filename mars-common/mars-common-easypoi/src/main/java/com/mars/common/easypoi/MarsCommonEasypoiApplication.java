package com.mars.common.easypoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * easypoi 文档: https://easypoi.mydoc.io/#text_186900
 */
@SpringBootApplication
public class MarsCommonEasypoiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsCommonEasypoiApplication.class, args);
    }

}
