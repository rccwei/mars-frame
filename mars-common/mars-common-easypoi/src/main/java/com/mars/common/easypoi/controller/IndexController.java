package com.mars.common.easypoi.controller;

import com.mars.common.easypoi.entity.User;
import com.mars.common.easypoi.service.UserService;
import com.mars.common.easypoi.utils.ExcelUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author 程序猿Mars
 * @date 2023-10-04 19:15
 */
@RestController
@AllArgsConstructor
public class IndexController {


    private final UserService userService;


    /**
     * 导出数据
     *
     * @param response response
     * @throws IOException IOException
     */
    @GetMapping(value = "/export")
    public void exportExcel(HttpServletResponse response) throws IOException {
        List<User> userList = userService.getUserList();
        ExcelUtils.exportExcel(userList, User.class, "员工信息", response);
    }

    /**
     * 导入excel数据
     *
     * @param multipartFile
     * @throws Exception
     */
    @PostMapping("/import")
    public void importExcel(@RequestParam("file") MultipartFile multipartFile) throws Exception {
        userService.importUser(multipartFile);
    }


}
