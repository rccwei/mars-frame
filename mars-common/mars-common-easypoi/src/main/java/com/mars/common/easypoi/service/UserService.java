package com.mars.common.easypoi.service;


import com.mars.common.easypoi.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    /**
     * 获取所有用户
     *
     * @return List<User>
     */
    List<User> getUserList();

    /**
     * 导入用户信息
     *
     * @param multipartFile multipartFile
     */
    void importUser(MultipartFile multipartFile) throws Exception;

}
