package com.mars.common.easypoi.request;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-02 14:47:14
 */
@Data
public class UserImportRequest {


    @Excel(name = "姓名")
    private String name;

    @Excel(name = "性别", replace = {"男_1", "女_0"})
    private Integer sex;

    @Excel(name = "年龄")
    private Integer age;

    @Excel(name = "地址")
    private String address;

    @Excel(name = "联系方式")
    private String phone;
}
