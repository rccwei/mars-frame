package com.mars.common.easypoi.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2023/10/27 12:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable {

    @Excel(name = "序号", orderNum = "0")
    private Integer id;

    @Excel(name = "姓名", orderNum = "1", width = 20)
    private String name;

    @Excel(name = "性别", orderNum = "2", replace = {"男_1", "女_0"})
    private Integer sex;

    @Excel(name = "年龄", orderNum = "3")
    private Integer age;

    @Excel(name = "地址", orderNum = "4")
    private String address;

    @Excel(name = "联系方式", orderNum = "5", width = 20)
    private String phone;
}
