package com.mars.common.easypoi.service.impl;


import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.alibaba.fastjson.JSONObject;
import com.mars.common.easypoi.entity.User;
import com.mars.common.easypoi.request.UserImportRequest;
import com.mars.common.easypoi.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2023/10/27 12:33
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public List<User> getUserList() {
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            User user = new User();
            user.setId(i + 1);
            user.setAddress("成都市");
            user.setAge(18);
            user.setSex(1);
            user.setName("程序员Mars");
            user.setPhone("18888888888");
            list.add(user);
        }
        return list;
    }

    @Override
    public void importUser(MultipartFile multipartFile) throws Exception {
        if (multipartFile.isEmpty()) {
            throw new RuntimeException("请选择文件");
        }
        ImportParams params = new ImportParams();
        //设置表格标题行数,默认0，这是读取时会跳过的行数
        params.setTitleRows(1);
        List<UserImportRequest> result = ExcelImportUtil.importExcel(multipartFile.getInputStream(), UserImportRequest.class, params);
        System.out.println(JSONObject.toJSONString(result));
    }
}
