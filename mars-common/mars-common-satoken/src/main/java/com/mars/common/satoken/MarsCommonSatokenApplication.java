package com.mars.common.satoken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsCommonSatokenApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsCommonSatokenApplication.class, args);
	}

}
