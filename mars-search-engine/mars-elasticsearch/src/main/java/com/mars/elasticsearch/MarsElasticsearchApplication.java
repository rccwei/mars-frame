package com.mars.elasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsElasticsearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsElasticsearchApplication.class, args);
    }

}
