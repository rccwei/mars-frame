package com.mars.marselk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * https://blog.csdn.net/u013592116/article/details/103954907
 */
@SpringBootApplication
@EnableScheduling
public class MarsElkApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsElkApplication.class, args);
    }

}
