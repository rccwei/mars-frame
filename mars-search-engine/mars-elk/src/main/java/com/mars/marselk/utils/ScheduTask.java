package com.mars.marselk.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author wq
 * @version 1.0
 * @date 2021/04/28 10:48
 */
@Component
@Slf4j
public class ScheduTask {


    @Scheduled(cron = "*/5 * * * * ?")
    public void task() {
        log.info("任务执行:" + new Date());
    }
}
