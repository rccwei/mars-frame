package com.mars.solr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSolrApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSolrApplication.class, args);
    }

}
