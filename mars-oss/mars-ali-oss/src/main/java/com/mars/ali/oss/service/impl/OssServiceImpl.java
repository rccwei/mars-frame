package com.mars.ali.oss.service.impl;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.ObjectMetadata;
import com.mars.ali.oss.manager.OssManager;
import com.mars.ali.oss.service.IOssService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 10:52:26
 */
@Service
@Slf4j
public class OssServiceImpl implements IOssService {
    @Override
    public String upload(MultipartFile file) {
        String imageUrl = null;

        //获取阿里云存储相关常量
        String endPoint = OssManager.END_POINT;
        String accessKeyId = OssManager.ACCESS_KEY_ID;
        String accessKeySecret = OssManager.ACCESS_KEY_SECRET;
        String bucketName = OssManager.BUCKET_NAME;

        try {
            //判断oss实例是否存在，如果不存在则创建，如果存在则获取
            OSSClient ossClient = new OSSClient(endPoint, accessKeyId, accessKeySecret);
            if (!ossClient.doesBucketExist(bucketName)) {
                //创建bucket
                ossClient.createBucket(bucketName);
                //设置oss实例的访问权限：公共读
                ossClient.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);
            }

            //获取上传文件流
            InputStream inputStream = file.getInputStream();

            //获取文件名称
            String fileName = file.getOriginalFilename();
            //在文件名称里面添加随机唯一的值
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            //把文件按照日期进行分类，构建日期路径: xpu-repair/2021/04/14
            String datePath = new DateTime().toString("yyyy/MM/dd");
            //拼接
            fileName = datePath + "/" + uuid + fileName;
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentType("image/jpg");
            //调用oss方法实现上传
            ossClient.putObject(bucketName, fileName, inputStream, objectMetadata);
            //关闭ossClient
            ossClient.shutdown();
            //需要把上传到阿里云oss路径手动拼接出来
            imageUrl = "https://" + bucketName + "." + endPoint + "/" + fileName;
            log.info("阿里云oss图片地址:{}", imageUrl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageUrl;
    }
}
