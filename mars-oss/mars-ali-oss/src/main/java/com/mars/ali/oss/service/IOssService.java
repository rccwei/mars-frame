package com.mars.ali.oss.service;

import org.springframework.web.multipart.MultipartFile;

public interface IOssService {

    /**
     * 文件上传至阿里云
     *
     * @param file file
     * @return String
     */
    String upload(MultipartFile file);
}
