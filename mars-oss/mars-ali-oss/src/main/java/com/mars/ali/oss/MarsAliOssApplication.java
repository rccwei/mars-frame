package com.mars.ali.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsAliOssApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsAliOssApplication.class, args);
	}

}
