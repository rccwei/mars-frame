package com.mars.ali.oss.controller;

import com.mars.ali.oss.result.R;
import com.mars.ali.oss.service.IOssService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-21 10:55:10
 */
@RestController
@AllArgsConstructor
@RequestMapping("/upload")
public class OssController {

    private final IOssService ossService;


    /**
     * 图片上传
     *
     * @param file file
     * @return String
     */
    @PostMapping("/image")
    private R upload(@RequestParam(value = "file") MultipartFile file) {
        return R.success(ossService.upload(file));
    }


}
