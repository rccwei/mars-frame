package com.mars.minio.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author mars
 * @description:
 * @version: v1.0
 * @since 2023-08-24 18:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MinioObject {
    private String bucket;
    private String region;
    private String object;
    private String etag;
    private long size;
    private boolean deleteMarker;
    private Map<String, String> userMetadata;
}
