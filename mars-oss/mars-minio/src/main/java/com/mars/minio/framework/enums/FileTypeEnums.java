package com.mars.minio.framework.enums;

public enum FileTypeEnums {
    /**
     * 图片上传
     */
    IMAGE("image", "图片上传"),

    /**
     * 视频上传
     */
    VIDEO("video", "视频上传"),

    /**
     * 文件上传
     */
    FILE("file", "文件上传");

    private String type;
    private String desc;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    FileTypeEnums() {
    }

    FileTypeEnums(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
