package com.mars.minio.framework.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author mars
 * @version 1.0
 * @classname OssConfig
 * @description
 * @since 2023/3/16 13:19
 */
@Component
public class OSSProperties implements InitializingBean {

    @Value("${spring.minio.endpoint}")
    private String endpoint;

    @Value("${spring.minio.accessKey}")
    private String accessKey;

    @Value("${spring.minio.secretKey}")
    private String secretKey;

    @Value("${spring.minio.bucketName}")
    private String bucketName;


    public static String EDNPOINT;
    public static String ACCESS_KEY_ID;
    public static String SECRECT;
    public static String BUCKET;



    @Override
    public void afterPropertiesSet() {
        EDNPOINT = endpoint;
        ACCESS_KEY_ID = accessKey;
        SECRECT = secretKey;
        BUCKET = bucketName;
    }
}
