package com.mars.minio.framework.strategy;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.web.multipart.MultipartFile;

/**
 * 策略接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-17 09:21:50
 */
public interface Strategy extends InitializingBean {

    /**
     * 上传接口
     *
     * @return String
     */
    String upload(MultipartFile file) throws Exception;
}
