package com.mars.minio.framework.factory;

import com.mars.minio.framework.enums.FileTypeEnums;
import com.mars.minio.framework.strategy.Strategy;
import org.springframework.util.ObjectUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 策略工厂
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-17 09:28:27
 */
public class StrategyFactory {

    private static final Map<FileTypeEnums, Strategy> STRATEGY_MAP = new ConcurrentHashMap<>();

    /**
     * 通过类型获取具体的策略
     *
     * @param type 名称
     * @return 策略
     */
    public static Strategy getStrategy(FileTypeEnums type) {
        return STRATEGY_MAP.get(type);
    }

    /**
     * 注册策略
     *
     * @param type    类型名称
     * @param handler 策略接口
     */
    public static void register(FileTypeEnums type, Strategy handler) {
        if (ObjectUtils.isEmpty(type) && handler == null) {
            return;
        }
        STRATEGY_MAP.put(type, handler);
    }
}
