package com.mars.minio.controller;

import com.mars.minio.framework.factory.StrategyFactory;
import com.mars.minio.result.R;
import com.mars.minio.framework.utils.FileUtils;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-24 21:16:11
 */
@RestController
@AllArgsConstructor
@RequestMapping("/minio")
public class MinioController {


    /**
     * 公共上传接口
     *
     * @param file 文件
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/upload")
    public R upload(@RequestParam(value = "file") MultipartFile file) throws Exception {
        String path = StrategyFactory.getStrategy(FileUtils.getFileType(file)).upload(file);
        return R.success(path);
    }


}
