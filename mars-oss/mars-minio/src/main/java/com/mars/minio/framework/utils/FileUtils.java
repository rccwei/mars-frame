package com.mars.minio.framework.utils;

import com.mars.minio.framework.enums.FileTypeEnums;
import org.springframework.web.multipart.MultipartFile;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-24 21:23:10
 */
public class FileUtils {

    public static FileTypeEnums getFileType(MultipartFile file) {
        String contentType = file.getContentType();
        if (contentType != null) {
            if (contentType.startsWith("image")) {
                return FileTypeEnums.IMAGE;
            } else if (contentType.startsWith("video")) {
                return FileTypeEnums.VIDEO;
            }
        }
        return FileTypeEnums.FILE;
    }
}
