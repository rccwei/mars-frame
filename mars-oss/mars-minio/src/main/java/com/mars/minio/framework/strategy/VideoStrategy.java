package com.mars.minio.framework.strategy;

import com.mars.minio.framework.enums.FileTypeEnums;
import com.mars.minio.framework.factory.StrategyFactory;
import com.mars.minio.framework.manager.MinioManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * 视频上传策略
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-17 09:21:50
 */
@Service
@AllArgsConstructor
public class VideoStrategy implements Strategy {

    private final MinioManager minioManager;

    @Override
    public void afterPropertiesSet() {
        StrategyFactory.register(FileTypeEnums.VIDEO, this);
    }

    @Override
    public String upload(MultipartFile file) {
        return minioManager.uploadVideo(file);
    }
}
