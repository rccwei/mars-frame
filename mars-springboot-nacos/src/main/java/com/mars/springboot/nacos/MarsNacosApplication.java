package com.mars.springboot.nacos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * springboot整合分布式配置中心Nacos 采用jasypt库来保证配置文件敏感信息被泄露
 *
 * 注意采用springboot 2.6+ nacos采用2.2.0以上
 */
@EnableDiscoveryClient
@SpringBootApplication
public class MarsNacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsNacosApplication.class, args);
    }

}
