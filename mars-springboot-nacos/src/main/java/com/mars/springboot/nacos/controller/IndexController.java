package com.mars.springboot.nacos.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-22 11:29:04
 */
@RefreshScope
@RestController
public class IndexController {

    @Value("${info.name}")
    private String name;

    @Value("${info.phone}")
    private String phone;

    /**
     * 获取名称
     *
     * @return String
     */
    @GetMapping(value = "/getInfo")
    public String getName() {
        return name + ":" + phone;
    }
}
