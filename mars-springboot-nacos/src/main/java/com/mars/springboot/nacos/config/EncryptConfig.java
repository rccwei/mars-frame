package com.mars.springboot.nacos.config;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;
import org.jasypt.util.text.BasicTextEncryptor;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-22 11:36:26
 */
public class EncryptConfig {
    public static void main(String[] args) {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        System.out.println("当前加密方式: 加密类: BasicTextEncryptor, 加密算法: PBEWithMD5AndDES ");
        // 1.设置秘钥
        String salt = "ftGKYey6ox+o/G6492Pu9w==";
        textEncryptor.setPassword(salt);
        // 2.加密
        // 2.1加密内容
        String pd = "lxpvkwojpnxafnoutgqowbecdwdsmpwq";
        System.out.println("加密前:  " + pd);
        // 2.2加密操作
        String pdAfterEncrypt = textEncryptor.encrypt(pd);
        System.out.println("加密后:  " + pdAfterEncrypt);
        // 3.解密操作
        String pdAfterDecrypt = textEncryptor.decrypt(pdAfterEncrypt);
        System.out.println("解密后:  " + pdAfterDecrypt);
    }
}
