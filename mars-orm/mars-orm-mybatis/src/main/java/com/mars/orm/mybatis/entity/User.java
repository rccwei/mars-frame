package com.mars.orm.mybatis.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:31
 */
@Data
public class User implements Serializable {

    private Integer id;

    private String name;

    private Date updateTime;
}
