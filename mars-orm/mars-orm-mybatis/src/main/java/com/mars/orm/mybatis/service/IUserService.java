package com.mars.orm.mybatis.service;

import com.mars.orm.mybatis.entity.User;

import java.util.List;

public interface IUserService {

    /**
     * 查询全部用户
     *
     * @return List<User>
     */
    List<User> findAll();
}
