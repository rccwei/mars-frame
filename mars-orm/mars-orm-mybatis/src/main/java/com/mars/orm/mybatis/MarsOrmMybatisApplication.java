package com.mars.orm.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.mars.orm.mybatis.mapper")
public class MarsOrmMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsOrmMybatisApplication.class, args);
    }

}
