package com.mars.orm.mybatis.mapper;


import com.mars.orm.mybatis.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 查询全部用户
     *
     * @return List<User>
     */
    List<User> findAll();
}
