package com.mars.orm.mybatis.service.impl;

import com.mars.orm.mybatis.entity.User;
import com.mars.orm.mybatis.mapper.UserMapper;
import com.mars.orm.mybatis.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserService {

    private final UserMapper userMapper;

    @Override
    @Transactional
    public List<User> findAll() {
        return userMapper.findAll();
    }
}
