package com.mars.oracle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsOrmOracleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsOrmOracleApplication.class, args);
    }

}
