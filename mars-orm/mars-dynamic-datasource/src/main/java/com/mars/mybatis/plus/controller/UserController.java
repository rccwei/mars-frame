package com.mars.mybatis.plus.controller;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.mars.mybatis.plus.base.PageInfo;
import com.mars.mybatis.plus.entity.Node;
import com.mars.mybatis.plus.entity.User;
import com.mars.mybatis.plus.entity.resp.TreeResponse;
import com.mars.mybatis.plus.request.UserRequest;
import com.mars.mybatis.plus.service.IUserService;
import com.mars.mybatis.plus.utils.Tree;
import lombok.AllArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:39
 */
@RestController
@AllArgsConstructor
public class UserController {

    static List<Node> list = new ArrayList<>();

    static {

        Node node1 = new Node(1, 0, "公司库");
        Node node2 = new Node(2, 0, "基金库");
        Node node3 = new Node(111, 1, "A股市场");
        Node node4 = new Node(112, 1, "港股市场");
        Node node5 = new Node(211, 2, "公墓基金池");

        Node node6 = new Node(212, 2, "非公墓基金池");
        Node node7 = new Node(11111, 111, "基础池");
        Node node9 = new Node(33333, 11111, "基础池三级1111");
        Node node10 = new Node(4444, 33333, "基础池四级");
        Node node11 = new Node(5555, 4444, "基础池五级");
        Node node12 = new Node(6666, 5555, "基础池六级");
        Node node13 = new Node(7777, 6666, "基础池七级");
        Node node14 = new Node(8888, 7777, "基础池八级");
        Node node8 = new Node(21211, 212, "可买池");
        list.add(node1);
        list.add(node2);
        list.add(node3);
        list.add(node4);
        list.add(node5);
        list.add(node6);
        list.add(node7);
        list.add(node8);
        list.add(node9);
        list.add(node10);
        list.add(node11);
        list.add(node12);
        list.add(node13);
        list.add(node14);

    }

    private final IUserService userService;

    @DS("master")
    @GetMapping("/getList")
    public List<User> getList() {
        return userService.list();
    }

    @DS("slave")
    @GetMapping("/getList2")
    public List<User> getList2() {
        return userService.list();
    }


    @GetMapping("/tree")
    public List<TreeResponse> tree() {
        return Tree.buildTree(list);
    }

    /**
     * 查询全部
     *
     * @return List<User>
     */
    @PostMapping("/page")
    public List<User> page(@RequestBody UserRequest request) {
        PageInfo<User> page = userService.page(request);
        if (!CollectionUtils.isEmpty(page.getList())) {
            return page.getList();
        }
        return null;
    }

    /**
     * 添加
     *
     * @return void
     */
    @GetMapping("/add")
    public void add() {
        userService.add();
    }


}
