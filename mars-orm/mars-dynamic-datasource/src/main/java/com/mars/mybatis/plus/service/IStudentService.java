package com.mars.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.mybatis.plus.entity.Student;

public interface IStudentService extends IService<Student> {

    void add(String name, Integer age);
}
