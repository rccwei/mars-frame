package com.mars.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.mybatis.plus.base.PageInfo;
import com.mars.mybatis.plus.entity.User;
import com.mars.mybatis.plus.request.UserRequest;

public interface IUserService extends IService<User> {

    /**
     * 分页
     *
     * @param request 请求参数
     * @return PageInfo<User>
     */
    PageInfo<User> page(UserRequest request);

    /**
     * 获取用户
     *
     * @return 用户列表
     */
    void add();
}
