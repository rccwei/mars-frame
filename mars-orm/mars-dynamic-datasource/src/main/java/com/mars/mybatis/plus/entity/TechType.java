package com.mars.mybatis.plus.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author wq
 * @date 2022/1/15 11:55
 */
@Data
@Accessors(chain = true)
public class TechType {

    @TableField("id")
    private Integer id;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField("update_time")
    private Date updateTime;
}
