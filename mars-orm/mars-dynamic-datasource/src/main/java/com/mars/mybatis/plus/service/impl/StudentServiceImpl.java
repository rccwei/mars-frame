package com.mars.mybatis.plus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.mybatis.plus.entity.Student;
import com.mars.mybatis.plus.mapper.StudentMapper;
import com.mars.mybatis.plus.service.IStudentService;
import org.springframework.stereotype.Service;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


    @Override
    public void add(String name, Integer age) {
        Student student = new Student();
        student.setAge(age);
        student.setName(name);
        super.save(student);
    }
}
