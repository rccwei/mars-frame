package com.mars.mybatis.plus.controller;

import com.mars.mybatis.plus.service.IStudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author wq
 * @date 2022/8/17 10:31
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Resource
    private IStudentService studentService;

    @GetMapping("/add")
    public String add(String name, Integer age) {
        studentService.add(name, age);
        return "success";
    }
}
