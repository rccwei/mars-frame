package com.mars.mybatis.plus.entity.resp;

import lombok.Data;

import java.util.List;

/**
 * @author wq
 * @date 2022/1/15 17:37
 */
@Data
public class TreeResponse {

    private Integer id;

    private String label;

    private List<TreeResponse> children;
}
