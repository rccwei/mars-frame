package com.mars.mybatis.plus.utils;

import java.util.ArrayList;

/**
 * @author wq
 * @date 2022/1/18 11:12
 */
public class PeekUtils {
    public static void main(String[] args) {
//        Student s1 = new Student("aa", 10);
//        Student s2 = new Student("bb", 20);
//        List<Student> studentList = Arrays.asList(s1, s2);
//        studentList.stream()
//                .peek(o -> o.setAge(100))
//                .forEach(System.out::println);
//        Map<String, Integer> map = studentList.stream().collect(Collectors.toMap(Student::getName, Student::getAge));
//        String collect = studentList.stream().map(Student::getName).collect(Collectors.toList()).stream().collect(Collectors.joining(
//                ","));
//        System.out.println(collect);
//        Integer integer = studentList.stream().map(Student::getAge).max(Integer::compare).get();
//        System.out.println(integer);

//        DoubleSummaryStatistics collect = studentList.stream().collect(Collectors.summarizingDouble(Student::getAge));
//        System.out.println(collect);
//        studentList.stream().collect(Collectors.toList());
//        Map<Integer, List<Student>> map = studentList.stream().collect(Collectors.groupingBy(Student::getAge));
//        System.out.println(map);


        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }

        // 1st argument, init value = 0
        int sum = list.stream().reduce(0, Integer::sum);

        System.out.println("sum : " + sum); // 55


    }
}
