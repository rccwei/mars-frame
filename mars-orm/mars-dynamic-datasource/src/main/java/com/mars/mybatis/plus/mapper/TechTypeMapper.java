package com.mars.mybatis.plus.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.mybatis.plus.entity.TechType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TechTypeMapper extends BaseMapper<TechType> {


}
