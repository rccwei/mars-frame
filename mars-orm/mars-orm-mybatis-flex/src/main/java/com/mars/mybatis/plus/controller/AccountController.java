package com.mars.mybatis.plus.controller;

import com.mars.mybatis.plus.dto.ArticleDTO;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.service.IAccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 账户控制器
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-05 15:32:28
 */
@RestController
@AllArgsConstructor
public class AccountController {


    private final IAccountService accountService;

    /**
     * 账户列表
     *
     * @return List<Account>
     */
    @GetMapping("/accountList")
    public List<Account> getAccountList() {
        return accountService.getList();
    }


    /**
     * 获取文章用户关联信息
     *
     * @return List<ArticleDTO>
     */
    @GetMapping("/getArticleList")
    public List<ArticleDTO> getArticleList() {
        return accountService.getArticleListDTO();
    }


}
