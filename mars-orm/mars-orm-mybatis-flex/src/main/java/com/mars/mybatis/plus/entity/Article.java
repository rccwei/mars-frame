package com.mars.mybatis.plus.entity;


import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table(value = "tb_article")
public class Article {

    private Long id;
    private Long accountId;
    private String title;
    private String content;
}
