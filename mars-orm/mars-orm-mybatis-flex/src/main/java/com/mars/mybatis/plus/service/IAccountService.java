package com.mars.mybatis.plus.service;

import com.mars.mybatis.plus.dto.ArticleDTO;
import com.mars.mybatis.plus.entity.Account;

import java.util.List;

public interface IAccountService {

    /**
     * 查询所有账户
     *
     * @return List<Account>
     */
    List<Account> getList();

    /**
     * 关联查询文章信息
     *
     * @return List<ArticleDTO>
     */
    List<ArticleDTO> getArticleListDTO();


}
