package com.mars.mybatis.plus.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleDTO {

    private Long id;
    private Long accountId;
    private String title;
    private String content;
    //以下用户相关字段
    private String userName;
    private int age;
    private Date birthday;
}
