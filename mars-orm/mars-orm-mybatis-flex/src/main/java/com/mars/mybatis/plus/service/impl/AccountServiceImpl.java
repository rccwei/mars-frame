package com.mars.mybatis.plus.service.impl;

import com.mars.mybatis.plus.dto.ArticleDTO;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.entity.table.AccountTableDef;
import com.mars.mybatis.plus.entity.table.ArticleTableDef;
import com.mars.mybatis.plus.mapper.AccountMapper;
import com.mars.mybatis.plus.service.IAccountService;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountServiceImpl implements IAccountService {


    @Autowired
    private AccountMapper accountMapper;

    @Override
    public List<Account> getList() {
        return accountMapper.selectListByQuery(QueryWrapper.create());
    }

    @Override
    public List<ArticleDTO> getArticleListDTO() {
        QueryWrapper query = QueryWrapper.create()
                .select(ArticleTableDef.Article.AllColumns)
                .select(AccountTableDef.ACCOUNT.AGE, AccountTableDef.ACCOUNT.USER_NAME, AccountTableDef.ACCOUNT.BIRTHDAY)
                .from(ArticleTableDef.Article)
                .leftJoin(AccountTableDef.ACCOUNT)
                .on(ArticleTableDef.Article.AccountId.eq(AccountTableDef.ACCOUNT.ID));
        return accountMapper.selectListByQueryAs(query, ArticleDTO.class);
    }
}
