package com.mars.mybatis.plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.mars.mars.orm.mybatisflex.mapper")
public class MarsOrmMybatisFlexApplication {


    public static void main(String[] args) {
        SpringApplication.run(MarsOrmMybatisFlexApplication.class, args);
    }

}
