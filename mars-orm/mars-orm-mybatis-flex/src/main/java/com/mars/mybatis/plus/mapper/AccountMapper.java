package com.mars.mybatis.plus.mapper;

import com.mars.mybatis.plus.entity.Account;
import com.mybatisflex.core.BaseMapper;

public interface AccountMapper  extends BaseMapper<Account> {
}
