package com.mars.mybatis.plus;


import com.alibaba.fastjson.JSONObject;
import com.mars.mybatis.plus.dto.ArticleDTO;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.service.IAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MarsOrmMybatisFlexApplication.class)
@Transactional(rollbackFor = Exception.class)
public class MainTest {

    @Resource
    private IAccountService accountService;

    @Test
    public void getListTest() {
        List<Account> list = accountService.getList();
        System.out.println(list);

    }


    @Test
    public void queryByCondition() {
        List<ArticleDTO> list = accountService.getArticleListDTO();
        System.out.println(JSONObject.toJSONString(list));
    }


}
