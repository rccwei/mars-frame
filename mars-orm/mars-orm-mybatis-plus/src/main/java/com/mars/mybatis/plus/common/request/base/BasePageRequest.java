package com.mars.mybatis.plus.request.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

/**
 * 基础分页参数
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 15:03:06
 */
@Data
public class BasePageRequest {

    private Integer current = 1;

    private Integer pageSize = 20;


    /**
     * 分页构造
     *
     * @return
     */
    public final <T> IPage<T> page() {
        return new Page<>(current, pageSize);
    }

}
