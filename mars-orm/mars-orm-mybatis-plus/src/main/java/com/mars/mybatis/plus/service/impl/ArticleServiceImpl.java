package com.mars.mybatis.plus.service.impl;

import com.github.yulichang.base.MPJBaseServiceImpl;
import com.mars.mybatis.plus.entity.Article;
import com.mars.mybatis.plus.mapper.ArticleMapper;
import com.mars.mybatis.plus.service.IArticleService;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:41:44
 */
@Service
public class ArticleServiceImpl extends MPJBaseServiceImpl<ArticleMapper, Article> implements IArticleService {
}
