package com.mars.mybatis.plus.common.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 15:15:14
 */
public class EncodeUtils {
    public static void main(String[] args) {
        StandardPBEStringEncryptor standardPBEStringEncryptor =new StandardPBEStringEncryptor();
        /*配置文件中配置如下的算法*/
        standardPBEStringEncryptor.setAlgorithm("PBEWithMD5AndDES");
        /*配置文件中配置的password*/
        standardPBEStringEncryptor.setPassword("EWRREWRERWECCCXC");
        /*要加密的文本*/
        String name = standardPBEStringEncryptor.encrypt("yyx-mall");
        String password =standardPBEStringEncryptor.encrypt("wkbyte2022@.");
        /*将加密的文本写到配置文件中*/
        System.out.println("name："+name);
        System.out.println("password："+password);
    }
}
