package com.mars.mybatis.plus.common.interceptor;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.mars.mybatis.plus.common.hander.TenantHandler;
import com.mars.mybatis.plus.common.manager.TenantManager;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 17:08:00
 */
public class CustomerInterceptor implements HandlerInterceptor {

    public static final String TENANT_ID = "tenant_id";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tenantId = request.getHeader(TENANT_ID);
        if (StringUtils.isNotEmpty(tenantId)){
            TenantManager.setCurrentTenantId(Long.valueOf(tenantId));
        }
        return true;
    }
}
