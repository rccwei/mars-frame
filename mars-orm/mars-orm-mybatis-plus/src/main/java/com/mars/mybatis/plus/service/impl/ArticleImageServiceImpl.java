package com.mars.mybatis.plus.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.yulichang.base.MPJBaseServiceImpl;
import com.mars.mybatis.plus.entity.ArticleImage;
import com.mars.mybatis.plus.mapper.ArticleImageMapper;
import com.mars.mybatis.plus.service.IArticleImageService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:41:44
 */
@Service
public class ArticleImageServiceImpl extends MPJBaseServiceImpl<ArticleImageMapper, ArticleImage> implements IArticleImageService {


    @Override
    public Map<Integer, List<ArticleImage>> getArticleImageByArticleId(List<Integer> articleIds) {
        return  super.list(Wrappers.lambdaQuery(ArticleImage.class).in(ArticleImage::getArticleId, articleIds))
                .stream().collect(Collectors.groupingBy(ArticleImage::getArticleId));

    }
}
