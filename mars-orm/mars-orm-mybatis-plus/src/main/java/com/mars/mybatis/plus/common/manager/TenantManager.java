package com.mars.mybatis.plus.common.manager;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 租户管理器
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 17:01:21
 */
public class TenantManager {

    /**
     * 当前用户租户 KEY
     */
    private static final String KEY_CURRENT_TENANT_ID = "KEY_CURRENT_PROVIDER_ID";
    /**
     * 保存当前租户ID
     */
    private static final Map<String, Object> TENANT_MAP = new ConcurrentHashMap<>();

    /**
     * 设置租户
     *
     * @param tenantId 租户ID
     */
    public static void setCurrentTenantId(Long tenantId) {
        TENANT_MAP.put(KEY_CURRENT_TENANT_ID, tenantId);
    }

    /**
     * 返回当前用户租户ID
     *
     * @return Long
     */
    public static Long getCurrentTenantId() {
        return (Long) TENANT_MAP.get(KEY_CURRENT_TENANT_ID);
    }


}
