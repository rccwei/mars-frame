package com.mars.mybatis.plus.request;

import com.mars.mybatis.plus.base.BasePageRequest;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wq
 * @version 1.0
 * @date 2021/03/16 19:03
 */
@Data
public class UserRequest extends BasePageRequest implements Serializable {

    private String mobile;

    private String password;

    private Integer gender;

    private String avatar;


}
