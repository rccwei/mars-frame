package com.mars.mybatis.plus.controller;

import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.request.AccountRequest;
import com.mars.mybatis.plus.response.AccountArticleResponse;
import com.mars.mybatis.plus.response.base.R;
import com.mars.mybatis.plus.service.IAccountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:48:27
 */
@RestController
@AllArgsConstructor
@RequestMapping(value = "/account")
public class AccountController {

    private final IAccountService accountService;


    /**
     * 添加
     *
     * @param request 请求参数
     * @return R
     */
    @PostMapping("/add")
    public R add(@RequestBody AccountRequest request) {
        accountService.add(request);
        return R.success();
    }


    /**
     * 删除
     *
     * @param id 请求参数
     * @return R
     */
    @DeleteMapping("/delete")
    public R delete(Integer id) {
        accountService.delete(id);
        return R.success();
    }


    /**
     * 修改
     *
     * @param request 请求参数
     * @return R
     */
    @PutMapping("/update")
    public R update(@RequestBody AccountRequest request) {
        accountService.updateAccount(request);
        return R.success();
    }


    /**
     * 获取账户列表
     *
     * @return List<Account>
     */
    @GetMapping(value = "/getAccountList")
    public R accountList() {
        return R.success(accountService.getAccountList());
    }

    /**
     * 查询账户信息分页列表（单表）
     *
     * @param request 请求参数
     * @return IPage<Account>
     */
    @PostMapping(value = "/getAccountPageList")
    public R getAccountPageList(@RequestBody AccountRequest request) {
        return R.success(accountService.getAccountPageList(request));
    }


    /**
     * 根据账户ID获取账户和文章关联列表
     *
     * @param id 账户ID
     * @return List<AccountArticleResponse>
     */
    @GetMapping("/getAccountArticleList")
    public List<AccountArticleResponse> getAccountArticleList(Integer id) {
        return accountService.getAccountArticleList(id);
    }


    /**
     * 根据账户ID分页获取账户和文章关联列表
     *
     * @param request 账户分页参数
     * @return List<AccountArticleResponse>
     */
    @PostMapping("/getAccountArticlePageList")
    public R getAccountArticlePageList(@RequestBody AccountRequest request) {
        return R.success(accountService.getAccountArticlePageList(request));
    }

}
