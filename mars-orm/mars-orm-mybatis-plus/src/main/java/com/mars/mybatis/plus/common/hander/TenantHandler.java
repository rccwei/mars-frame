package com.mars.mybatis.plus.common.hander;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.mars.mybatis.plus.common.manager.TenantManager;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.NullValue;
import net.sf.jsqlparser.expression.StringValue;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 16:59:19
 */
@Component
@Slf4j
public class TenantHandler implements TenantLineHandler {

    private static final List<String> IGNORE_TENANT_TABLES = Collections.singletonList("`sys_user`");

    /**
     * 获取租户 ID 值表达式，只支持单个 ID 值
     *
     * @return 租户 ID 值表达式
     */
    @Override
    public Expression getTenantId() {
        // 1.从header中的传入tenant_id
        // 2.通过拦截器从请求中获取对应租户id
        // 3.将tenant_id设置到上下文中
        Long tenantId = TenantManager.getCurrentTenantId();
        log.info("当前租户ID：{}", tenantId);
        if (Objects.isNull(tenantId)) {
            return new StringValue("1");
        }
        return new StringValue(String.valueOf(tenantId));
    }

    /**
     * 获取租户字段名
     * 默认字段名叫: tenant_id
     * 数据库中字段用的userId
     *
     * @return 租户字段名
     */
    @Override
    public String getTenantIdColumn() {
        return "tenant_id";
    }

    /**
     * 根据表名判断是否忽略拼接多租户条件
     * 默认都要进行解析并拼接多租户条件
     *
     * @param tableName 表名
     * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
     */
    @Override
    public boolean ignoreTable(String tableName) {
        return IGNORE_TENANT_TABLES.stream().anyMatch(ignoreTableName -> ignoreTableName.equalsIgnoreCase(tableName));
    }

}
