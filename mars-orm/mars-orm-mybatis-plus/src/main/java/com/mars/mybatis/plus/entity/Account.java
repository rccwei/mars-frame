package com.mars.mybatis.plus.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.mars.mybatis.plus.enums.GradeEnum;
import com.mars.mybatis.plus.hander.ListTypeHandler;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;


@Data
@TableName(value = "tb_account")
public class Account {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户年龄
     */
    private Integer age;
    /**
     * 生日
     */
    private Date birthday;

    /**
     * 年级
     */
    private GradeEnum grade;

    /**
     * 背景图片
     */
    @TableField(typeHandler = ListTypeHandler.class)
    private List<String> backgroundImages;

    /**
     * 逻辑删除
     */
    @TableLogic
    @TableField(fill = FieldFill.INSERT, value = "deleted")
    private Integer deleted;

    /**
     * 自动填充创建时间
     */
    @TableField(fill = FieldFill.INSERT, value = "create_time")
    private LocalDateTime createTime;

    /**
     * 自动填充更新时间
     */
    @TableField(fill = FieldFill.UPDATE, value = "update_time", update = "now()")
    private LocalDateTime updateTime;

    /**
     * 版本号
     */
    @Version
    private Integer version;

    /**
     * 租户ID
     */
    @TableField(value = "tenant_id")
    private Integer tenantId;

}
