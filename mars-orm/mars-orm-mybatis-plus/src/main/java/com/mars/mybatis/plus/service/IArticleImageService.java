package com.mars.mybatis.plus.service;

import com.github.yulichang.base.MPJBaseService;
import com.mars.mybatis.plus.entity.ArticleImage;

import java.util.List;
import java.util.Map;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:39:59
 */
public interface IArticleImageService extends MPJBaseService<ArticleImage> {

    /**
     * 文章列表
     *
     * @param articleIds articleIds
     * @return Map<Integer, List < String>>
     */
    Map<Integer, List<ArticleImage>> getArticleImageByArticleId(List<Integer> articleIds);
}
