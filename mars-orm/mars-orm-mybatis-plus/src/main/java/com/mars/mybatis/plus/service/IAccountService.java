package com.mars.mybatis.plus.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.yulichang.base.MPJBaseService;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.request.AccountRequest;
import com.mars.mybatis.plus.response.AccountArticleResponse;

import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:39:59
 */
public interface IAccountService extends MPJBaseService<Account> {


    /**
     * 添加账户
     *
     * @param request 账户
     */
    void add(AccountRequest request);

    /**
     * 删除账户
     *
     * @param id id
     */
    void delete(Integer id);

    /**
     * 更新账户
     *
     * @param request 请求参数
     */
    void updateAccount(AccountRequest request);

    /**
     * 获取账户列表（单表查询）
     *
     * @return List<Account>
     */
    List<Account> getAccountList();

    /**
     * 分页查询账户信息列表（单表）
     *
     * @param request 请求参数
     * @return IPage<Account>
     */
    IPage<Account> getAccountPageList(AccountRequest request);

    /**
     * 根据账户ID获取账户和文章关联列表 （简单关联查询）
     *
     * @param accountId 账户ID
     * @return List<AccountArticleResponse>
     */
    List<AccountArticleResponse> getAccountArticleList(Integer accountId);

    /**
     * 根据账户ID获取账户和文章关联列表 （分页关联查询）
     *
     * @param request 请求体
     * @return IPage<AccountArticleResponse>
     */
    IPage<AccountArticleResponse> getAccountArticlePageList(AccountRequest request);

}
