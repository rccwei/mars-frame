package com.mars.mybatis.plus.request;

import lombok.Data;

@Data
public class OrderedRequest {

    private Integer id;

    private String orderNumber;

    private Integer status;
}
