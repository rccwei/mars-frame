package com.mars.mybatis.plus.request;

import com.mars.mybatis.plus.enums.GradeEnum;
import com.mars.mybatis.plus.request.base.BasePageRequest;
import lombok.Data;

import java.util.Date;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 15:05:50
 */
@Data
public class AccountRequest extends BasePageRequest {

    /**
     * 账户ID
     */
    private Integer id;

    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户年龄
     */
    private Integer age;
    /**
     * 生日
     */
    private Date birthday;

    /**
     * 年级
     */
    private GradeEnum grade;



}
