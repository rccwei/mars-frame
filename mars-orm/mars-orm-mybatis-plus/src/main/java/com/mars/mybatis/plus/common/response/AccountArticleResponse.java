package com.mars.mybatis.plus.response;

import com.mars.mybatis.plus.entity.ArticleImage;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 14:00:25
 */
@Data
public class AccountArticleResponse {

    /**
     * 账户ID
     */
    private Integer id;
    /**
     * 用户名称
     */
    private String userName;
    /**
     * 用户年龄
     */
    private Integer age;
    /**
     * 生日
     */
    private Date birthday;


    /**
     * 文章ID
     */
    private Integer articleId;

    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;

    /**
     * 图片
     */
    private List<ArticleImage> image;

}
