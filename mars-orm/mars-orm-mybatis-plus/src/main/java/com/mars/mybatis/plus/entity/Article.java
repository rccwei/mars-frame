package com.mars.mybatis.plus.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "tb_article")
public class Article {

    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 账户ID
     */
    private Long accountId;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
}
