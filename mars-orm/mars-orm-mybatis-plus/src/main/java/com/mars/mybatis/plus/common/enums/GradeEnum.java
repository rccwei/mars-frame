package com.mars.mybatis.plus.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;

/**
 * 性别枚举
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-08 22:31:12
 */
public enum GradeEnum {


    PRIMARY(1, "小学"),
    SECONDARY(2, "中学"),
    HIGH(3, "高中");

    GradeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @EnumValue
    private final int code;

    private final String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

}
