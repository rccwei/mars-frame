package com.mars.mybatis.plus.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.yulichang.base.MPJBaseServiceImpl;
import com.github.yulichang.toolkit.JoinWrappers;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.entity.Article;
import com.mars.mybatis.plus.entity.ArticleImage;
import com.mars.mybatis.plus.mapper.AccountMapper;
import com.mars.mybatis.plus.request.AccountRequest;
import com.mars.mybatis.plus.response.AccountArticleResponse;
import com.mars.mybatis.plus.service.IAccountService;
import com.mars.mybatis.plus.service.IArticleImageService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:41:44
 */
@Service
@AllArgsConstructor
@DS("master")
public class AccountServiceImpl extends MPJBaseServiceImpl<AccountMapper, Account> implements IAccountService {

    private final IArticleImageService articleImageService;

    @Override
    public void add(AccountRequest request) {
        Account account = new Account();
        BeanUtils.copyProperties(request, account);
        account.setBirthday(new Date());
        super.save(account);
    }

    @Override
    public void delete(Integer id) {
        super.removeById(id);
    }

    @Override
    public void updateAccount(AccountRequest request) {
        Account account = new Account();
        BeanUtils.copyProperties(request, account);
        super.updateById(account);
    }

    @Override
    @DS("slave_1")
    public List<Account> getAccountList() {
        return super.list();
    }

    @Override
    public IPage<Account> getAccountPageList(AccountRequest request) {
        return super.page(request.page(), Wrappers.lambdaQuery(Account.class));
    }

    @Override
    public List<AccountArticleResponse> getAccountArticleList(Integer accountId) {
        MPJLambdaWrapper<Account> wrapper = getAccountMPJLambdaWrapper(accountId);
        return super.selectJoinList(AccountArticleResponse.class, wrapper);
    }

    @Override
    public IPage<AccountArticleResponse> getAccountArticlePageList(AccountRequest request) {
        MPJLambdaWrapper<Account> wrapper = getAccountMPJLambdaWrapper(request.getId());
        IPage<AccountArticleResponse> resultPage = super.selectJoinListPage(request.page(), AccountArticleResponse.class, wrapper);
        List<AccountArticleResponse> records = resultPage.getRecords();
        this.buildImageList(resultPage, records);
        return resultPage;
    }

    private MPJLambdaWrapper<Account> getAccountMPJLambdaWrapper(Integer id) {
        return JoinWrappers.lambda(Account.class)
                .selectAll(Account.class)
                .select(Article::getContent, Article::getTitle)
                .selectAs(Article::getId, AccountArticleResponse::getArticleId)
                .leftJoin(Article.class, Article::getAccountId, Account::getId)
                .eq(Article::getAccountId, id);
    }


    /**
     * 处理图片
     *
     * @param page    page
     * @param records records
     */
    private void buildImageList(IPage<AccountArticleResponse> page, List<AccountArticleResponse> records) {
        List<Integer> articleIds = records.stream().map(AccountArticleResponse::getArticleId).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(articleIds)) {
            Map<Integer, List<ArticleImage>> imageMap = articleImageService.getArticleImageByArticleId(articleIds);
            List<AccountArticleResponse> collect = records.stream().peek(x -> {
                x.setImage(imageMap.get(x.getArticleId()));
            }).collect(Collectors.toList());
            page.setRecords(collect);
        }
    }
}
