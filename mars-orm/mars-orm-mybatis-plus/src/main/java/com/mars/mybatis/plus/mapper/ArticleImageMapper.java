package com.mars.mybatis.plus.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import com.mars.mybatis.plus.entity.ArticleImage;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:39:07
 */
public interface ArticleImageMapper extends MPJBaseMapper<ArticleImage> {
}
