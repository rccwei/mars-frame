package com.mars.mybatis.plus.service;

import com.github.yulichang.extension.mapping.base.MPJDeepService;
import com.mars.mybatis.plus.entity.Article;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-07 13:39:59
 */
public interface IArticleService extends MPJDeepService<Article> {
}
