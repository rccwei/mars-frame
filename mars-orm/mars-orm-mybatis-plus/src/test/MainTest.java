import com.mars.mybatis.plus.MybatisPlusApplication;
import com.mars.mybatis.plus.entity.Account;
import com.mars.mybatis.plus.enums.GradeEnum;
import com.mars.mybatis.plus.mapper.AccountMapper;
import com.mars.mybatis.plus.service.IAccountService;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

/**
 * 测试
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 13:56:58
 */
@SpringBootTest(classes = MybatisPlusApplication.class)
@RunWith(SpringRunner.class)
public class MainTest {


    @Resource
    private AccountMapper accountMapper;

    @Resource
    private IAccountService accountService;

    @Test
    public void updateById() {
        Account account = new Account();
        account.setBirthday(new Date());
        account.setAge(33);
        account.setUserName("张三");
        account.setId(1);
        account.setBackgroundImages(null);
        accountMapper.updateById(account);
    }

    /**
     * 解决mybatis-plus无法更新字段为null问题
     */
    @Test
    public void alwaysUpdateSomeColumnById() {
        Account account = new Account();
        account.setBirthday(new Date());
        account.setAge(21);
        account.setUserName("李四");
        account.setGrade(GradeEnum.HIGH);
        account.setId(1);
        account.setCreateTime(LocalDateTime.now());
        account.setBackgroundImages(null);
        accountMapper.alwaysUpdateSomeColumnById(account);

    }


    /**
     * mybatis-plus内置saveBatch
     */
    @Test
    public void insertBatch() {
        ArrayList<Account> list = getAccounts();
        long l = System.currentTimeMillis();
        accountService.saveBatch(list);
        long l1 = System.currentTimeMillis();
        System.out.println("accountService.saveBatch耗时:" + (l1 - l));

    }

    /**
     * 解决mybatis-plus baseMapper无批量插入功能
     */
    @Test
    public void insertBatchSomeColumn() {
        ArrayList<Account> list = getAccounts();
        long l = System.currentTimeMillis();
        accountMapper.insertBatchSomeColumn(list);
        long l1 = System.currentTimeMillis();
        System.out.println("accountMapper.insertBatchSomeColumn耗时:" + (l1 - l));
    }

    @NotNull
    private ArrayList<Account> getAccounts() {
        Account account = new Account();
        account.setBirthday(new Date());
        account.setAge(21);
        account.setUserName("张三");
        account.setGrade(GradeEnum.HIGH);
        account.setCreateTime(LocalDateTime.now());
        account.setBackgroundImages(null);

        ArrayList<Account> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            Account accountNew = new Account();
            BeanUtils.copyProperties(account, accountNew);
            list.add(accountNew);
        }
        return list;
    }

    /**
     * 验证乐观锁
     */
    @Test
    public void testOptimisticLocker() {
        Account account = accountMapper.selectById(100026);
        account.setUserName("李四1");
        accountMapper.updateById(account);
    }

    /**
     * 乐观锁 version实现
     */
    @Test
    public void multiTestOptimisticLocker() {
        Thread thread = new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "开始执行");
            Account account = accountMapper.selectById(100026);
            account.setUserName("李四2");
            accountMapper.updateById(account);
        }, "T1");
        thread.start();

        System.out.println(Thread.currentThread().getName() + "开始执行");
        Account account = accountMapper.selectById(100026);
        account.setUserName("李四1");
        accountMapper.updateById(account);


    }


}
