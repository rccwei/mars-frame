# springboot+ mybatis-plus 自动填充功能
### 1.需求: 用mybatis-plus 插入一条数据到数据库，自动设置它的创建时间和逻辑删除的默认值（不能使用数据库的默认值）

### 2.实现方式:
```
自定义类实现 MetaObjectHandler
重写方法insertFill

```


### 3.具体步骤
### 1.自定义实现类 MetaObjectHandler
```
@Slf4j
@Component
public class CustomerMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
 
}

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}

```
### 2.具体字段上指定填充类型FieldFill.INSERT
```
@TableField(fill = FieldFill.INSERT, value = "create_time")
```

