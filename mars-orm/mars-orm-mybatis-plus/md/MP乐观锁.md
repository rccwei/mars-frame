# springboot+ mybatis-plus 乐观锁实现
## 实现方式: 采用OptimisticLockerInnerInterceptor插件

## 实现思路:
```
 1、先查询，获得版本号 version = 1 

--> A 
update user set name = "maomao", 
version = version + 1 
where id = 2 and version = 1 


--> B 线程抢先完成，这个时候 version = 2，会导致 A 修改失败！
 update user set name = "maomao", 
 version = version + 1 where id = 2 and version = 1


```


## 具体步骤
### 1.实体类加对应的字段 version
```
@Version
private Integer version;

```
### 2.注册组件
```
@Bean
public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
    return interceptor;
}
```

