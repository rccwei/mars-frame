package com.mars.orm.jpa.service;


import com.mars.orm.jpa.entity.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

}
