package com.mars.orm.jpa.controller;

import com.mars.orm.jpa.entity.User;
import com.mars.orm.jpa.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:39
 */
@RestController
@AllArgsConstructor
public class UserController {

    private final IUserService userService;

    /**
     * 查询全部
     *
     * @return List<User>
     */
    @GetMapping("/findAll")
    public List<User> findAll() {
        return userService.findAll();
    }
}
