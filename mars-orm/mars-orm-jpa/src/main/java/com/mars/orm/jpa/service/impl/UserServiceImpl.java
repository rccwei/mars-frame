package com.mars.orm.jpa.service.impl;

import com.mars.orm.jpa.entity.User;
import com.mars.orm.jpa.mapper.UserRepository;
import com.mars.orm.jpa.service.IUserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:33
 */
@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserService {


    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
