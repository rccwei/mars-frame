package com.mars.orm.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsOrmJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsOrmJpaApplication.class, args);
    }

}
