package com.mars.web.vue.main.two;

import java.util.List;

/**
 * @author wq
 * @date 2022/2/10 13:42
 */
public class Student extends AcademicStaff {


    /**
     * 学习计划
     */
    private String programmeOfStudy;

    public Student(Integer id, String name, String address, String universityName, String researchArea, String programmeOfStudy) {
        super(id, name, address, universityName, researchArea);
        this.programmeOfStudy = programmeOfStudy;
    }

    public static void main(String[] args) {
        Student student1 = new Student(1, "lisi", "shanghai", "shanghaidaxue", "java", "java");
        Student student = new Student(2, "mars", "shanghai", "shanghaidaxue", "python", "python");
        list.add(student);
        list.add(student1);
        List<AcademicStaff> list = AcademicStaff.search();
        for (AcademicStaff academicStaff : list) {
            System.out.println(academicStaff.getId());
        }
    }

    public String getProgrammeOfStudy() {
        return programmeOfStudy;
    }

    public void setProgrammeOfStudy(String programmeOfStudy) {
        this.programmeOfStudy = programmeOfStudy;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + super.getId() +
                ", name='" + super.getName() + '\'' +
                ", programmeOfStudy='" + programmeOfStudy + '\'' +
                ", address='" + super.getAddress() + '\'' +
                ", universityName='" + super.getUniversityName() + '\'' +
                '}';
    }

}
