package com.mars.web.vue.main.two;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @date 2022/2/10 13:56
 */
public class AcademicStaff extends Staff {

    static List<AcademicStaff> list = new ArrayList<>();

    private String researchArea;

    public AcademicStaff(Integer id, String name, String address, String universityName, String researchArea) {
        super(id, name, address, universityName);
        this.researchArea = researchArea;
    }

    public static void main(String[] args) {
        AcademicStaff academicStaff = new AcademicStaff(1, "lisi", "shanghai", "shanghaidaxue", "java");
        AcademicStaff academicStaff1 = new AcademicStaff(2, "lisi", "shanghai", "shanghaidaxue", "python");
        list.add(academicStaff);
        list.add(academicStaff1);
        List<AcademicStaff> list = AcademicStaff.search();
        for (AcademicStaff staff : list) {
            System.out.println(staff);
        }
    }

    public static List<AcademicStaff> search() {
        return list;
    }

    @Override
    public String toString() {
        return "Staff{" + "id=" + super.getId() + ", name='" + super.getName() + '\'' + ", address='" + super.getAddress() + '\'' + ", universityName='" + super.getUniversityName() + '\'' + ", positionTitle='" + researchArea + '\'' + '}';

    }
}
