package com.mars.web.vue.main.four;

/**
 * @author wq
 * @date 2022/2/10 15:47
 */
public class Rectangle implements Shape {
    private double width, height;
    private String type;

    public Rectangle(int x, int y) {
        type = "矩形";
        width = x;
        height = y;
    }

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(3, 4);
        Double area = rectangle.getArea();
        System.out.println("面积:" + area);

    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public Double getArea() {
        return width * height;
    }

    @Override
    public String toString() {
        return " 宽：" + width + "高：" + height;
    }
}
