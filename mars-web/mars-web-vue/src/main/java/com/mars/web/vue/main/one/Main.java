package com.mars.web.vue.main.one;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wq
 * @date 2022/2/10 13:44
 */
public class Main {
    static List<Student> list = new ArrayList<>();

    public static void main(String[] args) {
        Student student = new Student();
        student.insert(1,"lisi","study java","chengdu","xihuadaxue");
        student.print();
    }


    /**
     * 添加
     *
     * @param student
     * @return
     */
    public static List<Student> insert(Student student) {
        list.add(student);
        return list;
    }

    public static void print() {
        for (Student student : list) {
            System.out.println(student);
        }
    }
}
