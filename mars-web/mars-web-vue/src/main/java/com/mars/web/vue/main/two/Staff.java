package com.mars.web.vue.main.two;

/**
 * @author wq
 * @date 2022/2/10 13:56
 */
public class Staff {

    /**
     * id
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 地址
     */
    private String address;
    /**
     * 大学名称
     */
    private String universityName;


    public Staff() {
    }

    public void insert(Integer id, String name, String address, String universityName) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.universityName = universityName;
    }


    @Override
    public String toString() {
        return "Staff{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", universityName='" + universityName + '\'' +
                '}';
    }

    public Staff(Integer id, String name, String address, String universityName) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.universityName = universityName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
