package com.mars.web.vue.main.six;

import java.util.*;

/**
 * @author CGY
 * @date 2022-02-12 11:05
 */
public class Main {
    public static void main(String[] args) {
        // 创建堆栈对象
        Stack<Integer> container = new Stack<>();
        System.out.println("1111, 2222, 3333 三个元素入栈");
        //向 栈中 压入整数 1111
        container.push(1111);
        container.push(2222);
        container.push(3333);
        //显示栈中的所有元素
        Stack<Integer> integers = printStack(container);
        System.out.println("堆栈中的元素：");
        for (Integer integer : integers) {
            System.out.println(integer);
        }


        System.out.println("------------------");
        System.out.println("元素倒过来的堆栈是:");
        Stack<Integer> stack = reverseStack(container);
        System.out.println(stack);
        System.out.println("-------------------");
        Stack<Integer> stack1 = removeElement(container, 1111);
        System.out.println(stack1);

    }

    private static Stack<Integer> removeElement(Stack<Integer> s,Integer val) {
        int result = s.search(val);
        s.remove(result);
        return s;
    }


    private static Stack<Integer> reverseStack(Stack<Integer> s) {
        Collections.reverse(s);
        return s;
    }

    private static Stack<Integer> printStack(Stack<Integer> s) {
        if (s.empty()) {
            System.out.println("堆栈是空的，没有元素");
        } else {
            Stack<Integer> container = new Stack<>();

            // 得到 stack 中的枚举对象
            Enumeration items = s.elements();
            while (items.hasMoreElements()) {
                container.push((Integer) items.nextElement());
            }
            return container;
        }
        return null;

    }
}
