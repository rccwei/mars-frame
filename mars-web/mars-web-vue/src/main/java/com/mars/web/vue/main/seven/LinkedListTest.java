package com.mars.web.vue.main.seven;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author CGY
 * @date 2022-02-12 11:27
 */
public class LinkedListTest {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);
        list.add(null);
        System.out.println("原来链表是:");
        System.out.println(list);
        List<Integer> collect = list.stream().filter(x -> Objects.nonNull(x)).collect(Collectors.toList());
        Collections.reverse(collect);
        System.out.println("相反方式的链表是:" + collect);
    }
}
