package com.mars.web.vue.main.five;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author wq
 * @date 2022-02-12 10:47
 */
public class PriorityQueueTest {

    public static void main(String[] args) {
        //不用比较器，默认升序排列
        Queue<Integer> q = new PriorityQueue<>();
        q.add(3);
        q.add(2);
        q.add(4);
        Integer second = new ArrayList<>(q).get(1);
        System.out.println("优先级队列中第二个最小的值:" + second);
        System.out.println("--------------------------------");
        Integer first = new ArrayList<>(q).get(0);
        System.out.println("队列中最小的是:" + first);
        q.remove(first);
        System.out.println("删除后的队列:" + q);
        System.out.println("---------------------------------");

        q.add(5);
        System.out.println("添加后的队列是:" + q);

        boolean empty = q.isEmpty();
        System.out.println("队列是否为空:" + empty);

    }
}
