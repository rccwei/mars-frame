package com.mars.web.vue.main.four;

/**
 * @author wq
 * @date 2022/2/10 15:47
 */
public class Ellipse implements Shape {
    private double majorAxis, minorAxis;
    private String type;

    public Ellipse(int x, int y) {
        type = "椭圆";
        majorAxis = x;
        minorAxis = y;
    }

    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(3, 4);
        Double area = ellipse.getArea();
        System.out.println("椭圆面积:" + area);

    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public Double getArea() {
        return Math.PI * majorAxis * minorAxis;
    }

}
