package com.mars.web.vue.main.four;

/**
 * @author wq
 * @date 2022/2/10 15:23
 */
public interface Shape {

    String getType();

    Double getArea();

}





