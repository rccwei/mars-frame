package com.mars.web.vue.main.one;

/**
 * @author wq
 * @date 2022/2/10 13:42
 */
public class Student {


    /**
     * id
     */
    private Integer id;

    /**
     * 名称
     */
    private String name;

    /**
     * 学习计划
     */
    private String programmeOfStudy;
    /**
     * 地址
     */
    private String address;
    /**
     * 大学名称
     */
    private String universityName = "xihua";


    public Student() {
    }

    public void insert(Integer id, String name, String programmeOfStudy, String address, String universityName) {
        this.id = id;
        this.name = name;
        this.programmeOfStudy = programmeOfStudy;
        this.address = address;
        this.universityName = universityName;
    }

    public void print() {
        System.out.println("Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", programmeOfStudy='" + programmeOfStudy + '\'' +
                ", address='" + address + '\'' +
                ", universityName='" + universityName + '\'' +
                '}');

    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", programmeOfStudy='" + programmeOfStudy + '\'' +
                ", address='" + address + '\'' +
                ", universityName='" + universityName + '\'' +
                '}';
    }

    public Student(Integer id, String name, String programmeOfStudy, String address) {
        this.id = id;
        this.name = name;
        this.programmeOfStudy = programmeOfStudy;
        this.address = address;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProgrammeOfStudy() {
        return programmeOfStudy;
    }

    public void setProgrammeOfStudy(String programmeOfStudy) {
        this.programmeOfStudy = programmeOfStudy;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUniversityName() {
        return universityName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }
}
