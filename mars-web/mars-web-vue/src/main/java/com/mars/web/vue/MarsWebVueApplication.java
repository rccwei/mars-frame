package com.mars.web.vue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsWebVueApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebVueApplication.class, args);
    }

}
