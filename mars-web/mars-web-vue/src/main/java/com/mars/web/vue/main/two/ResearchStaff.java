package com.mars.web.vue.main.two;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author wq
 * @date 2022/2/10 13:57
 */
public class ResearchStaff extends Staff {

    static List<ResearchStaff> list = new ArrayList<>();

    /**
     * 职位名称
     */
    private String positionTitle;

    public ResearchStaff(Integer id, String name, String address, String universityName, String positionTitle) {
        super(id, name, address, universityName);
        this.positionTitle = positionTitle;
    }

    public static List<ResearchStaff> insert(ResearchStaff researchStaff) {
        list.add(researchStaff);
        return list;
    }

    public static ResearchStaff getById(Integer id) {
        return list.stream().filter(x -> x.getId().equals(id)).collect(Collectors.toList()).get(0);
    }

    public static void main(String[] args) {
        ResearchStaff researchStaff = new ResearchStaff(1, "lisi", "shanghai", "shanghaidaxue", "teacher");
        ResearchStaff researchStaff1 = new ResearchStaff(2, "lisi", "shanghai", "shanghaidaxue", "teacher");
        ResearchStaff.insert(researchStaff);
        ResearchStaff.insert(researchStaff1);
        ResearchStaff staff = ResearchStaff.getById(2);
        System.out.println(staff);
    }

    @Override
    public String toString() {
        return "Staff{" + "id=" + super.getId() + ", name='" + super.getName() + '\'' + ", address='" + super.getAddress() + '\'' + ", universityName='" + super.getUniversityName() + '\'' + ", positionTitle='" + positionTitle + '\'' + '}';

    }
}
