package com.mars.web.vue.main.four;

/**
 * @author wq
 * @date 2022/2/10 15:55
 */
public class Triangle implements Shape {
    private double side1, side2;
    private double angleBetween;
    private String type;

    public Triangle(double x, double y, double angleBetween) {
        type = "三角形";
        side1 = x;
        side2 = y;
        this.angleBetween = angleBetween;
    }

    public static void main(String[] args) {
        Triangle triangle = new Triangle(2, Math.sqrt(3.0), 60);
        Double area = triangle.getArea();
        System.out.println("面积:" + area);


    }

    public String getType() {
        return type;
    }

    public Double getArea() {
        double a = Math.toRadians(angleBetween);
        return ((side1 * side2 * Math.sin(a)) / 2);
    }

}
