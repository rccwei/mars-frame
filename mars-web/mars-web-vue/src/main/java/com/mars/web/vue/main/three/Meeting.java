package com.mars.web.vue.main.three;

/**
 * @author wq
 * @date 2022/2/10 14:55
 */
public class Meeting {


    /**
     * 时间
     */
    private String time;

    /**
     * 位置
     */
    private String location;

    /**
     * 主题
     */
    private String subject;

    public Meeting(String time, String location, String subject) {
        this.time = time;
        this.location = location;
        this.subject = subject;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void printDetails(Meeting meeting) {
        System.out.println("Meeting in " + meeting.getLocation() + " at " + meeting.getTime() + ";" + "Subject:" + meeting.getSubject());
    }

    public static void main(String[] args) {
        Meeting meeting = new Meeting("12:30", "room 205", "Examiner’s meeting");
        meeting.printDetails(meeting);
    }

}
