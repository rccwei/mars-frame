package com.mars.web.filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class MarsWebFilterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebFilterApplication.class, args);
    }

}
