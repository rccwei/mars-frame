package com.mars.web.filter.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 17:06
 */
@RestController
public class IndexController {


    @GetMapping("/index")
    public String index() {
        return "success";
    }
}
