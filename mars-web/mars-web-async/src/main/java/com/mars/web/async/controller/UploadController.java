package com.mars.web.async.controller;

import com.mars.web.async.service.IUploadService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 16:56
 */
@RestController
@AllArgsConstructor
public class UploadController {

    private final IUploadService uploadService;


    @GetMapping("/upload")
    public String upload() {
        uploadService.upload();
        return "success";
    }
}
