package com.mars.web.async.service.impl;

import com.mars.web.async.service.IUploadService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author wq
 * @version 1.0
 * @date 2021/02/26 16:55
 */
@Service
public class UploadServiceImpl implements IUploadService {
    @Override
    @Async
    public void upload() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
