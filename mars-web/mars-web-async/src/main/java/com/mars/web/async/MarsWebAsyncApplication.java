package com.mars.web.async;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * springboot异步执行
 */
@SpringBootApplication
@EnableAsync
public class MarsWebAsyncApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebAsyncApplication.class, args);
    }

}
