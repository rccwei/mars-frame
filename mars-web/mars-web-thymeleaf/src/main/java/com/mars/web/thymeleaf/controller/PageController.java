package com.mars.web.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Stack;

@Controller
@RequestMapping("/page")
public class PageController {

    public static void main(String[] args) {
        Stack<String> stack = new Stack<>();
        stack.push("1");
        stack.push("2");
        stack.push("3");
        stack.push("4");
        stack.push("5");

//        for (String s : stack) {
//            System.out.println(s);
//        }

        System.out.println(stack.search("2"));
    }

    @GetMapping("{path}")
    public String toPage(@PathVariable String path) {

        return path;
    }
}
