package com.mars.web.thymeleaf.common;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Result<T> implements Serializable {
    private String code;
    private String message;
    private T data;

    /**
     * @param code    1
     * @param message 2
     * @param data    3
     * @return Result<T>
     * @description 返回成功自定义参数
     * @author zhangbin
     * @date 2021/4/26 13:42
     */
    public static <T> Result<T> succeedWith(String code, String message, T data) {
        return new Result<>(code, message, data);
    }

    /**
     * @param message 1
     * @return Result<T>
     * @description 返回成功（带消息）
     * @author zhangbin
     * @date 2021/4/26 13:42
     */
    public static <T> Result<T> succeed(String message) {
        return succeedWith("200", message, null);
    }

    /**
     * @param data 1
     * @return Result<T>
     * @description 返回成功（带数据）
     * @author zhangbin
     * @date 2021/4/26 13:42
     */
    public static <T> Result<T> succeed(T data) {
        return succeedWith("200", "成功", data);
    }

    /**
     * @param message 1
     * @param data    2
     * @return Result<T>
     * @description 返回成功（带消息和数据）
     * @author zhangbin
     * @date 2021/4/26 13:43
     */
    public static <T> Result<T> succeed(String message, T data) {
        return succeedWith("200", message, data);
    }

    /**
     * @param code    1
     * @param message 2
     * @param data    3
     * @return Result<T>
     * @description 返回失败（自定义参数）
     * @author zhangbin
     * @date 2021/4/26 13:43
     */
    public static <T> Result<T> failedWith(String code, String message, T data) {
        return new Result<>(code, message, data);
    }

    /**
     * @param message 1
     * @return Result<T>
     * @description 返回失败（带消息）
     * @author zhangbin
     * @date 2021/4/26 13:43
     */
    public static <T> Result<T> failed(String message) {
        return succeedWith("202", message, null);
    }

    /**
     * @param
     * @return Result<T>
     * @description 返回失败（带数据）
     * @author zhangbin
     * @date 2021/4/26 13:43
     */
    public static <T> Result<T> failed() {
        return succeedWith("202", "未知错误", null);
    }

}
