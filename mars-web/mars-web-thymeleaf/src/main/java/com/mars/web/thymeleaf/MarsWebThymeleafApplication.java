package com.mars.web.thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsWebThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebThymeleafApplication.class, args);
    }

}
