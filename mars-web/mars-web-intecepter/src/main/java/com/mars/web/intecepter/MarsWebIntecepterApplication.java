package com.mars.web.intecepter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsWebIntecepterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebIntecepterApplication.class, args);
    }

}
