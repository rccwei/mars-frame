package com.mars.webservice.utils;

import com.mars.webservice.rent.Person;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Operate {

    private List<Person> list = new ArrayList<Person>();//用户添加信息业务逻辑控制

    public void addLogic() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //调用菜单添加菜单
            m.addMenu();
            //判断菜单是否在1到3这个区间
            int key = reg.menuRegex(1, 3);
            switch (key) {
                case 1:
                    //添加操作
                    addOperation();
                    break;
                case 2:
                    //查询全部用户信息
                    showAll();
                    break;
                case 3:
                    //返回上一级
                    return;
            }
        }
    }

    //用户查询信息业务逻辑控
    public void searchLogic() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //调用搜索菜单选项
            m.searchMenu();
            //控制用户输入的区间在 1到7
            int key = reg.menuRegex(1, 7);
            switch (key) {
                case 1:
                    //如果是1  按照名称来搜索
                    searchByName();
                    break;
                case 2:
                    //如果是2  按照年龄来搜索
                    searchByAge();
                    break;
                case 3:
                    //如果是3  按照性别来搜索
                    searchBySex();
                    break;
                case 4:
                    //如果是4  按照手机号来搜索
                    searchByTelNum();
                    break;
                case 5:
                    //如果是5  按照地址来搜索
                    searchByAdd();
                    break;
                case 6:
                    //如果是6 展示全部
                    showAll();
                    break;
                case 7:
                    //返回上一级
                    return;
            }
        }
    }

    //修改信息业务逻辑控制
    public void modifyLogicLogic() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //创建修改菜单选项
            m.modifyMenu();
            //判断用户输入的选项是否在1 到 3
            int key = reg.menuRegex(1, 3);
            switch (key) {
                case 1:
                    //展示全部
                    showAll();
                    break;
                case 2:
                    //修改逻辑处理
                    modifyLogicLogic1();
                    break;
                case 3:
                    //返回上一级
                    return;
            }
        }
    }

    //修改有三层跳转
    public void modifyLogicLogic1() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //创建子修改选项
            m.subModifyMenu();
            int key = reg.menuRegex(1, 6);
            switch (key) {
                case 1:
                    //修改名称
                    nameModify();
                    break;
                case 2:
                    //修改年龄
                    ageModify();
                    break;
                case 3:
                    //修改性别
                    sexModify();
                    break;
                case 4:
                    //修改手机号
                    telNumModify();
                    break;
                case 5:
                    //修改地址
                    addressModify();
                    break;
                case 6:
                    //返回
                    return;
            }
        }
    }

    //删除信息业务逻辑控制
    public void deleteLogic() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //展示删除菜单
            m.deleteMenu();
            int key = reg.menuRegex(1, 4);
            switch (key) {
                case 1:
                    //展示全部
                    showAll();
                    break;
                case 2:
                    //删除一个
                    delete();
                    break;
                case 3:
                    //删除全部信息
                    deleteAll();
                    break;
                case 4:
                    return;
            }
        }
    }

    //排序信息业务逻辑控制
    public void orderLogic() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            m.orderMenu();
            int key = reg.menuRegex(1, 5);
            switch (key) {
                case 1:
                    //按照名称排序
                    orderName();
                    break;
                case 2:
                    //按照年龄排序
                    orderAge();
                    break;
                case 3:
                    //按照性别排序
                    orderSex();
                    break;
                case 4:
                    //展示全部信息
                    showAll();
                    break;
                case 5:
                    return;
            }
        }
    }

    //添加新用户信息
    @SuppressWarnings("resource")
    public void addOperation() {
        TelNoteRegex reg = new TelNoteRegex();
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入联系人id:");
        //用户键盘输入
        String id = sc.nextLine();
        System.out.println("请输入要添加的姓名:");
        String name = sc.nextLine();
        //用户输入的名称校验
        name = reg.nameRegex(name);
        System.out.println("请输入要添加的年龄:");
        String age = sc.nextLine();
        //用户输入的年龄校验
        age = reg.ageRegex(age);
        System.out.println("请输入要添加的性别:");
        String sex = sc.nextLine();
        //用户输入的性别校验
        sex = reg.sexRegex(sex);
        System.out.println("请输入要添加的手机号码:");
        String telNum = sc.nextLine();
        //用户输入的手机号码校验
        telNum = reg.telNumRegex(telNum);
        System.out.println("请输入要添加的地址:");
        String address = sc.nextLine();
        //用户输入的地址校验
        address = reg.addressRegex(address);
        int personId = Integer.parseInt(id);
        //创建person对象  并设置属性
        Person p = new Person(personId, name, age, sex, telNum, address);
        //向集合中添加用户信息
        list.add(p);

    }
// //判断序号是否已经存在插入成功失败
//   @SuppressWarnings("resource")
//   public String judgeId(String id){
//    Scanner sc=new Scanner(System.in);
//    Iterator<Person> it=list.iterator();
//    String newId=id;
//    while(it.hasNext()){
//     Person p=(Person)it.next();
//     if(newId.equals(p.getId())){
//      System.out.println("此序号已经存在,请重新输入序号");
//      newId=sc.nextLine();
//      continue;
//     }else{
//      return newId;
//     }
//    }
//    return id;
//   }

    //查询全部用户信息
    public void showAll() {
        System.out.println("序号\t\t" + "姓名\t\t" + "年龄\t\t" + "性别\t\t" + "手机号\t\t" + "地址");
        //遍历全部的信息
        Iterator<Person> it = list.iterator();
        //判断是否还有下一个元素
        while (it.hasNext()) {
            //获取下一个元素
            Person p = (Person) it.next();
            System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
        }
    }

    //按姓名查询用户信息
    @SuppressWarnings("resource")
    public void searchByName() {
        //创建用户输入校验对象
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("输入你要查询的姓名:");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();
        //判断名称是否合法
        name = reg.nameRegex(name);
        //遍历集合
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            //如果用户输入名称和集合中的名称一样的返回该集合中的数据
            if (name.equals(p.getName())) {
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            }
        }
    }

    //按年龄查询用户信息
    @SuppressWarnings("resource")
    public void searchByAge() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("输入你要查询的年龄:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String age = sc.nextLine();
        //用户输入年龄校验
        age = reg.ageRegex(age);
        //遍历集合
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            //如果年龄相同的返回该集合中的数据
            if (age.equals(p.getAge())) {
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            }
        }
    }

    //按性别查询用户信息
    @SuppressWarnings("resource")
    public void searchBySex() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("输入你要查询的性别:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String sex = sc.nextLine();
        //校验用户输入的性别
        sex = reg.sexRegex(sex);
        //遍历集合中的性别
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            //如果性别相同的返回该集合中的数据
            if (sex.equals(p.getSex())) {
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            }
        }
    }

    //按电话号码查询用户信息
    @SuppressWarnings("resource")
    public void searchByTelNum() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("输入你要查询的电话号码:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String telNum = sc.nextLine();
        //校验用户输入的手机号
        telNum = reg.telNumRegex(telNum);
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            //如果手机号相同的返回该集合中的数据
            if (telNum.equals(p.getTelNum())) {
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            }
        }
    }

    //按地址查询用户信息
    @SuppressWarnings("resource")
    public void searchByAdd() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("输入你要查询的地址:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String address = sc.nextLine();
        //校验用户输入的地址
        address = reg.addressRegex(address);
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            //如果地址相同的返回该集合中的数据
            if (address.equals(p.getAddress())) {
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            }
        }
    }

    // //修改指定记录信息
// public void modify(){
//  System.out.println("请输入你要修改的信息:");
//
// }
    //修改姓名信息
    @SuppressWarnings("resource")
    public void nameModify() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("请输入你要修改的序号:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        //字符串转int
        int id = Integer.parseInt(num);
        System.out.println("请输入你要修改的姓名:");
        String name = sc.nextLine();
        //对用户输入的名称校验
        name = reg.nameRegex(name);
        //遍历集合
        Iterator<Person> it = list.iterator();
        //判断是否还有下一个元素
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //设置名称
                p.setName(name);
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            } else {
                System.out.println("修改失败,请重试!");
            }
        }

    }

    //修改年龄信息
    @SuppressWarnings("resource")
    public void ageModify() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("请输入你要修改的序号:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int id = Integer.parseInt(num);
        System.out.println("请输入你要修改的年龄:");
        String age = sc.nextLine();
        //年龄校验
        age = reg.ageRegex(age);
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //设置年龄
                p.setAge(age);
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            } else {
                System.out.println("修改失败,请重试!");
            }
        }
    }

    //修改性别信息
    @SuppressWarnings("resource")
    public void sexModify() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("请输入你要修改的序号:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int id = Integer.parseInt(num);
        System.out.println("请输入你要修改的性别:");
        String sex = sc.nextLine();
        //校验性别
        sex = reg.sexRegex(sex);
        //遍历集合
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //设置性别
                p.setSex(sex);
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            } else {
                System.out.println("修改失败,请重试!");
            }
        }
    }

    //修改号码信息
    @SuppressWarnings("resource")
    public void telNumModify() {
        System.out.println("请输入你要修改的序号:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int id = Integer.parseInt(num);
        System.out.println("请输入你要修改的电话号码:");
        String telNum = sc.nextLine();
        //遍历集合
        Iterator<Person> it = list.iterator();
        //判断是否还有下一个元素
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //设置电话号码
                p.setTelNum(telNum);
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            } else {
                System.out.println("修改失败,请重试!");
            }
        }
    }

    //修改地址信息
    @SuppressWarnings("resource")
    public void addressModify() {
        TelNoteRegex reg = new TelNoteRegex();
        System.out.println("请输入你要修改的序号:");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int id = Integer.parseInt(num);
        System.out.println("请输入你要修改的地址:");
        String address = sc.nextLine();
        //地址校验
        address = reg.addressRegex(address);
        Iterator<Person> it = list.iterator();
        //判断是否还有下一个元素
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //设置地址
                p.setAddress(address);
                System.out.println(p.getId() + "#" + "\t\t" + p.getName() + "\t\t" + p.getAge() + "\t\t" + p.getSex() + "\t\t" + p.getTelNum() + "\t\t" + p.getAddress());
            } else {
                System.out.println("修改失败,请重试!");
            }
        }
    }

    //删除指定用户信息
    @SuppressWarnings("resource")
    public void delete() {
        System.out.println("请输入你要删除的id：");
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        String num = sc.nextLine();
        int id = Integer.parseInt(num);
        //遍历集合
        Iterator<Person> it = list.iterator();
        while (it.hasNext()) {
            Person p = (Person) it.next();
            if (id == p.getId()) {
                //删除元素
                list.remove(id);
                System.out.println("删除成功");
            } else {
                System.out.println("删除失败");
            }

        }

    }

    //删除全部用户信息
    public void deleteAll() {
        //删除全部用户信息
        list.clear();
        System.out.println("删除成功");
    }

    //按用户姓名排序信息
    public void orderName() {
        Comparator<Person> comparator = new Comparator<Person>() {
            //按用户姓名排序
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        Collections.sort(list, comparator);
        showAll();
    }

    //按用户年龄排序信息
    public void orderAge() {
        Comparator<Person> comparator = new Comparator<Person>() {
            //按用户年龄排序
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getAge().compareTo(o2.getAge());
            }
        };
        Collections.sort(list, comparator);
        showAll();
    }

    //按用户性别排序信息
    public void orderSex() {
        Comparator<Person> comparator = new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getSex().compareTo(o2.getSex());
            }
        };
        Collections.sort(list, comparator);
        showAll();
    }
}