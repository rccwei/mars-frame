package com.mars.webservice.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/01 10:15
 */
@WebService(name = "CommonService", // 暴露服务名称
        targetNamespace = "http://webservice.mars.com/"// 命名空间,一般是接口的包名倒序
)
public interface CommonService {

    /**
     * 暴露接口
     *
     * @param name
     * @return
     */
    @WebMethod
    String sayHello(@WebParam(name = "userName") String name);
}
