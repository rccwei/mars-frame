package com.mars.webservice.rent;

public class Person {
    private int id;//用户ID属性
    private String name;//用户姓名属性
    private String age;//用户年龄属性
    private String sex;//用户性别属性
    private String telNum;//用户电话号码属性
    private String address;//用户地址属性

    public Person() {
    }

    //构造方法
    public Person(int id, String name, String age, String sex, String telNum, String address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
        this.telNum = telNum;
        this.address = address;
    }

    //获取对象ID
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    //获取对象名称
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //获取对象年龄
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    //获取对象性别
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    //获取对象手机号
    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    //获取对象地址
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}