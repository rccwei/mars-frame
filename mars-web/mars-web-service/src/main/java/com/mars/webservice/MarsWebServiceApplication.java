package com.mars.webservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsWebServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebServiceApplication.class, args);
    }

}
