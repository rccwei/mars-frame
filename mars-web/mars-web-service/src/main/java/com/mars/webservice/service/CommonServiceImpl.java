package com.mars.webservice.service;

import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/01 10:18
 */
@WebService(serviceName = "CommonService", // 与接口中指定的name一致
        targetNamespace = "http://webservice.mars.com/", // 与接口中的命名空间一致,一般是接口的包名倒
        endpointInterface = "com.mars.webservice.service.CommonService"// 接口地址
)
@Component
public class CommonServiceImpl implements CommonService {
    @Override
    public String sayHello(String name) {
        return "HELLO" + name;
    }
}
