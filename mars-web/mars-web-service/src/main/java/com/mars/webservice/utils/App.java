package com.mars.webservice.utils;

public class App {
    //程序的主入口每次程序启动的入口
    public static void main(String[] args) {
        //创建当前APP 实例 并调用 start 方法
        new App().start();
    }

    public void start() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建操作对象  通过new 关键字
        Operate o = new Operate();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //调用主菜单打印主界面
            m.mainMenu();
            //调用校验用户输入的数字方法
            int key = reg.menuRegex(1, 6);
            switch (key) {
                //如果用户输入的是1
                case 1:
                    //用户添加信息业务逻辑控制
                    o.addLogic();
                    break;
                case 2:
                    //用户搜索业务逻辑控制
                    o.searchLogic();
                    break;
                case 3:
                    //用户修改业务逻辑控制
                    o.modifyLogicLogic();
                    break;
                case 4:
                    //用户修改业务逻辑控制
                    o.deleteLogic();
                    break;
                case 5:
                    //用户排序业务逻辑控制
                    o.orderLogic();
                    break;
                case 6:
                    //用户退出系统
                    System.exit(0);
                    break;
            }
        }
    }
}
