package com.mars.webservice.config;

import com.mars.webservice.service.CommonService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;

/**
 * @author wq
 * @version 1.0
 * @date 2021/07/01 10:12
 */
@Configuration
public class CxfConfig {

    @Resource
    private Bus bus;

    @Resource
    CommonService commonService;

    /**
     * JAX-WS
     **/
    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, commonService);
        //发布地址
        endpoint.publish("/CommonService");
        return endpoint;
    }
}
