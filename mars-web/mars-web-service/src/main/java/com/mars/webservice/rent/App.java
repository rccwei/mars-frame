package com.mars.webservice.rent;

public class App {
    //程序的主入口每次程序启动的入口
    public static void main(String[] args) {
        //创建当前APP 实例 并调用 start 方法
        new App().start();
    }

    public void start() {
        //创建菜单对象  通过new 关键字
        Menu m = new Menu();
        //创建操作对象  通过new 关键字
        Operate o = new Operate();
        //创建用户输入对象  通过new 关键字
        TelNoteRegex reg = new TelNoteRegex();
        while (true) {
            //调用主菜单打印主界面
            m.mainMenu();
            //调用校验用户输入的数字方法
            int key = reg.menuRegex(1, 3);
            switch (key) {
                //如果用户输入的是1
                case 1:
                    boolean b = o.adminOperate();
                    if (b) {
                        o.switchRole();
                    }
                    break;
                case 2:
                    boolean b1 = o.userOperate();
                    if (b1) {
                        o.memberList();
                    }
                    break;
                case 3:
                    //用户退出系统
                    System.exit(0);
                    break;
            }
        }
    }
}
