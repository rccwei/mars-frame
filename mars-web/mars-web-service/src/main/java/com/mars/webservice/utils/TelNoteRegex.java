package com.mars.webservice.utils;

import java.util.Scanner;

public class TelNoteRegex {
    //对菜单项校验
    @SuppressWarnings("resource")
    public int menuRegex(int min, int max) {
        String regex = "[0-9]{1}";
        Scanner sc = new Scanner(System.in);
        while (true) {
            String input = sc.nextLine();
            //根据合法性的校验
            if (input.matches(regex)) {
                int key = Integer.parseInt(input);
                if (key >= min && key <= max) {
                    return key;
                } else {
                    System.out.println("输入的菜单项不符合要求!");
                }
            } else {
                System.out.println("输入内容不正确!");
            }
        }

    }

    //用户输入姓名的验证
    @SuppressWarnings("resource")
    public String nameRegex(String name) {
        //正则表达式
        String pattern = "[\\u4e00-\\u9fa5]+";
        String str = name;
        //创建键盘输入对象
        Scanner sc = new Scanner(System.in);
        while (true) {
            //如果用户输入的符合正则表达式返回用户输入的字符串
            if (str.matches(pattern)) {
                return str;
            } else {
                System.out.println("输入不合法字符,请重新输入中文汉字");
                str = sc.nextLine();
                continue;
            }
        }
    }

    //用户输入年龄的验证
    @SuppressWarnings("resource")
    public String ageRegex(String age) {
        String pattern = "^([0-9]|[0-9]{2}|100)$";
        String str = age;
        //创建键盘输入对象 满足正则表达式
        Scanner sc = new Scanner(System.in);
        while (true) {
            //如果用户输入的符合正则表达式返回用户输入的字符串
            if (str.matches(pattern)) {
                return str;
            } else {
                System.out.println("输入不合法字符,请重新输入数字");
                str = sc.nextLine();
                continue;
            }
        }
    }

    //用户性别的验证
    @SuppressWarnings("resource")
    public String sexRegex(String sex) {
        String pattern = "[男女MmFf]";
        String str = sex;
        //创建键盘输入对象 满足正则表达式
        Scanner sc = new Scanner(System.in);
        while (true) {
            //如果用户输入的符合正则表达式返回用户输入的字符串
            if (str.matches(pattern)) {
                return str;
            } else {
                System.out.println("输入不合法字符,请重新输入男女MmFf");
                str = sc.nextLine();
                continue;
            }
        }
    }

    //用户输入电话号码的验证
    @SuppressWarnings("resource")
    public String telNumRegex(String telNum) {
        String pattern = "[0-9]{11}";
        String str = telNum;
        //创建键盘输入对象 满足正则表达式
        Scanner sc = new Scanner(System.in);
        while (true) {
            //如果用户输入的符合正则表达式返回用户输入的字符串
            if (str.matches(pattern)) {
                return str;
            } else {
                System.out.println("输入不合法字符,请重新输入11位的手机号码");
                str = sc.nextLine();
                continue;
            }
        }
    }

    //用户输入地址的验证
    @SuppressWarnings("resource")
    public String addressRegex(String address) {
        String pattern = "[\\u4e00-\\u9fa5]+";
        String str = address;
        //创建键盘输入对象 满足正则表达式
        Scanner sc = new Scanner(System.in);
        while (true) {
            //如果用户输入的符合正则表达式返回用户输入的字符串
            if (str.matches(pattern)) {
                return str;
            } else {
                System.out.println("输入不合法字符,请重新输入中文汉字");
                str = sc.nextLine();
                continue;
            }
        }
    }
}