package com.mars.vue.resp;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * 分页
 *
 * @author : wq
 * @date : 2023-09-04
 **/
public class AppPageInfo<T> {

    /**
     * 数据集合
     */
    private List<T> list;

    /**
     * 返回最大游标
     */
    private Integer maxCursor;

    /**
     * 是否还有数据
     */
    private boolean hasMore = false;


    public AppPageInfo() {

    }


    /**
     * 转换参数
     *
     * @param page 分页
     * @param <T>  T
     * @return PageInfo
     */
    public static <T> AppPageInfo<T> buildPage(IPage<T> page) {
        AppPageInfo<T> pageInfo = new AppPageInfo<>();
        pageInfo.setList(page.getRecords());
        return pageInfo;
    }


    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Integer getMaxCursor() {
        return maxCursor;
    }

    public void setMaxCursor(Integer maxCursor) {
        this.maxCursor = maxCursor;
    }

    public boolean isHasMore() {
        return hasMore;
    }

    public void setHasMore(boolean hasMore) {
        this.hasMore = hasMore;
    }
}
