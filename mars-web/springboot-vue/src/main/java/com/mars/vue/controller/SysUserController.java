package com.mars.vue.controller;

import com.mars.vue.request.SysUserRequest;
import com.mars.vue.resp.R;
import com.mars.vue.service.ISysUserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author Administrator
 */
@RequestMapping("/sysUser")
@RestController
@AllArgsConstructor
public class SysUserController {

    private final ISysUserService sysUserService;

    /**
     * 获取用户列表
     *
     * @param name   name
     * @param mobile mobile
     * @return List<SysUser>
     */
    @GetMapping("/list")
    public R list(String name, String mobile) {
        return R.success(sysUserService.getList(name, mobile));
    }

    /**
     * 分页列表
     * @param request
     * @return
     */
    @PostMapping("/pageList")
    public R pageList(@RequestBody SysUserRequest request) {
        return R.success(sysUserService.pageList(request));
    }

}
