package com.mars.vue.request;

import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-04 14:03:12
 */
@Data
public class SysUserRequest extends BasePageRequest{

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 名称
     */
    private String name;

    /**
     * 最大游标
     */
    private Integer maxCursor;


}
