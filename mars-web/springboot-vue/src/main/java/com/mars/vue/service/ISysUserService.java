package com.mars.vue.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.vue.entity.SysUser;
import com.mars.vue.request.SysUserRequest;
import com.mars.vue.resp.AppPageInfo;

import java.util.List;


public interface ISysUserService extends IService<SysUser> {

    /**
     * 根据名称搜索
     *
     * @param name 名称
     * @return List<SysUser>
     */
    List<SysUser> getList(String name, String mobile);

    /**
     * 分页查询
     *
     * @param request request
     * @return PageInfo<SysUser>
     */
    AppPageInfo<SysUser> pageList(SysUserRequest request);
}
