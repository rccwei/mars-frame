package com.mars.vue.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.vue.entity.SysUser;
import com.mars.vue.request.SysUserRequest;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface SysUserMapper extends MyBaseMapper<SysUser> {


    /**
     * 查询用户
     *
     * @param name 名称
     * @param date 时间
     * @return List<SysUser>
     */
    List<SysUser> getUser(@Param("name") String name, @Param("date") String date);

    /**
     * 分页查询
     *
     * @param page    page
     * @param request request
     * @return IPage<SysUser>
     */
    IPage<SysUser> selectPageListV1(@Param("page") IPage<SysUser> page, @Param("request") SysUserRequest request);

    /**
     * 分页查询 V2
     *
     * @param page  page
     * @param request request
     * @return IPage<SysUser>
     */
    IPage<SysUser> selectPageListV2(@Param("page") IPage<SysUser> page, @Param("request") SysUserRequest request);
}
