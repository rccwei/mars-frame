package com.mars.vue.request;

import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-04 14:04:43
 */
@Data
public class BasePageRequest {

    /**
     * 当前页
     */
    private Integer pageNumber = 1;

    /**
     * 每页大小
     */
    private Integer pageSize = 10;

    /**
     * 查询版本
     */
    private String version;

}
