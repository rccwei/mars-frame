package com.mars.vue.service.imp;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.vue.entity.SysUser;
import com.mars.vue.mapper.SysUserMapper;
import com.mars.vue.request.SysUserRequest;
import com.mars.vue.resp.AppPageInfo;
import com.mars.vue.service.ISysUserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * @author Administrator
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {


    @Override
    public List<SysUser> getList(String name, String mobile) {
        return baseMapper.selectList(Wrappers.lambdaQuery(SysUser.class)
                .like(StringUtils.isNotEmpty(name), SysUser::getNickname, name)
                .like(StringUtils.isNotEmpty(mobile), SysUser::getMobile, mobile));

    }


    @Override
    public AppPageInfo<SysUser> pageList(SysUserRequest request) {
        return getSysUserPageList(request);
    }




    private AppPageInfo<SysUser> getSysUserPageList(SysUserRequest request) {
        StopWatch stopWatch = StopWatch.createStarted();
        Page<SysUser> page = new Page<>(request.getPageNumber(), request.getPageSize());
        LambdaQueryWrapper<SysUser> wrapper = buildWrapper(request);
        page.setSearchCount(false);
        IPage<SysUser> pageResult = baseMapper.selectPage(page, wrapper);
        // SELECT id,nickname,avatar,mobile,email,password,role_id,create_time,deleted FROM sys_user WHERE deleted=0 AND (id > ?) ORDER BY id ASC LIMIT ?
        AppPageInfo<SysUser> buildPage = AppPageInfo.buildPage(pageResult);
        List<SysUser> list = buildPage.getList();
        if (CollectionUtils.isNotEmpty(list)) {
            List<SysUser> userList = list.stream().sorted(Comparator.comparing(SysUser::getId)
                    .reversed()).collect(Collectors.toList());
            buildPage.setMaxCursor(userList.get(0).getId());
            if (list.size() == request.getPageSize()) {
                buildPage.setHasMore(true);
            }
        }
        log.info("请求耗时:{}", stopWatch.getTime(TimeUnit.MILLISECONDS));
        return buildPage;
    }

    @NotNull
    private LambdaQueryWrapper<SysUser> buildWrapper(SysUserRequest request) {
        // SELECT id,nickname,avatar,mobile,email,password,role_id,create_time,deleted FROM sys_user WHERE deleted=0 AND (id > ?) ORDER BY id ASC LIMIT ?
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotEmpty(request.getName()), SysUser::getNickname, request.getName());
        wrapper.like(StringUtils.isNotEmpty(request.getMobile()), SysUser::getMobile, request.getMobile());
        wrapper.gt(SysUser::getId, request.getMaxCursor());
        wrapper.orderByAsc(SysUser::getId);
        return wrapper;
    }



    /**
     * V1 : mybatis-plus分页
     *
     * @param request request
     * @return PageInfo<SysUser>
     */
    private AppPageInfo<SysUser> getSysUserPageListV1(SysUserRequest request) {
        IPage<SysUser> page = new Page<>(request.getPageNumber(), request.getPageSize());
        StopWatch stopWatch = StopWatch.createStarted();
        IPage<SysUser> pageResult = baseMapper.selectPageListV1(page, request);
        log.info("请求耗时:{}", stopWatch.getTime(TimeUnit.MILLISECONDS));
        return AppPageInfo.buildPage(pageResult);
    }
}
