package com.mars.vue.mapper;

import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 自定义Mapper
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-08-10 13:54:54
 */
public interface MyBaseMapper<T> extends MPJBaseMapper<T> {

    /**
     * 全字段更新，不会忽略null值
     *
     * @param entity 实体对象
     */
    void alwaysUpdateSomeColumnById(@Param("et") T entity);

    /**
     * 全量插入,等价于batch insert
     *
     * @param entityList 实体集合
     */
    void insertBatchSomeColumn(List<T> entityList);


}
