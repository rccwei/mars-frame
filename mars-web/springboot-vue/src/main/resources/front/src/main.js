// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

// 引入element
import 'element-ui/lib/theme-chalk/index.css';
import Element from 'element-ui';

Vue.use(Element);
import axios from 'axios'

Vue.prototype.$http = axios
/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  template: '<App/>'
})
