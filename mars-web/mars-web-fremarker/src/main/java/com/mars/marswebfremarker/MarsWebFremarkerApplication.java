package com.mars.marswebfremarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsWebFremarkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWebFremarkerApplication.class, args);
    }

}
