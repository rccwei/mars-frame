package com.mars.marswebfremarker.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @date 2022/1/20 18:24
 */
@RestController
public class PageController {


    @GetMapping("/index")
    public String index() {
        return "this is page111";
    }
}
