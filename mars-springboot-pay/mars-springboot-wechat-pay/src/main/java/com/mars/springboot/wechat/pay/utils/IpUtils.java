package com.mars.springboot.wechat.pay.utils;

import javax.servlet.http.HttpServletRequest;


/**
 * @author wq
 */
public class IpUtils {

    private final static String UNKNOWN = "unknown";

    /**
     * 获取用户真实IP地址
     *
     * @param request request
     * @return String
     */
    public static String getRealIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || UNKNOWN.equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }

}
