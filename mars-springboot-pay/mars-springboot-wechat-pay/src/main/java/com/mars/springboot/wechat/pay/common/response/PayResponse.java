package com.mars.springboot.wechat.pay.common.response;

import lombok.Data;

/**
 * 签名信息
 *
 * @author wq
 * @date 2023/4/19 11:11
 */
@Data
public class PayResponse {

    /**
     * 小程序ID
     */
    private String appId;
    /**
     * 时间戳
     */
    private String timeStamp;
    /**
     * 随机串
     */
    private String nonceStr;
    /**
     * packages （包含pre_pareId）
     */
    private String packages;

    /**
     * mweb_url
     */
    private String mweb_url;
    /**
     * 支付签名
     */
    private String paySign;
    /**
     * 签名类型
     */
    private String signType;

    /**
     * 微信支付商户号
     */
    private String partnerId;


}
