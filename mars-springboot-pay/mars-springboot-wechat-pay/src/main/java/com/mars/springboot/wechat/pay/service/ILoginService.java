package com.mars.springboot.wechat.pay.service;


import com.mars.springboot.wechat.pay.common.response.WxSessionResponse;

/**
 * 微信登录相关接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:43:11
 */
public interface ILoginService {

    /**
     * 微信通过code登录
     *
     * @param code code
     * @return WxSessionResponse
     */
    WxSessionResponse doLogin(String code);




}
