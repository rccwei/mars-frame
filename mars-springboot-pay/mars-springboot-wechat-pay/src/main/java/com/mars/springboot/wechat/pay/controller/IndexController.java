package com.mars.springboot.wechat.pay.controller;


import com.mars.springboot.wechat.pay.common.base.R;
import com.mars.springboot.wechat.pay.service.ILoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:42:41
 */
@RestController
public class IndexController {


    @GetMapping("/index")
    public R index() {
        System.out.println("操作成功");
        return R.success();
    }


}
