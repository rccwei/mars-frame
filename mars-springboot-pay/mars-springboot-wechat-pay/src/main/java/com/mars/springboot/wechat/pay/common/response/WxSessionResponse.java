package com.mars.springboot.wechat.pay.common.response;


import lombok.Data;

import java.io.Serializable;

/**
 * 微信登录响应对象
 *
 * @author: Mars
 * @create: 2023-08-19 15:29
 */
@Data
public class WxSessionResponse implements Serializable {

    private String openId;

    private String sessionKey;
}
