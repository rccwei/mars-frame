package com.mars.springboot.wechat.pay.entity;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * 签名信息
 *
 * @author wq
 * @date 2023/4/19 11:11
 */
@Data
public class SignInfo {

    /**
     * 小程序ID
     */
    private String appId;
    /**
     * 时间戳
     */
    private String timeStamp;
    /**
     * 随机串
     */
    private String nonceStr;
    /**
     * package
     */
    @XStreamAlias("package")
    private String repay_id;
    /**
     * 签名类型
     */
    private String signType;

}
