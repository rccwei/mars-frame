package com.mars.springboot.wechat.pay.service;


import com.mars.springboot.wechat.pay.common.request.PayRequest;
import com.mars.springboot.wechat.pay.common.response.PayResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 统一支付接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-11 11:11:42
 */
public interface IPayService {

    /**
     * 统一支付接口
     *
     * @param request 请求参数
     * @return PayResponse
     */
    PayResponse doPay(PayRequest request) throws Exception;

    /**
     * 支付完成回调
     *
     * @param request  请求参数
     * @param response response
     */
    void callback(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
