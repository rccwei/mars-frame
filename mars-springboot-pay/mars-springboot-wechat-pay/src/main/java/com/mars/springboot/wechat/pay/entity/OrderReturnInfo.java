package com.mars.springboot.wechat.pay.entity;

import lombok.Data;

/**
 * 订单返回消息实体类
 *
 * @author wq
 * @date 2023/4/19 11:08
 */
@Data
public class OrderReturnInfo {
    /**
     * 返回码
     */
    private String return_code;
    /**
     * 返回信息
     */
    private String return_msg;
    /**
     * 返回状态码
     */
    private String result_code;
    /**
     * appid
     */
    private String appid;
    /**
     * 商户ID
     */
    private String mch_id;
    /**
     * 随机字符串
     */
    private String nonce_str;
    /**
     * 签名
     */
    private String sign;
    /**
     * mweb_url
     */
    private String mweb_url;
    /**
     * prepay_id
     */
    private String prepay_id;
    /**
     * trade_type
     */
    private String trade_type;

}
