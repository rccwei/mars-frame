package com.mars.springboot.wechat.pay.common.constant;

public class WxPayConstant {

    /**
     * 公众号appId
     */
    public static final String OFFICIAL_ACCOUNTS_APP_ID = "wx365f73c3c186ca55";

    /**
     * 微信（食堂点餐）小程序APPID
     */
    public static final String WX_APP_ID = "wx83dafa9fc1b400f1";

    /**
     * 成功标识
     */
    public static final String SUCCESS = "SUCCESS";

    /**
     * 微信 prepay_id 前缀
     */
    public static final String WX_PREPAY_ID_PREFIX = "prepay_id=";

    /**
     * 公众号秘钥
     */
    public static final String OFFICIAL_ACCOUNTS_APP_SECRET = "70720d2c39fdeb925ecace9507fd8866";
    /**
     * 公众号通过code 获取openId
     */
    public static final String OFFICIAL_ACCOUNTS_TOKEN_API = "https://api.weixin.qq.com/sns/oauth2/access_token";

    /**
     * h5 域名地址
     */
    public static final String H5_HOST = "https://fresh.newhopescm.com";

    /**
     * 订单已支付状态
     */
    public static final String ORDER_PAID = "ORDERPAID";

    /**
     * 微信错误请求
     */
    public static final String INVALID_REQUEST = "INVALID_REQUEST";

    /**
     * 交易订单号
     */
    public static final String OUT_TRADE_NO = "out_trade_no";

    /**
     * 返回码
     */
    public static final String RETURN_CODE = "return_code";

    /**
     * 退款进行中
     */
    public static final String REFUND_PROCESSING = "PROCESSING";

    /**
     * sign
     */
    public static final String SIGN = "sign";


    /**
     * 微信小程序支付接口
     */
    public static final String JSAPI_API = "https://api.mch.weixin.qq.com/v3/pay/transactions/jsapi";

    /**
     * 微信小程序H5 支付
     */
    public static final String H5_PAY_API = "https://api.mch.weixin.qq.com/v3/pay/transactions/h5";
    /**
     * 微信统一下单
     */
    public static final String PAY_UNIFIEDORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 微信退款 接口
     */
    public static final String WX_PAY_REFUND_API = "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds";

    /**
     * 退款单查询
     */
    public static final String WX_PAY_REFUND_QUERY_API = "https://api.mch.weixin.qq.com/v3/refund/domestic/refunds/";

}
