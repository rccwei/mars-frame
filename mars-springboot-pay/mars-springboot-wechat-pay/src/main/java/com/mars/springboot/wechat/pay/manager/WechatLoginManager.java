package com.mars.springboot.wechat.pay.manager;

import com.alibaba.fastjson.JSONObject;
import com.mars.springboot.wechat.pay.common.response.WxSessionResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Objects;


/**
 * @author: Mars
 * @create: 2023-08-19 15:28
 */
@Component
@Slf4j
public class WechatLoginManager {

    /**
     * 微信code登录获取openId
     */
    public static final String WX_LOGIN_OPENID = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";

    /**
     * 获取 WX_ACCESSTOKEN
     */
    public static final String WX_ACCESSTOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";


    /**
     * 获取手机号
     */
    public static final String WX_PHONE_NUMBER_URL = "https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=%s";


    @Resource
    private RestTemplate restTemplate;

    @Value("${wechat.pay.appId}")
    private String appId;

    @Value("${wechat.pay.appSecret}")
    private String appSecret;


    /**
     * 通过code 获取openId
     *
     * @param code code
     * @return WxSessionResponse
     */
    public WxSessionResponse sessionCode(String code) {
        String url = String.format(WX_LOGIN_OPENID, appId, appSecret, code);
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        String body = entity.getBody();
        if (StringUtils.isNotEmpty(body)) {
            JSONObject jsonObject = JSONObject.parseObject(body);
            WxSessionResponse session = new WxSessionResponse();
            session.setOpenId(jsonObject.getString("openid"));
            session.setSessionKey(jsonObject.getString("session_key"));
            return session;
        }
        return null;
    }

    /**
     * @param accessToken accessToken
     * @param code        code
     * @return String
     */
    public String getPhoneNumber(String accessToken, String code) {
        RestTemplate client = new RestTemplate();
        JSONObject postData = new JSONObject();
        postData.put("code", code);
        String url = String.format(WX_PHONE_NUMBER_URL, accessToken);
        JSONObject json = client.postForEntity(url, postData, JSONObject.class).getBody();
        if (Objects.nonNull(json)) {
            Integer errcode = json.getInteger("errcode");
            if (errcode == 0) {
                return json.getJSONObject("phone_info").getString("phoneNumber");
            }
        }
        return null;
    }

    /**
     * 获取AccessToken
     *
     * @return String
     */
    public String getAccessToken() {
        String url = String.format(WX_ACCESSTOKEN, appId, appSecret);
        ResponseEntity<String> entity = restTemplate.getForEntity(url, String.class);
        String body = entity.getBody();
        if (StringUtils.isNotEmpty(body)) {
            JSONObject jsonObject = JSONObject.parseObject(body);
            return jsonObject.getString("access_token");
        }
        return null;
    }

}
