package com.mars.springboot.wechat.pay.controller;


import com.mars.springboot.wechat.pay.common.base.R;
import com.mars.springboot.wechat.pay.service.ILoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:42:41
 */
@Api(tags = "登录相关接口")
@RestController
@RequestMapping("/login")
public class LoginController {

    @Resource
    public ILoginService loginService;

    /**
     * 1.微信通过code登录 获取openId和sessionKey
     *
     * @param code code
     * @return R
     */
    @ApiOperation(value = "1.微信通过code登录")
    @GetMapping("/doLogin")
    public R doLogin(String code) {
        return R.success(loginService.doLogin(code));
    }




}
