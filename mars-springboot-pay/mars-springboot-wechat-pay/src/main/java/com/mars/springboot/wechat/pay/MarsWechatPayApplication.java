package com.mars.springboot.wechat.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * 位置支付
 */
@SpringBootApplication
public class MarsWechatPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsWechatPayApplication.class, args);
    }

}
