package com.mars.springboot.wechat.pay.service.impl;

import com.alibaba.fastjson.JSONObject;

import com.mars.springboot.wechat.pay.common.response.WxSessionResponse;
import com.mars.springboot.wechat.pay.manager.WechatLoginManager;
import com.mars.springboot.wechat.pay.service.ILoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:44:38
 */
@Slf4j
@Service
public class LoginServiceImpl implements ILoginService {

    @Resource
    private WechatLoginManager wechatLoginManager;


    @Override
    public WxSessionResponse doLogin(String code) {
        log.info("当前微信登录code:{}", code);
        WxSessionResponse sessionCode = wechatLoginManager.sessionCode(code);
        log.info("登录成功响应参数:{}", JSONObject.toJSONString(sessionCode));
        if (Objects.isNull(sessionCode)) {
            throw new RuntimeException("微信登录失败");
        }
        return sessionCode;
    }


}
