package com.mars.springboot.wechat.pay.common.base;

import lombok.Data;

import java.io.Serializable;

/**
 * 统一响应体
 *
 * @author Mars
 */
@Data
public class R implements Serializable {

    private static final long serialVersionUID = -637415113996231595L;

    /**
     * 响应时间
     */
    private long timestamp;

    /**
     * 响应状态值
     */
    private int status;

    /**
     * 响应消息
     */
    private String msg;

    /**
     * 响应数据
     */
    private Object data;


    public R(int status, String message, Object data) {
        this.timestamp = System.currentTimeMillis();
        this.status = status;
        this.msg = message;
        this.data = data;
    }

    public static R success(Object data) {
        return new R(200, "操作成功", data);
    }

    public static R success() {
        return new R(200, "操作成功", null);
    }

    public static R error() {
        return new R(-1, "操作失败", null);
    }
    public static R error(String message) {
        return new R(-1, message, null);
    }

    public R(int status, String message) {
        this(status, message, null);
    }

    public R() {
    }
}
