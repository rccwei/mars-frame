package com.mars.springboot.wechat.pay.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mars.springboot.wechat.pay.common.constant.WxPayConstant;
import com.mars.springboot.wechat.pay.common.request.PayRequest;
import com.mars.springboot.wechat.pay.common.response.PayResponse;
import com.mars.springboot.wechat.pay.manager.WxPayManager;
import com.mars.springboot.wechat.pay.service.IPayService;
import com.mars.springboot.wechat.pay.utils.PayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;


/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 15:29:20
 */
@Slf4j
@Service
public class PayServiceImpl implements IPayService {

    @Resource
    private WxPayManager wxPayManager;

    @Value("${wechat.pay.apiV2Key}")
    private String apiV2Key;

    public static final String UTF_8 = "utf-8";


    @Override
    public PayResponse doPay(PayRequest request) throws Exception {
        log.info("统一下单接口请求参数:" + JSONObject.toJSONString(request));
        PayResponse response = wxPayManager.doPay(request);
        log.info("统一下单接口返回参数:" + JSONObject.toJSONString(response));
        return response;
    }

    @Override
    public void callback(HttpServletRequest request, HttpServletResponse response) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        String notifyXml = sb.toString();
        log.info("接收到的报文：{}", notifyXml);
        String resXml = "";
        Map<String, String> map = PayUtil.xmlToMap(notifyXml);
        String returnCode = map.get(WxPayConstant.RETURN_CODE);
        if (WxPayConstant.SUCCESS.equals(returnCode)) {
            //验证签名是否正确 回调验签时需要去除sign和空值参数
            Map<String, String> validParams = PayUtil.paraFilter(map);
            // 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
            String validStr = PayUtil.createLinkString(validParams);
            // 验证微信签名
            String sign = PayUtil.sign(validStr, apiV2Key, UTF_8).toUpperCase();
            // 因为微信回调会有八次之多,所以当第一次回调成功了,那么我们就不再执行逻辑了
            log.info("签名信息:{}", sign);
            //根据微信官网的介绍，此处不仅对回调的参数进行验签，还需要对返回的金额与系统订单的金额进行比对等
            if (sign.equals(map.get(WxPayConstant.SIGN))) {
                String outTradeNo = map.get(WxPayConstant.OUT_TRADE_NO);
                log.info("支付成功当前支付订单号:{}", outTradeNo);
                // todo 处理业务逻辑 支付成功更新相应单据状态 记得处理幂等 采用分布式锁


                log.info("===================微信支付成功回调完成==================");
                resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                        + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";
            }
        } else {
            resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                    + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
        }
        log.info("微信支付响应结果:{}", resXml);
        BufferedOutputStream out = new BufferedOutputStream(
                response.getOutputStream());
        out.write(resXml.getBytes());
        out.flush();
        out.close();
    }
}
