package com.mars.springboot.wechat.pay.common.request;

import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 15:16:01
 */
@Data
public class PayRequest {

    /**
     * 商品名称
     */
    private String body;
    /**
     * 交易订单号
     */
    private String outTradeNo;
    /**
     * 支付金额 单位分
     */
    private Integer totalFee;
    /**
     * 交易类型 JSAPI：公众号支付或小程序支付  NATIVE：原生扫码支付  APP：APP支付   MWEB：H5支付
     */
    private String tradeType;
    /**
     * 小程序用户openId
     */
    private String openId;


}
