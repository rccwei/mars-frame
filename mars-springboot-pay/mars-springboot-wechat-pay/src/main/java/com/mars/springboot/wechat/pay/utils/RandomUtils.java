package com.mars.springboot.wechat.pay.utils;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.Random;

/**
 * @author wq
 * @date 2022-09-16
 */
public class RandomUtils {

    /**
     * 随机字符串
     *
     * @param num num
     * @return String
     */
    public static String randomStr(int num) {
        Random random = new Random();
        char[] chars = new char[num];
        for (int i = 0; i < num; i++) {
            int ch = random.nextInt(26) + 97;
            chars[i] = (char) ch;
        }
        return String.valueOf(chars);
    }

    /**
     * 创建随机字符串
     *
     * @return String
     */
    public static String createNonceStr() {
        return DigestUtils.md5Hex(String.valueOf(System.currentTimeMillis()));
    }

}
