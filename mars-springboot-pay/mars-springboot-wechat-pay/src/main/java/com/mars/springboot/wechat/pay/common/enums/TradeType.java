package com.mars.springboot.wechat.pay.common.enums;

/**
 * 交易类型
 */
public enum TradeType {
    /**
     * MWEB
     */
    MWEB("MWEB"),

    /**
     * JSAPI
     */
    JSAPI("JSAPI"),

    ;

    private final String type;

    TradeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
