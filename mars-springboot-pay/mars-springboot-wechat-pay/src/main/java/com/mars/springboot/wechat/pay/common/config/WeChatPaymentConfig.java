package com.mars.springboot.wechat.pay.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "wechat.pay")
public class WeChatPaymentConfig {
    /**
     * appId
     */
    private String appId;
    /**
     * appId
     */
    private String appSecret;

    /**
     * 商户ID
     */
    private String mchId;
    /**
     * 商户序列号
     */
    private String mchSerialNo;
    /**
     * 私钥
     */
    private String privateKey;
    /**
     * apiV2Key
     */
    private String apiV2Key;

    /**
     * 回调地址
     */
    private String notifyUrl;


}
