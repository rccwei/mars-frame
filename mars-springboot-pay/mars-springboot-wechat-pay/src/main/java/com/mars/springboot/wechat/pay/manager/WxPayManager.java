package com.mars.springboot.wechat.pay.manager;


import com.alibaba.fastjson.JSONObject;
import com.mars.springboot.wechat.pay.common.config.WeChatPaymentConfig;
import com.mars.springboot.wechat.pay.common.constant.WxPayConstant;
import com.mars.springboot.wechat.pay.common.enums.SignType;
import com.mars.springboot.wechat.pay.common.enums.TradeType;
import com.mars.springboot.wechat.pay.common.request.PayRequest;
import com.mars.springboot.wechat.pay.common.response.PayResponse;
import com.mars.springboot.wechat.pay.entity.OrderInfo;
import com.mars.springboot.wechat.pay.entity.OrderReturnInfo;
import com.mars.springboot.wechat.pay.entity.SignInfo;
import com.mars.springboot.wechat.pay.utils.HttpUtils;
import com.mars.springboot.wechat.pay.utils.IpUtils;
import com.mars.springboot.wechat.pay.utils.RandomUtils;
import com.mars.springboot.wechat.pay.utils.SignatureUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.AnyTypePermission;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * 微信支付(v2版本 统一下单接口)
 * 接入前准备（必看）: <a href="https://pay.weixin.qq.com/wiki/doc/apiv3/open/pay/chapter2_8_1.shtml">...</a>
 * <p>
 * 微信支付商户平台首页: <a href="https://pay.weixin.qq.com/index.php/core/home/login?return_url=%2F">...</a>
 *
 * @author Mars
 * @date 2023-09-11
 */
@Component
@Slf4j
public class WxPayManager {


    public static final String XML_TYPE = "xml";

    @Resource
    private WeChatPaymentConfig config;

    @Resource
    private HttpServletRequest httpServletRequest;

    /**
     * 小程序AppId
     */
    @Value("${wechat.pay.appId}")
    private String appId;

    /**
     * 商户号
     */
    @Value("${wechat.pay.mchId}")
    private String mchId;


    /**
     * 统一下单
     *
     * @param request 请求参数
     * @return PayResponse
     */
    public PayResponse doPay(PayRequest request) throws Exception {
        log.info("微信统一支付下单参数{}", JSONObject.toJSONString(request));
        // 随机字符串
        String nonceStr = RandomUtils.createNonceStr();
        String realIp = IpUtils.getRealIp(httpServletRequest);
        OrderInfo preOrder = new OrderInfo();
        // 构建预支付订单
        this.buildPreOrder(appId, mchId, request.getBody(), request.getOutTradeNo(),
                request.getTotalFee(), request.getTradeType(), request.getOpenId(), nonceStr, realIp, preOrder);
        //生成签名
        String sign = SignatureUtils.getSign(preOrder, config.getApiV2Key());
        preOrder.setSign(sign);
        String result = HttpUtils.sendPost(WxPayConstant.PAY_UNIFIEDORDER, preOrder);
        log.info("微信统一支付下单返回参数:{}", result);
        XStream xStream = new XStream();
        xStream.addPermission(AnyTypePermission.ANY);
        xStream.alias(XML_TYPE, OrderReturnInfo.class);
        OrderReturnInfo returnInfo = (OrderReturnInfo) xStream.fromXML(result);
        // 备注: 微信统一下单 必须二次签名
        if (WxPayConstant.SUCCESS.equals(returnInfo.getReturn_code()) && returnInfo.getReturn_code().equals(returnInfo.getResult_code())) {
            SignInfo signInfo = new SignInfo();
            signInfo.setAppId(appId);
            signInfo.setTimeStamp(String.valueOf(System.currentTimeMillis() / 1000));
            signInfo.setNonceStr(nonceStr);
            signInfo.setRepay_id(WxPayConstant.WX_PREPAY_ID_PREFIX + returnInfo.getPrepay_id());
            signInfo.setSignType(SignType.MD5.getType());
            //生成二次签名
            String sign1 = SignatureUtils.getSign(signInfo, config.getApiV2Key());
            PayResponse payResponse = new PayResponse();
            BeanUtils.copyProperties(signInfo, payResponse);
            payResponse.setPackages(signInfo.getRepay_id());
            payResponse.setPaySign(sign1);
            if (TradeType.MWEB.getType().equals(request.getTradeType())) {
                payResponse.setMweb_url(returnInfo.getMweb_url());
            }
            if (TradeType.JSAPI.getType().equals(request.getTradeType())) {
                payResponse.setPartnerId(mchId);
            }
            log.info("微信预支付信息:{}", JSONObject.toJSONString(payResponse));
            return payResponse;
        }
        return null;
    }

    /**
     * 构建预支付订单
     *
     * @param appId      公众号appId
     * @param mchId      商户号
     * @param body       商品名称
     * @param outTradeNo 平台订单号
     * @param totalFee   总金额
     * @param tradeType  交易类型
     * @param openId     openID
     * @param nonceStr   随机字符串
     * @param realIp     realIp
     * @param preOrder   preOrder
     */
    private void buildPreOrder(String appId, String mchId, String body, String outTradeNo, Integer totalFee, String tradeType, String openId, String nonceStr, String realIp, OrderInfo preOrder) {
        preOrder.setAppid(appId);
        preOrder.setMch_id(mchId);
        preOrder.setNonce_str(nonceStr);
        preOrder.setBody(body);
        preOrder.setOut_trade_no(outTradeNo);
        // 单位为分
        preOrder.setTotal_fee(totalFee);
        preOrder.setSpbill_create_ip(realIp);
        // 统一支付回调地址走v2版本
        preOrder.setNotify_url(config.getNotifyUrl());
        preOrder.setTrade_type(tradeType);
        if (org.apache.commons.lang3.StringUtils.isNotEmpty(openId)) {
            preOrder.setOpenid(openId);
        }
        if (TradeType.MWEB.getType().equals(tradeType)) {
            JSONObject sceneInfo = new JSONObject();
            JSONObject h5Info = new JSONObject();
            h5Info.put("type", "Wap");
            h5Info.put("wap_url", WxPayConstant.H5_HOST);
            h5Info.put("wap_name", "订单支付");
            sceneInfo.put("h5_info", h5Info);
            preOrder.setScene_info(JSONObject.toJSONString(sceneInfo));
            preOrder.setSign_type(SignType.MD5.getType());
        }
    }

}
