package com.mars.springboot.wechat.pay.entity;

import lombok.Data;

/**
 * 预订单信息 (注意：下面的字段不能修改和微信保持一致)
 *
 * @author wq
 * @date 2023/4/19 10:52
 */
@Data
public class OrderInfo {

    /**
     * 小程序ID
     */
    private String appid;
    /**
     * 商户号
     */
    private String mch_id;
    /**
     * 随机字符串
     */
    private String nonce_str;
    /**
     * 签名类型
     */
    private String sign_type;
    /**
     * 签名
     */
    private String sign;
    /**
     * 商品描述
     */
    private String body;
    /**
     * 商户订单号
     */
    private String out_trade_no;
    /**
     * 标价金额 ,单位为分
     */
    private int total_fee;
    /**
     * 终端IP
     */
    private String spbill_create_ip;
    /**
     * 通知地址
     */
    private String notify_url;
    /**
     * 交易类型
     */
    private String trade_type;
    /**
     * 用户标识
     */
    private String openid;
    /**
     * 场景信息
     */
    private String scene_info;

}
