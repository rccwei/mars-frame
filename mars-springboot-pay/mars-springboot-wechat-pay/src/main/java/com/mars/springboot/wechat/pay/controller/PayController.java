package com.mars.springboot.wechat.pay.controller;

import com.mars.springboot.wechat.pay.common.base.R;
import com.mars.springboot.wechat.pay.common.request.PayRequest;
import com.mars.springboot.wechat.pay.service.IPayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 支付控制器
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-11 10:59:55
 */
@Api(value = "支付接口管理")
@RequestMapping("/pay")
@RestController
@AllArgsConstructor
public class PayController {

    private final IPayService payService;

    /**
     * 统一支付
     *
     * @param request 请求参数
     * @return R
     */
    @ApiOperation(value = "统一支付")
    @PostMapping(value = "doPay")
    public R doPay(@RequestBody PayRequest request) throws Exception {
        return R.success(payService.doPay(request));
    }

    /**
     * 支付完成回调
     *
     * @param request request
     */
    @ApiOperation(value = "支付完成回调")
    @RequestMapping(value = "notify")
    public void callback(HttpServletRequest request, HttpServletResponse response) throws IOException {
        payService.callback(request, response);
    }

}
