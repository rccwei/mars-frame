package com.mars.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsNettyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsNettyApplication.class, args);
	}

}
