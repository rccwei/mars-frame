package com.mars.netty.bio;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/12/24 21:44
 */
public class SocketServer {
    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        Socket socket = null;
        InputStream is = null;
        ByteArrayOutputStream baos = null;
        try {
            //有一个地址（本地），端口999
            serverSocket = new ServerSocket(999);
            while (true) {
                //等待客户端连接
                socket = serverSocket.accept();
                //读取客户端消息
                is = socket.getInputStream();

                //管道流
                baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int len;
                while ((len = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, len);
                }
                System.out.println(baos.toString());
                for (int i = 0; i < buffer.length; i++) {
                    System.out.println(buffer[i]);
                }
                System.out.println(new String(buffer, 0, buffer.length));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (baos == null) {
                try {
                    baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is == null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (socket == null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (serverSocket == null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
