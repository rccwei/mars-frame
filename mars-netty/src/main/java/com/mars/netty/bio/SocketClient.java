package com.mars.netty.bio;

import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author :mars
 * @version :1.0
 * @date :Created in 2022/12/24 21:46
 */
public class SocketClient {

    public static void main(String[] args) {
        try {
            //要知道服务端的地址和端口号
            InetAddress serverIp=InetAddress.getByName("127.0.0.1");
            int port =999;
            //创建一个socket连接
            Socket socket=new Socket(serverIp,port);
            //System.out.println(serverIp.toString());
            //发送消息给os
            OutputStream os =socket.getOutputStream();
            os.write("hello 世界".getBytes());
            os.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
