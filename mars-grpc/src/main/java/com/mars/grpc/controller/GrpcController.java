package com.mars.grpc.controller;

import com.mars.grpc.client.GrpcClientExample;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @date 2022/11/10 11:24
 */
@RequestMapping("/api/v1/grpc")
@RestController
public class GrpcController {


    private final GrpcClientExample grpcClientExample;

    public GrpcController(GrpcClientExample grpcClientExample) {
        this.grpcClientExample = grpcClientExample;
    }

    @GetMapping("/get/{name}")
    public Object grpc(@PathVariable("name") String name) {
        String names = grpcClientExample.receiveGreeting(name);
        System.out.println("response.getMessage() = " + names);
        return names;
    }

}
