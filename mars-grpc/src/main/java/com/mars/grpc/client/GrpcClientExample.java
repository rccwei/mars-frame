package com.mars.grpc.client;

import com.mars.grpc.service.MyServiceGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import net.devh.boot.grpc.examples.lib.HelloRequest;
import org.springframework.stereotype.Component;

/**
 * @author wq
 * @date 2022/11/10 11:23
 */
@Component
public class GrpcClientExample {

    /**
     * 这里的名称为 proto 文件中 service 对应的名称，注意首字母小写
     */
    @GrpcClient("myService")
    private MyServiceGrpc.MyServiceBlockingStub myServiceStub;

    public String receiveGreeting(String name) {
        HelloRequest request = HelloRequest.newBuilder()
                .setName(name)
                .build();
        return myServiceStub.sayHello(request).getMessage();
    }
}
