package com.mars.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsGrpcApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsGrpcApplication.class, args);
    }

}
