package com.mars.login.wechat.service;

import com.mars.login.wechat.common.request.LoginRequest;
import com.mars.login.wechat.common.response.WxSessionResponse;

/**
 * 微信登录相关接口
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:43:11
 */
public interface ILoginService {

    /**
     * 微信通过code登录
     *
     * @param code code
     * @return WxSessionResponse
     */
    WxSessionResponse doLogin(String code);

    /**
     * 获取token
     *
     * @return String
     */
    String getAccessToken();

    /**
     * 授权获取手机号
     *
     * @param request request
     */
    String getPhoneNumber(LoginRequest request) throws Exception;

    /**
     * 更新用户信息
     *
     * @param request 请求参数
     */
    void updateUserInfo(LoginRequest request);


}
