package com.mars.login.wechat.common.request;

import lombok.Data;

/**
 * 登录请求参数
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-13 13:14:49
 */
@Data
public class LoginRequest {

    /**
     * code(通过getPhoneNumber获取code，不是wx.login的code)
     */
    private String code;

    /**
     * 微信OpenID
     */
    private String openId;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 头像
     */
    private String avatarUrl;


}
