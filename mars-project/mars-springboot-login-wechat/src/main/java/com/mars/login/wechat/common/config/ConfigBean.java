package com.mars.login.wechat.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author wq
 * @date 2022/6/9 17:03
 */
@Configuration
public class ConfigBean {

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }


}
