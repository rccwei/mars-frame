package com.mars.login.wechat.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.login.wechat.mapper.UserMapper;
import com.mars.login.wechat.service.IUserService;
import com.mars.login.wechat.entity.User;
import org.springframework.stereotype.Service;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 17:23:31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Override
    public User getUserByOpenId(String openId) {
        return super.getOne(Wrappers.lambdaQuery(User.class).eq(User::getOpenId, openId));
    }
}
