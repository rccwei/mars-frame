package com.mars.login.wechat.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mars.login.wechat.entity.User;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 17:20:16
 */
public interface UserMapper extends BaseMapper<User> {
}
