package com.mars.login.wechat.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.mars.login.wechat.common.request.LoginRequest;
import com.mars.login.wechat.manager.WechatManager;
import com.mars.login.wechat.service.IUserService;
import com.mars.login.wechat.entity.User;
import com.mars.login.wechat.common.response.WxSessionResponse;
import com.mars.login.wechat.service.ILoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Objects;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:44:38
 */
@Slf4j
@Service
public class LoginServiceImpl implements ILoginService {

    @Resource
    private WechatManager wechatManager;

    @Resource
    private IUserService userService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public WxSessionResponse doLogin(String code) {
        log.info("当前微信登录code:{}", code);
        WxSessionResponse sessionCode = wechatManager.sessionCode(code);
        log.info("登录成功响应参数:{}", JSONObject.toJSONString(sessionCode));
        if (Objects.isNull(sessionCode)) {
            throw new RuntimeException("微信登录失败");
        }
        User user = userService.getUserByOpenId(sessionCode.getOpenId());
        if (Objects.isNull(user)) {
            user = User.builder().createTime(new Date()).openId(sessionCode.getOpenId()).build();
        } else {
            user.setLastLoginTime(new Date());
        }
        userService.saveOrUpdate(user);
        return sessionCode;
    }

    @Override
    public String getPhoneNumber(LoginRequest request) {
        String accessToken = this.getAccessToken();
        String phoneNumber = wechatManager.getPhoneNumber(accessToken, request.getCode());
        User user = userService.getUserByOpenId(request.getOpenId());
        if (Objects.nonNull(user)) {
            user.setMobile(phoneNumber);
            userService.updateById(user);
        }
        return phoneNumber;
    }

    @Override
    public void updateUserInfo(LoginRequest request) {
        User user = userService.getUserByOpenId(request.getOpenId());
        if (Objects.nonNull(user)) {
            user.setAvatar(request.getAvatarUrl());
            user.setName(request.getNickName());
            userService.updateById(user);
        }
    }

    @Override
    public String getAccessToken() {
        return wechatManager.getAccessToken();
    }
}
