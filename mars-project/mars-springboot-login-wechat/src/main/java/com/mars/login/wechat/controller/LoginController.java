package com.mars.login.wechat.controller;

import com.mars.login.wechat.common.base.R;
import com.mars.login.wechat.common.request.LoginRequest;
import com.mars.login.wechat.service.ILoginService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 16:42:41
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Resource
    public ILoginService loginService;

    /**
     * 1.微信通过code登录 获取openId和sessionKey
     *
     * @param code code
     * @return R
     */
    @GetMapping("/doLogin")
    public R doLogin(String code) {
        return R.success(loginService.doLogin(code));
    }


    /**
     * 2.获取手机号
     *
     * @param request 请求参数
     * @return R
     */
    @PostMapping("/getPhoneNumber")
    public R getPhoneNumber(@RequestBody LoginRequest request) throws Exception {
        return R.success(loginService.getPhoneNumber(request));
    }

    /**
     * 3.更新用户信息
     *
     * @param request 请求参数
     * @return R
     */
    @PostMapping("/updateUserInfo")
    public R updateUserInfo(@RequestBody LoginRequest request) {
        loginService.updateUserInfo(request);
        return R.success();
    }


}
