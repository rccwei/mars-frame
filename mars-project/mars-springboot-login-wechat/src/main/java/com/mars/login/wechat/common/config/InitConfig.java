package com.mars.login.wechat.common.config;

import com.mars.login.wechat.service.IUserService;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-13 13:23:51
 */
@Component
public class InitConfig implements InitializingBean {

    @Resource
    private IUserService userService;

    @Override
    public void afterPropertiesSet() {
//        List<Object> list = new ArrayList<>();
//        ExecutorService threadPool = new ThreadPoolExecutor(1, 1,
//                1L, TimeUnit.SECONDS,
//                new LinkedBlockingQueue<>(3),
//                Executors.defaultThreadFactory(),
//                new ThreadPoolExecutor.AbortPolicy());
//
//        threadPool.submit(() -> {
//            while (true) {
//                int compute = compute();
//                System.out.println(compute);
//            }
//        });
        userService.list();

    }

    private static int compute() {
        int a = 2048;
        int b = 1024;
        return (a + b) * 10;
    }
}
