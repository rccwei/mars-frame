package com.mars.login.wechat.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mars.login.wechat.entity.User;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-12 17:22:58
 */
public interface IUserService extends IService<User> {

    /**
     * 通过openId查询用户
     *
     * @param openId openId
     * @return User
     */
    User getUserByOpenId(String openId);
}
