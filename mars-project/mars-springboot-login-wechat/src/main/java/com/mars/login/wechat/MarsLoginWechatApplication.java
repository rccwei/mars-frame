package com.mars.login.wechat;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能实现步骤：微信小程序新版授权登录
 * 1.前端小程序采用wx.login 获取code
 * 2.将code传给服务端获取openId和sessionKey
 * 3.获取头像和昵称并通过openId更新到数据库
 * 4.前端小程序调用getPhoneNumber获取code传给后台解密手机号
 *
 */
@SpringBootApplication
@MapperScan("com.mars.login.wechat.mapper")
public class MarsLoginWechatApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsLoginWechatApplication.class, args);
    }

}
