package com.mars.login.wechat.entity;

import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-09-13 13:51:15
 */
@Data
public class WechatUserInfo {

    private String nickName;

    private String avatarUrl;

    private String country;

    private String province;

    private String city;

    private Integer gender;


}
