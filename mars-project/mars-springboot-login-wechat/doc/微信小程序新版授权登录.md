# 微信小程序新版授权登录

### 前言
在我们工作中微信小程序授权登录用的地方非常的多，只要你们公司有小程序开发就大概会涉及到，
里面还是有不少的坑，以前是可以通过
相关api接口就可以直接获取
但是由于最近微信的官方限制了直接获取用户的头像和昵称，需要用户手动获取，以前的版本
wx.getUserProfile就不能直接获取了
所以我们这里介绍新的获取方式


#### 1.前端小程序采用wx.login 获取code

```
	uni.login({
    					provider: "weixin",
    					success(res) {}
            })
```

#### 2.将code传给服务端获取openId和sessionKey

  
```
uni.request({
							url: 'http://localhost:8080/login/doLogin?code=' + code,
							method: 'GET',
							success: function(resp) {
								console.log(resp.data.data.openId)
								that.openId = resp.data.data.openId
								that.sessionKey = resp.data.data.sessionKey
								uni.setStorageSync('openId',that.openId)
								uni.setStorageSync('sessionKey',that.sessionKey)
								uni.navigateTo({
									url:'/pages/user-info/user-info'
								})
							}
						})
```


#### 3.获取头像和昵称并通过openId更新到数据库
vue 代码如下

```
	<image class="avatar" :src="avatarUrl" style="width: 100upx;height: 100upx;border-radius: 50upx;"></image>

		<button class="avatar-wrapper" open-type="chooseAvatar" @chooseavatar="onChooseAvatar"
			style="margin-top: 50upx;">
			获取头像
		</button>
		<input type="nickname" @change="nickNameChange" :value="nickName" placeholder="请输入昵称"
			style="margin-top: 50upx;text-align: center;" />




```

method代码如下
```

nickNameChange(e) {
				this.nickname = e.detail.value;
				console.log(111111111111111)
				console.log(e.detail.value)
				let params = {}
				params.nickName = this.nickname;
				params.openId = uni.getStorageSync('openId');
				wx.request({
					url: 'http://localhost:8080/login/updateUserInfo',
					method: 'POST',
					data: params,
					success: function(res) {
						uni.showToast({
							title: '操作成功',
							icon: 'none'
						})
					}
				})

			},
			onChooseAvatar(e) {

				console.log(e.detail.avatarUrl)
				this.avatarUrl = e.detail.avatarUrl
				let params = {}
				params.avatarUrl = this.avatarUrl;
				params.openId = uni.getStorageSync('openId');
				wx.request({
					url: 'http://localhost:8080/login/updateUserInfo',
					method: 'POST',
					data: params,
					success: function(res) {
						uni.showToast({
							title: '操作成功',
							icon: 'none'
						})
					}
				})
			},

```

#### 4.前端小程序调用getPhoneNumber获取code传给后台解密手机号

```
	<button open-type="getPhoneNumber" @getphonenumber="getPhoneNumber" style="margin-top: 50upx;">获取手机号</button>

```

```
getPhoneNumber(e) {
				console.log("1111111111111111")
				console.log(e)
				console.log(e.detail.code)

				let params = {}
				params.openId = uni.getStorageSync('openId')
				params.code = e.detail.code
				var that = this
				wx.request({
					url: 'http://localhost:8080/login/getPhoneNumber',
					method: 'POST',
					data: params,
					success: function(res) {
						uni.showToast({
							title: '获取成功',
							icon: 'none'
						})
						that.phoneNumber = res.data.data
					}
				})

			},

```

