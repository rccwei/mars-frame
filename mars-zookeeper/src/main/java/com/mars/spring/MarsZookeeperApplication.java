package com.mars.spring;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.mars.zookeeper.mapper")
@SpringBootApplication(scanBasePackages = "com.mars.spring")
public class MarsZookeeperApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarsZookeeperApplication.class, args);
	}

}
