package com.mars.spring.config;


import com.mars.spring.watch.ZookeeperWatches;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@Configuration
public class ZookeeperConfig {


    //集群地址,分割不能有空格 在程序里写connectString不可使用ip,必须使用主机名
    private String connectString = "localhost:2181";
    //连接超时时间
    private int sessionTimeout = 5000;
    //会话存活时间,根据业务灵活指定
    private Integer sessionTimeOut = 5000;
    //重试机制时间参数
    private Integer sleepMsBetweenRetry = 1000;
    //重试机制重试次数
    private Integer maxRetries = 3;
    //命名空间(父节点名称)
    private String namespace = "app";

    /**
     * - `session`重连策略
     * - `RetryPolicy retry Policy = new RetryOneTime(3000);`
     * - 说明：三秒后重连一次，只重连一次
     * - `RetryPolicy retryPolicy = new RetryNTimes(3,3000);`
     * - 说明：每三秒重连一次，重连三次
     * - `RetryPolicy retryPolicy = new RetryUntilElapsed(1000,3000);`
     * - 说明：每三秒重连一次，总等待时间超过个`10`秒后停止重连
     * - `RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000,3)`
     * - 说明：这个策略的重试间隔会越来越长
     * - 公式：`baseSleepTImeMs * Math.max(1,random.nextInt(1 << (retryCount + 1)))`
     * - `baseSleepTimeMs` = `1000` 例子中的值
     * - `maxRetries` = `3` 例子中的值
     */
    @Bean("curatorClient")
    @Order(200)
    public CuratorFramework curatorClient() throws Exception {
        CuratorFramework client = CuratorFrameworkFactory.builder()
                .connectString(connectString)
                .connectionTimeoutMs(sessionTimeout)
                .sessionTimeoutMs(sessionTimeOut)
                //session重连策略
                .retryPolicy(new ExponentialBackoffRetry(sleepMsBetweenRetry, maxRetries))
                //设置命名空间 在操作节点的时候，会以这个为父节点
                .namespace(namespace)
                .build();
        client.start();
        //注册监听器
        ZookeeperWatches zookeeperWatches=new ZookeeperWatches(client);
        zookeeperWatches.znodeWatcher();
        zookeeperWatches.znodeChildrenWatcher();
        return client;
    }

}
