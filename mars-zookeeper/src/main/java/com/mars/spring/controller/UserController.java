package com.mars.spring.controller;

import com.mars.spring.service.IUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {

    @Resource
    private IUserService userService;


    @RequestMapping("save")
    public String save(String name) {
        userService.add(name);
        return "success";
    }

    @RequestMapping("update")
    public String update(String name) {
        userService.update(name);
        return "success";
    }


}
