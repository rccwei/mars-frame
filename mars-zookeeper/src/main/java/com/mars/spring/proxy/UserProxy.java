package com.mars.spring.proxy;


import com.mars.spring.service.IUserService;
import com.mars.spring.service.impl.UserServiceImpl;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Component
public class UserProxy implements InvocationHandler {

    private UserServiceImpl hg = new UserServiceImpl();

    public IUserService getProxy() {
        return (IUserService) Proxy.newProxyInstance(
                getClass().getClassLoader(),
                hg.getClass().getInterfaces(),
                this);
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(method, args);
    }
}
