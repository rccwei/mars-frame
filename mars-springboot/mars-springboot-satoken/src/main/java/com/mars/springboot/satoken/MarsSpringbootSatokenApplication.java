package com.mars.springboot.satoken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 源码字节
 */
@SpringBootApplication
public class MarsSpringbootSatokenApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringbootSatokenApplication.class, args);
    }

}
