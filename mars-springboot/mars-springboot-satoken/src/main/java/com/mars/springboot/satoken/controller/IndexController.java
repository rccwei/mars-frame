package com.mars.springboot.satoken.controller;

import cn.dev33.satoken.stp.StpUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Mars
 * @date 2022-08-06 13:50
 */
@RestController
@RequestMapping("/user")
public class IndexController {

    /**
     * 测试登录，浏览器访问： http://localhost:8081/user/doLogin?username=wq&password=123456
     *
     * @param username username
     * @param password password
     * @return String
     */
    @RequestMapping("doLogin")
    public String doLogin(String username, String password) {
        // 此处仅作模拟示例，真实项目需要从数据库中查询数据进行比对
        if ("wq".equals(username) && "123456".equals(password)) {
            StpUtil.login(10001);
            return StpUtil.getTokenInfo().getTokenValue();
        }
        return "登录失败";
    }

    /**
     * 查询登录状态，浏览器访问： http://localhost:8081/user/isLogin
     *
     * @return String
     */
    @RequestMapping("isLogin")
    public String isLogin() {
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

    /**
     * 退出，浏览器访问： http://localhost:8081/user/logout
     *
     * @return
     */
    @RequestMapping("logout")
    public String logout() {
        StpUtil.logout();
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

    /**
     * 校验是否登录
     *
     * @return String
     */
    @RequestMapping("checkLogin")
    public String checkLogin() {
        StpUtil.checkLogin();
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

    /**
     * 获取当前会话账号id, 如果未登录，则抛出异常：`NotLoginException`
     *
     * @return
     */
    @RequestMapping("getLoginId")
    public String getLoginId() {
        StpUtil.getLoginId();
        System.out.println(StpUtil.getLoginIdAsInt());
        System.out.println(StpUtil.getLoginIdAsLong());
        System.out.println(StpUtil.getLoginIdDefaultNull());
        return "当前会话登录用户：" + StpUtil.getLoginId();
    }

    @RequestMapping("getTokenValue")
    public String getTokenValue() {
        System.out.println(StpUtil.getTokenValue());
        System.out.println(StpUtil.getTokenName());
        System.out.println(StpUtil.getTokenTimeout());
        System.out.println(StpUtil.getTokenInfo());
        return "当前会话登录用户Token：" + StpUtil.getTokenValue();
    }

    /**
     * 当前账号所拥有的权限集合
     *
     * @return List<String>
     */
    @RequestMapping("getPermissionList")
    public String getPermissionList() {
        List<String> permissionList = StpUtil.getPermissionList();
        return "当前账号所拥有的权限集合：" + permissionList;
    }

    /**
     * 将指定账号踢下线
     *
     * @return String
     */
    @RequestMapping("kickout")
    public String kickout() {
        StpUtil.kickout(10001);
        return "当前会话是否登录：" + StpUtil.isLogin();
    }

}
