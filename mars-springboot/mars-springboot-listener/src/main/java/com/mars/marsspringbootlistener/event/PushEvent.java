package com.mars.marsspringbootlistener.event;

import com.mars.marsspringbootlistener.config.BaseEvent;
import com.mars.marsspringbootlistener.entity.PushMessage;

public class PushEvent extends BaseEvent<PushMessage> {


    public PushEvent(Object source, PushMessage eventData) {
        super(source, eventData);
    }

    public PushEvent(PushMessage eventData) {
        super(eventData);
    }
}
