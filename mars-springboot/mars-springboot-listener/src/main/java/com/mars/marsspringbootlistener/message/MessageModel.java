package com.mars.marsspringbootlistener.message;

import lombok.Data;

@Data
public class MessageModel {

    private String message;
}
