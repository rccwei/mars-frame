package com.mars.marsspringbootlistener.factory;

import com.lmax.disruptor.EventFactory;
import com.mars.marsspringbootlistener.message.MessageModel;

public class HelloEventFactory implements EventFactory<MessageModel> {
    @Override
    public MessageModel newInstance() {
        return new MessageModel();
    }
}
