package com.mars.marsspringbootlistener.controller;

import com.mars.marsspringbootlistener.config.SpringUtil;
import com.mars.marsspringbootlistener.entity.PushMessage;
import com.mars.marsspringbootlistener.event.PushEvent;
import com.mars.marsspringbootlistener.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @date 2022/1/20 15:42
 */
@RestController
public class UserController {

    @Resource
    private UserService userService;

    @RequestMapping("/push")
    public String push() {
        SpringUtil.publishEvent(new PushEvent(this, new PushMessage().setMessage("测试消息")));
        return "success";
    }

    @RequestMapping("/index")
    public String index() {
        userService.index();
        return "success";
    }
}
