package com.mars.marsspringbootlistener.controller;


import com.lmax.disruptor.RingBuffer;
import com.mars.marsspringbootlistener.event.EventMessage;
import com.mars.marsspringbootlistener.message.MessageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wq
 * @date 2022/1/20 15:42
 */
@Slf4j
@RestController
public class DisruptorMessageController {

    @Autowired
    private RingBuffer<MessageModel> messageModelRingBuffer;



    @RequestMapping("/sendMsg")
    public String sendMsg(String msg) {
        log.info("record the message: {}", msg);
        sendMessage(msg);
        return "success";
    }

    private void sendMessage(String msg) {
        long sequence = messageModelRingBuffer.next();
        try {
            // 给Event填充数据
            MessageModel event = messageModelRingBuffer.get(sequence);
            event.setMessage(msg);
            log.info("往消息队列中添加消息：{}", event);
        } catch (Exception e) {
            log.error("failed to add event to messageModelRingBuffer for : e = {},{}", e, e.getMessage());
        } finally {
            // 发布Event，**观察者去消费，将sequence传递给改消费者
            // 注意最后的publish方法必须放在finally中以确保必须得到调用；如果某个请求的sequence未被提交将会堵塞后续的发布操作或者其他的producer
            messageModelRingBuffer.publish(sequence);
        }
    }


}
