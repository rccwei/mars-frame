package com.mars.marsspringbootlistener.event;

import org.springframework.context.ApplicationEvent;

public class EventMessage extends ApplicationEvent {
    private String msg;


    public EventMessage(Object source, String msg) {
        super(source);
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
