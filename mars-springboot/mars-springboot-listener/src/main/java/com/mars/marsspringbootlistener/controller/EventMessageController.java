package com.mars.marsspringbootlistener.controller;


import com.mars.marsspringbootlistener.event.EventMessage;
import com.mars.marsspringbootlistener.event.PushEvent;
import com.mars.marsspringbootlistener.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wq
 * @date 2022/1/20 15:42
 */
@RestController
public class EventMessageController {

    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping("/pushMsg")
    public String pushMsg(String msg) {
        ApplicationEvent event = new EventMessage(this, msg);
        applicationContext.publishEvent(event);
        return "success";
    }


}
