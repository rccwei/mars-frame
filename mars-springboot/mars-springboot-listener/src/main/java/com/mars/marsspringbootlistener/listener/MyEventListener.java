package com.mars.marsspringbootlistener.listener;

import com.mars.marsspringbootlistener.event.PushEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author wq
 * @date 2022/1/20 15:40
 */
@Component
public class MyEventListener {


    @EventListener
    public void onApplicationEvent(PushEvent event) {
        //真正做业务的地方
        try {
            System.out.println("开始做事" + Thread.currentThread().getName());
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String s = event.getEventData().toString();
        System.out.println("结束做事" + s);
    }
}
