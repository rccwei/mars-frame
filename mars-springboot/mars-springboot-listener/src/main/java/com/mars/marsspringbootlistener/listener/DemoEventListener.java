package com.mars.marsspringbootlistener.listener;

import com.mars.marsspringbootlistener.event.EventMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DemoEventListener {

    @EventListener
    public void eventListener(EventMessage event) {
        log.info(this.getClass().getSimpleName() + "监听到数据：" + event.getMsg());
    }
}
