package com.mars.marsspringbootlistener.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author wq
 * @date 2022/1/20 15:39
 */
@Configuration
@ComponentScan("com.mars.marsspringbootlistener")
public class ExtConfig {
}
