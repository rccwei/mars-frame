package com.mars.marsspringbootlistener;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarsSpringbootListenerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarsSpringbootListenerApplication.class, args);
    }

}
