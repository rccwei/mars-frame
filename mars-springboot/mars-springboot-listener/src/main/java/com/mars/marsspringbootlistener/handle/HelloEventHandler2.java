package com.mars.marsspringbootlistener.handle;

import com.lmax.disruptor.EventHandler;
import com.mars.marsspringbootlistener.message.MessageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class HelloEventHandler2 implements EventHandler<MessageModel> {
    @Override
    public void onEvent(MessageModel messageModel, long l, boolean b) throws Exception {
        try {
            if (messageModel != null) {
                log.info("消费者消费的信息是：{}", messageModel);
            }
        } catch (Exception e) {
            log.info("消费者处理消息失败");
        }
    }
}
