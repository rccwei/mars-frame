package com.mars.marsspringbootlistener.service;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wq
 * @date 2022/1/20 17:59
 */
@Service
public class UserService {

    @Resource
    private OrderService orderService;

    public void index() {
        orderService.index();
    }
}
