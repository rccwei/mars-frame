package com.mars.marsspringbootlistener.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wq
 * @date 2022/1/20 15:59
 */
@Data
@Accessors(chain = true)
public class PushMessage {

    private String message;
}
