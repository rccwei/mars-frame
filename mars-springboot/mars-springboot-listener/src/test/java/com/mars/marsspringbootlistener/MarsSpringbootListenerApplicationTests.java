package com.mars.marsspringbootlistener;

import com.mars.marsspringbootlistener.config.ExtConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootTest
public class MarsSpringbootListenerApplicationTests {

    @Test
    public void contextLoads() {

        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(ExtConfig.class);
        applicationContext.publishEvent(new ApplicationEvent(new String("我发布的事件")) {
        });
        applicationContext.close();
    }

}
